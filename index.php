<?php
		ini_set('display_errors', 1);ini_set('display_startup_errors', 1);error_reporting(E_ALL);

?>

<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
	<link rel="manifest" href="manifest.json"></link>
	<meta name="description" content="PWA de PMC Vitae">
	<meta name="theme-color" content="#186099">
	<meta name="apple-mobile-web-app-title" content="PWA de PMC Vitae">
	<meta name="appre-mobile-web-app-status-bar-style" content="black">
	<meta name="appre-mofle-web-app-capable" content="yes">
	<link rel="apple-touch-icon" href="icons/pwa/maskable_icons/192X192.png">
	
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">

	
	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - Login</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link href="./css/style.css" rel="stylesheet">
	<!-- Ajustes -->
    <link href="./css/ajustes.css" rel="stylesheet">
	<style>
		.form-control > button {
			display:none;
		}
		.form-control > select {
			padding-top: 0 !important;
			padding-left: 0.3em;
		}
	</style>
</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12" style="border: 3px solid #36C95F; border-radius: 10px;">
                                <div class="auth-form">
									<div class="profile-photo text-center mb-2">
										<img src="images/logo.png" width="52" class="img-fluid" alt="">
									</div> 
                                    <h3 class="text-center mb-4 text-primary">Iniciar sesión en su cuenta</h3> 
                                    <form role="form" action="" id="frmAcceso" name="frmAcceso" method="POST" autocomplete="off" >
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Usuario</strong></label>
                                            <input type="text" id="txtUsuario" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Clave</strong></label>
                                            <input type="password" class="form-control" id="txtClave">
                                        </div>
										<div class="form-group" id="div_region" style="display:none; padding:0px !important">
                                            <label class="mb-1"><strong>Region</strong></label>
                                            <select id="region">

                                            </select>
                                        </div>
                                        <div class="text-center" style="margin-top:50px">
											<button type="submit" class="btn btn-primary btn-block">Iniciar sesión</button>
                                        </div>
                                    </form> 
									<div class="new-account mt-3 text-center">
										<a id="appInstall" class="btn btn-info info btn-block" style="display:none" aria-label="Instalar" ><i class="lni lni-download"></i> Instalar en dispositivo </a>
									</div>	
									<div class="form-group text-center">
										<p><a href="page-forgot-password.html">¿Se te olvidó tu contraseña?</a></p>
									</div>
                                    <div class="new-account mt-3 text-center" style="display:none">
                                        <p>¿No tienes una cuenta? <a class="text-primary" href="./page-register.html">Regístrate</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="./js/custom.min.js"></script>
    <script src="./js/deznav-init.js"></script>
	<!-- Select2 -->
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<script>
		$(function(){
			
			$("#txtUsuario").on('change',function(){
					var txtUsuario = $(this).val();
					$.ajax({
					url:'controller/conexion_bd.php',
					type : 'POST',
					dataType: 'json',
					data: {txtUsuario: txtUsuario,oper:"listar"},
					success: function(resp){
						console.log(resp);
						if ( resp.count == 0 ) {
							swal("Error","Nombre de usuario inválido","error");
							$("#txtUsuario").val("").focus();
							return false;					
						}else{
							if(resp.count > 1){
								$("#div_region").show();
								$("#region").select2();
								$("#region").empty();
								$("#region").append(resp.option).trigger('change');		
								$('button[data-id="region"]').hide();				
							}else{
								$("#div_region").hide();
								$("#region").empty();
								$("#region").append(resp.option).trigger('change');
							}
						}
					},
					error: function(data){	
						console.log('error');
					}
				});
				});
			
			$('#frmAcceso').submit(function(){
				//console.log('paso');
				var txtUsuario = $("#txtUsuario").val();
				var txtClave = $("#txtClave").val();
				var bd = $("#region").val();
				$.ajax({
					url:'controller/login.php',
					type : 'POST',
					dataType: 'json',
					data: {txtUsuario: txtUsuario, txtClave: txtClave, bd:bd},
					success: function(data){
						//console.log('ok');
						if ( data.error == true ) {
							$('#error').css('display','block');
							var error = document.getElementById('error');
							error.innerHTML = data.msg;
						
							setTimeout(function(){
								window.scrollTo(0, 1);
							}, 100);

						}else {
							if(data.bd == 'homecare2018'){
								window.localStorage.setItem("latitud","8.984700");
								window.localStorage.setItem("longitud","-79.521019");
							}
							if(data.bd == 'homecareSLV'){
								window.localStorage.setItem("latitud","13.690862235009162");
								window.localStorage.setItem("longitud","-89.21677007097624");
							}
							if(data.bd == 'homecareRD'){
								window.localStorage.setItem("latitud","18.480978698112562");
								window.localStorage.setItem("longitud","-69.92737426180265");
							}
							localStorage.setItem("user",txtUsuario);
							localStorage.setItem("clave",txtClave);
							localStorage.setItem("user_id",data.msg);
							localStorage.setItem("base_datos",data.bd);
							if(data.nombreorganizacion == "PHM"){
								location.href='phm.php';
							}else if(data.nivel == "16"){
                                location.href='dataclinic.php';
                            }else{
								location.href='dashboard.php';
							}
							console.log(data)
							return false;
						}
					
					},
					error: function(data){	
						console.log('error');
					}
				});
			return false;
			})
		});
	//PWA CONFIG
		//let installButton = document.getElementById('appInstall');
		var deferredPrompt ;
		var btnAdd = document.getElementById('appInstall') ;
		launchPromptPwa();
		function launchPromptPwa(){
			var deferredPrompt;
			btnAdd = document.getElementById('appInstall') ;
			window.addEventListener('beforeinstallprompt',  (e) => {
				// Prevent Chrome 67 and earlier from automatically showing the prompt
				e.preventDefault();
				// Stash the event so it can be triggered later.
				deferredPrompt = e;
				btnAdd.style.display = "block";
				showAddToHomeScreen();
			});

			btnAdd.addEventListener('click', (e) => {
				//btnAdd.style.display = 'none';
				//Show the prompt
				deferredPrompt.prompt();
				// Wait for the user to respond to the prompt
				deferredPrompt.userChoice
					.then((choiceResult) => {
						if (choiceResult.outcome === 'accepted') {
							console.log('User accepted the A2HS prompt');
						} else {
							console.log('User dismissed the A2HS prompt');
						}
						deferredPrompt = null;
					});
			});
			window.addEventListener('appinstalled', (evt) => {
				console.log('pwa installed');
				btnAdd.style.display = "none";
			});
			if (window.matchMedia('(display-mode: standalone)').matches) {
				console.log('display-mode is standalone');
			}
		}
		
		if('serviceWorker' in navigator){		
			window.addEventListener('load',()=>{	
				navigator.serviceWorker.register('service-worker.js').then((reg)=>{
					console.log("Service Worker registered", reg);
				})
			})
		}
</script>

</body>

</html>