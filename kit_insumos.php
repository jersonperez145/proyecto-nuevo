<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
	verificarLogin();
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Kit de insumos</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    
    <!-- Ajustes -->
    <link href="./css/ajustes.css<?php autoVersiones().''.$nombre; ?>" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

</head>
<body>
    <!--*******************
        ORVERLAY
    ********************-->
    <div id="overlay">
		<div id="text"><strong>Procesando...</strong></div>
    </div>
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper" class="show">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="#top" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Chat box start
        ***********************************-->
		<div class="chatbox">
			<div class="chatbox-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#notes">Notas</a>
					</li><!--
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#alerts">Alertas</a>
					</li>-->
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#chat">Chat</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="chat" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box">
							<div class="card-header">
								<div>
									<h6 class="mb-1">Lista de Chats</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body">
								<ul class="contacts" id="chat_usuarios">
								</ul>
							</div>
						</div>
						<div class="card chat dz-chat-history-box d-none" id="sala_chat">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-chat-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Chat con <span id="chatcon"></span></h6>
									<p class="mb-0 text-success">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3">
								<div id="chat_enfermero"></div>
							</div>
							<div class="card-footer type_msg">
								<div class="input-group">
								    <input type="hidden" class="form-control" name="idsala" id="idsala" autocomplete="off">
									<textarea class="form-control" id="body" placeholder="Escribir Mensaje..."></textarea>
									<div class="input-group-append">
										<button type="button" class="btn btn-primary" id="boton-enviar-mensaje"><i class="fa fa-location-arrow"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="notes">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-nota-user-box">
							<div class="card-header">
								<div>
                                      <h6 class="mb-1">Lista de Notas</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Notas_Body">
								<ul class="contacts" id="listado_notas">
								</ul>
							</div>
						</div>
						<div class="card chat dz-nota-history-box d-none" id="sala_nota">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-nota-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Nota - <span id="notacon"></h6>
									<p class="mb-0 text-info">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Notas_Body3">
								<div id="nota_detalle"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Chat box End
        ***********************************-->
        
        <!--**********************************
            Configuración start
        ***********************************-->
		<div class="config">
			<div class="config-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#filtrosconfig">Filtros</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="filtrosconfig" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0">
							<div class="card-header d-none">
								<div>
                                      <h6 class="mb-1">Filtros</h6>
								</div>
							</div>
							<div class="card-body p-0 dz-scroll" id="DZ_W_Filtros_Body">
								<div class="form-config">
                                    <form>
                                        <div class="d-block my-3">
                                             <div class="custom-control custom-radio mb-2">
                                                <input id="evitae" name="empresaConfig" type="radio" class="custom-control-input" checked="" required="">
                                                <label class="custom-control-label" for="evitae">Vitae</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="emapfre" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="emapfre">MAPFRE</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="enestle" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="enestle">Nestlé</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="ebgm" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="ebgm">BGM</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="eemg" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="eemg">EMG</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Configuración End
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header" name="top">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <a  href="#top">
							<a href="#" class="btn btn-primary ir-arriba"><i class="las la-arrow-up"></i></a>
                            <div class="dashboard_bar">
                                Inventario / Kit de insumos
                            </div>
                          </a>  
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
                                    <i class="fas fa-bell text-success"></i>
									<!--<span class="badge light text-white bg-primary" id="totalincidentes">0</span>-->
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
										<ul class="timeline" id="incidentesnotific">
										</ul>
									</div>
                                    <a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
                                </div>
                            </li>
							<li class="nav-item dropdown notification_dropdown" style="display: none;">
                                <a class="nav-link bell bell-link" href="javascript:;">
                                    <i class="fas fa-comments text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown notification_dropdown" style="display:none;">
                                <a class="nav-link bell config-link" href="javascript:;">
                                    <i class="fas fa-cogs text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                                    <!--<img src="images/logo.png" width="20" alt=""/>-->
                                    <div class="round-header"><?php echo $inombre; ?></div>
									<div class="header-info">
										<span><?php echo $nombre; ?></span>
									</div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right"><!--
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>-->
                                    <a href="index.php" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Cerrar Sesion </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php menu(); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid" style="display:padding-top: 0px !important">
				<div class="form-head d-flex mb-3 mb-md-4 align-items-start">
					<div class="mr-auto d-none d-lg-block" style="display: none !important">
						<h3 class="text-black font-w600">Bienvenido a Vitae!</h3>
						<p class="mb-0 fs-18">Tu aliado de salud en casa</p>
					</div>
					<!--
					<div class="input-group search-area ml-auto d-inline-flex">
						<input type="text" class="form-control" placeholder="Buscar...">
						<div class="input-group-append">
							<button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
						</div>
					</div>
					-->
				</div>
			
                <div class="card">
                    <div class="card-body">
                        <div class="col-12">
                                <div class="card-header">
                                    <h4 class="card-title">Listado</h4>
                                    <div>
                                        <button type="button" class="btn btn-primary mb-2" id="btn_excel_global" style="background: white;color: #36C95F;border-color: 36C95F;"><i class="fas fa-file-excel" aria-hidden="true"></i> Excel</button>
                                        <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target=".modal-nuevo-kit"><i class="fas fa-plus" aria-hidden="true"></i> Nuevo</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tabla_kit_insumos" class="display min-w850" style="width:100%">
                                            <thead>
                                                <tr>
                                                        <th>id</th>
                                                        <th>Acciones</th>
                                                        <th>Nombre</th>
                                                        <th>Costo promedio</th>
                                                        <th>Actualizado por</th>
                                                        <th>Fecha actualizacion</th>                                                
                                                </tr>
                                                </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        <!--**********************************
            MDOALES  start
        ***********************************-->
            <!-- MODAL VER -->
            <div data-backdrop="static" class="modal fade bd-example-modal-lg modal_ver_kit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Kit: <span id="ver_nombre_kit"></span></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row col-12">
                                <div class="form-group col-md-6">
                                    <label>Actualizado por</label>
                                    <input type="text" id="ver_actualizado_por" class="form-control" readonly="readnoly">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Fecha de actualización</label>
                                    <input type="text" id="ver_fecha_actualizacion" class="form-control" readonly="readnoly">
                                </div>
                            </div>
                            <div class="col-12">
                            <div class="table-responsive">
                                    <table id="tabla_ver_kit_insumos" class="display min-w850" style="width:100%">
                                        <thead>
                                            <tr>
                                                    <th>itemid</th>
                                                    <th>Tipo</th>
                                                    <th>Item</th>                                                    
                                                    <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MODAL NUEVO -->
            <div data-backdrop="static" class="modal fade bd-example-modal-lg modal-nuevo-kit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Nuevo<span id="nuevo_nombre"></span></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row col-12">
                                <div class="form-group col-md-12">
                                    <label><span style="color:red">* </span>Nombre</label>
                                    <input type="text" id="nuevo_nombre_kit" class="form-control" >
                                </div>
                                <div class="form-group col-md-10">                                    
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label" for="">Item</label>
                                        <select class="form-control" id="select_item_nuevo" style="width:100%"></select>
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Cantidad</label>
                                    <input type="number" step="1" min="1" id="nuevo_cantidad_item" class="form-control" >
                                </div>
                                <div class="form-group col-md-5 text-center"></div>
                                <div class="form-group col-md-2 text-center">
                                    <button type="button" id="anadir_nuevo" class="btn btn-info btn-md btn-block">Añadir</button>
                                </div>
                                <div class="form-group col-md-5 text-center"></div>
                            </div>
                            <div class="col-12">
                            <div class="table-responsive">
                                    <table id="tabla_nuevo_kit_insumos" class="display min-w850" style="width:100%">
                                        <thead>
                                            <tr>
                                                    <th>Quitar</th>
                                                    <th>Tipo</th>
                                                    <th>Item</th>                                                    
                                                    <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary light" id="boton-guardar-nuevo">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MODAL EDITAR -->
            <div data-backdrop="static" class="modal fade bd-example-modal-lg modal_editar_kit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Editar Kit: <span id="editar_nombre"></span></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="editar_id_kit">
                            <div class="form-row col-12">
                                <div class="form-group col-md-12">
                                    <label><span style="color:red">* </span>Nombre</label>
                                    <input type="text" id="editar_nombre_kit" class="form-control" >
                                </div>
                                <div class="form-group col-md-10">                                    
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label" for="">Item</label>
                                        <select class="form-control" id="select_item_editar" style="width:100%"></select>
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Cantidad</label>
                                    <input type="number" step="1" min="1" id="editar_cantidad_kit" class="form-control" >
                                </div>

                                <div class="form-group col-md-5 text-center"></div>
                                <div class="form-group col-md-2 text-center">
                                    <button type="button" id="anadir_editar" class="btn btn-info btn-md btn-block">Añadir</button>
                                </div>
                                <div class="form-group col-md-5 text-center"></div>
                            </div>
                            <div class="col-12">
                            <div class="table-responsive">
                                    <table id="tabla_editar_kit_insumos" class="display min-w850" style="width:100%">
                                        <thead>
                                            <tr>
                                                    <th>itemid</th>
                                                    <th>Quitar</th>
                                                    <th>Tipo</th>
                                                    <th>Item</th>                                                    
                                                    <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary light" id="boton-guardar-editar">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        <!--**********************************
            MODALES end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php linksFooter(); ?>
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>		
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
	

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://kit.fontawesome.com/7f9e31f86a.js" crossorigin="anonymous"></script>	
<script src="js/kit_insumos/kit_insumos.js<?php //autoVersiones(); ?>" ></script>

</body>
</html>