$("#btn_excel_global").on('click',function(){
    window.open('controller/kit_insumos/excel_kit_insumos_global.php');
})
var items_nuevo ="[]";

var tabla_kit_insumos = $("#tabla_kit_insumos").DataTable({
    "ajax"		:"controller/kit_insumos/kit_insumos_back.php?oper=cargarkits",
    "order":	[2,'ASC'],
    "lengthChange": true,
    "pageLength": 10,
    "columns"	: [
        { 	"data": "id" },
        { 	"data": "id",
            render: function(data,type,row){
                return `
                <div class="dropdown">
                    <button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
                        <svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item editar_kit"  data-id="${data}" data-toggle="modal" data-target=".modal_editar_kit">Editar</a>
                        <a class="dropdown-item duplicar_kit"  data-id="${data}" data-toggle="modal" data-target=".modal-nuevo-kit">Duplicar</a>
                        <a class="dropdown-item eliminar_kit text-danger"  data-id="${data}" data-nombre="${row.nombre}">Eliminar</a>
                    </div>
                </div>
                `;
            }
        },
        { 	"data": "nombre",
            render: function(data,type,row){
                let items = '';
                $.map(row.items,function(registro){
                    items+=`${registro.itemtxt} (${registro.cantidad}) <br>`;
                })
                return `
                <div id="accordion-ten_${row.id}" class="accordion accordion-header-shadow accordion-rounded">                                    
                    <div class="accordion__item_${row.id}">
                        <div class="accordion__header collapsed accordion__header--success" data-toggle="collapse" data-target="#header-shadow_collapseThree_${row.id}" style=" box-shadow: none;">
                            <span class="text-info accordion__header--text">${data}</span>
                        </div>
                        <div id="header-shadow_collapseThree_${row.id}" class="collapse accordion__body" data-parent="#accordion-ten_${row.id}">
                            <div class="accordion__body--text">
                                ${items}
                            </div>
                        </div>
                    </div>
                </div>
                `;
            }
        },
        { 	"data": "costo_promedio" },
        { 	"data": "actualizado_por" },
        { 	"data": "fecha_actualizacion" }
    ],
    rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
    "columnDefs": [ //OCULTAR LA COLUMNA ID
        {
            "targets"	: [ 0,3 ],
            "visible"	: false,
            "searchable": false
        },{
            "targets"	: [2],
            "width"		: "40%"
        },{
            "targets"	: [1],
            "width"		: "5%"
        }
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "Mostrando página _PAGE_ de _PAGES_"
    },
    "dom": '<"top"f><"float-left"pl>rt<"bottom"ip><"clear">',

});


const data_kit_vacio = {"data" : {
                                    "tipo": "",
                                    "cantidad": "",
                                    "itemid": "",
                                    "itemtxt": "",
                                    "tipotxt": ""
                                }
                    };
var tabla_ver_kit_insumos = $("#tabla_ver_kit_insumos").DataTable({
    "searching": true,
    "lengthChange": false,
    data : data_kit_vacio,
    "columns"	: [
        { 	"data": "itemid" },
        { 	"data": "tipotxt" },
        { 	"data": "itemtxt" },
        { 	"data": "cantidad" }
    ],
    "columnDefs": [ //OCULTAR LA COLUMNA ID
        {
            "targets"	: [ 0 ],
            "visible"	: false,
            "searchable": false
        }
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "Mostrando página _PAGE_ de _PAGES_"
    }	
});

var tabla_editar_kit_insumos = $("#tabla_editar_kit_insumos").DataTable({
    "searching": true,
    "lengthChange": false,
    data : data_kit_vacio,
    "columns"	: [
        { 	"data": "itemid" },
        { 	"data": "itemid",
            render: function(data,type,row){
                return `
                    <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item_editar" data-itemid="${data}" data-tipo="${row.tipo}">
                    <span class="fa fa-minus" aria-hidden="true"></span>
                    <div class="ripple-container"></div>
                    </button>
                `;
            }
        },
        { 	"data": "tipotxt" },
        { 	"data": "itemtxt" },
        { 	"data": "cantidad",
            render: function(data,type,row){
                return`
                        <input type="number" class="form-control editar_kit_cantidad" step="1" min="1" data-iditem="${row.iditem}" data-tipo="${row.tipo}" value="${data}" data-valor="${data}">
                    `;
            }
        }
    ],
    "columnDefs": [ //OCULTAR LA COLUMNA ID
        {
            "targets"	: [ 0 ],
            "visible"	: false,
            "searchable": false
        },
        {
            "targets"	: [ 1,2,4 ],
            "width": "5%"
        }
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "Mostrando página _PAGE_ de _PAGES_"
    }	
});

$("#tabla_kit_insumos").on('draw.dt',function(){
    $(".ver_kit").off();
    $(".ver_kit").on('click',function(){
        tabla_ver_kit_insumos.clear();
        $("#ver_nombre_kit").html('');
        $("#ver_actualizado_por").val('');
        $("#ver_fecha_actualizacion").val('');
        let id = $(this).attr("data-id");
        $.get("controller/kit_insumos/kit_insumos_back.php",
            {"oper": "getkit", "id": id},
            function(response){
                console.log(response);
                $("#ver_nombre_kit").html(response.nombre);
                $("#ver_actualizado_por").val(response.modificado_por);
                $("#ver_fecha_actualizacion").val(response.fecha_modificacion);
                tabla_ver_kit_insumos.rows.add(response.items);
                tabla_ver_kit_insumos.draw();
            }
        ,'json')
    });
    $(".editar_kit").off();
    $(".editar_kit").on('click',function(){
        tabla_editar_kit_insumos.clear();
        $("#editar_nombre").html('');
        $("#editar_nombre_kit").val('');
        let id = $(this).attr("data-id");                
        abrirEditar(id);
    });
    $(".duplicar_kit").off();
    $(".duplicar_kit").on('click',function(){
        let id = $(this).attr("data-id");
        var items = items_nuevo;
        $.get("controller/kit_insumos/kit_insumos_back.php",
            {"oper": "getkit", "id": id},
            function(response){
                console.log(response);                
                $('#nuevo_nombre_kit').val(response.nombre).trigger('change');
                $('#nuevo_nombre').html(response.nombre);
            $.map(response.items,function(items_){
                itemtxt = items_.itemtxt.replace(/"/g,'\\"');
                var nuevafila = '{"tipo":"'+items_.tipo+'","tipotxt":"'+items_.tipotxt+'","itemid": "'+items_.itemid+'","itemtxt": "'+itemtxt+'","cantidad": "'+items_.cantidad+'"}';
                if(items != '' && items != '[]')
                items += ',';
                items += nuevafila;
                items = items.replace('[','');
                items = items.replace(']','');
                items = '['+items+']'
            });
            
            items_nuevo = items;
            cargarTablaNuevo(items_nuevo);          
                
            }
        ,'json')
    });
    $(".eliminar_kit").off();
    $(".eliminar_kit").on('click',function(){
        let id = $(this).attr('data-id');
        let nombre = $(this).attr('data-nombre');  
        swal({
			title: "Confirmar",
			text: "¿Esta seguro de eliminar el kit "+nombre+"?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: {
				cancel: "No.",
				catch: {
				  text: "Sí, eliminar",
				  value: "eliminar",
				  className: "btn-danger"
				}
			  },
			}).then((value) => {
            if (value == 'eliminar') {
                $.post( "controller/kit_insumos/kit_insumos_back.php?oper=deletekit", 
                { 
                    onlydata : "true",
                    id : id,
                    fecha : getFechaActual(),
                    user_id : getCookie('user_id')
                }, function(result){
                    if(result == 1){
                        swal('Buen trabajo!','Kit eliminado satisfactoriamente','success');		
                        // RECARGAR TABLA Y SEGUIR EN LA MISMA PAGINA (2do parametro)
                        tabla_kit_insumos.ajax.reload(null, false);
                    } else {
                        swal('ERROR!','Ha ocurrido un error al eliminar el kit, intente más tarde','error');
                    }
                });
            } 
        });
    });

});

$.get("controller/kit_insumos/kit_insumos_back.php?oper=COMBO_ITEMS",function(result){
	$("#select_item_nuevo, #select_item_editar").select2();
	$("#select_item_nuevo, #select_item_editar").select2();
	$("#select_item_nuevo, #select_item_editar").append(result).trigger("change");	  
    $('button[data-id="select_item_nuevo"], button[data-id="select_item_editar"]').hide();
});

//NUEVO 
    
    $('#anadir_nuevo').click(function(){
        var itemid = $('#select_item_nuevo').val();
        var cantidad = $('#nuevo_cantidad_item').val();
        
        var items = items_nuevo;
        if (validar_agregar_item(itemid,cantidad) == 1){
            var tipo= $('#select_item_nuevo').select2('data')[0].element.dataset.tipo;
            if(tipo == 'I')
                var tipotxt = 'Insumos';
            if(tipo == 'M')
                var tipotxt = 'Medicamentos';
            if(tipo == 'E')
                var tipotxt = 'Equipos';			
            var itemtxt = $('#select_item_nuevo').select2('data')[0].text.trim();		

            itemtxt = itemtxt.replace(/"/g,'\\"');
            if (buscar_valor(items,tipo,itemid)== 0){
                var nuevafila = '{"tipo":"'+tipo+'","tipotxt":"'+tipotxt+'","itemid": "'+itemid+'","itemtxt": "'+itemtxt+'","cantidad": "'+cantidad+'"}';
                if(items != '' && items != '[]')
                items += ',';
                items += nuevafila;
                items = items.replace('[','');
                items = items.replace(']','');
                items = '['+items+']';
                items_nuevo = items;
                cargarTablaNuevo(items);
                $('#select_item_nuevo').val('').trigger('change');
                $('#nuevo_cantidad_item').val('1').trigger('change');
            }else{
                swal('Error!','Ya se ha agregado este item al kit','error');
            }
        }
    });
   
	function eliminar_item(json,tipo, item){
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
		  if(json[i].itemid == item && json[i].tipo == tipo){ 
			json.splice(i, 1);
		  }
		}
		return JSON.stringify(json);
	}
	
	function buscar_valor(json,tipo,item){
		var encontrado = 0;
		json = JSON.parse(json);
		if(json !== undefined){
			for(var i = 0; i < json.length; i++){
				if(json[i].itemid == item && json[i].tipo == tipo ){    
					encontrado = 1;
				}
			}
		}
		return encontrado;
	}
	
	function validar_agregar_item(item,cantidad) {
		var res = 1;
		if (item == '' || item == '0'){
			swal('Error!','Debe seleccionar un Item','error');
			res = 0;
		}else if (cantidad == '' || cantidad == 0){
			swal('Error!','Debe seleccionar una cantidad', 'error');
			res = 0;
		}
		return res; 
	}
    
	function cargarTablaNuevo(items_){
		////console.log('*** cargarTablaNuevoOC ***');
		var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:160px;">Tipo</th><th style="width:350px;">Item</th><th style="width:100px;">Cantidad</th></tr>';
		if(items_ != ''){
			//console.log(items_);
			items_json = JSON.parse(items_);
			$.map(items_json,function(filas){
				html += `<tr>
                            <td class="centrar">
                                <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item" data-itemid="${filas['itemid']}" data-tipo="${filas['tipo']}">
                                    <span class="fa fa-minus" aria-hidden="true"></span>
                                    <div class="ripple-container"></div>
                                </button>
                            </td>
                            <td class="campo">${filas['tipotxt']}</td><td class="">${filas['itemtxt']}</td>
                            <td class="campo">
                                <input min="1" type="number" value="${filas['cantidad']}" data-id="${filas['itemid']}" data-tipo="${filas['tipo']}" style="width: 75px;" class="cantidad_item_nuevo"/>
                            </td>
                        </tr>`
			});
		}
        ;
		html += '</table>';
		$('#tabla_nuevo_kit_insumos').empty();
		$.when($('#tabla_nuevo_kit_insumos').append(html)).done(function(){
			$(".btn-quitar-item").each(function(){
				$(this).on('click',function(){
					var itemid = $(this).attr('data-itemid');
					var tipo = $(this).attr('data-tipo');
					items_nuevo = eliminar_item(items_nuevo,tipo,itemid);
					cargarTablaNuevo(items_nuevo);
				});
			});		
            
            $(".cantidad_item_nuevo").each(function(){
                $(this).on('change',function(){
                    if ($(this).val() == ''){
                        $(this).val('0').trigger('change');
                    }else{
                        let id = $(this).attr('data-id');
                        let tipo = $(this).attr('data-tipo');
                        let cantidad = $(this).val();
                        remplazar_valor_nuevo(items_nuevo,tipo,id,cantidad,'cantidad');                   
                    }
                });
            });	
		});
	}

	function guardarNuevo(){
		let nombre 	= $("#nuevo_nombre_kit").val();
		let items = JSON.parse(items_nuevo);
		if(validar_guardar(nombre,items_nuevo) == 1){
			$.ajax({
				type: 'post',
				url: 'controller/kit_insumos/kit_insumos_back.php',
				data: { 
					'oper'			: 'create',
					'items' 		: items,
					'nombre' 		: nombre,
                    'fecha'         : getFechaActual(),
                    'user_id'       :getCookie('user_id')
				},
				beforeSend: function() {
                    $("#overlay").show();
				},
				success: function (response) {
                    $("#overlay").hide();
					if(response == 1){
						swal('Buen trabajo!','Kit guardado satisfactoriamente','success').then(function(){
							tabla_kit_insumos.ajax.reload(null,false);
                            $(".modal-nuevo-kit").modal('hide');
                            items_nuevo ="[]";
                            $('#tabla_nuevo_kit_insumos').empty();
                            $("#nuevo_nombre_kit").val('');
						});	
					}else{
						swal('Error!','Ha ocurrido un error al guardar el kit, intente mas tarde','error');
					}
				},
				error: function () {
					demo.showSwal('error-message','ERROR!','Ha ocurrido un error al guardar el kit, intente más tarde');
				}
			});			
		}
	}

	function validar_guardar(nombre,items){
		var res = 1;
		if(nombre ==''){
			swal('Error!','El nombre es obligatorio','error');
			res = 0;
		}else if (nombre.length <=3){
			swal('Error!','El nombre debe tener mas de 3 caracteres','error');
			res = 0;
		}
		if(items === undefined || items =='' || items == '[]'){
			swal('Error!','Debe seleccionar al menos un item','error');
			res = 0;
		}
		return res;
	}
	
	$("#boton-guardar-nuevo").on("click",function(){
		guardarNuevo();
	});	
    
//EDITAR 
    var items_editar = '[]';
    function abrirEditar(id){
        $("#editar_id_kit").val(id);
        $.get('controller/kit_insumos/kit_insumos_back.php?oper=getkit',{
			id: id
		},function(datos){
			let nombre = datos.nombre;
			let items = '';
			$('#editar_nombre_kit').val(nombre).trigger('change');
			$('#editar_nombre').html(nombre);
			$.map(datos.items,function(items_){
				itemtxt = items_.itemtxt.replace(/"/g,'\\"');
				var nuevafila = '{"tipo":"'+items_.tipo+'","tipotxt":"'+items_.tipotxt+'","itemid": "'+items_.itemid+'","itemtxt": "'+itemtxt+'","cantidad": "'+items_.cantidad+'"}';
				if(items != '' && items != '[]')
				items += ',';
				items += nuevafila;
				items = items.replace('[','');
				items = items.replace(']','');
				items = '['+items+']'
			});
			
			items_editar = items;
			cargarTablaEditar(items_editar);
		},'json');
        
    }

    function cargarTablaEditar(items_){
		var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:160px;">Tipo</th><th style="width:350px;">Item</th><th style="width:100px;">Cantidad</th></tr>';
		if(items_ != ''){
			items_json = JSON.parse(items_);
			$.map(items_json,function(filas){
				html += '<tr><td class="centrar"><button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item-editar" data-tipo="'+filas['tipo']+'" data-itemid="'+filas['itemid']+'">\
                <span class="fa fa-minus" aria-hidden="true"></span>\
								<div class="ripple-container"></div> \
							</button> \
						</td><td class="campo">'+filas['tipotxt']+'</td><td class="">'+filas['itemtxt']+'</td>';
				html += '<td class="campo"><input min="1" type="number" value="'+filas['cantidad']+'" data-tipo="'+filas['tipo']+'" data-itemid="'+filas['itemid']+'" style="width: 75px;" class="cantidad_ig"/></td></tr>';
			});
		}
		
		html += '</table>';
		
		$('#tabla_editar_kit_insumos').empty();
		$('#tabla_editar_kit_insumos').append(html);

		$(".btn-quitar-item-editar").each(function(){
			$(this).on('click',function(){
				var itemid = $(this).attr('data-itemid');
				var tipo = $(this).attr('data-tipo');
				items_editar = eliminar_item(items_editar,tipo,itemid);
				cargarTablaEditar(items_editar);
			});
		});
		
		$(".cantidad_ig").each(function(){
			$(this).on('change',function(){
				if ($(this).val() == ''){
					$(this).val('0').trigger('change');
				}else{
					let costo_item = $(this).attr('data-costo'); 
					let id = $(this).attr('data-itemid');
					let tipo = $(this).attr('data-tipo');
					let cantidad = $(this).val();
					remplazar_valor_editar(items_editar,tipo,id,cantidad,'cantidad');                   
				}
			});
		});
	}


	// AÑADIR 
	$('#anadir_editar').click(function(){
		let itemid = $('#select_item_editar').val();
		let cantidad = $('#editar_cantidad_kit').val();
		let items = items_editar;
        let tipotxt = '';
		if (validar_agregar_item(itemid,cantidad) == 1){
			let tipo= $('#select_item_editar').select2('data')[0].element.dataset.tipo;
			if(tipo == 'I')
				tipotxt = 'Insumos';
			if(tipo == 'M')
				tipotxt = 'Medicamentos';
			if(tipo == 'E') 
				tipotxt = 'Equipos';			
			let itemtxt = $('#select_item_editar').select2('data')[0].text.trim().split('|')[1];	
			itemtxt = itemtxt.replace(/"/g,'\\"');
			if (buscar_valor(items,tipo,itemid) == 0){
				let nuevafila = '{"tipo":"'+tipo+'","tipotxt":"'+tipotxt+'","itemid": "'+itemid+'","itemtxt": "'+itemtxt+'","cantidad": "'+cantidad+'"}';
				if(items != '' && items != '[]')
				items += ',';
				items += nuevafila;
				items = items.replace('[','');
				items = items.replace(']','');
				items = '['+items+']';
				items_editar = items;
				cargarTablaEditar(items);
				$('#select_item_editar').val('').trigger('change');
				$('#editar_cantidad_kit').val('1').trigger('change');
			}else{
				swal('Error!','Ya se ha agregado este item al kit','error');
			}
		}
	});
	
	function remplazar_valor_nuevo(json,tipo,item,valor,campo){
		console.log(tipo + item + valor + campo);
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
            if(json[i]['itemid'] == item && json[i]['tipo'] == tipo){    
                json[i][campo] = valor;
            }
		}
		items_nuevo = JSON.stringify(json);
		console.log(json);
    }
	function remplazar_valor_editar(json,tipo,item,valor,campo){
		console.log(tipo + item + valor + campo);
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
            if(json[i]['itemid'] == item && json[i]['tipo'] == tipo){    
                json[i][campo] = valor;
            }
		}
		items_editar = JSON.stringify(json);
		console.log(json);
    }
    
    
	function guardarEditar(){
		let id 			= $("#editar_id_kit").val();
		let nombre 		= $("#editar_nombre_kit").val();
		let items = JSON.parse(items_editar);
		if (validar_guardar(nombre,items_editar) == 1){
			$.ajax({
				type: 'post',
				url: 'controller/kit_insumos/kit_insumos_back.php',
				data: { 
					'oper'		: 'updatekit',
					'id'		: id,
					'fecha'		: getFechaActual(),
					'user_id'   : getCookie('user_id'),
					'nombre' 	: nombre,
					'items' 	: items
				},
				beforeSend: function() {
                    $("#overlay").show();
				},
				success: function (response) {
                    $("#overlay").hide();
					if(response == 1){
						swal('Buen trabajo!','Kit actualizado satisfactoriamente','success').then(function(){
                            tabla_kit_insumos.ajax.reload(null,false);
                            $(".modal_editar_kit").modal('hide');
                            items_editar ="[]";
                            $('#tabla_editar_kit_insumos').empty();
                            $("#editar_nombre_kit").val('');
                            $("#editar_nombre").html('');
                            $("#editar_id_kit").val('');
                        });	
					}else{
						swal('Error!','Ha ocurrido un error al guardar el kit, intente mas tarde','error');
					}
				},
				error: function () {
                    $("#overlay").hide();
					swal('¡ERROR!','Ha ocurrido un error al guardar el kit, intente más tarde','error');
				}
			});			
		}
	}

    $("#boton-guardar-editar").on('click',function(){
        guardarEditar();
    });