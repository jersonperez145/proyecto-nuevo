var tabla_items = $("#tabla_items").DataTable({
    responsive: true,
    lengthChange: true,
    pageLength: 50,
    searching: true,
    order: [[6,'desc']],
    "ajax"		: "controller/gestion-inventario/gestion-inventario-back.php?oper=cargarInventario&tipoinventario=1&item=&tipo=",
    "columns"	: [
        { "data": "id",
            render: function(data,type,row){
                return `
                <div class="dropdown">
                    <button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
                        <svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item ver-movimientos"  data-id="${data}" data-tipo="${row.tipo.charAt(0)}" data-toggle="modal" data-target=".modal-ver-movimientos">Ver movimientos</a>                       
                        <a class="dropdown-item ver-comprometido"  data-id="${data}" data-tipo="${row.tipo.charAt(0)}" data-toggle="modal" data-target=".modal-ver-comprometido">Ver comprometido</a>                       
                    </div>
                </div>
                `;
            }
        },// 0
        { "data": "id"},		// 1
        { "data": "tipo" },		// 2
        { "data": "codigo" },	// 3
        { "data": "nombre" },	// 4
        { "data": "costounitario" },	// 5
        { "data": "valor" },	// 6
        { "data": "cantidad_disponible"}, //7
        { "data": "cantidad_comprometido"}, //8
        { "data": "cantidad_total"} //9

        ],
    rowId: 'id',
    "columnDefs": [
        {
            "targets"	: [ 0 ],
            "width"		:  "5%",
            "sercheable": false
        },{
            "targets"	: [1],
            "visible"	: false,
            "sercheable": false
        },{
            "targets"	: [ 4 ],
            "width"		:  "35%"
        },{
            "targets"	: [5,6,7,8,9],
            "width"		:  "5%",
            "sercheable": false
        }
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "MOSTRANDO PAGINA _PAGE_ DE _PAGES_"
    },
    "dom": '<"top"f><"float-left"pl>rt<"bottom"ip><"clear">',
    "drawCallback": function( settings ) {
        $('.dataTables_paginate').addClass("pagination-gutter");
    }
});

var tabla_movimientos = $("#tabla_movimientos").DataTable({
    responsive: true,
    lengthChange: true,
    pageLength: 50,
    searching: true,
    order: [[2,'desc']],
    "ajax"		: "controller/gestion-inventario/gestion-inventario-back.php?oper=cargar_movimientos_todos&paciente=&desde=&hasta=",
    "columns"	: [
        { "data": "id",
            render: function(data,type,row){
                return `
                    <div class="dropdown">
                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
                            <svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item ver-movimiento"  data-id="${data}" data-toggle="modal" data-target=".modal-ver-movimiento-ajustes">Ver movimiento</a>                       
                            <a class="dropdown-item imprimir-movimiento"  data-id="${data}" href="controller/gestion-inventario/imprimir-movimiento.php?id=${data}" target="blank">Imprimir</a>                       
                        </div>
                    </div>
                `;
            }
        }, 	// 0
        { "data": "id" },			// 1
        { "data": "fecha" },		// 2
        { "data": "tipo" },			// 3
        { "data": "movimiento" },	// 4
        { "data": "responsable" }	// 5
        ],
    rowId: 'id',
    "columnDefs": [
        {
            "targets"	: [ 0 ],
            "width"		:  "5%",
            "sercheable": false
        },{
            "targets"	: [1],
            "visible"	: true,
            "sercheable": false
        }
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "MOSTRANDO PAGINA _PAGE_ DE _PAGES_"
    },
    "dom": '<"top"f><"float-left"pl>rt<"bottom"ip><"clear">',

});

var tablaMovimientos = $("#tablaMovimientos").DataTable({
    responsive: true,
    lengthChange: false,
    pageLength:10,
    searching: true,
    order: [[1,'desc']],
    "ajax"		: "controller/gestion-inventario/gestion-inventario-back.php?oper=cargar_movimientos&iditem=&tipo=&movimiento=&inventario=",
    "columns"	: [
        {"data": 'id',
            render: function(data,type,row){
                return `
                <div class="dropdown">
                    <button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
                        <svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item mov-imprimir-movimiento"  data-id="${data}" data-tipo="${row.tipo}" data-toggle="modal" data-target=".modal-ver-movimientos">Imprimir movimiento</a>                       
                        <a class="dropdown-item mov-imprimir-origen"  data-id="${data}" data-tipo="${row.tipo}" data-toggle="modal" data-target=".modal-ver-comprometido">Imprimir ${row.tipo}</a>                       
                    </div>
                </div>
                `;
            }
        },
        {"data": 'id'},
        {"data": 'fecha'},
        {"data": 'paciente'},
        {"data": 'cantidad',
            render: function(data,type,row){
                var s = '';
                if(row.movimiento == 'Entrada'){
                    s = '+'+data;
                } else {
                    s = '-'+data;
                }
                return s;					
            }
        },
        {"data": 'movimiento'},
        {"data": 'tipo',
            render: function(data,type,row){
                let text = capitalizarPrimeraLetra(data);
                return text;
            }
        }
        
        ],
    rowId: 'id',
    "columnDefs": [
        {
            "targets"	: [ 1 ],
            "visible"	: false
        },
    ],
    "language": {
        "url": "js/Spanish.json",
        "info": "MOSTRANDO PAGINA _PAGE_ DE _PAGES_"
    }
});	
data_ver_ajuste = {"data":{
    "tipo" :"",
    "codigo" :"",
    "nombre" :"",
    "cantidad" :"",
    "costo" :"",
    "total" :"",
    "inventario" :""
}};
var tabla_detalle_movimiento = $("#tabla_detalle_movimiento").DataTable({
    responsive: true,
    lengthChange: true,
    pageLength:10,
    searching: true,
    order: [[1,'desc']],
    data : data_ver_ajuste,
    "columns"	: [
        {"data": 'tipo'},
        {"data": 'codigo'},
        {"data": 'nombre'},
        {"data": 'cantidad' },
        {"data": 'costo'},
        {"data": 'total'},
        {"data": 'inventario'}
    ],        
    "language": {
        "url": "js/Spanish.json",
        "info": "MOSTRANDO PAGINA _PAGE_ DE _PAGES_"
    }
});

$("#tabla_items").on('draw.dt',function(){
    $(".ver-movimientos").off();
    $(".ver-movimientos").on('click',function(){
        var item = $(this).attr('data-id');
        var tipo = $(this).attr('data-tipo');
        var inventario = $("#select-inventario").val();
        tablaMovimientos.ajax.url("controller/gestion-inventario/gestion-inventario-back.php?oper=cargar_movimientos&iditem="+item+"&tipo="+tipo+"&movimiento=&inventario="+inventario).load(null,false);				
        var reg = $(this).closest('tr');
        var row =tabla_items.row(reg).data();
        var datosJson = row;
        $("#tipo_mov").val(datosJson.tipo);
        $("#codigo_mov").val(datosJson.codigo);
        $("#nombre_mov").val(datosJson.nombre);
        $("#iditem_mov").val(item);
        $("#tipoitem_mov").val(tipo);
    })
    // TOOLTIPS
    $('[data-toggle="tooltip"]').tooltip();
})

$("#tabla_movimientos").on('draw.dt',function(){
    $(".ver-movimiento").off();
    $(".ver-movimiento").on('click',function(){
        let id = $(this).attr('data-id');
        
        tabla_detalle_movimiento.ajax.url("controller/gestion-inventario/gestion-inventario-back.php?oper=get_ajuste&id="+id).load(null,false);				
        var reg = $(this).closest('tr');
        var row =tabla_movimientos.row(reg).data();
        var datosJson = row;
        $("#detalle_ajuste_nro").val(datosJson.id);
        $("#detalle_ajuste_fecha").val(datosJson.fecha);
        $("#detalle_ajuste_movimiento").val(datosJson.movimiento);
        $("#detalle_ajuste_tipo").val(datosJson.tipo);
        $("#detalle_ajuste_responsable").val(datosJson.responsable);
    })
});

//CARGAR LISTAS DESPLEGABLES 
    $.get("controller/gestion-inventario/gestion-inventario-back.php",{
        'oper': 'LISTAR_ITEMS',
        'bodega' : 1
    },function(response){
        $("#select_item_mover,#select_item_ajuste").select2();
        $("#select_item_mover,#select_item_ajuste").empty();
        $("#select_item_mover,#select_item_ajuste").append(response);
        $('button[data-id="select_item_mover"],button[data-id="select_item_ajuste"]').hide();

    });
    $.get("controller/gestion-inventario/gestion-inventario-back.php",{
        'oper': 'LISTAR_BODEGAS'
    },function(response){
        // filtro masivo
        $("#select-filtro-bodega").select2();
        $("#select-filtro-bodega").empty();
        $("#select-filtro-bodega").append(response);
        $('button[data-id="select-filtro-bodega"]').hide();
        //modal mover entre bodegas
        $("#select-inventario-origen-mover,#select-inventario-destino-mover").select2();
        $("#select-inventario-origen-mover,#select-inventario-destino-mover").empty();
        $("#select-inventario-origen-mover,#select-inventario-destino-mover").append(response);
        $('button[data-id="select-inventario-origen-mover"], button[data-id="select-inventario-destino-mover"]').hide();
        //ajustes
        $("#select-inventario-ajuste").select2();
        $("#select-inventario-ajuste").empty();
        $("#select-inventario-ajuste").append(response);
        $('button[data-id="select-inventario-ajuste"]').hide();
    });
    $("#select-inventario-origen-mover").on('change',function(){
        $.get("controller/gestion-inventario/gestion-inventario-back.php",{
            'oper': 'LISTAR_ITEMS',
            'bodega' : $(this).val()
        },function(response){
            // $("#select_item_mover").select2();
            $("#select_item_mover").empty();
            $("#select_item_mover").append(response);
            // $('button[data-id="select_item_mover"]').hide();
    
        });
    });
    $("#select-inventario-destino-mover").on('change',function(){
        if($(this).val() == $("#select-inventario-origen-mover").val() && $(this).val()!= 0){
            swal("Error!","Debe seleccionar una bodega de destino diferente","error"),
            $(this).val(0);
        }
    })
//moviemiento de bodegas
    var items_mover_bodegas = '[]';
    $("#select_item_mover").on('change',function(){
        let iditem = $(this).val();
        if(iditem == 0 || iditem == '' || iditem === null || iditem === undefined){
            return false;
        }else{ 
            let tipo= $(this).select2('data')[0].element.dataset.tipo;
            let disponible= $(this).select2('data')[0].element.dataset.disponible;
            if(disponible > 0){
                $("#mover_cantidad").val(1);
                $("#mover_cantidad").prop("max",disponible);
                $("#mover_disponible").val(disponible);            
            }else{
                swal("Error!","No hay disponibilidad de este item","error");
                $("#mover_disponible").val('');            
                $(this).val(0);
            }
        }
    })
    $("#mover_cantidad").on('change',function(){
        if(parseInt($(this).val()) > parseInt($(this).prop('max'))){
            $(this).val($(this).prop('max'))
        }
    })
    $("#mover_anadir").on('click',function(){
        let origen =$("#select-inventario-origen-mover").val();
        let destino =$("#select-inventario-destino-mover").val();
        if(origen == destino){
            swal("Error!","Debe seleccionar una bodega de destino diferente","error");
            return false;
        }
        let iditem =$("#select_item_mover").val();            
        if(validar_anadir_item(origen,iditem)){
            let items = items_mover_bodegas;
            let itemtxt = $('#select_item_mover').select2('data')[0].text.trim();		
            let tipo= $("#select_item_mover").select2('data')[0].element.dataset.tipo;
            let disponible= $("#select_item_mover").select2('data')[0].element.dataset.disponible;
            let cantidad = $('#mover_cantidad').val();  
            let tipotxt = '';              
            if(tipo == 'I')
                tipotxt = 'Insumos';
            if(tipo == 'M')
                tipotxt = 'Medicamentos';
            if(tipo == 'E')
                tipotxt = 'Equipos';
            itemtxt = itemtxt.replace(/"/g,'\\"');
            if (buscar_valor(items,tipo,iditem)== 0){
                let nuevafila = '{"tipo":"'+tipo+'","tipotxt":"'+tipotxt+'","iditem": "'+iditem+'","itemtxt": "'+itemtxt+'","cantidad": "'+cantidad+'","disponible":"'+disponible+'"}';
                if(items != '' && items != '[]')
                items += ',';
                items += nuevafila;
                items = items.replace('[','');
                items = items.replace(']','');
                items = '['+items+']';
                items_mover_bodegas = items;
                cargarTablaMoverBodegas(items);
                $('#select_item_nuevo').val(0).trigger('change');
                $('#nuevo_cantidad_item').val('').trigger('change');
                $("#select-inventario-origen-mover").prop("disabled",true);
                $("#select-inventario-destino-mover").prop("disabled",true);
            }else{
                swal('Error!','Ya se ha agregado este item','error');
            }
        
        }
    })
    function cargarTablaMoverBodegas(items){
        var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:160px;">Tipo</th><th style="width:350px;">Item</th><th style="width:100px;">Cantidad</th></tr>';
        if(items_mover_bodegas != ''){
            items_json = JSON.parse(items_mover_bodegas);
            $.map(items_json,function(filas){
                html += `<tr>
                            <td class="centrar">
                                <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item" data-iditem="${filas['iditem']}" data-tipo="${filas['tipo']}">
                                    <span class="fa fa-minus" aria-hidden="true"></span>
                                    <div class="ripple-container"></div>
                                </button>
                            </td>
                            <td class="campo">${filas['tipotxt']}</td><td class="">${filas['itemtxt']}</td>
                            <td class="campo">
                                <input min="1" max="${filas['disponible']}" type="number" value="${filas['cantidad']}" data-id="${filas['iditem']}" data-tipo="${filas['tipo']}" style="width: 75px;" class="cantidad_item_nuevo"/>
                            </td>
                        </tr>`
            });
        }
        ;
        html += '</table>';
        $('#modal-tabla-mover-bodegas').empty();
        $.when($('#modal-tabla-mover-bodegas').append(html)).done(function(){
            $(".btn-quitar-item").each(function(){
                $(this).on('click',function(){
                    var iditem = $(this).attr('data-iditem');
                    var tipo = $(this).attr('data-tipo');
                    items_mover_bodegas = eliminar_item(items_mover_bodegas,tipo,iditem);
                    cargarTablaMoverBodegas(items_mover_bodegas);
                    if(JSON.parse(items_mover_bodegas).length == 0){
                        $("#select-inventario-origen-mover").prop("disabled",false);
                        $("#select-inventario-destino-mover").prop("disabled",false);
                    }
                });
            });		
            
            $(".cantidad_item_nuevo").each(function(){
                $(this).on('change',function(){
                    if ($(this).val() == ''){
                        $(this).val('0').trigger('change');
                    }else{
                        if(parseInt($(this).val()) > parseInt($(this).prop('max'))){
                            $(this).val($(this).prop('max')).trigger('change');
                        }
                        let id = $(this).attr('data-id');
                        let tipo = $(this).attr('data-tipo');
                        let cantidad = $(this).val();
                        remplazar_valor(items_mover_bodegas,tipo,id,cantidad,'cantidad');                   
                    }
                });
            });	
        });
    }
    function validar_anadir_item(origen,iditem){
        if(origen == 0 || origen == '' || origen === null || origen === undefined){
            swal("Error!","Debe seleccionar la bodega de origen antes de añadir un item","error");
            return false;
        }else if(iditem == 0 || iditem == '' || iditem === null || iditem === undefined){
            swal("Error!","Debe seleccionar un item","error");
            return false
        }
        return true;
    }   
    function eliminar_item(json,tipo, item){
        json = JSON.parse(json);
        for(var i = 0; i < json.length; i++){
            if(json[i].iditem == item && json[i].tipo == tipo){ 
            json.splice(i, 1);
            }
        }
        return JSON.stringify(json);
    }
    function buscar_valor(json,tipo,item){
        var encontrado = 0;
        json = JSON.parse(json);
        if(json !== undefined){
            for(var i = 0; i < json.length; i++){
                if(json[i].iditem == item && json[i].tipo == tipo ){    
                    encontrado = 1;
                }
            }
        }
        return encontrado;
    }
    function remplazar_valor(json,tipo,item,valor,campo){
        json = JSON.parse(json);
        for(var i = 0; i < json.length; i++){
            if(json[i]['iditem'] == item && json[i]['tipo'] == tipo){    
                json[i][campo] = valor;
            }
        }
        items_mover_bodegas = JSON.stringify(json);
    }
    const data_mover_vacio = {"data" : {
                "tipo": "",
                "cantidad": "",
                "iditem": "",
                "itemtxt": "",
                "tipotxt": ""
            }
    };
    var tabla_mover_bodegas = $("#modal-tabla-mover-bodegas").DataTable({
        "searching": true,
        "lengthChange": false,
        data : data_mover_vacio,
        "columns"	: [
            { 	"data": "iditem" },
            { 	"data": "iditem",
                render: function(data,type,row){
                    return `
                        <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item_editar" data-iditem="${data}" data-tipo="${row.tipo}">
                        <span class="fa fa-minus" aria-hidden="true"></span>
                        <div class="ripple-container"></div>
                        </button>
                    `;
                }
            },
            { 	"data": "tipotxt" },
            { 	"data": "itemtxt" },
            { 	"data": "cantidad",
                render: function(data,type,row){
                    return`
                            <input type="number" class="form-control editar_kit_cantidad" step="1" min="1" data-iditem="${row.iditem}" data-tipo="${row.tipo}" value="${data}" data-valor="${data}">
                        `;
                }
            }
        ],
        "columnDefs": [ //OCULTAR LA COLUMNA ID
            {
                "targets"	: [ 0 ],
                "visible"	: false,
                "searchable": false
            },
            {
                "targets"	: [ 1,2,4 ],
                "width": "5%"
            }
        ],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        }	
    });
    $("#boton-guardar-movimiento").on('click',function(){
        let origen = $("#select-inventario-origen-mover").val();
        let destino = $("#select-inventario-destino-mover").val();
        let fecha = $("#mover_fecha").val();
        if(validar_guardar_movimiento(origen,destino,fecha,items_mover_bodegas)){       
            /// AQUI VA EL CODIGO DE GUARDAR         
            swal("Buen trabajo!","Movimiento realizado exitosamente","success");
            $("#modal-mover-bodegas").modal('hide');
            items_mover_bodegas ="[]";
            $('#modal-tabla-mover-bodegas').empty();
            $('#select_item_nuevo').val(0).trigger('change');
            $("#select-inventario-origen").val(0);
            $("#select-inventario-destino").val(0);
            $("#mover_cantidad").val('');
            $("#mover_disponible").val('');
        }
    });
    function validar_guardar_movimiento(origen,destino,fecha,items){
        if(origen == 0 || origen == '' || origen === null || origen === undefined){
            swal("Error!","Debe seleccionar la bodega de origen antes de añadir un item","error");
            return false;
        }else if(destino == 0 || destino == '' || destino === null || destino === undefined){
            swal("Error!","Debe seleccionar una bodega de destino","error");
            return false
        }else if(fecha == '' || fecha ===undefined || fecha === null){
            swal("Error!","Debe seleccionar una fecha","error");
            return false
        }else if(JSON.parse(items).length <= 0){
            swal("Error!","Debe añadir al menos un item","error");
            return false
        }
        return true;
    }
    $("#modal-mover-bodegas").on("hidden.bs.modal", function () {
        $("#select-inventario-origen-mover").val(0).trigger('change');
        $("#select-inventario-origen-mover").prop("disabled",false);
        $("#select-inventario-destino-mover").prop("disabled",false);
        $("#select-inventario-destino-mover").val(0).trigger('change');
        $("#mover_fecha").val('');        
        $("#mover_disponible").val('');
        $("#mover_cantidad").val('');
        items_mover_bodegas = '[]';
        cargarTablaMoverBodegas(items_mover_bodegas);
    });

//AJUSTE DE INVENTARIO
    $("#select-inventario-ajuste").on('change',function(){
        $.get("controller/gestion-inventario/gestion-inventario-back.php",{
            'oper': 'LISTAR_ITEMS',
            'bodega' : $(this).val()
        },function(response){
            // $("#select_item_mover").select2();
            $("#select_item_ajuste").empty();
            $("#select_item_ajuste").append(response);
            // $('button[data-id="select_item_mover"]').hide();
    
        });
    });
    var items_ajuste = '[]';
    $("#select_item_ajuste").on('change',function(){
        let iditem = $(this).val();
        if(iditem == 0 || iditem == '' || iditem === null || iditem === undefined){
            return false;
        }else{ 
            let movimiento =$("#select-movimiento-ajuste").val();            
            let tipo= $(this).select2('data')[0].element.dataset.tipo;
            let disponible= $(this).select2('data')[0].element.dataset.disponible;
            if(movimiento == 'salida'){
                if(disponible <= 0){                        
                    swal("Error!","No hay disponibilidad de este item","error");
                    $("#mover_disponible").val('');            
                    $(this).val(0);
                    return false;
                }
            }
            $("#ajuste_cantidad").val(1);
            $("#ajuste_cantidad").prop("max",disponible);
            $("#ajuste_disponible").val(disponible);            
            
        }
    })
    $("#ajuste_cantidad").on('change',function(){
        let movimiento =$("#select-movimiento-ajuste").val();            
        if(movimiento == 'salida'){
            if(parseInt($(this).val()) > parseInt($(this).prop('max'))){
                $(this).val($(this).prop('max'))
            }
        }            
    })
    $("#ajuste_anadir").on('click',function(){
        let bodega =$("#select-inventario-ajuste").val();
        let iditem =$("#select_item_ajuste").val();            
        let movimiento =$("#select-movimiento-ajuste").val();            
        if(validar_anadir_item_ajuste(bodega,iditem,movimiento)){
            let items = items_ajuste;
            let itemtxt = $('#select_item_ajuste').select2('data')[0].text.trim();		
            let tipo= $("#select_item_ajuste").select2('data')[0].element.dataset.tipo;
            let disponible= $("#select_item_ajuste").select2('data')[0].element.dataset.disponible;
            let cantidad = $('#ajuste_cantidad').val();  
            let tipotxt = '';              
            if(tipo == 'I')
                tipotxt = 'Insumos';
            if(tipo == 'M')
                tipotxt = 'Medicamentos';
            if(tipo == 'E')
                tipotxt = 'Equipos';
            itemtxt = itemtxt.replace(/"/g,'\\"');
                if (buscar_valor(items,tipo,iditem)== 0){
                    let nuevafila = '{"tipo":"'+tipo+'","tipotxt":"'+tipotxt+'","iditem": "'+iditem+'","itemtxt": "'+itemtxt+'","cantidad": "'+cantidad+'","disponible":"'+disponible+'"}';
                    if(items != '' && items != '[]')
                    items += ',';
                    items += nuevafila;
                    items = items.replace('[','');
                    items = items.replace(']','');
                    items = '['+items+']';
                    items_ajuste = items;
                    cargarTablaAjuste(items);
                    $('#select_item_ajuste').val(0).trigger('change');
                    $('#ajuste_cantidad').val();
                    $("#select-movimiento-ajuste").prop("disabled",true); 
                    $("#select-inventario-ajuste").prop("disabled",true); 
            }else{
                swal('Error!','Ya se ha agregado este item','error');
            }
        
        }
    })
    function cargarTablaAjuste(items){
        console.log(items);
        var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:160px;">Tipo</th><th style="width:350px;">Item</th><th style="width:100px;">Cantidad</th></tr>';
        if(items_ajuste != ''){
            items_json = JSON.parse(items_ajuste);
            $.map(items_json,function(filas){
                html += `<tr>
                            <td class="centrar">
                                <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item" data-iditem="${filas['iditem']}" data-tipo="${filas['tipo']}">
                                    <span class="fa fa-minus" aria-hidden="true"></span>
                                    <div class="ripple-container"></div>
                                </button>
                            </td>
                            <td class="campo">${filas['tipotxt']}</td><td class="">${filas['itemtxt']}</td>
                            <td class="campo">
                                <input min="1" max="${filas['disponible']}" type="number" value="${filas['cantidad']}" data-id="${filas['iditem']}" data-tipo="${filas['tipo']}" style="width: 75px;" class="cantidad_item_nuevo"/>
                            </td>
                        </tr>`
            });
        }
        ;
        html += '</table>';
        $('#modal-tabla-ajuste').empty();
        $.when($('#modal-tabla-ajuste').append(html)).done(function(){
            $(".btn-quitar-item").each(function(){
                $(this).on('click',function(){
                    var iditem = $(this).attr('data-iditem');
                    var tipo = $(this).attr('data-tipo');
                    items_ajuste = eliminar_item(items_ajuste,tipo,iditem);
                    cargarTablaAjuste(items_ajuste);
                    if(JSON.parse(items_ajuste).length == 0){
                        $("#select-movimiento-ajuste").prop("disabled",false);
                        $("#select-inventario-ajuste").prop("disabled",false); 
                    }
                });                  
            });		
            
            $(".cantidad_item_nuevo").each(function(){
                let movimiento =$("#select-movimiento-ajuste").val();  
                $(this).on('change',function(){
                    if ($(this).val() == ''){
                        $(this).val('0').trigger('change');
                    }else{
                            if(movimiento == 'salida'){
                                if(parseInt($(this).val()) > parseInt($(this).prop('max'))){
                                    $(this).val($(this).prop('max')).trigger('change');
                                }
                            }
                            let id = $(this).attr('data-id');
                            let tipo = $(this).attr('data-tipo');
                            let cantidad = $(this).val();
                            remplazar_valor(items_ajuste,tipo,id,cantidad,'cantidad');                   
                    }
                });
            });	
        });
    }
    function validar_anadir_item_ajuste(bodega,iditem, movimiento){
        if(bodega == 0 || bodega == '' || bodega === null || bodega === undefined){
            swal("Error!","Debe seleccionar la bodega antes de añadir un item","error");
            return false;
        }else if(movimiento == 0 || movimiento == '' || movimiento === null || movimiento === undefined){
            swal("Error!","Debe seleccionar un movimiento de inventario","error");
            return false          
        }else if(iditem == 0 || iditem == '' || iditem === null || iditem === undefined){
            swal("Error!","Debe seleccionar un item","error");
            return false
        }
        return true;
    }   
    const data_ajuste_vacio = {"data" : {
                "tipo": "",
                "cantidad": "",
                "iditem": "",
                "itemtxt": "",
                "tipotxt": ""
            }
    };
    var tabla_mover_bodegas = $("#modal-tabla-ajuste").DataTable({
        "searching": true,
        "lengthChange": false,
        data : data_ajuste_vacio,
        "columns"	: [
            { 	"data": "iditem" },
            { 	"data": "iditem",
                render: function(data,type,row){
                    return `
                        <button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item_editar" data-iditem="${data}" data-tipo="${row.tipo}">
                        <span class="fa fa-minus" aria-hidden="true"></span>
                        <div class="ripple-container"></div>
                        </button>
                    `;
                }
            },
            { 	"data": "tipotxt" },
            { 	"data": "itemtxt" },
            { 	"data": "cantidad",
                render: function(data,type,row){
                    return`
                            <input type="number" class="form-control editar_kit_cantidad" step="1" min="1" data-iditem="${row.iditem}" data-tipo="${row.tipo}" value="${data}" data-valor="${data}">
                        `;
                }
            }
        ],
        "columnDefs": [ //OCULTAR LA COLUMNA ID
            {
                "targets"	: [ 0 ],
                "visible"	: false,
                "searchable": false
            },
            {
                "targets"	: [ 1,2,4 ],
                "width": "5%"
            }
        ],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        }	
    });
    $("#boton-guardar-ajuste").on('click',function(){
        let bodega = $("#select-inventario-ajuste").val();
        let movimiento = $("#select-movimiento-ajuste").val();
        let fecha = $("#ajuste_fecha").val();
        if(validar_guardar_ajuste(bodega,movimiento,fecha,items_ajuste)){       
            /// AQUI VA EL CODIGO DE GUARDAR         
            swal("Buen trabajo!","Ajuste realizado exitosamente","success");
            $("#modal-ajuste").modal('hide');
            items_ajuste ="[]";
            $('#modal-tabla-ajuste').empty();
            $("#select-inventario-ajuste").val(0).trigger('change');
            $("#select-movimiento-ajuste").val(0).trigger('change');
            $("#ajuste_cantidad").val('');
            $("#ajuste_fecha").val('');
            $("#ajuste_disponible").val('');
        }
    });
    function validar_guardar_ajuste(bodega,movimiento,fecha,items){
        if(bodega == 0 || bodega == '' || bodega === null || bodega === undefined){
            swal("Error!","Debe seleccionar la bodega antes de añadir un item","error");
            return false;
        }else if(movimiento == 0 || movimiento == '' || movimiento === null || movimiento === undefined){
            swal("Error!","Debe seleccionar un movimiento de inventario","error");
            return false
        }else if(fecha == '' || fecha ===undefined || fecha === null){
            swal("Error!","Debe seleccionar una fecha","error");
            return false
        }else if(JSON.parse(items).length <= 0){
            swal("Error!","Debe añadir al menos un item","error");
            return false
        }
        return true;
    }
    $("#modal-ajuste").on("hidden.bs.modal", function () {
        $("#select-inventario-ajuste").val(0).trigger('change');
        $("#select-inventario-ajuste").prop("disabled",false);
        $("#select-movimiento-ajuste").prop("disabled",false);
        $("#select-movimiento-ajuste").val(0).trigger('change');
        $("#ajuste_fecha").val('');        
        $("#ajuste_disponible").val('');
        $("#ajuste_cantidad").val('');
        items_ajuste = '[]';
        cargarTablaAjuste(items_ajuste);
    });
//select2() 
    $("#moviemiento_mov").select2();
    $("#select-filtro-tipo").select2();
    $("#select-movimiento-ajuste").select2();
//FILTROS 
    $("#filtros").on('click',function(){
        $("#btn_filtro").click();
    })
    $("#cerrar_filtros").on('click',function(){
        $("#btn_filtro").click();
    })
    $("#boton-aplicar-filtros").on('click',function(){
        let bodega = $("#select-filtro-bodega").val();
        let tipo = $("#select-filtro-tipo").val();
        let fecha = $("#filtro-fecha").val();
        let filtros = localStorage.getItem('filtros');
        if(filtros === null) filtros = '[]';
        let nuevo_filtro = '{"modulo":"gestion-inventario","filtros":{"bodega":"'+bodega+'","tipo":"'+tipo+'","fecha": "'+fecha+'"}}';
                
        if (buscar_valor_filtro(JSON.parse(filtros),'gestion-inventario')== 0){
        
            if(filtros != '' && filtros != '[]' && filtros !== null)
                filtros += ',';
            filtros += nuevo_filtro;
            filtros = filtros.replace('[','');
            filtros = filtros.replace(']','');
            filtros = '['+filtros+']';
            localStorage.setItem('filtros',filtros);
        }else{
            remplazar_valor_filtro(JSON.parse(filtros),'gestion-inventario',nuevo_filtro);
        }

    })
    function buscar_valor_filtro(json,modulo){
        var encontrado = 0;
        if(json !== undefined){
            for(var i = 0; i < json.length; i++){
                if(json[i]['modulo'] == modulo){    
                    encontrado = 1;
                }
            }
        }else{
        }
        return encontrado;
    }
    function remplazar_valor_filtro(json,modulo,nuevo){        
        for(var i = 0; i < json.length; i++){
            if(json[i]['modulo'] == modulo){    
                json[i] = nuevo;
            }
        }    
        localStorage.setItem('filtros',JSON.stringify(json));
    }
