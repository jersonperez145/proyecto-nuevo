$(document).ready(function() {
    //$('#items').selectpicker('refresh');
    //$('#proveedor').selectpicker('refresh');

	var itemsoc = "[]";
	var subtotal = 0;
	
    $.get("controller/compras/comprasback.php?oper=comboproveedores&tipoItem=", { onlydata:"true" }, function(result){
		$('#proveedor').empty();	
		$('#proveedor').append(result);	
		$('#proveedor').select2({placeholder: ""});
        $('button[data-id="proveedor"]').hide();
	});

	console.log(jQuery.fn.dataTableExt.oPagination);

    var tablacompras = $("#tablacompras").DataTable({
		"ajax"		:"controller/compras/comprasback.php?oper=listado",
		"order":	[2,'ASC'],
		"sDom": '<"top"f><"float-left"pl>rt<"bottom"ip><"clear">',
		"columns"	: [
			{ 	"data": "id" },
			{ 	"data": "acciones" },
            { 	"data": "categoria" },
            { 	"data": "numero" },
            { 	"data": "proveedor" },
            { 	"data": "total" },
            { 	"data": "factura" },
            { 	"data": "fecha" },
            { 	"data": "estado" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID
			{
				"targets"	: [ 0 ],
				"visible"	: false,
				"searchable": false
			},{
				"targets"	: [1],
				"width"		: "10%"
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}
	});

    $('#tablacompras').on( 'draw.dt', function () {
		$('.boton-editar').off();
		$('.boton-aprobar').off();
		$(".boton-ver-detalle").off();
		$(".boton-imprimir").off();
		$('.boton-eliminar').off();
		$('.boton-rechazar').off();

		// DAR FUNCIONALIDAD AL BOTON EDITAR
        $('.boton-editar').each(function(){
			$(this).on( 'click', function() {
				var id = $(this).attr("data-id");
				// TODO:FUNCION EDITAR
			});
		});

        $('.boton-aprobar').each(function(){
			$(this).on( 'click', function() {
				var id = $(this).attr("data-id");
				var proveedor = $(this).parent().parent().next().next().html();
				var numero = $(this).parent().parent().next().html();
				var url = "odcproveedor.php?token="+id;
				// TODO: FUNCION APROBAR(OC,ID)
			});
		});

        $(".boton-ver-detalle").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				var numero = $(this).parent().parent().next().html(); // TODO: TRAER NUMERO DE BACK
				$.when(cargar_detalles(id,numero)).done(function(){ // TODO: TRAER FUNCION
					$("#modal-detalles").modal('show'); // TODO: HACER MODAL
				});
			});
		});

        $(".boton-imprimir").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				var urlreporte = 'controller/imprimirodc.php?id='+id;
				window.open(urlreporte,'_blank');
			});
		});

        $('.boton-eliminar').each(function(){
			var id = $(this).attr("data-id");
			var proveedor = $(this).parent().parent().next().next().html();
			var numero = $(this).parent().parent().next().html();
			$(this).on( 'click', function() {
				eliminarOrden(id,numero,proveedor); // TODO: TRAER FUNCION
			});
		});

        $('.boton-rechazar').each(function(){
			var id = $(this).attr("data-id");
			var accion = $(this).attr("data-original-title").split(' ')[0];
			var numero = $(this).parent().parent().next().html();
			var proveedor = $(this).parent().parent().next().next().html();
			$(this).on( 'click', function() {
				rechazarOrden(id,numero,proveedor,accion); // TODO: TRAER FUNCION anularOrden
			});
		});
    });


	// NUEVA COMPRA
	$('#proveedor').on('change',function(){
		var idproveedor = $(this).val();
		$.get("controller/compras/comprasback.php?oper=itemProveedor&idproveedor="+idproveedor, { onlydata:"true" }, function(result){
			$('#items').empty();	
			$('#presentacion').empty();
			$('#costo').val('').trigger('change');
			$('#items').append(result);	
			$('#items').select2({placeholder: ""});
			$('button[data-id="items"]').hide();
		});	
	});

	$('#items').on('change',function(){
		var idproveedor = $('#proveedor').val();
		var iditem = $(this).val();
		var tipo = $(this).select2('data')[0].element.dataset.tipo;
		$.get("controller/compras/comprasback.php?oper=presentacionItem&idproveedor="+idproveedor+"&iditem="+iditem+"&tipo="+tipo, { onlydata:"true" }, function(result){
			$('#presentacion').empty();
			$('#costo').val('').trigger('change');	
			$('#presentacion').append(result);	
			$('#presentacion').select2({placeholder: ""});
			$('button[data-id="presentacion"]').hide();
		});	
	});

	$('#presentacion').on('change',function(){
		var precio = $(this).select2('data')[0].element.dataset.precio;
		$('#costo').val(precio).trigger('change');
	});

	$('#extra').on('change',function(){
		var extra = parseFloat($(this).val());
		$('#totalTXT').text('$. '+(subtotal+extra));
	});


	// AÑADIR 
	$('#anadir').click(function(){
		var itemid = $('#items').val();
		var itemtxt = $('#items').select2('data')[0].text;
		var cantidad = $('#cantidad').val();
		var costo = $('#costo').val();
		var presentacion = $('#presentacion').val();
		var presentaciontxt = $('#presentacion').select2('data')[0].text;
		var total = parseFloat(costo) * parseInt(cantidad);
		var extra = parseFloat($('#extra').val());
		subtotal += total;
		$('#subtotalTXT').text('$. '+subtotal);
		$('#totalTXT').text('$. '+(subtotal+extra));

		if(existe(itemsoc,itemid,presentacion)){
			// error
			swal('Error!','Este item ya se encuentra en la compra','error')
		} else {
			var items = itemsoc;
			itemtxt = itemtxt.replace('"','\\"');
			var nuevafila = '{"itemid": "'+itemid+'","itemtxt": "'+itemtxt+'","costo": "'+costo+'","cantidad": "'+cantidad+'","presentacion": "'+presentacion+'","presentaciontxt": "'+presentaciontxt+'","total": "'+total+'"}';
			if(items != '' && items != '[]'){
				items = nuevafila + ',' + items;
			} else {
				items = nuevafila;
			}
			
			items = items.replace('[','');
			items = items.replace(']','');
			items = '['+items+']';
			itemsoc = items;
			cargarTabla(items);

			//console.log(nuevafila);
			$('#items').val('').trigger('change');
			$('#presentacion').val('').trigger('change');
			$('#costo').val('').trigger('change');
			$('#cantidad').val('').trigger('change');
		}
		
		
	});

	function existe(json,itemid,presentacion){
		resultado = false;
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
		  if(json[i].itemid == itemid && json[i].presentacion == presentacion){ 
			resultado = true;
		  }
		}

		return resultado;
	}


    function cargarTabla(items){
		////console.log('*** cargarTabla ***');
		var html = '<table style="width:100%;" class="table"><tr><th style="width:60%;">Item</th><th style="">Costo</th><th style="">Cantidad</th><th style="">Presentación</th><th style="">Total</th></tr>';
		if(items != ''){
			console.log(itemsoc);
			items_json = JSON.parse(items);
			
			$.map(items_json,function(filas){
				html += '<tr><td class="">'+filas['itemtxt']+'</td><td class=""> $ '+filas['costo']+'</td><td class="">'+filas['cantidad']+'</td><td class="">'+filas['presentaciontxt']+'</td><td class="">$ '+filas['total']+'</td>';
				html += '</tr>';
			});
		}
		
		html += '</table>';
		
		$('#tablaTemporal').empty();
		$('#tablaTemporal').append(html);

		$(".btn-quitar-item").each(function(){
			$(this).on('click',function(){
				var itemid = $(this).attr('data-itemid');
				itemsoc = eliminar_item(itemsoc,itemid);
				cargarTabla(itemsoc);
			});
		});
	}


	// FILTROS
	$("#filtros").on('click',function(){
		$("#btn_filtro").click();
	})
	$("#cerrar_filtros").on('click',function(){
		$("#btn_filtro").click();
	});
});