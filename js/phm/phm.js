$(document).ready(function(){
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	}); 
	$(".hamburger").click();
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) { dd = '0'+dd } 
	if(mm<10) { mm = '0'+mm } 
	today = yyyy + '-' + mm + '-' + dd;
	$("#reload").on('click',function(){
		cargar_info(0,'','');
	});
	/* ARREGLAR OVERLAY Y SCROLL EN MODALES */
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
    function cargar_info(idpaciente,fecha_inicio,fecha_fin){
		$.ajax({
			"url"		:"controller/phm/phmback.php?opcion=PACIENTES_CON_ALERTAS",
			data: { 
				'oper' 		: 'listado_pacientes',
				'idpaciente' : idpaciente,
				'fecha_inicio' : fecha_inicio,
				'fecha_fin' : fecha_fin,
			},
			cache: false,
			dataType: "json",
			method: "GET"
		}).done(function(response){
			tablaPacientes_alerta.clear();
			tablaPacientes_alerta.rows.add(response.data);
			tablaPacientes_alerta.draw();
			grafica_sv(response.grafica);
            $("#totalpaciente_alertas").html(response.data.length);
			tabla_tratamientos_finalizados.ajax.reload(null, false);
			historico_reporte_diario.ajax.reload(null, false);
			tabla_tratamientos_editados.ajax.reload(null, false);
			tabla_planes_editados.ajax.reload(null, false);
			tabla_reposicion_medicamentos.ajax.reload(null, false);
		});
	}
	setTimeout(function(){ 
		setInterval(cargar_info(0,'',''),60000);;		
	}, 60000);
	
    // Pacientes con alertas
    let dataSet = {"data" : {
        'id': '','acciones': '','nombre': '1','condicion': '2','riesgo': '3','fecha': '','idvisita': '','fc': '','rfc': '','fr': '','rfr': '','so': '','rox': '','paa': '','rsi': '','pab': '','rdi': '','tc': '','rtm': '','dolor': '','rdl': '','gc': '','rgc': '','e': '','peso': '','imc': '','rim': '','condicion_cardiaca': '','condicion_respiratoria': '','condicion_oximetria': '','condicion_sistolica': '','condicion_diastolica': '','condicion_temp': '','condicion_dolor': '','condicion_glicemia': ''
    }};
    var tablaPacientes_alerta = $("#tablaPacientes_alerta").DataTable({ 
        "searching": true,
		"lengthChange": false,
		//order: [[13,14,15,16,17]],
		//"paging": false,
		//"ajax"		: data,
        //"scrollY":        "700px",
        data : dataSet,
        "columns"	: [
            { 	"data": "id" },
            { 	"data": "hogar" },
            { 	"data": "nombre" },
            { 	"data": "riesgo" },
            { 	"data": "fecha" },
            { 	"data": "recurso" },
            { 	"data": "fc",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_cardiaca,row.dfc,row.rfc);
                }
            },
            { 	"data": "fr",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_respiratoria,row.dfr,row.rfr);
                } 
            },
            { 	"data": "so",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_oximetria,row.dox,row.rox);
                } 
            },
            { 	"data": "paa",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_sistolica,row.dsi,row.rsi);
                } 
            },
            { 	"data": "pab",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_diastolica,row.ddi,row.rsi);
                } 
            },
            { 	"data": "tc",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_temp,row.dtc,row.rtm);
                } 
            },
            { 	"data": "dolor",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_dolor,row.ddolor,row.rdl); 
                } 
            },
            { 	"data": "gc",
                render: function(data,type,row){
                    return rangos(data,type,row,row.condicion_glicemia,row.dgc,row.rgc); 
                } 
            },
            { 	"data": "condicion",
                render: function(data){
                    if(data == 1){
                        return '<span class="  fa fa-heartbeat green" data-toggle="tooltip" data-original-title="Paciente Estable" data-placement="left" aria-hidden="true"></span>';
                    }
                    else if(data == 2){
                        return '<span class=" fa fa-heartbeat yellow" data-toggle="tooltip" data-original-title="Paciente requiere atención"  data-placement="left" aria-hidden="true"></span>';
                    }
                    else if(data == 3){
                        return '<span class=" fa fa-heartbeat red" data-toggle="tooltip" data-original-title="Paciente requiere atención inmediata"  data-placement="left" aria-hidden="true"></span>';
                    }
                    else {
                        return '<span class=" fa fa-heartbeat blue" data-toggle="tooltip" data-original-title="Valor no registrado"  data-placement="left" aria-hidden="true"></span>';
                    }
                } 
            },
            { 	"data": "imc",
                render: function(data,type,row){
                    return rangos(data,type,row,'',row.rim);
                } 
            },				
            { 	"data": "rfc" },
            { 	"data": "rfr" },
            { 	"data": "rox" },
            { 	"data": "rsi" },
            { 	"data": "rdi" },
            { 	"data": "rtm" },
            { 	"data": "rdl" },
            { 	"data": "rgc" },
            { 	"data": "peso" },
            { 	"data": "rim" },
            { 	"data": "idpaciente",
				render: function(data,type,row){
					return `<div class="dropdown ml-auto text-right">
								<div class="btn-link" data-toggle="dropdown">
									<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
								</div>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="historial.php?idpaciente=`+data+`" >Ver Historial</a>
								</div>
							</div>`;
				}
			},

        ],
		"rowId": 'id',
		"columnDefs": [		
			{
				"targets"	:  [0],
				"width"		:  "10%",
                "visible"   : false
			},{
				"targets"	:  13,
				"width"		:  "10%",
			},{
				"targets"	: [15,16,17,18,19,20,21,22,23,24,25],
				"visible"	: false,
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}	
	});

    $("#tablaPacientes_alerta").on('draw.dt',function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    })

    function rangos(data,type,row,val,val2,resultado){		
		let color = '';
        let simbolo = '';
		let color2 = '';
        let simbolo2 = '';
		if(data != 0 && data != ''){
			switch(val){
				case 'normal':
					color 	= 'green';
					simbolo = 'fa-thumbs-up';
					break;
				case 'alta':
					color 	= 'red';
					simbolo = 'fa-arrow-up';
					break;
				case 'baja':
					color 	= 'red';
					simbolo	= 'fa-arrow-down';
					break;
			}
            switch(val2){				
				case 'alta':
					color2 	= 'red';
					simbolo2 = 'fa-caret-up';
					break;
				case 'baja':
					color2 	= 'red';
					simbolo2	= 'fa-caret-down';
					break;
			}
		}else{
			color 	= 'black';
			simbolo	= 'fa-minus';			
			data = '';
		}
		if(resultado === null || resultado == 'NULL' || resultado === undefined){
			resultado = 'Valor no cargado';
		}
		let img = "<div style='float:left;margin-right:10px;' class='ui-pg-div ui-inline-custom'>";
				img	+= " <i class='"+color+" fa "+simbolo+"' data-toggle='tooltip' data-original-title='"+resultado+"' data-placement='right'></i> "+data;
                img	+= " <i class='"+color2+" fa "+simbolo2+"' data-toggle='tooltip' data-placement='right'></i> ";
				img += '</div>';		
		return img;
	}

    //ORDENAR DATATABLE POR MULTIPLES COLUMNAS
	tablaPacientes_alerta.order( [  [ 17, 'asc' ],[ 18, 'asc' ],[ 19, 'asc' ],[ 20, 'asc' ],[ 21, 'asc' ],[ 22, 'asc' ],[ 23, 'asc' ],[ 24, 'asc' ],[ 25, 'asc' ] ]).draw();
    function grafica_sv(grafica){
		if(grafica !== undefined){	
			for(var i = 0; i <grafica.series.arrFrecuenciaCardiaca.data.length; i++){                
				grafica.series.arrFrecuenciaCardiaca.data[i].y = Number(grafica.series.arrFrecuenciaCardiaca.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrFrecuenciaRespiratoria.data.length; i++){                
				grafica.series.arrFrecuenciaRespiratoria.data[i].y = Number(grafica.series.arrFrecuenciaRespiratoria.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrOximetria.data.length; i++){                
				grafica.series.arrOximetria.data[i].y = Number(grafica.series.arrOximetria.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrSistolica.data.length; i++){                
				grafica.series.arrSistolica.data[i].y = Number(grafica.series.arrSistolica.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrDiastolica.data.length; i++){                
				grafica.series.arrDiastolica.data[i].y = Number(grafica.series.arrDiastolica.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrTemperatura.data.length; i++){                
				grafica.series.arrTemperatura.data[i].y = Number(grafica.series.arrTemperatura.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrDolor.data.length; i++){                
				grafica.series.arrDolor.data[i].y = Number(grafica.series.arrDolor.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrGlicemia.data.length; i++){                
				grafica.series.arrGlicemia.data[i].y = Number(grafica.series.arrGlicemia.data[i].y);
			}
			let longitud=grafica.arrCategorias.length -1;
			var signosvitales = {
				chart: {
					renderTo: "chart", 
					defaultSeriesType: "spline",
					backgroundColor:"rgba(255, 255, 255, 0.0)",
					scrollablePlotArea: {
						minWidth: 3000,
						scrollPositionX: 0
					}
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function() {
									abrir_visita(this.id);
								}
							}
						}
					}
				},
				credits: {
					enabled: false
				},
				title: {
					text: null
				},
				subtitle: {
					text: null
				},
				xAxis: {
					labels: {
						style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
					},
					categories: grafica.arrCategorias,
					range: longitud,
					min : 0,
					max: longitud,  
					tickInterva: 0,
					step: 0,
					pointInterval: 0
				},
				yAxis: {
					title: {
						text: null
					},
					plotLines: [{
						width: 1,
						color: "#808080"
					}],
					max: null
				},
				tooltip: {
					valueDecimals: 1,
					valuePrefix: null,
					valueSuffix: null
				},
				scrollbar: {
					enabled: true,
				},
				legend: {
					enable: true,
					verticalAlign: 'top', 
					// y: 100, 
					align: 'center' 
					
				},
				series: [
					{ 
						data: grafica.series.arrFrecuenciaCardiaca.data,
						name: "Frec. Cardíaca",
						color: "#D50000"
					},{ 
						data: grafica.series.arrFrecuenciaRespiratoria.data,
						name: "Frec. Respiratoria",
						color: "#AA00FF"
					},{ 
						data: grafica.series.arrOximetria.data,
						name: "Sat. Oxigeno",
						color: "#304FFE",
					},{ 
						data: grafica.series.arrSistolica.data,
						name: "Sistólica",
						color: "#2962FF",
					},{ 
						data: grafica.series.arrDiastolica.data,
						name: "Diastólica",
						color: "#00BFA5",
					},{  
						data: grafica.series.arrTemperatura.data,
						name: "Temperatura",
						color: "#00C853",
					},{ 
						data: grafica.series.arrDolor.data,
						name: "Nivel de Dolor",
						color: "#AEEA00",
					},{ 
						data: grafica.series.arrGlicemia.data,
						name: "Glicemia Capilar",
						color: "#FFAB00",
					}
				]
			};
			var chart = new Highcharts.Chart(signosvitales);
			$(".highcharts-background").attr('stroke','');
		}else{
			$("#chart").html('');
		}
	}

    //TRATAMIENTOS POR FINALIZAR 
    var tabla_tratamientos_finalizados = $("#tabla_tratamientos_finalizados").DataTable({
		//"lengthMenu": [3, 4],
		"ajax"		:"controller/phm/phmback.php?opcion=TABLA_TRATAMIENTOS_POR_FINALIZAR",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "nombremedicamento" },
			{ 	"data": "via" },
			{ 	"data": "frecuencia" },
			{ 	"data": "dosis" },
			{ 	"data": "fecha_inicio",
                        render: function(data){
                            return '<strong style="color:green">'+data+'</strong>'
                        } 
            },
			{ 	"data": "fecha_fin",
                        render: function(data){
                            return '<strong style="color:red">'+data+'</strong>'
                        } 
            },
			{ 	"data": "observaciones" },
			{ 	"data": "estado" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID
            {
				"targets"	:  [8,9],
				"width"		:  "10%",
                "visible"   : false
			},
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#totaltfinalizados').text( this.api().data().length )
        }
    });
  
	//TRATAMIENTOS POR REPOSICIÓN 
    var tabla_reposicion_medicamentos = $("#tabla_reposicion_medicamentos").DataTable({
		//"lengthMenu": [3, 4],
        //"scrollY":        "700px",
		"ajax"		:"controller/phm/phmback.php?opcion=TABLA_REPOSICION_MEDICAMENTOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "medicamento" },
			{ 	"data": "proxima_reposicion",
                        render: function(data){
                            return '<strong style="color:red">'+data+'</strong>'
                        } 
            },
			{ 	"data": "contacto" },			
			{ 	"data": "telefono_contacto" },			
			{ 	"data": "comentarios" },			
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID
			{
				"targets"	:  [3,4],
                "visible"   : false
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#med_reposicion').text( this.api().data().length);
        }
    });
	
	var json = []; //Variable para guardar reposición de tratamientos

	$("#tabla_reposicion_medicamentos").on('draw.dt',function(){
		$(".registrar_reposicion").off();
		$(".registrar_reposicion").on('click',function(){
			json = [];
			let idpaciente = $(this).attr('data-idpaciente');
			let idmedicamento = $(this).attr('data-idmedicamento');
			$("#reposicion_idpaciente").val(idpaciente);
			$.get("controller/phm/phmback.php?opcion=TABLA_REPOSICION_MEDICAMENTOS&idpaciente="+idpaciente,function(response){
				if(response.error){
					swal("Error",response.message,"error");
				}else{
					$("#rep_nombre_paciente").html(response.paciente);
					let html = '';
					let option = '<option value="0" seleted>Todos</option>';
					$.map(response.data, function(row){
						let nuevafila = `{
								"registrar" : "0",
								"idmedicamento" : "${row.idmedicamento}",
								"medicamento" : "${row.medicamento}",
								"cantidad": "0", 
								"fecha_estimada": "${row.proxima_reposicion}",
								"fecha_de_reposicion": "${moment().format('YYYY-MM-DD')}",
								"proxima_reposicion": "",
								"comentarios": "${row.comentarios}"
								}`;
								
						let items = JSON.stringify(json);
						if(items === undefined){
							items = '';
						}
						if(items != '' && items != '[]')
							items += ',';
						items = items.replace('[','');
						items = items.replace(']','');
						items += nuevafila;
						items = '['+items+']';
						json = JSON.parse(items);
						option +=`<option value="${row.idmedicamento}">${row.medicamento}</option>`;
						html+=`
							<div class="row_reposicion card"  data-idmedicamento="${row.idmedicamento}">
								<div class="card-body">
									<row style="border-top:1px">
										<div>
											<input class="reposicion_chk" data-idmedicamento="${row.idmedicamento}" type="checkbox" style=""/>
											<h5 style="display:inline">${row.medicamento}</h5>
										</div>
										<div>
											<label><span style="color:red">* </span>Cantidad</label>
											<input class="form-control reposicion_cantidad" data-idmedicamento="${row.idmedicamento}"  type="number" step="1" style="width:100%"/> 
										</div>
										<div>
											<label><span style="color:red">* </span> Fecha de recepción</label>
											<input class="form-control fecha reposicion_fecha" data-idmedicamento="${row.idmedicamento}" value="${moment().format('YYYY-MM-DD')}" type="text" style="width:100%"/> 
										</div>													
										<div>
											<label><span style="color:red">* </span> Fecha de pr&oacute;xima reposici&oacute;n</label>
											<input class="form-control fecha reposicion_prox_fecha" data-idmedicamento="${row.idmedicamento}"  type="text" style="width:100%"/> 
										</div>													
										<div>
											<label>comentarios</label>
											<textarea class="form-control reposicion_comentarios" data-idmedicamento="${row.idmedicamento}"  type="text" style="width:100%; min-height:58px;max-height:58px; min-width:180px;" value="${row.comentarios}"></textarea> 
										</div>									
									</row>
								</div>
							</div>
						`;
					});
					$("#body_reposicion").html(html);
					$("#filtro_reposicion").empty();
					$.when($("#filtro_reposicion").append(option)).done(function(){
						console.log(json);
						$("#filtro_reposicion").on('change',function(){
							if($(this).val() != 0){
								$(".row_reposicion").hide();
								$(".row_reposicion[data-idmedicamento='"+$(this).val()+"']").show();
							}else{
								$(".row_reposicion").show();
							}
						});
						$('.reposicion_fecha').bootstrapMaterialDatePicker({
							weekStart: 0,
							time: false,
						});
						$('.reposicion_prox_fecha').bootstrapMaterialDatePicker({
							weekStart: 0,
							time: false,
							minDate: moment()		
						});
						$(".reposicion_prox_fecha").each(function(){
							$(this).on('change',function(){
								let idmedicamento = $(this).attr('data-idmedicamento');
								let fecha = $(this).val();
								let cantidad = $('.reposicion_cantidad[data-idmedicamento="'+idmedicamento+'"]').val();
								console.log(cantidad);
								console.log(fecha);
								console.log(idmedicamento);
								if(fecha != '' && cantidad!=''){
									$('.reposicion_chk[data-idmedicamento="'+idmedicamento+'"]').prop('checked',true);
									remplazar_valor_reposicion(json,idmedicamento,'registrar',1);
								}
								remplazar_valor_reposicion(json,idmedicamento,'proxima_reposicion',fecha);
							});
						});
						$(".reposicion_cantidad").each(function(){
							$(this).on('change',function(){
								let idmedicamento = $(this).attr('data-idmedicamento');
								let cantidad = $(this).val();
								let fecha = $('.reposicion_prox_fecha[data-idmedicamento="'+idmedicamento+'"]').val();
								console.log(cantidad);
								console.log(fecha);
								console.log(idmedicamento);
								if(fecha != '' && cantidad!=''){
									$('.reposicion_chk[data-idmedicamento="'+idmedicamento+'"]').prop('checked',true);
									remplazar_valor_reposicion(json,idmedicamento,'registrar',1);
								}
								remplazar_valor_reposicion(json,idmedicamento,'cantidad',cantidad);
							});
						});
						$(".reposicion_comentarios").each(function(){
							$(this).on('change',function(){
								let idmedicamento = $(this).attr('data-idmedicamento');
								let comentarios = $(this).val();																		
								remplazar_valor_reposicion(json,idmedicamento,'comentarios',comentarios);
							});
						});
						$(".reposicion_fecha").each(function(){
							$(this).on('change',function(){
								console.log("si");
								let idmedicamento = $(this).attr('data-idmedicamento');
								let fecha_reposicion = $(this).val();																		
								remplazar_valor_reposicion(json,idmedicamento,'fecha_reposicion',fecha_reposicion);
							});
						});
						$(".reposicion_chk").each(function(){
							$(this).on('change',function(){
								console.log("click");
								let idmedicamento = $(this).attr('data-idmedicamento');
								if($(this).is(':checked')){
									console.log("si");
									remplazar_valor_reposicion(json,idmedicamento,'registrar',1);
								}else{
									console.log("no");
									remplazar_valor_reposicion(json,idmedicamento,'registrar',0);
								}
							});
						});

					});
					//$("#filtro_reposicion").select2({'placeholeder':'Seleccione'});
				}
				$("#modal-reposicion-medicamentos").modal('show');
			},'json');
		});
		$(".historico_reposicion").off();
		$(".historico_reposicion").each(function(){
			$(this).on('click',function(){
				let idpaciente = $(this).attr('data-idpaciente');
				let idmedicamento = $(this).attr('data-idmedicamento');
				let paciente = $(this).attr('data-paciente');
				let medicamento = $(this).attr('data-medicamento');
				
				// tabla_historico_rep.ajax.url('controller/phm/phmback.php?opcion=TABLA_HISTORICO_REPOSICION_MEDICAMENTOS&idpaciente='+idpaciente+'&idmedicamento='+idmedicamento).load();
				cargar_historico_reposicion(idpaciente,idmedicamento,paciente,medicamento);
			});
		});

	});

	$("#btn-guardar-reposicion").on('click',function(){
		console.log(json);	
		let data = [];
		for(let i = 0; i < json.length; i++){
			console.log(json['i']);
			if(json[i]['registrar'] == 1){
				if(json[i]['proxima_reposicion'] == ''){
					Swal.fire({
						icon: 'error',
						title: 'ERROR!',
						text: 'Debe asignar una fecha de reposición para el medicamento "'+json[i]['medicamento']+'"'
					})		
					return;
				}else if(json[i]['cantidad'] <=0 || json[i]['cantidad'] == ''){
					Swal.fire({
						icon: 'error',
						title: 'ERROR!',
						text: 'Debe asignar una cantidad de ingreso para el medicamento "'+json[i]['medicamento']+'"'
					});
					return;
				}else{
					data.push(json[i]);
				}
			}
		}		
		console.log(data);
		if(data.length <=0){
			Swal.fire({
				icon: 'error',
				title: 'ERROR!',
				text: 'Debe cargar información de al menos un medicamento para guardar'
			});
			return;
		}else{
			$.ajax({
				type: 'post',
				url: 'controller/phm/phmback.php',
				data: { 
					opcion: 'GUARDAR_REPOSICION_MEDICAMENTOS',
					idpaciente : $("#reposicion_idpaciente").val(),
					contacto : $("#reposicion_contacto").val(),
					tlf_contacto : $("#reposicion_tlf_contacto").val(),
					data : data					
				},
				dataType: 'json',
				beforeSend: function() {
					//$('#overlay').css('display','block');
					document.getElementById("preloader").style.display = "block";
				},
				success: function (response) {
					document.getElementById("preloader").style.display = "none";
					if(response.error){
						console.log(response.message)
						Swal.fire({
							icon: 'error',
							title: 'ERROR!',
							text: 'Ha ocurrido un error al grabar, intente más tarde'				  
						})
					}else{
						Swal.fire('Buen trabajo!','Registro almacenado exitosamente','success');
						$("#modal-reposicion-medicamentos").modal('hide');
						tabla_reposicion_medicamentos.ajax.reload(null, false);
					}
				},
				error: function () {
					Swal.fire({
						icon: 'error',
						title: 'ERROR!',
						text: 'Ha ocurrido un error al grabar, intente más tarde'				  
					})
					
				}
			});
			
		}
	});
	
	function remplazar_valor_reposicion(json,idmedicamento,campo,valor){
		for(let i = 0; i < json.length; i++){
			if(json[i]['idmedicamento'] == idmedicamento){    
				json[i][campo] = valor;
			}
		}		
		console.log(json);
	}
	
    //REPORTE DIARIO 
    var historico_reporte_diario = $("#historico_reporte_diario").DataTable({
		order: [[1,'desc']],
		"ajax"		:"controller/phm/phmback.php?opcion=LISTAR_REPORTES_DIARIOS",
		"columns"	: [	
            { 	"data": "fecha",
                render : function(data){
                    if(data == today){
                        return '<strong style="color:green">'+data+'</strong>'
                    }else{
                        return data;
                    }
                }
            },
            { 	"data": "preparado_por" },		
            { 	"data": "estado",
                render: function(data,row){
                    if(data == 'Enviado'){
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-success">Reporte enviado</span>';
                    }else{
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-danger">RevisadoReporte no enviado</span>';
                    }
                }
            },
            { 	"data": "acciones" }
			],
		rowId: 'id',
		"columnDefs": [
			{
				"targets"	: [ 0,1,2 ],
				"width"		:  "25%"
				
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "MOSTRANDO PAGINA _PAGE_ DE _PAGES_"
		},
		initComplete: function() {
            let rows = this.api().data();
            let found = searchRow(rows,today);
            console.log(today);
            console.log(found);
            if(found[0] !== undefined){
                $("#rep_diario").html('<span style="color:red">* </span>Nuevo')
            }else{
                $("#rep_diario").html('En espera')
            }
            
        }
	});
                
    function searchRow(data,date) {
        return data.filter(
            function(data) {
                return data.fecha == date
            }
        );
    }

    var tabla_tratamientos_editados = $("#tabla_tratamientos_editados").DataTable({
		"ajax"		:"controller/phm/phmback.php?opcion=TABLA_TRATAMIENTOS_EDITADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "fecha_actualizacion_tarjeta" },
			{ 	"data": "estado",
                render: function(data){
                    if(data == 'Tarjeta Enviada'){
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-success">'+data+'</span>';
                    }else{
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-warning">'+data+'</span>';
                    }
                }
            },
			{ 	"data": "creado_por" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#totalteditados').text( this.api().data().length )
        }
	});

	var tabla_planes_editados = $("#tabla_planes_editados").DataTable({
		"ajax"		:"controller/phm/phmback.php?opcion=TABLA_PLAN_EDITADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "fecha_actualizacion" },
			{ 	"data": "estado",
                render: function(data){
                    if(data == 'Plan envido'){
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-success">'+data+'</span>';
                    }else{
                        return '<span style="text-transform: capitalize; cursor: pointer;" class="badge badge-warning">'+data+'</span>';
                    }
                }
            },
			{ 	"data": "creado_por" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#totalPlanEditados').text( this.api().data().length )
        }
	});
	function cargar_historico_reposicion(idpaciente,idmedicamento,paciente,medicamento){
		$.get("controller/phm/phmback.php?opcion=HISTORICO_REPOSICION_MEDICAMENTOS&idpaciente="+idpaciente+"&idmedicamento="+idmedicamento,function(response){
			console.log(response);
			if(response.length > 0){
				$("#hist_rep_nombre_paciente").html(paciente);
				$("#hist_rep_medicamento").html(medicamento);
				$("#modal-historico-reposicion-medicamentos").modal('show');
				let html = '';
				$.map(response, function (row){
					 html = `
						<div class="row_reposicion card">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<label>Fecha de reposicion: 
										<br><strong style="color:green">${row.fecha_reposicion}</strong></label> 
									</div>
									<div class="col-sm-6 col-xs-12">
										<label>Cantidad: 
										<br><strong style="color:green">${row.cantidad}</strong></label> 
									</div>
									<div class="col-sm-6 col-xs-12">
										<label>Fecha estimada: 
										<br><strong>${row.fecha_estimada}</strong></label> 
									</div>
									<div class="col-sm-6 col-xs-12">
										<label>Proxima reposición: 
										<br><strong>${row.proxima_reposicion}</strong></label> 
									</div>
									<div class="col-sm-6 col-xs-12">
									<label>Contacto: 
									<br><strong>${row.contacto}</strong></label> 
									</div>			
									<div class="col-sm-6 col-xs-12">
									<label>Teléfono: 
									<br><strong>${row.telefono_contacto}</strong></label> 
									</div>			
									<div class="col-sm-12 col-xs-12">
										<label>Comentarios: 
										<br><strong>${row.comentarios}</strong></label> 
									</div>	
									<div class="col-sm-12 col-xs-12">
										<label>Colaborador: 
										<br><strong>${row.usuario}</strong></label> 
									</div>			
								</div>
							</div>
						</div>
					`;
				});
				$("#historico_reposicion").html(html);
			}else{
				Swal.fire({
					icon: 'info',
					title: 'Sin información!',
					text: 'No hay datos para mostrar'				  
				})
			}
		},'json');
	}
	
	$(document).on('change', '.select-fecha', function (e) {			
		let cantidadDias = e.target.value
		console.log(cantidadDias)
		let fecha = moment().subtract(cantidadDias, 'days').format("YYYY-MM-DD");
        console.log(moment().subtract(cantidadDias, 'days').format("YYYY-MM-DD"));
		$(".select-fecha").val(cantidadDias);
		cargar_info('',fecha,fecha);
    });
	
	$(document).on('click', '.ver-tarjeta', function (e) {
		
		//$("#modal-ver-tarjeta").modal('show');
		let id = e.target.dataset.id;
		let tarjetasimple = e.target.dataset.simple;
		console.log(id)
		console.log(tarjetasimple)
		/*$.get("controller/phm/phmback.php?opcion=VER_TARJETA&id="+id,function(response){
			console.log(response);
		})*/
		
		
		
		var html = '';
		$.get("controller/phm/phmback.php?opcion=VER_TARJETA&id="+id,function(response){
			var datos = response.tarjeta;
			console.log(response)
			/*$("#nombre").val(datos.paciente).trigger('change');
			$("#cedula").val(datos.cedula).trigger('change');
			$("#edad").val(datos.edad).trigger('change');
			$("#hogar").val(datos.hogar).trigger('change');
			$("#diagnosticos").val(datos.diagnostico).trigger('change');*/
			$.map(response.detalle,function(tratamiento){  
				
				
				
				if(tarjetasimple == "1"){			
					html += `
						<div class="card">			
							<div class="card-body">
								<div class="row">									
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Estado</span>:  ${tratamiento.estado} </div>
									
									<div class="col-sm-12 col-lg-8" style="margin-bottom: 10px;">
										<p class="card-text"><span style="font-weight: bold;">Medicamento:</span> ${tratamiento.medicamento}
										</p>
									</div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha inicio: </span> ${tratamiento.fecha_inicio}</div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha fin: </span> ${tratamiento.fecha_fin}</div>
									
									<div class="col-sm-12" style="margin-bottom: 10px;"><span style="font-weight: bold;">Observaciones: </span> ${tratamiento.observaciones}</div>
									
									<div class="col-sm-6 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Al levantarse: </span> ${tratamiento.al_levantarse}</div>
									<div class="col-sm-6 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Desayuno: </span> ${tratamiento.desayuno}</div>
									<div class="col-sm-6 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Almuerzo: </span> ${tratamiento.almuerzo}</div>
									<div class="col-sm-6 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Cena: </span> ${tratamiento.cena}</div>
									<div class="col-sm-6 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Al acostarse: </span> ${tratamiento.al_acostarse}</div>
									
									
								</div>
							</div>
						</div>
				
					`;
				}else{
					html += `
						<div class="card">											
							<div class="card-body">
								<div class="row">									
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Estado</span>:  ${tratamiento.estado} </div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Hora</span>: ${tratamiento.hora} ${tratamiento.horario}</div>
									
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<p class="card-text"><span style="font-weight: bold;">Medicamento:</span> ${tratamiento.medicamento}
										</p>
									</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Dosis a administrar: </span> ${tratamiento.dosis}</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Vía: </span> ${tratamiento.via}</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Frecuencia: </span> ${tratamiento.frecuencia}</div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha inicio: </span> ${tratamiento.fecha_inicio}</div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha fin: </span> ${tratamiento.fecha_fin}</div>
									<div class="col-sm-12" style="margin-bottom: 10px;"><span style="font-weight: bold;">Observaciones: </span> ${tratamiento.observaciones}</div>
								</div>
								
							</div>
						</div>
					`;
				}
				
				
				
				/*html += `<tr>
							<!--td>`+tratamiento.estado+`</td-->`
				if(tarjetasimple == "0"){		
					html +=	`<td>`+tratamiento.hora+` `+tratamiento.horario+`</td>`
					
					$("#tabla-tarjeta-medicamentos th")[0].style.display = ""
					$("#tabla-tarjeta-medicamentos th")[2].style.display = ""
					
				}else{
					$("#tabla-tarjeta-medicamentos th")[0].style.display = "none"
					$("#tabla-tarjeta-medicamentos th")[2].style.display = "none"
				}
				
				html +=		`<td>`+tratamiento.medicamento+`</td>`
				if(tarjetasimple == "0"){	
					html +=`<td>`+tratamiento.dosis+`</td>
							<td>`+tratamiento.via+`</td>
							<td>`+tratamiento.frecuencia+`</td>`
					
					$("#tabla-tarjeta-medicamentos th")[3].style.display = ""		
					$("#tabla-tarjeta-medicamentos th")[4].style.display = ""	
					
					//$("#tabla-tarjeta-medicamentos th")[4].style.display = ""		
					//$("#tabla-tarjeta-medicamentos th")[5].style.display = ""	
					//$("#tabla-tarjeta-medicamentos th")[6].style.display = ""
					
							
				}else{
					$("#tabla-tarjeta-medicamentos th")[3].style.display = "none"		
					$("#tabla-tarjeta-medicamentos th")[4].style.display = "none"
					
					//$("#tabla-tarjeta-medicamentos th")[4].style.display = "none"		
					//$("#tabla-tarjeta-medicamentos th")[5].style.display = "none"	
					//$("#tabla-tarjeta-medicamentos th")[6].style.display = "none"
				}			
				
				html +=		`<td>`+tratamiento.fecha_inicio+`</td>
							<td>`+tratamiento.fecha_fin+`</td>
							<td style="max-width:306px">`+tratamiento.observaciones+`</td>
						</tr>`;*/
			});
			$("#tarjeta-medicamento-card").html(html);
			$("#modal-ver-tarjeta").modal('show');
		},'json');
		
	});
	
	$(document).on('click', '.ver-plan', function (e) {
		console.log("ver-plan")
		
		let id = e.target.dataset.id
		
		$.get("controller/phm/phmback.php?opcion=VER_PLAN&id="+id,function(response){
			console.log(response)
			var datos = response.datos;
			/*$("#nombre-paciente-plan").val(datos.paciente).trigger('change');
			$("#cedula-paciente-plan").val(datos.cedula).trigger('change');
			$("#fechanac-paciente-plan").val(datos.fechanac).trigger('change');
			$("#hogar-paciente-plan").val(datos.hogar).trigger('change');
			$("#diagnosticos-paciente-plan").val(datos.diagnostico).trigger('change');
			var enfermedadespasadas = datos.diagnostico;
			$.ajax({
				type: 'GET',
				url: 'controller/enfermedadesback.php?oper=cargar_&ids=' + enfermedadespasadas
			}).then(function (data) {
				$.map (JSON.parse(data), function(enfermedad, index){
					var option = new Option(enfermedad.text, enfermedad.id, true, true);
					$("#diagnosticos-paciente-plan").append(option).trigger('change');
					$("#diagnosticos-paciente-plan").trigger({
						type: 'select2:select',
						params: {
							data: data
						}
					});
				});
			});*/
			
			let html = "";
			$.map(response.detalle,function(item){  
			
				/*html += `<tr>
							<td>${item.hora} ${item.horario}</td>
							<td>${item.tipocuidado}</td>
							<td><div readonly disabled rows="8" style="width:100%;">${item.estrategiacuidado.replace(/%%/g,"'")}</div></td>
							<td>${item.frecuencia}</td>
							<td>${item.fecha_inicio}</td>
							<td>${item.fecha_fin}</td>
						</tr>`;*/
						
				html += `
						<div class="card">											
							<div class="card-body">
								<div class="row">									
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Horario</span>:  ${item.hora} ${item.horario} </div>
									<div class="col-sm-12 col-lg-6" style="margin-bottom: 10px;"><span style="font-weight: bold;">Tipo cuidado</span>: ${item.tipocuidado}</div>
									
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<p class="card-text"><span style="font-weight: bold;">Estrategia cuidado:</span> ${item.estrategiacuidado.replace(/%%/g,"'")}
										</p>
									</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Frecuencia:</span> ${item.frecuencia}</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha inicio:</span> ${item.fecha_inicio}</div>
									<div class="col-sm-12 col-lg-4" style="margin-bottom: 10px;"><span style="font-weight: bold;">Fecha fin:</span> ${item.fecha_fin}</div>
								</div>
								
							</div>
						</div>
				
				`;		
						
			});

			$("#plan-cuidado-card").html(html);
			$("#modal-ver-plan").modal('show');
		},'json');
		
		
	});

});