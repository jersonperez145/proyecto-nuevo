$(document).ready(function() {
	var itemsoc ="[]";
	var itemsoc_edit ="[]";

	$('#equipos_, #equipos_editar').selectpicker('refresh');
	//OBTENER COMBO DE EQUIPOS
	$.get("controller/grupos_de_equipos/gruposdeequiposback.php?oper=comboequipos", { onlydata:"true" }, function(result){
		$('#equipos_, #equipos_editar').empty();
		$("#equipos_, #equipos_editar").select2();
		$("#equipos_, #equipos_editar").append(result).trigger("change");	
		$('button[data-id="equipos_editar"], button[data-id="equipos_"]').hide();
	});	


	var tablainsumos = $("#tablaequipos").DataTable({
		"ajax"		:"controller/grupos_de_equipos/gruposdeequiposback.php?oper=cargarkits",
		"order":	[2,'ASC'],
		"sDom": '<"top"f><"float-left"pl>rt<"bottom"ip><"clear">',
		"columns"	: [
			{ 	"data": "id" },
			{ 	"data": "acciones" },
            { 	"data": "nombre",
				render: function(data,type,row){
					let items = '';
					$.map(row.insumos,function(registro){
						items+=`${registro}<br>`;
					})
					return `
					<div id="accordion-ten_${row.id}" class="accordion accordion-header-shadow accordion-rounded">                                    
						<div class="accordion__item_${row.id}">
							<div class="accordion__header collapsed accordion__header--success" data-toggle="collapse" data-target="#header-shadow_collapseThree_${row.id}" style=" box-shadow: none;">
								<span class="text-info accordion__header--text">${data}</span>
							</div>
							<div id="header-shadow_collapseThree_${row.id}" class="collapse accordion__body" data-parent="#accordion-ten_${row.id}">
								<div class="accordion__body--text">
									${items}
								</div>
							</div>
						</div>
					</div>
					`;
				}
        	},
			{ 	"data": "updated_by",
			render: function(data,type,row){
				let items = '';
				$.map(row.insumos,function(registro){
					items+=`${registro}<br>`;
				})
				return `
				${row.updated_at} (${data})
				`;
			}
		    },
			{ 	"data": "updated_at" },
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID
			{
				"targets"	: [ 0,4 ],
				"visible"	: false,
				"searchable": false
			},{
				"targets"	: [3],
				"width"		: "30%"
			},{
				"targets"	: [1],
				"width"		: "10%"
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}
	});

	const data_grupo_vacio = {"data" : {
		"itemid": "",
		"itemid": "",
		"itemtxt": ""
		}
	};


	var tabla_editar_grupo = $("#tabla_editar_grupo").DataTable({
		"searching": true,
		"lengthChange": false,
		data : data_grupo_vacio,
		"columns"	: [
			{ 	"data": "itemid" },
			{ 	"data": "itemid",
				render: function(data,type,row){
					return `
						<button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item_editar" data-itemid="${data}" data-tipo="${row.tipo}">
						<span class="fa fa-minus" aria-hidden="true"></span>
						<div class="ripple-container"></div>
						</button>
					`;
				}
			},
			{ 	"data": "itemtxt" }
		],
		"columnDefs": [ //OCULTAR LA COLUMNA ID
			{
				"targets"	: [ 0 ],
				"visible"	: false,
				"searchable": false
			},
			{
				"targets"	: [ 1,2 ],
				"width": "5%"
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}	
	});


	
	function editarkit(id){
		tabla_editar_grupo.clear();
		// CARGAR DATOS
		$.get('controller/grupos_de_equipos/gruposdeequiposback.php?oper=getkit',{
			id: id
		},function(datos){
			var nombre = datos.nombre;
			var items = '';
			
			$('#nombre_editar').val(nombre).trigger('change');
			$.map(datos.equipos,function(items_){
				itemtxt = items_.nombre.replace('"','\\"');
				var nuevafila = '{"itemid": "'+items_.id+'","itemtxt": "'+itemtxt+'"}';
				if(items != '' && items != '[]')
				items += ',';
				items += nuevafila;
				items = items.replace('[','');
				items = items.replace(']','');
				items = '['+items+']'
			});
			
			itemsoc_edit = items;
			cargarTablaEditar(itemsoc_edit);
			$('#editar-grupo-equipos').modal('show');
			localStorage.setItem('idkiteditar',id);
		},'json');
	}

	var filaid = 0;
	
	function eliminarkit(id,nombre){
		var id = id;
		swal({
			title: "Confirmar",
			text: "¿Esta seguro de eliminar el kit "+nombre+"?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: {
				cancel: "No.",
				catch: {
				  text: "Sí, eliminar",
				  value: "eliminar",
				  className: "btn-danger"
				}
			  },
			})
			.then((value) => {
				if (value == 'eliminar') {
					$.get( "controller/grupos_de_equipos/gruposdeequiposback.php?oper=deletekit", 
					{ 
						onlydata : "true",
						id : id
					}, function(result){
						if(result == 1){
							swal('Buen trabajo!','Grupo eliminado satisfactoriamente','success');		
							// RECARGAR TABLA Y SEGUIR EN LA MISMA PAGINA (2do parametro)
							tablainsumos.ajax.reload(null, false);
						} else {
							swal('ERROR!','Ha ocurrido un error al eliminar el grupo, intente más tarde','error');
						}
					});
				} 
			}
		);
	}	
	
	function duplicar(id,nombre){
		var id = id;
		swal({
			title: "Confirmar",
			text: "¿Esta seguro de duplicar el kit "+nombre+"?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: {
				cancel: "No.",
				catch: {
				  text: "Sí, duplicar",
				  value: "duplicar",
				  className: "btn-success"
				}
			  },
			})
			.then((value) => {
				if (value == 'duplicar') {
					$.get( "controller/grupos_de_equipos/gruposdeequiposback.php?oper=duplicar", 
					{ 
						onlydata : "true",
						id : id
					}, function(result){
						if(result == 1){
							swal('Buen trabajo!','Grupo duplicado satisfactoriamente','success');		
							// RECARGAR TABLA Y SEGUIR EN LA MISMA PAGINA (2do parametro)
							tablainsumos.ajax.reload(null, false);
						} else {
							swal('ERROR!','Ha ocurrido un error al duplicar el grupo, intente más tarde','error');
						}
					});
				} 
			}
		);
	}
	
	
	
	// AL CARGARSE LA TABLA
	$('#tablaequipos').on( 'draw.dt', function () {
		$('.boton-editar').off();
		$('.boton-eliminar').off();
		$('.boton-duplicar').off();


		// DAR FUNCIONALIDAD AL BOTON EDITAR
        $('.boton-editar').on('click',function(){
			var id = $(this).attr("data-id");
			editarkit(id);
		});
		
		// DAR FUNCIONALIDAD AL BOTON ELIMINAR
        $('.boton-eliminar').on('click',function(){
			var id = $(this).attr("data-id");
			var nombre = $(this).attr("data-nombre");
			eliminarkit(id,nombre);
		});
		
		$('.boton-duplicar').on('click',function(){
			var id = $(this).attr("data-id");
			var nombre = $(this).attr("data-nombre");
			duplicar(id,nombre);
		});
		
		// TOOLTIPS
		$('[data-toggle="tooltip"]').tooltip(); 

		
    });
	

	
	// AÑADIR 
	$('#anadir').click(function(){
		var itemid = $('#equipos_').val();
		var itemtxt = $('#equipos_').select2('data')[0].text;

		if(existe(itemsoc,itemid)){
			// error
			swal('Error!','Este equipo ya se encuentra en el grupo','error')
		} else {
			var items = itemsoc;
			itemtxt = itemtxt.replace('"','\\"');
			var nuevafila = '{"itemid": "'+itemid+'","itemtxt": "'+itemtxt+'"}';
			if(items != '' && items != '[]'){
				//items += ',';
				items = nuevafila + ',' + items;
			} else {
				items = nuevafila;
			}
			
			items = items.replace('[','');
			items = items.replace(']','');
			items = '['+items+']';
			itemsoc = items;
			cargarTabla(items);

			console.log(nuevafila);
			$('#equipos_').val('').trigger('change');
		}
		
		
	});
 
	// AÑADIR EDITAR
	$('#anadir_editar').click(function(){
		var itemid = $('#equipos_editar').val();
		var itemtxt = $('#equipos_editar').select2('data')[0].text;
		
		if(existe(itemsoc_edit,itemid)){
			// error
			swal('Error!','Este equipo ya se encuentra en el grupo','error')
		} else {
			var items = itemsoc_edit;
			itemtxt = itemtxt.replace('"','\\"');
			var nuevafila = '{"itemid": "'+itemid+'","itemtxt": "'+itemtxt+'"}';
			if(items != '' && items != '[]'){
				//items += ',';
				items = nuevafila + ',' + items;
			} else {
				items = nuevafila;
			}
			items = items.replace('[','');
			items = items.replace(']','');
			items = '['+items+']';
			itemsoc_edit = items;
			cargarTablaEditar(items);

			$('#equipos_editar').val('').trigger('change');
		}
	});
	
	
	function cargarTabla(itemsoc_){
		////console.log('*** cargarTabla ***');
		var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:90%;">Item</th></tr>';
		if(itemsoc_ != ''){
			//console.log(itemsoc);
			items_json = JSON.parse(itemsoc_);
			
			$.map(items_json,function(filas){
				html += '<tr><td class="centrar pb-2"><button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item" data-itemid="'+filas['itemid']+'">\
								<span class="fa fa-minus"></span>\
								<div class="ripple-container"></div> \
							</button> \
						</td><td class="">'+filas['itemtxt']+'</td>';
				html += '</tr>';
			});
		}
		
		html += '</table>';
		
		$('#tablaTemporal').empty();
		$('#tablaTemporal').append(html);

		$(".btn-quitar-item").each(function(){
			$(this).on('click',function(){
				var itemid = $(this).attr('data-itemid');
				itemsoc = eliminar_item(itemsoc,itemid);
				cargarTabla(itemsoc);
			});
		});
	}

	
	
	function cargarTablaEditar(itemsoc_edit_){
		////console.log('*** cargarTabla ***');
		var html = '<table  style="width:100%;"><tr><th style="width:160px;">Quitar</th><th style="width:90%;">Item</th></tr>';
		if(itemsoc_edit_ != ''){
			//console.log(itemsoc_edit_);
			items_json = JSON.parse(itemsoc_edit_);
			
			$.map(items_json,function(filas){
				html += '<tr><td class="centrar pb-2"><button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item_editar" data-itemid="'+filas['itemid']+'">\
								<span class="fa fa-minus"></span>\
								<div class="ripple-container"></div> \
							</button> \
						</td><td class="">'+filas['itemtxt']+'</td>';
				html += '</tr>';
			});
		}
		
		html += '</table>';
		
		$('#tabla_editar_grupo').empty();
		$('#tabla_editar_grupo').append(html);

		$(".btn-quitar-item_editar").each(function(){
			$(this).on('click',function(){
				var itemid = $(this).attr('data-itemid');
				itemsoc_edit = eliminar_item(itemsoc_edit,itemid);
				cargarTablaEditar(itemsoc_edit);
			});
		});
	}
	
	function eliminar_item(json,item){
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
		  if(json[i].itemid == item){ 
			json.splice(i, 1);
		  }
		}
		return JSON.stringify(json);
	}

	function existe(json,itemid){
		resultado = false;
		json = JSON.parse(json);
		for(var i = 0; i < json.length; i++){
		  if(json[i].itemid == itemid){ 
			resultado = true;
		  }
		}

		return resultado;
	}
	
	
	$("#boton-guardar").on("click",function(){
		guardarGrupo();
	});
	
	$("#boton-guardar-editar").on("click",function(){
		guardarEditarGrupo();
	});

	function guardarGrupo(){
		var nombre 	= $("#nombre").val();
		var equipos = JSON.parse(itemsoc);

		$.ajax({
			type: 'post',
			url: 'controller/grupos_de_equipos/gruposdeequiposback.php',
			data: { 
				'oper'			: 'create',
				'insumos' 		: equipos,
				'nombre' 		: nombre
			},
			beforeSend: function() {
				$("#overlay").show();
			},
			success: function (response) {
				$("#overlay").hide();
				swal('Buen trabajo!','Grupo guardado satisfactoriamente','success').then(function(){
					$('.nuevo-grupo-equipos').modal('hide');
					tablainsumos.ajax.reload(null, false);

					// BORRAR DATOS
					$('#nombre').val('').trigger('change');
					$('#equipos_').val('').trigger('change');
					itemsoc = "[]";
					cargarTabla(itemsoc);
				});	
			},
			error: function () {
				swal('ERROR!','Ha ocurrido un error al guardar el grupo, intente más tarde','error-message');
			}
		});			
	}

	function guardarEditarGrupo(){
		var id 		= localStorage.getItem('idkiteditar');
		var nombre  = $('#nombre_editar').val();
		var equipos = JSON.parse(itemsoc_edit);
		
		//console.log(equipos);
		$.ajax({
			type: 'post',
			url: 'controller/grupos_de_equipos/gruposdeequiposback.php',
			data: { 
				'oper'		: 'updatekit',
				'id'		: id,
				'nombre' 	: nombre,
				'insumos' 	: equipos
			},
			beforeSend: function() {
				$("#overlay").show();
			},
			success: function (response) {
				$("#overlay").hide();
				swal('Buen trabajo!','Grupo actualizado satisfactoriamente','success').then(function(){
					$('#editar-grupo-equipos').modal('hide');
					tablainsumos.ajax.reload(null, false);

					// BORRAR DATOS
					$('#nombre_editar').val('').trigger('change');
					$('#equipos_editar').val('').trigger('change');
					itemsoc_edit = "[]";
					cargarTablaEditar(itemsoc_edit);
				});	
			},
			error: function () {
				swal('ERROR!','Ha ocurrido un error al guardar el grupo, intente más tarde','error-message');
			}
		});			

	}
});