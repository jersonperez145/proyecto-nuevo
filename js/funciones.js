// $(document).ready(function() {
//     $(".hamburger").click(); // ocultar menú automaticamente
// });
// var dropdownMenu;
// $(window).on('show.bs.dropdown', function(e) {
//   dropdownMenu = $(e.target).find('.dropdown-menu.droptable');
//   $('body').append(dropdownMenu.detach());
//   dropdownMenu.css('display', 'block');
//   dropdownMenu.position({
//     'my': 'right top',
//     'at': 'right bottom',
//     'of': $(e.relatedTarget)
//   })
// });
// $(window).on('hide.bs.dropdown', function(e) {
//   $(e.target).append(dropdownMenu.detach());
//   dropdownMenu.hide();
// });

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
  
function setCookie(cName, cValue, expDays = 1) {
    let date = new Date();
    date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + date.toUTCString();
    document.cookie = cName + "=" + cValue + "; " + expires + "; path=/";
}

function getFechaActual(){	
	let f=new Date();
	let fecha = f.getFullYear()+'-'+(f.getMonth()+1)+'-'+f.getDate()+' '+f.getHours()+":"+(f.getMinutes()<10?'0':'') + f.getMinutes() +":"+f.getSeconds();		
	return fecha;
} 

//funciones para reemplazar labels y textos 
function impuesto(){
    let impuesto = getCookie('impuesto');
    $(".impuesto").each(function(){
        $(this).html(decodeURIComponent(impuesto));
    })
}	
function chk_impuesto(){
    let chkimpuesto = getCookie('chk_impuesto');
    if(chkimpuesto == 0){
        $(".chk_impuesto").each(function(){
            $(this).attr('checked',false);
            $(this).prop( "disabled", true );
        })
    }else if(chkimpuesto == 1){
        $(".chk_impuesto").each(function(){
            $(this).attr('checked', 'checked'); 
            $(this).prop( "disabled", true );
        })
    }else{
        $(".chk_impuesto").each(function(){
            $(this).attr('checked', 'checked'); 				
        })
    }
}
function moneda(){
    let signo_moneda = getCookie('moneda');
    $(".moneda").each(function(){
        $(this).html(decodeURIComponent(signo_moneda));
    })
}
function doc_identificacion(){
    let doc_identificacion = getCookie('doc_identificacion');
    $(".doc_identificacion").each(function(){
        $(this).html(unescape(doc_identificacion));
    })
}
function tipo_identificacion(){
    let tipo_identificacion = getCookie('tipo_identificacion');
    $(".tipo_identificacion").each(function(){
        $(this).html(decodeURIComponent(tipo_identificacion));
    })
}	


function capitalizarPrimeraLetra(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  