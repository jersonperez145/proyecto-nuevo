	// TOOLTIPS
	
$(document).ready(function(){
  //$('[data-toggle="tooltip"]').tooltip(); 
  
   /* ARREGLAR OVERLAY Y SCROLL EN MODALES */
  $(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
  });
  
  document.getElementById('acuse-info-fecha').value= moment().format("YYYY-MM-DD");
   $('#acuse-info-fecha').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
		minDate: moment()		
    });
	
	//$("#acuse-info-colaborador").select2();
	//$("#acuse-info-estatus").select2();
	//$("#acuse-info-inventario").select2();
	//$("#acuse-info-item").select2();
	
	$.get("controller/logisticaback.php?opcion=tableros", function(response){
		let data = response.data;
		document.getElementById("solicitudes-rechazadas").textContent = data.rechazadas;
		document.getElementById("solicitudes-pendientes").textContent = data.pendientes;
		document.getElementById("solicitudes-acusados").textContent = data.acusados;	
		document.getElementById("solicitudes-enviados").textContent = data.enviados;	
	}, 'json');
	
	$.get("controller/logisticaback.php?opcion=colaboradores", { onlydata:"true" }, function(result){
		var userid = getCookie('user_id');
		console.log(userid)
		$('#acuse-info-colaborador').empty();  
		$('#acuse-info-colaborador').append(result); 
		//$('#acuse-info-colaborador').select2({placeholder: ""});
		$('#acuse-info-colaborador').val(userid).trigger('change');
    });
	
	$.get("controller/logisticaback.php?opcion=estatus_acuse", { onlydata:"true" }, function(result){
		$('#acuse-info-estatus').empty();  
		$('#acuse-info-estatus').append(result); 
		$("#acuse-info-estatus").val("44").trigger('change');
		//$('#acuse-info-estatus, #estatus_acuse_editar').select2({placeholder: ""});
	});
	
	$.get("controller/logisticaback.php?opcion=bodegas", { onlydata:"true" }, function(result){
		//$('#acuse-info-inventario').select2({'placeholder':'Seleccione'});
		$('#acuse-info-inventario').append(result);
		$('#acuse-info-inventario').trigger('change');
	});	
	
	$('#acuse-info-inventario').on('change',function(){
		console.log("acuse-info-inventario change")
		var tipoinventario= $(this).val();
		var item =  $('#acuse-info-item').val()
		$.get("controller/logisticaback.php?opcion=listar_items&tipoinventario="+tipoinventario, { onlydata:"true" }, function(result){
		  $('#acuse-info-item').empty();
		  $('#acuse-info-item').append(result);
		  //$('#acuse-info-item').select2({placeholder: ""});
		  $('#acuse-info-item').val(item).trigger('change');
		});   
		$("#acuse-info-cantidad").val('');
  });
  
   $('#acuse-info-item').on('change',function(){
    var cantidad_disponible = $('#acuse-info-item').select2('data')[0].element.dataset.cantidad;
    var costo_unitario = $('#acuse-info-item').select2('data')[0].element.dataset.costounitario;
	var ubicacion = $('#acuse-info-item').select2('data')[0].element.dataset.ubicacion;
    $("#acuse-info-cantidad").val(cantidad_disponible);
    //$("#costo_unitario").val(costo_unitario);
    $("#acuse-info-ubicacion").val(ubicacion);
	
  });
  
  $.get("controller/logisticaback.php?opcion=insumoskit", { onlydata:"true" }, function(result){
    $('#select-plantilla').empty();  
    $('#select-plantilla').append(result); 
	$('#select-plantilla').val(0);
    //$('#select-plantilla').select2({placeholder: ""});
  });
  
	setInterval(function(){
		$('#tabla_logistica').DataTable().ajax.reload(); 
		$('#tabla_logistica_item').DataTable().ajax.reload(); 
		$('#tabla_logistica_1').DataTable().ajax.reload(); 
		$('#tabla_logistica_2').DataTable().ajax.reload(); 
		$('#tabla_kitinsumos').DataTable().ajax.reload(); 
	}, 60000);
   var tabla_logistica = $("#tabla_logistica").DataTable({"scrollX": true,        
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/logisticaback.php?opcion=solicitud_enfermeria",
        "columns"	: [
			{ 	"data": "id" },
			{ 	"data": "detalles" },
			{ 	"data": "cedula" },
			{ 	"data": "nombrepaciente" },
			{ 	"data": "nombresolicitante" },
			{ 	"data": "observacion" },
			{ 	"data": "nombreprioridad" },
            { 	"data": "fechasolicitud" },
			{  "data": "estado"	},           
            { 	"data": "acciones" }		
        ],
        rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
			{
                "targets"	: [ 0 ],
                "visible"	: false,
                "searchable": false
            },
            {
    			//targets		: [6],
    			//className	: "text-center"
    		}
        ],
		"order": [[0, "asc"]],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
		initComplete: function() {
            //$('#totalteditados').text( this.api().data().length )
        }
    });
	
	

	var tabla_logistica_item = $("#tabla_logistica_item").DataTable({"scrollX": true,        
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/logisticaback.php?opcion=detalle_solicitud&idsolicitud=0",
        "columns"	: [
			//{ 	"data": "itemid" },
			{ 	"data": "codigo" },
            { 	"data": "itemtxt" },
            { 	"data": "tipotxt" },
			{ 	"data": "cantidadsolicitar" }
        ],
        rowId: 'itemid', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
			{
                "targets"	: [ 0,1,3, ],
                //"width"		:  "40%"
            }
        ],
		"order": [[0, "desc"]],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
		initComplete: function() {			  
			  tabla_logistica_item.columns.adjust().draw();
		}
    });
	
	var tabla_logistica_1 = $("#tabla_logistica_1").DataTable({"scrollX": true,        
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/logisticaback.php?opcion=solicitud_enfermeria_acusado",
        "columns"	: [
			{ 	"data": "id" },
			{ 	"data": "detalles" },
			{ 	"data": "cedula" },
			{ 	"data": "nombrepaciente" },
			{ 	"data": "nombresolicitante" },
			{ 	"data": "observacion" },
			{ 	"data": "nombreprioridad" },
            { 	"data": "fechasolicitud" },
			{  "data": "estado"	},           
            { 	"data": "acciones" }		
        ],
        rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
			{
                "targets"	: [ 0 ],
                "visible"	: false,
                "searchable": false
            },
            {
    			//targets		: [6],
    			//className	: "text-center"
    		}
        ],
		"order": [[0, "asc"]],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
		initComplete: function() {
            //$('#totalteditados').text( this.api().data().length )
        }
    });
	
	var tabla_logistica_2 = $("#tabla_logistica_2").DataTable({"scrollX": true,        
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/logisticaback.php?opcion=solicitud_enfermeria_enviado",
        "columns"	: [
			{ 	"data": "id" },
			{ 	"data": "detalles" },
			{ 	"data": "cedula" },
			{ 	"data": "nombrepaciente" },
			{ 	"data": "nombresolicitante" },
			{ 	"data": "observacion" },
			{ 	"data": "nombreprioridad" },
            { 	"data": "fechasolicitud" },
			{  "data": "estado"	},           
            //{ 	"data": "acciones" }		
        ],
        rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
			{
                "targets"	: [ 0 ],
                "visible"	: false,
                "searchable": false
            },
            {
    			//targets		: [6],
    			//className	: "text-center"
    		}
        ],
		"order": [[0, "asc"]],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
		initComplete: function() {
            //$('#totalteditados').text( this.api().data().length )
        }
    });
	
	var tabla_kitinsumos = $("#tabla_kitinsumos").DataTable({"scrollX": true,
		order: [[1,'desc']],
		responsive: true,
		colReorder: false,
		searching: false, 
		ordering: false,
		bAutoWidth: false,    
		"ajax"    :"controller/logisticaback.php?opcion=insumoskittabla&id="+0,
		"columns" : [
		  {"data": "acciones" },
		  {"data": "id" },
		  {"data": "tipo" },
		  {"data": "codigo" },
		  {"data": "descripcion" },
		  {"data": "cantidad" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID
		  {
			"targets" : [ 0 ],
			"width" : "5%",
			"searchable": false,
			"visible": false
		  }
		],
		"language": {
		  "url": "js/Spanish.json",
		  "info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            tabla_kitinsumos.columns.adjust().draw();
        }
	  });
	


	$("#btn-aceptar-plantilla").on('click',function(){
		console.log("funcionando")
		var id =  $("#select-plantilla").val();
		$.get("controller/logisticaback.php?opcion=get_items_plantilla", { id:id }, function(result){
		  if(itemsoc != '[]'){
			var items_todos =  JSON.parse(itemsoc);
		  }else {
			var items_todos = {};
		  }       
		  
		  // CARGAR DETALLES
			$.map(result.detalle,function(detalle){      
				var nuevafila = '{"cantidad": "'+detalle.cantidad+'", \
						  "cantidad_facturada": "0", \
						  "codigo": "'+detalle.codigo+'",\
						  "costo_unitario": "'+detalle.costounitario+'",\
						  "fechapic": "",\
						  "inventario": "1",\
						  "facturable": "1",\
						  "itemid": "'+detalle.itemid+'",\
						  "ubicacion": "'+detalle.ubicacion+'",\
						  "itemtxt": "'+detalle.itemtxt.replace(/"/g,'\\"')+'",\
						  "pic": "",\
						  "tipo": "'+detalle.tipo+'",\
							"tipotxt": "'+detalle.tipotxt+'",\
						  "cantidad_entregada":"0"\
						}';
				var items = JSON.stringify(items_todos.NUEVO); //acuse nuevo
				console.log(items);
				if(items === undefined){
					if($("#idacuse").val() != ''){		
						items = JSON.stringify(items_todos);  //acuse editar
						var nuevafila = '{\
							  "id" : "id",\
							  "cantidad": "'+detalle.cantidad+'", \
							  "cantidad_facturada": "0", \
							  "codigo": "'+detalle.codigo+'",\
							  "costo_unitario": "'+detalle.costounitario+'",\
							  "fechapic": "",\
							  "inventario": "1",\
							  "facturable": "1",\
							  "itemid": "'+detalle.itemid+'",\
							  "ubicacion": "'+detalle.ubicacion+'",\
							  "itemtxt": "'+detalle.itemtxt.replace(/"/g,'\\"')+'",\
							  "pic": "",\
							  "tipo": "'+detalle.tipo+'",\
								"tipotxt": "'+detalle.tipotxt+'",\
							  "cantidad_entregada":"0"\
							}';
					}
					if(items === undefined || items == '{}'){
						items = '';
					}
				}
				if(items != '' && items != '[]'){
					items += ',';
				}
				items = items.replace('[','');
				items = items.replace(']','');
				items += nuevafila;
				items = '['+items+']';
				//if($("#idacuse").val() != ''){ // editar acuse
				//	items_todos= JSON.parse(items)
				//}else{ //acuse nuevo
					items_todos.NUEVO= JSON.parse(items);
				//}
			}); 
		  
		  // CARGAR EQUIPOS
		  var j = 0;
		  html = '<table>';
		  $.map(result.grupos,function(grupos,i){
			j++;
			var nombregrupo = i;
			html += '<tr><th colspan="2">'+nombregrupo+'</th></tr>';
			$.map(grupos,function(grupo){
			  if(grupo.paciente != ''){
				html += '<tr><td><input disabled="disabled" type="radio" name="'+nombregrupo+'" value="'+grupo.itemid+'" data-ubicacion="'+grupo.ubicacion+'" data-itemtxt="'+grupo.codigo+' | '+grupo.itemtxt+'"/></td><td>'+grupo.codigo+' | '+grupo.itemtxt+' ('+grupo.paciente+')</td></tr>';          
			  } else {
				html += '<tr><td><input type="radio" name="'+nombregrupo+'" value="'+grupo.itemid+'" data-ubicacion="'+grupo.ubicacion+'" data-itemtxt="'+grupo.codigo+' | '+grupo.itemtxt+'"/></td><td><strong style="color:#007eff;">'+grupo.codigo+' | '+grupo.itemtxt+'</strong></td></tr>';         
			  }
			});
			
		  });
		  html += '</table>';
		  
		  if(j>0){
			$('#equiposGrupos').empty();
			$('#equiposGrupos').append(html);   
			$('#modal-equipos').modal('show');
		  }
		  
			itemsoc = JSON.stringify(items_todos);
			//if($("#idacuse").val() != ''){ // editar acuse
			//	cargarTablaAcusesEditar(itemsoc);				
			//}else{ //acuse nuevo			
				cargarTablaOC(itemsoc);				
			//}
		  $("#modal-plantilla").modal("hide");
		  
		},'json');
	  });




	
	//$("#select-plantilla").on('change',function(){
	$(document).on('change', '#select-plantilla', (e)=>{	
		var id = e.target.value;
		tabla_kitinsumos.clear().ajax.url("controller/logisticaback.php?opcion=insumoskittabla&id="+id).load();
	});  
	
	$(document).on('click', '.boton-detalle-solicitud', (e)=>{
		e.preventDefault()
		console.log(e.currentTarget.dataset.id)
		let idItem = e.currentTarget.dataset.id;
		tabla_logistica_item.ajax.url( 'controller/logisticaback.php?opcion=detalle_solicitud&idsolicitud='+ idItem ).load();
		$("#bd-example-modal-lg").modal('show')
	});
	
	$(document).on('click', '.boton-info-enfermera', (e)=>{
		
		e.preventDefault()
		console.log(e.currentTarget.dataset.usuario)
		let idUsuario = e.currentTarget.dataset.usuario;
		
		$.ajax({
     		type: 'post',
			url: "controller/logisticaback.php?opcion=detalle_enfermero&idusuario="+idUsuario,
			dataType: 'json',
            success: function(response) {                
				let data = response.data;
				document.getElementById("nombre-detalle-enfermero").textContent = data.nombre;
				document.getElementById("nivel-detalle-enfermero").textContent = data.nombrenivel;
				document.getElementById("direccion-detalle-enfermero").textContent = data.direccion;
				document.getElementById("telefono-detalle-enfermero").textContent = data.telefono;
				document.getElementById("email-detalle-enfermero").textContent = data.correo;
				document.getElementById("sexo-detalle-enfermero").textContent = data.sexo;
				
		   }
	    });
		
		$("#detalle-enfermera-modal-lg").modal('show')
		
	});
	
	$(document).on('click', '.boton-info-paciente', (e)=>{
		
		e.preventDefault()
		console.log(e.currentTarget.dataset.paciente)
		let idPaciente = e.currentTarget.dataset.paciente;
		
		$.ajax({
     		type: 'post',
			url: "controller/logisticaback.php?opcion=detalle_paciente&idpaciente="+idPaciente,
			dataType: 'json',
            success: function(response) {                
				let data = response.data;
				document.getElementById("nombre-detalle-paciente").textContent = data.nombre;
				document.getElementById("direccion-detalle-paciente").textContent = data.direccion;
				document.getElementById("comentario-direccion-detalle-paciente").textContent = data.comentariosimportantes;
				document.getElementById("telefono-detalle-paciente").textContent = data.celular;
				document.getElementById("email-detalle-paciente").textContent = data.email;
				//document.getElementById("sexo-detalle-enfermero").textContent = data.sexo;
				
		   }
	    });
		
		$("#detalle-paciente-modal-lg").modal('show')
		
	});
	
	
	
	$(document).on('click', '.boton-acuse', (e)=>{	
		e.preventDefault()
		console.log(e.currentTarget.dataset.id)
		console.log(e.currentTarget.dataset.paciente)
		let idPaciente = e.currentTarget.dataset.paciente;
		let idSolicitud = e.currentTarget.dataset.id;
		
		
		$.ajax({
     		type: 'post',
			url: "controller/logisticaback.php?opcion=detalle_paciente&idpaciente="+idPaciente,
			dataType: 'json',
            success: function(response) {                
				let data = response.data;
				$("#idpaciente_nuevo").val(idPaciente);
				$("#idsolicitud_nuevo").val(idSolicitud);
				document.getElementById("acuse-nombre-paciente").textContent = data.nombre;
				document.getElementById("acuse-cedula-paciente").textContent = data.cedula;
				
		   }
	    });
		
		let nuevos = {}; 
		let solicitudes = []; 
		$.get("controller/logisticaback.php?opcion=get_itemsfacturados",{'idpaciente':idPaciente},function (response){
					
			
			$.get("controller/logisticaback.php?opcion=detalle_solicitud&idsolicitud="+ idSolicitud, function(result){
				
				let items = result.data;
				
				items.map((item) => {
					
					let solicitud =  {"tipo": item.tipo, "tipotxt":item.tipotxt, "codigo": item.codigo, "itemid": item.itemid, "itemtxt": item.itemtxt, "costo_unitario": item.costo_unitario, "ubicacion": item.ubicaciontxt, "cantidad": item.cantidadsolicitar, "cantidad_facturada": "0", "fechapic": "", "inventario": "1", "pic": "", "facturable": "1", "cantidad_entregada":"0" };
					
					solicitudes = [...solicitudes, solicitud]
					
				})
				
				nuevos	= {"NUEVO" : solicitudes}
				console.log(nuevos)
				
				
				let todos = {...response, ...nuevos}
			
				//console.log(todos)
				//console.log(JSON.stringify(todos))
				//console.log(JSON.stringify(response))
				
				//itemsoc = JSON.stringify(response);
				itemsoc = JSON.stringify(todos);
				console.log(itemsoc)
				items_originales  =  itemsoc;
				cargarTablaOC(itemsoc);
				
			}, 'json');			
		  
		},'json');
		
		$("#acuse-modal-lg").modal('show')
		
	});
	
	$('#anadir').click(function(){
		//var tipo = $('#select-item').select2('data')[0].element.dataset.tipo;
		//var tipo = $('#select-item').select2('data')[0].element.dataset.tipo;
		var tipo = $('#acuse-info-item').find(':selected').data('tipo');
		var costo_unitario = $('#acuse-info-item').find(':selected').data('costo_unitario');
		
		//var itemid = $('#select-item').val();
		var itemid = $('#acuse-info-item').val();
		
		//var cantidad = $('#cantidad_anadir').val();
		var cantidad = $('#acuse-info-cantidad-asignar').val();
		
		//var costo_unitario  = $("#costo_unitario").val();
		
		//var tipoinventario = $("#select-inventario").val();
		var tipoinventario = $("#acuse-info-inventario").val()
		if(tipoinventario== 0){
			//swal('Error!','Debe seleccionar un inventario','error');
			Swal.fire({
			  icon: 'error',
			  title: 'ERROR!',
			  text: 'Debe seleccionar un inventario'				  
			})
			
			return;
		}
		if(itemsoc != '[]'){
		  var items_todos =  JSON.parse(itemsoc);
		}else {
		  var items_todos = {};
		}
		if(buscar_valor(itemsoc,'NUEVO',tipo,itemid) == 0){
		  if (validar_agregar_item(itemid,cantidad) == 1){
			if(tipo == 'I')
			  var tipotxt = 'Insumos';
			if(tipo == 'M')
			  var tipotxt = 'Medicamentos';
			if(tipo == 'E'){
			  var tipotxt = 'Equipos';  
			  cantidad = 1;
			}
			
			//var itemtxt = $('#select-item').select2('data')[0].text;
			var itemtxt = $('#acuse-info-item').find(':selected').text();
			
			//var ubicacion = $('#select-item').select2('data')[0].element.dataset.ubicacion;
			var ubicacion = $('#acuse-info-item').find(':selected').data('ubicacion');
			
			
			itemtxt = itemtxt.replace(/"/g,'\\"');
			var nuevafila = '{"cantidad": "'+cantidad+'", \
					"cantidad_facturada": "0", \
					"codigo": "",\
					"costo_unitario": "'+costo_unitario+'",\
					"fechapic": "",\
					"inventario": "'+tipoinventario+'",\
					"itemid": "'+itemid+'",\
					"itemtxt": "'+itemtxt+'",\
					"ubicacion": "'+ubicacion+'",\
					"pic": "",\
					"facturable": "1",\
					"tipo": "'+tipo+'",\
					"tipotxt": "'+tipotxt+'",\
					"cantidad_entregada":"0"\
					}';
			console.log(nuevafila);
			var items = JSON.stringify(items_todos.NUEVO);
			if(items === undefined){
			  items = '';
			}
			if(items != '' && items != '[]')
			  items += ',';
			items = items.replace('[','');
			items = items.replace(']','');
			items += nuevafila;
			items = '['+items+']';
			items_todos.NUEVO= JSON.parse(items);
			itemsoc = JSON.stringify(items_todos);
			cargarTablaOC(itemsoc);
			//$('#select-tipoitem').val('').trigger('change');
			$('#select-item').val('').trigger('change');
			$('#cantidad_anadir').val('').trigger('change');
		  }
		} else {
		  //swal('Error!','Ya este item existe en el acuse','error');
		    Swal.fire({
				icon: 'error',
				title: 'ERROR!',
				text: 'Ya este item existe en el acuse'				  
			})
		  
		}
	});
	
	$("#restaurar-tabla").on('click',function(){
	
	Swal.fire({
	  title: 'Confirmar',
	  text: "Se perderan los cambios realiazos!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#09b354',
	  cancelButtonColor: 'red',
	  confirmButtonText: 'Si',
	  cancelButtonText: "No"
	}).then((result) => {
	  if (result.isConfirmed) {
		cargarTablaOC(items_originales);
        itemsoc = items_originales;      
		Swal.fire(
		  'OK!',
		  'Lista de items restaurada',
		  'success'
		)
	  }
	})
	
	
  });
  
  $("#vaciar-lista").on('click',function(){
	  
	Swal.fire({
	  title: 'Confirmar',
	  text: "Se eliminarán los items agregados a la lista!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#09b354',
	  cancelButtonColor: 'red',
	  confirmButtonText: 'Si',
	  cancelButtonText: "No"
	}).then((result) => {
	  if (result.isConfirmed) {
		itemsoc = '[]';
        cargarTablaOC(itemsoc); 
		Swal.fire(
		  'OK!',
		  'Lista de items vacia',
		  'success'
		)
	  }
	})
	
	
  });
	
	var numero_pic = '';
	function cargarTablaOC(items){
		var total = 0;
		if(items != ''){
		  var items_json = JSON.parse(items);
		  var cantidad = 0;
		  var cantidad_facturada= 0;
		  var costo_unitario = 0;
		  var total = 0;
		  var total_item = 0;
		  var select_inventario ='';
		 
		  $("#lista_pic").empty();
		  $("#pestanas-pic").empty();
		  
		  $.map(items_json,function(prepic, idpic){    
			
			var html = `<table id="table-lista-items" class="table table-responsive-md">
						<thead>
							<tr>
								<th class="width50"></th>
								<th><strong>Tipo</strong></th>
								<th><strong>Item</strong></th>
								<th><strong>UBICACIÓN</strong></th>
								<th><strong>CANTIDAD FACTURADA</strong></th>
								<th><strong>CANTIDAD ENTREGADA</strong></th>
								<th><strong>INVENTARIO</strong></th>
								<th><strong>CANT. A ENTREGAR</strong></th>
					  `;
					  
					  
			if(idpic == 'NUEVO'){
			  html += `<th class="titulo_tabla"style="width:5%;"><strong><a href="javascript:;" id="todosFacturables">FACTURABLE</a></strong></th>`;
			}
			  html += `<!-- th class="titulo_tabla"style="width:10%;"><strong>TOTAL</strong></th --> \
					</tr></thead>`;
			numero_pic = idpic;
			/*var lista_pic = '<li class="nav-item"> \
					  <a class="nav-link tabulador" data-toggle="tab" href="#tablaTemporalOC_'+idpic+'" id="tab_'+idpic+'" role="tablist">\
					  '+idpic+'\
					  </a>\
					</li>';*/
			
			var lista_pic = `<li class="nav-item">
								<a id="tab_${idpic}" class="nav-link tabulador" data-toggle="tab" href="#tablaTemporalOC_${idpic}">${idpic}</a>
							</li>`;
							
			//var pestana  = '<div class="tab-pane" id="tablaTemporalOC_'+idpic+'" class="tablaitems" data-id="'+idpic+'"></div>';
			
			//var pestana  = `<div class="tab-pane" id="tablaTemporalOC_${idpic}" class="tablaitems" data-id="${idpic}"></div>`;
			var pestana  = `<div class="tab-pane" id="tablaTemporalOC_${idpic}" data-id="${idpic}" role="tabpanel"></div>`;
			
			/*var select_filtro = '<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">\
										<label class="control-label" for="filtroitem">Filtro por Item</label>\
										<select id="select_filtros_item_'+idpic+'" style="width:100%" class="select_filtros"><option value="0">Todos</option>';*/
										
			var select_filtro = `<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
									<label class="control-label" for="filtroitem">Filtro por Item</label>
									<select id="select_filtros_item_${idpic}" style="width:100%" class="select_filtros"><option value="0">Todos</option>`;
										
				
			$("#lista_pic").append(lista_pic);
			$("#pestanas-pic").append(pestana);

			var i = 1;
			var optiones = '';
			$.map(prepic,function(fila){
			select_filtro += `<option value="${fila.itemid}" data-tipo="${fila.tipo}" data-pic ="${numero_pic}" >${fila.itemtxt}</option>`;
			  var maximo = (parseInt(fila.cantidad_facturada)-parseInt(fila.cantidad_entregada));
			  var valor =fila.cantidad;
			  if (fila.cantidad_facturada == 0){
				maximo = 9999;
			  }
			  if(fila.cantidad == '' || fila.cantidad === undefined){
				cantidad = 0;
			  } else {
				cantidad = fila.cantidad;
			  }
			  if(fila.cantidad_facturada == '' || fila.cantidad_facturada === undefined){
				cantidad_facturada = 0;
			  } else {
				cantidad_facturada = fila.cantidad_facturada;
			  }
			  if(fila.costo_unitario == '' || fila.costo_unitario === undefined){
				costo_unitario = 0;
			  } else {
				costo_unitario = fila.costo_unitario;
			  }
			  total_item = (parseFloat(costo_unitario))*(parseInt(cantidad));
			  total += parseFloat(total_item);

			  if(fila.inventario == 1){
			  select_inventario =`<select class="form-control form-control-lg tipoinventario"  style="width:100%;" data-itemid="${fila.itemid}" data-tipo="${fila.tipo}" data-pic ="${numero_pic}">
					<option value="1" selected="true">General</option>
					<option value="2">Panel</option>
				  </select>`;
			  }else{
			  select_inventario =`<select class="form-control form-control-lg tipoinventario"  style="width:100%;" data-itemid="${fila.itemid}" data-tipo="${fila.tipo}" data-pic ="${numero_pic}">
						  <option value="1">General</option>
						  <option value="2" selected="true">Panel</option>
						</select>`;
			  }
			  if(numero_pic == 'NUEVO'){
				html += `<tr id="${numero_pic+'_'+i}" data-tipo="${fila.tipo}" data-pic="${numero_pic}" data-itemid="${fila.itemid}" class="tr_tabla">`;
			  }else{
				html += `<tr id="${numero_pic+'_'+i}" data-tipo="${fila.tipo}" data-pic="${fila.pic}" data-itemid="${fila.itemid}" class="tr_tabla">`;
			  }
			  html += `<td>
					  <!--button type="button" class="btn btn-danger btn-xs btn-item btn-quitar-item">
						<span class="glyphicon glyphicon-minus"></span>
						<div class="ripple-container"></div> 
					  </button-->
						<a href="#" class="btn btn-danger shadow btn-xs sharp mr-1 btn-quitar-item" data-tipo="${fila.tipo}" data-itemid="${fila.itemid}" data-id="${numero_pic+'_'+i}" data-pic ="${numero_pic}">
							<i class="fa fa-minus"></i>
						</a>
					</td>
					<td>${fila.tipotxt}</td>
					<td>${fila.itemtxt}</td>
					<td>${fila.ubicacion}</td>
					<td>${cantidad_facturada}</td>
					<td>${fila.cantidad_entregada}</td>
					<td>${select_inventario}</td>`;
			  
			  if(fila.tipo == 'E'){
				var disabled = 'disabled="true"';
				valor = 1;
			} else {
				var disabled = '';
			}
			  
			  if(idpic == 'NUEVO'){
				  
				/*html += ' <td class="centrar"> \
					  <input class="cantidad_item" '+disabled+'" data-tipo="'+fila.tipo+'" data-itemid="'+fila.itemid+'" type="number" min="0" max="'+maximo+'" value="'+valor+'" data-costo="'+costo_unitario+'"  data-pic ="'+numero_pic+'" data-facturada ="0"  data-entregada ="0" style="width:60px;"/> \
					</td> \
					';*/
					
				html += `<td>
					  <input class="cantidad_item" ${disabled} data-tipo="${fila.tipo}" data-itemid="${fila.itemid}" type="number" min="0" max="${maximo}" value="${valor}" data-costo="${costo_unitario}"  data-pic ="${numero_pic}" data-facturada ="0"  data-entregada ="0" style="width:60px;"/>
					</td>`;
			  } else {
				html += ' <td class="centrar"> \
					  <input class="cantidad_item" data-tipo="'+fila.tipo+'" data-itemid="'+fila.itemid+'" type="number" min="0" max="'+maximo+'" value="0" data-costo="'+costo_unitario+'"  data-pic ="'+numero_pic+'" data-facturada ="'+cantidad_facturada+'" data-entregada ="'+fila.cantidad_entregada+'" style="width:60px;"/> \
					</td> \
					';
			  }
			  if(idpic == 'NUEVO'){
				if(fila.facturable == '1'){
				  var facturable = 'checked';
				} else {
				  var facturable = '';
				}
				html += '<td class="centrar"> \
					  <input class="facturable" data-tipo="'+fila.tipo+'" data-itemid="'+fila.itemid+'" '+facturable+' type="checkbox"  style=""/> \
					</td> \
					';
			  }
			  
			  html += '<!-- td class="centrar">'+total_item.toFixed(2)+'</td --> \
				  </tr>';
			  i++;
			});
			select_filtro +='</select></div>';
			//$('#totaltxt').html(select_filtro);
			bandera = true;
			html += '</table>';
			select_filtro +=html; 
			// console.log (select_filtro);
			$('#tablaTemporalOC'+numero_pic).empty();
			//$('#tablaTemporalOC'+numero_pic).html(select_filtro);
			
			$.when($('#tablaTemporalOC_'+numero_pic).append(select_filtro)).done(function(){
			  //$("#select_filtros_item_"+numero_pic).select2({placeholder:"Seleccione..."});
			  $("#select_filtros_item_"+numero_pic).on('change',function(){
					console.log("SELECT ITEM");
					var itemid = $(this).val();
					var pic =$(this).select2('data')[0].element.dataset.pic;
					var tipo = $(this).select2('data')[0].element.dataset.tipo;
					if(itemid !=0){
						$(".tr_tabla[data-pic='"+pic+"']").hide();
						$(".tr_tabla[data-itemid='"+itemid+"'][data-tipo='"+tipo+"'][data-pic='"+pic+"']").show();
					}else{
						console.log("mostrar todos");
						$(".tr_tabla").show();
					}
						
			  });
				$('#tab_'+numero_pic).click();
			 
			  $(".btn-quitar-item").each(function(){
				  
				$(this).on('click',function(e){
				  e.preventDefault()	
				  var dataid = $(this).attr('data-id');
				  var datapic =$(this).attr('data-pic');
				  var itemid = $(this).attr('data-itemid');
				  var tipo = $(this).attr('data-tipo');
				  
				  $.when(eliminar_item(itemsoc,numero_pic,dataid,tipo,itemid)).done(function (){
					$("#"+dataid).remove();
				  });
				 
				});
			  });
			  
			  $(".cantidad_item").each(function(){
				$(this).on('change',function(){
				  if ($(this).val() == ''){
					$(this).val('0').trigger('change');
				  }else{
					var costo_item = $(this).attr('data-costo'); 
					var itemid = $(this).attr('data-itemid');
					var tipo = $(this).attr('data-tipo');                    
					var datapic =$(this).attr('data-pic');
					var cantidad = $(this).val();
					var facturada = $(this).attr('data-facturada');
					var entregada = $(this).attr('data-entregada');
					//console.log("cantidad: "+cantidad);
					console.log(" cantidad_item ");
					if(datapic != 'NUEVO'){
						if(tipo == 'E' && cantidad > 1){
							//swal('Error!','En equipos, la cantidad a entregar no puede ser mayor a 1','error');
							Swal.fire({
								icon: 'error',
								title: 'ERROR!',
								text: 'En equipos, la cantidad a entregar no puede ser mayor a 1'				  
							})	
							$(this).val('0');
							return;
						}
						if(cantidad > (parseInt(facturada) - parseInt(entregada))){
							//swal('Error!','Para entregar una cantidad mayor a la facturada, debe agregar la cantidad excedente como item nuevo','error');
							Swal.fire({
								icon: 'error',
								title: 'ERROR!',
								text: 'Para entregar una cantidad mayor a la facturada, debe agregar la cantidad excedente como item nuevo'				  
							})		
							$(this).val('0');
							return;
						}
					}
					
					
					$.when(remplazar_valor(itemsoc,datapic,tipo,itemid,cantidad,'cantidad')).done(function (){          
					  calcular_total_nuevo(itemsoc);
					});                      
				  }
				});
				// console.log(itemsoc);
			  });
			  
			  // TODOS FACTURABLES
			  $('#todosFacturables').off();
			  $('#todosFacturables').click(function(){
					$(".facturable").each(function(){
						$(this).click();
					});
			  });
			  
			  
			  $(".facturable").each(function(){
				$(this).on('click',function(){
				  var itemid = $(this).attr('data-itemid');
				  var tipo = $(this).attr('data-tipo');  
				  var facturable = $(this).prop('checked');
				  
				  remplazar_valor(itemsoc,'NUEVO',tipo,itemid,facturable,'facturable');
				});
			  });
			  $(".tipoinventario").each(function(){
				$(this).on('change',function(){
				  var itemid = $(this).attr('data-itemid');
				  var tipo = $(this).attr('data-tipo');
				  var datapic =$(this).attr('data-pic');
				  var tipo_inventario = $(this).val();
				  $.when(remplazar_valor(itemsoc,datapic,tipo,itemid,tipo_inventario,'inventario')).done(function(){
				  });
				});
			  });
			});       
		  });

		}

	}
	

	function calcular_total_nuevo(items){
    var total_cambio = 0;
    var items_json = JSON.parse(items);
    $.map(items_json,function(perpic, idpic){        
      $.map(perpic,function(fila){
        total_cambio += (parseFloat(fila.costo_unitario))*(parseInt(fila.cantidad));
      });
    });  
   
    $('#totaltxt').html(total_cambio.toFixed(2));
  }
  function remplazar_valor(json,pic,tipo,item,valor,campo){
    json = JSON.parse(json);
    for(var i = 0; i < json[pic].length; i++){
      if(json[pic][i]['itemid'] == item && json[pic][i]['tipo'] == tipo){    
   
        json[pic][i][campo] = valor;
      }
    }
    itemsoc = JSON.stringify(json);
  }
  function buscar_valor(json,pic,tipo,item){
    var encontrado = 0;
    json = JSON.parse(json);
    if(json[pic] !== undefined){
      for(var i = 0; i < json[pic].length; i++){
        if(json[pic][i]['itemid'] == item && json[pic][i]['tipo'] == tipo){    
          encontrado = 1;
   
        }
      }
    }
 
 
 
    return encontrado;
  }
	function buscar_valor_editar(json,tipo,item){
		var encontrado = 0;
		json = JSON.parse(json);
		if(json !== undefined){
		  for(var i = 0; i < json.length; i++){
			if(json[i]['itemid'] == item && json[i]['tipo'] == tipo){    
			  encontrado = 1;	   
			}
		  }
		}
		return encontrado;
	}
	
  function remplazar_valor_editar(json,tipo,item,valor,campo,id_detalle){
    json = JSON.parse(json);
	console.log(json)
    for(var i = 0; i < json.length; i++){
      if(json[i]['itemid'] == item && json[i]['tipo'] == tipo && json[i].id == id_detalle){    
        // console.log("encontrado");   
        json[i][campo] = valor;
      }
    }
    itemsoc = JSON.stringify(json);
  }
  
  function eliminar_item(json,numero_pic,dataid,tipo, item){
    json = JSON.parse(json);
    for(var i = 0; i < json[numero_pic].length; i++){
      if(json[numero_pic][i].itemid == item && json[numero_pic][i].tipo == tipo){ 
        if(json[numero_pic][i].id !== undefined && json[numero_pic][i]!= 'id')      
          detalle_eliminar+= json[numero_pic][i].id+',';
        json[numero_pic].splice(i, 1);
      }
    }
    itemsoc = JSON.stringify(json);
  }
  
  function validar_agregar_item(item,cantidad) {
    var res = 1;
    if (item == '' || item == '0'){
      //swal('Error!','Debe seleccionar un Item','error');
	  Swal.fire({
			icon: 'error',
			title: 'ERROR!',
			text: 'Debe seleccionar un Item'				  
		})	
      res = 0;
    }else if (cantidad == '' || cantidad == 0){
      //swal('Error!','Debe seleccionar una cantidad', 'error');
	  Swal.fire({
			icon: 'error',
			title: 'ERROR!',
			text: 'Debe seleccionar una cantidad'				  
		})
      res = 0;
    }
    return res; 
  }
  
  
   $("#guardar_acuse").on('click',function(){
	   
		var fecha = $("#acuse-info-fecha").val();
		var colaborador = $("#acuse-info-colaborador").val();
		var estatus = $("#acuse-info-estatus").val();
		var idpaciente = $("#idpaciente_nuevo").val();
		var items = JSON.parse(itemsoc);
		var observaciones = $("#observaciones").val();
					  
	 
		var mensaje = '<table id="tablalistadoitem" class="table table-striped table-bordered" style="width:100%"><thead><th style="width:80%">ITEM</th><th style="width:20%">CANTIDAD</th></thead>';
		$.map(items,function(itemp){
			console.log(items) 
			$.map(itemp,function(item){
				if(item.cantidad != '0'){
					
					if(item.pic != ''){
						console.log("itemtxt ",item.itemtxt)
						console.log("pic ", item.pic)
						mensaje += '<tr><td>'+item.itemtxt+'('+item.pic+')</td><td>'+item.cantidad+'</td></tr>';
					} else {
						mensaje += '<tr><td>'+item.itemtxt+'</td><td>'+item.cantidad+'</td></tr>';
					}
				}	
			});
		});
		
		mensaje += '</table>';
		$("#listadeitems").html(mensaje);
		$("#tablalistadoitem").DataTable({
			responsive: true,
			colReorder: false,
			lengthChange: false,
			searching: true, 
			ordering: false,
			bAutoWidth: false,
			"language": {
				"url": "js/Spanish.json"				
			}
		});
		$("#modal-final-acuse").modal('show');
  });
  
  
  $("#btn-aceptar-lista").on('click',function(){
	guardar_acuse_nuevo();  
  });
  
  function guardar_acuse_nuevo(){
    var fecha = $("#acuse-info-fecha").val();
    var colaborador = $("#acuse-info-colaborador").val();
    var estatus =$("#acuse-info-estatus").val();
    var idpaciente = $("#idpaciente_nuevo").val();
    var idsolicitud = $("#idsolicitud_nuevo").val()
    var items = JSON.parse(itemsoc);
					
    var observaciones = $("#observaciones").val();
	$.when($.map(items,function(prepic, idpic){    
	    for(var i = 0; i < prepic.length; i++){  
			if(prepic[i].cantidad == 0){ 				
				prepic.splice(i, 1);
				i = i-1;
	 
			}
	    }
	})).done(function(){
		$.ajax({
			type: 'post',
			url: 'controller/logisticaback.php',
			data: { 
				opcion: 'guardar_acuse_nuevo',
				fecha :  fecha,
				idpaciente : idpaciente,
				idsolicitud : idsolicitud,
				colaborador:  colaborador,
				estatus: estatus,
				observaciones:  observaciones,
				items : items
			},
			beforeSend: function() {
				//$('#overlay').css('display','block');
				document.getElementById("preloader").style.display = "block";
			},
			success: function (response) {
				document.getElementById("preloader").style.display = "none";
				//$('#overlay').css('display','none');
				$.when(Swal.fire('Buen trabajo!','Acuse de entrega creado exitosamente con el número #'+response+'','success')).done(function(){
					$("#acuse-modal-lg").modal('hide');
					$("#modal-final-acuse").modal('hide');
					$("#acuse-info-inventario").val(0).trigger('change');
					tabla_logistica.ajax.reload(null,false);
					tabla_logistica_1.ajax.reload(null,false);
				});               
			},
			error: function () {
				//Swal.fire('Error','ERROR!','Ha ocurrido un error al grabar, intente más tarde');
				Swal.fire({
				  icon: 'error',
				  title: 'ERROR!',
				  text: 'Ha ocurrido un error al grabar, intente más tarde'				  
				})
				
			}
		});
	});

  }
  
  
	$(document).on('click', '.boton-solicitud-estado', (e)=>{
		e.preventDefault();
		/*console.log(e.currentTarget)
		
		console.log(e.currentTarget.dataset.estado)
		console.log(e.currentTarget.dataset.solicitud)*/
		
		let idestado = e.currentTarget.dataset.estado;
		let idsolicitud = e.currentTarget.dataset.solicitud;
		$.get("controller/logisticaback.php?opcion=actualizar_estado_solicitud&idsolicitud="+idsolicitud+"&idestado="+idestado, function(response){
				tabla_logistica.ajax.reload(null,false);
				tabla_logistica_1.ajax.reload(null,false);
				tabla_logistica_2.ajax.reload(null,false);
		}, 'json');
		
	});
	
	/*$(document).on('click', '.boton-imprimir-acuse', (e)=>{
		console.log("boton-imprimir-acuse")
		
		
	});	
	
	$(document).on('click', '.boton-imprimir-acuse-interno', (e)=>{
		console.log("boton-imprimir-acuse-interno")
		
	});	*/
	
	$(document).on('click', '#anadir_plantilla', (e)=>{
		$("#modal-plantilla").modal("show")
	});	
  
  
	function getCookie(cname) {
	    let name = cname + "=";
	    let decodedCookie = decodeURIComponent(document.cookie);
	    let ca = decodedCookie.split(';');
	    for(let i = 0; i <ca.length; i++) {
		  let c = ca[i];
		  while (c.charAt(0) == ' ') {
		    c = c.substring(1);
		  }
		  if (c.indexOf(name) == 0) {
		  return c.substring(name.length, c.length);
		}
	  }
	  return "";
	}
	
	
	mapa();
	function mapa(tipo) {
	var styledMapType = new google.maps.StyledMapType(
            [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#ffffff"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#dadada"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#c9c9c9"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  }
			],
            {name: 'Styled Map'}
        );

	/*$.ajax({
		url: "controller/dashboardback.php",
		cache: false,
		dataType: "json",
		method: "POST",
		data: {
			"opcion": "MAPA",
			"tipo"	:tipo
		}
	}).done(function(data) {
		var latitud = parseFloat(window.localStorage.getItem("latitud"));
		var longitud = parseFloat(window.localStorage.getItem("longitud"));
        var map = new google.maps.Map(document.getElementById('mapa'), {
          center: {lat: latitud, lng: longitud},
          zoom: 10
        });
		
		$.map(data, function (datos) {
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
				icon: datos.icon,
				title: datos.title,
				label: {
					text: datos.label,
					fontSize: '1px',
					color: '#1f4380'
				},
				map: map
			});
			
			marker.addListener('click', function() {
				map.setZoom(8);
				map.setCenter(marker.getPosition());
				var xUnidad = marker.label.text;
				console.log(marker.title.text);
			});
		});

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
	});*/
	
	
	 var map = new google.maps.Map(document.getElementById('mapa'), {
		  center: {lat: 79.5202202 , lng: 8.9870728 },
		  zoom: 10
		});
	map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
}
	
	

	
    function scrollpage(contenedor){
    	var header = contenedor - $('.header').height();
        $('html, body').stop().animate({
            scrollTop: header
        }, 1000);
        return false;
    }

    $('.ancla').click(function(){
    	var name = $(this).attr('name');
        scrollpage( $('.'+name).offset().top );
    });
  
  
  
  
});




