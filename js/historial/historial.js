$(document).ready(function(){
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	var id = getParameterByName('idpaciente');
	
	$.get('controller/historialback.php?opcion=get_paciente&id='+id,'',function(result){
		$("#contenido").html(result);
	});
	$.get('controller/historialback.php?opcion=enfermedades_actuales&id='+id,'',function(result){
		$("#enfermedadesactuales").html(result);
	});
	$.get('controller/historialback.php?opcion=enfermedades_pasadas&id='+id,'',function(result){
		$("#enfermedadespasadas").html(result);
	});
	$.get('controller/historialback.php?opcion=medico_tratante&id='+id,'',function(result){
		$("#medico_tratante").html(result);
	});
	$.get('controller/historialback.php?opcion=estatus_hospitalizacion&id='+id,'',function(result){
		$("#estatus_hospitalizacion").html(result);
	});
	$.get('controller/historialback.php?opcion=paciente_contactos&id='+id,'',function(result){
		$("#paciente_contactos").html(result);
	});
	$.get('controller/historialback.php?opcion=notas_enfermeria&id='+id,'',function(result){
		$("#notas_enfermeria").html(result);
	});
	
	$.get('controller/historialback.php?opcion=notas_medicos&id='+id,'',function(result){
		$("#notas_medicos").html(result);
	});
	
	$.get('controller/historialback.php?opcion=antecedentes_nopatologicos&id='+id,'',function(result){
		$("#antecedentes_nopatologicos").html(result);
	});
	$.get('controller/historialback.php?opcion=antecedentes_personales&id='+id,'',function(result){
		$("#antecedentes_personales").html(result);
	});
	$.get('controller/historialback.php?opcion=antecedentes_familiares&id='+id,'',function(result){
		$("#antecedentes_familiares").html(result);
	});
	$.get('controller/historialback.php?opcion=antecedentes_quirurgicos&id='+id,'',function(result){
		$("#antecedentes_quirurgicos").html(result);
	});
	$.get('controller/historialback.php?opcion=examen_laboratorio&id='+id,'',function(result){
		$("#examen_laboratorio").html(result);
	});
	
	$.get('controller/historialback.php?opcion=receta_medica&id='+id,'',function(result){
		$("#receta_medica").html(result);
	});
	
	$.get('controller/historialback.php?opcion=evaluaciones&id='+id,'',function(result){
		$.when($("#evaluaciones").html(result)).done(function(){
			$("#maseva").on( 'click', function() {
				$(".accordion__item").each(function(){
					$(this).show();
					$("#maseva").hide();
					$("#menoseva").show();
				});
			});
			$("#menoseva").on( 'click', function() {
				$(".accordion__item").each(function(){
					let dataid = $(this).attr('data-id');
					
					if(dataid>2){
						$(this).hide();
					}
					
					$("#menoseva").hide(); 
					$("#maseva").show();
				});
			});
		});
	});
	
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});

	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
	$("#imprimir_historial").on( 'click', function() {
		let fechas = $("#fecha").val();
		let fecha = fechas.split('-');
		let desde = fecha[0].replace(/\//g, "-");
		let hasta = fecha[1].replace(/\//g, "-");;
		window.open("controller/reporte_historial_paciente.php?&id="+id+"&fecha_inicio="+desde+"&fecha_fin="+hasta); 
	});
	
	//Tablas

	$("#planes").on('click',function(){
		$('#tabla_planes').dataTable().fnDestroy();
		var tabla_planes = $("#tabla_planes").DataTable({"scrollX": true, 
			//"lengthMenu": [3, 4],
			responsive: true,
			scrollCollapse: true,
			"scrollY": "100%",
			"ajax"		:"controller/dashboardback.php?opcion=TABLA_PLANES&idpaciente="+id,
			"columns"	: [
				{ 	"data": "acciones" },
				
				{ 	"data": "fecha_actualizacion_tarjeta" },
				{ 	"data": "estado" },
				{ 	"data": "creado_por" }
				
			],
			rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
			"columnDefs": [ //OCULTAR LA COLUMNA ID

			],
			"language": {
				"url": "js/Spanish.json",
				"info": "Mostrando página _PAGE_ de _PAGES_"
			},
			initComplete: function() {
			//$('#totaltfinalizados').text( this.api().data().length )
			}
		});
		$("#tabla_planes").on('draw.dt',function(){
			$('.imprimir_plan').each(function(){
				$(this).on('click',function(){
					var id = $(this).attr("data-id");
					$.get('controller/dashboardback.php?opcion=imprimir_plan&id='+id,'',function(result){
						if(result == 1){
							var url = 'controller/reporte_planes.php?id='+id;
							window.open(url, '_blank');
						} else if(result != '') {
							swal('ERROR!','El usuario '+result+' se encuentra editando este plan de cuidado','error');
						}
					});
				});
			}); 
			$('.activar_plan').each(function(){
				
				$(this).on('click',function(){
					var id = $(this).attr("data-id");
					swal({
					  title: "Confirmar",
					  text: "¿Estas seguro de Activar este Plan de cuidados?",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
					})
					.then((willDelete) => {
					  if (willDelete) {
						  $.get( "controller/dashboardback.php?opcion=activar_plan&id="+id, 
								{ 
									id: id
								}, function(result){
									if(result == 1){
										swal("Tratamiento Activado!", {icon: "success",});
										$('#tabla_planes').DataTable().ajax.reload();
									} else if(result != '') {
										swal('ERROR!','El usuario '+result+' se encuentra editando este Plan de cuidados, aun no se puede Activar','error');
										$('#tabla_planes').DataTable().ajax.reload();
									}
							});
						
					  } else {
						swal("Este Plan de cuidados no se puede Activar");
						$('#tabla_planes').DataTable().ajax.reload();
					  }
					});
				}); 
			}); 
		});
	});
	
		
	$("#tarjeta").on('click',function(){
		$('#tabla_tratamientos_finalizados').dataTable().fnDestroy();
		var tabla_tratamientos_finalizados = $("#tabla_tratamientos_finalizados").DataTable({"scrollX": true, 
			//"lengthMenu": [3, 4],
			responsive: true,
			scrollCollapse: true,
			"scrollY": "100%",
			"ajax"		:"controller/dashboardback.php?opcion=TABLA_TRATAMIENTOS_FINALIZADOS&idpaciente="+id,
			"columns"	: [
				{ 	"data": "acciones" },
				{ 	"data": "fecha" },
				{ 	"data": "creador" },
				{ 	"data": "estado" },
				{ 	"data": "tipotarjeta" }
				
			],
			rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
			"columnDefs": [ //OCULTAR LA COLUMNA ID

			],
			"language": {
				"url": "js/Spanish.json",
				"info": "Mostrando página _PAGE_ de _PAGES_"
			},
			initComplete: function() {
		  $('#totaltfinalizados').text( this.api().data().length )
			}
		});
		
	});
	
	//fin tablas
	
	//chart
	function cargar_info(idpaciente,fecha_inicio,fecha_fin){
	
		$.ajax({
			"url"		:"controller/historialback.php",
			data: { 
				'opcion' 		: 'listado_pacientes',
				'idpaciente' : idpaciente,
			},
			cache: false,
			dataType: "json",
			method: "POST"
		}).done(function(response){
			grafica_sv(response.grafica);
		});
	}
	cargar_info(id,'','');
	function grafica_sv(grafica){
		if(grafica !== undefined){	
			for(var i = 0; i <grafica.series.arrFrecuenciaCardiaca.data.length; i++){                
				grafica.series.arrFrecuenciaCardiaca.data[i].y = Number(grafica.series.arrFrecuenciaCardiaca.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrFrecuenciaRespiratoria.data.length; i++){                
				grafica.series.arrFrecuenciaRespiratoria.data[i].y = Number(grafica.series.arrFrecuenciaRespiratoria.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrOximetria.data.length; i++){                
				grafica.series.arrOximetria.data[i].y = Number(grafica.series.arrOximetria.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrSistolica.data.length; i++){                
				grafica.series.arrSistolica.data[i].y = Number(grafica.series.arrSistolica.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrDiastolica.data.length; i++){                
				grafica.series.arrDiastolica.data[i].y = Number(grafica.series.arrDiastolica.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrTemperatura.data.length; i++){                
				grafica.series.arrTemperatura.data[i].y = Number(grafica.series.arrTemperatura.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrDolor.data.length; i++){                
				grafica.series.arrDolor.data[i].y = Number(grafica.series.arrDolor.data[i].y);
			}
			for(var i = 0; i <grafica.series.arrGlicemia.data.length; i++){                
				grafica.series.arrGlicemia.data[i].y = Number(grafica.series.arrGlicemia.data[i].y);
			}
			let longitud=grafica.arrCategorias.length-1;
			var signosvitales = {
				chart: {
					renderTo: "chart", 
					defaultSeriesType: "spline",
					backgroundColor:"rgba(255, 255, 255, 0.0)",
					
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function() {
									abrir_visita(this.id);
								}
							}
						}
					}
				},
				credits: {
					enabled: false
				},
				title: {
					text: null
				},
				subtitle: {
					text: null
				},
				xAxis: {
					labels: {
						style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
					},
					categories: grafica.arrCategorias,
					range: longitud,
					min : 0,
					max: longitud,  
					tickInterva: 0,
					step: 0,
					pointInterval: 0
				},
				yAxis: {
					title: {
						text: null
					},
					plotLines: [{
						width: 2,
						color: "#808080"
					}],
					max: null
				},
				tooltip: {
					valueDecimals: 1,
					valuePrefix: null,
					valueSuffix: null
				},
				scrollbar: {
					enabled: true,
				},
				legend: {
					enable: true,
					verticalAlign: 'top', 
					// y: 100, 
					align: 'right' 
					
				},
				series: [
					{ 
						data: grafica.series.arrFrecuenciaCardiaca.data,
						name: "Frec. Cardíaca",
						color: "#D50000"
					},{ 
						data: grafica.series.arrFrecuenciaRespiratoria.data,
						name: "Frec. Respiratoria",
						color: "#AA00FF"
					},{ 
						data: grafica.series.arrOximetria.data,
						name: "Sat. Oxigeno",
						color: "#304FFE",
					},{ 
						data: grafica.series.arrSistolica.data,
						name: "Sistólica",
						color: "#2962FF",
					},{ 
						data: grafica.series.arrDiastolica.data,
						name: "Diastólica",
						color: "#00BFA5",
					},{  
						data: grafica.series.arrTemperatura.data,
						name: "Temperatura",
						color: "#00C853",
					},{ 
						data: grafica.series.arrDolor.data,
						name: "Nivel de Dolor",
						color: "#AEEA00",
					},{ 
						data: grafica.series.arrGlicemia.data,
						name: "Glicemia Capilar",
						color: "#FFAB00",
					}
				]
			};
			var chart = new Highcharts.Chart(signosvitales);
			$(".highcharts-background").attr('stroke','');
		}else{
			$("#chart").html('');
		}
	}
	//
	
});