jQuery(function($) {
	$('#fecha,#fechafin').datepicker({
		format: 'yyyy-mm-dd',
		language: 'es',
		todayBtn: "linked",
		todayHighlight: true
	});
	// PACIENTES

	$.get("controller/dashboardback.php?oper=pacientes", { onlydata:"true" }, function(result){
		$('#filtropaciente').empty(result);  
		$('#filtropaciente').append(result); 
		$('#filtropaciente').select2({placeholder: ""});
	}); 
	$('#filtropaciente').on('change',function(){
		var idpaciente = $(this).val();		
		$.get("controller/combosback.php?oper=hogares", { onlydata:"true" }, function(result){
				$('#filtrohogar').empty(result);  
				$('#filtrohogar').append(result); 
				$('#filtrohogar').select2({placeholder: ""});
			});		
	});
	
	$.get("controller/combosback.php?oper=hogares", { onlydata:"true" }, function(result){
		$('#filtrohogar').empty(result);  
		$('#filtrohogar').append(result); 
		$('#filtrohogar').select2({placeholder: ""});
	}); 
	$('#filtrohogar').on('change',function(){
		var idhogar = $(this).val();		
		$.get("controller/combosback.php?oper=pacientes", { onlydata:"true" }, function(result){
			$('#filtropaciente').empty(result);  
			$('#filtropaciente').append(result); 
			$('#filtropaciente').select2({placeholder: ""});
		}); 
		
	});
	
	$('#filtropaciente').on('change',function(){
		if($(this).val() == ''){
		$('#fechafin').prop('disabled',true);
		}else{
		$('#fechafin').prop('disabled',false);
		}		
	}); 
	
	$('#fechafin').prop('disabled',true);							  
	$("#btn-filtrar").on('click',function(){
		$('#chart_mes').hide();	
		$("#chart_paciente_mes").hide();
		$('#fechafin').prop('disabled',true);
		// var idhogar = $('#filtrohogar').val();
		var idpaciente = $('#filtropaciente').val();
		var fecha = $("#fecha").val();
		var fechafin = $("#fechafin").val();
		if(idpaciente != 0){
			//$("#botonera").show();
			
			
			$('#fechafin').prop('disabled',false);
		}else{
			//$("#botonera").hide();
			$('#fecha').prop('disabled',false);
			
		}
		if(idpaciente != '0'){			
			$("#hogar").hide();
			$("#chart").hide();
			$("#chart_mes").hide();
			$("#chart_paciente").show();
			tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente+'&fecha='+fecha+'&fechafin='+fechafin).load();   
			
			if($("#todos").hasClass('oculto')){
				console.log("Cayó en el primer if")
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_semana',
						'idpaciente' : idpaciente,
						'fechaf' : fechafin
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
	 
					var data = response.data;
					arridvisita =[];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = []; 
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)", 
							scrollablePlotArea: {
								minWidth: 2000,
								scrollPositionX: 1
							}
						},
					
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											console.log(this.id)
											abrir_visita(this.id);
										}
									}
								}
							}
						},
		
						credits: {
							enabled: false
						},    
						title: {
							text: null 
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud, 
							tickInterva: 1,
							step: 1,
							pointInterval: 1
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
			}else{
				console.log("Cayó en el segundo if")
				$("#todos").hide();
				$("#unsolopaciente").hide();
				$("#unsolopaciente").show();
				var fecha = $("#fecha").val();
				var fechafin = $("#fechafin").val();						
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_semana',
						'idpaciente' : idpaciente,
						'fecha' : fecha,
						'fechaf' : fechafin,
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita =[];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					  
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							scrollablePlotArea: {
								minWidth: 2000,
								scrollPositionX: 1

							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											console.log(this.id);
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
					   
							range: longitud,
							min : 1,
							max: longitud, 
							tickInterva: 1,
							step: 1,
							pointInterval: 1
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
					   
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
					   
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
					   
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
					   
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
					   
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
					   
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
					   
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
				
		}else{
			tpacientes.ajax.url("controller/dashboardvitaeback.php?oper=listadovisitasapp&hogar=-1&fecha="+fecha).load();  
			grafica_todos();
	
		}
		
	});
	
	$("#btn-todos").on('click',function(){
		$('#filtropaciente').val(0).trigger('change');	
		$("#botonera").hide();
		$("#fecha").val('');
		$("#fecha").prop('disabled',false);
		$("#fechafin").prop('disabled',true);							   
		$("#todos").show();
		$("#unsolopaciente").hide();
		$("#hogar").hide();
		$("#chart_mes").hide();
		$("#chart").show();	
		$('#fecha').val('');
		$('#fechafin').val('');				 
	});
	
	
	function grafica_todos(){
		$.ajax({
			"url"		:"controller/dashboardback.php",
			data: { 
				'oper' 		: 'listado_pacientes',
				'fecha' : $("#fecha").val(),
				'fechafin' : $("#fechafin").val()				 
			},
			cache: false,
			dataType: "json",
			method: "POST"
		}).done(function(response){
			var data = response.data;
			arridvisita = [];	 
			arrCategorias = [];
			arrFrecuenciaCardiaca = [];
			arrFrecuenciaRespiratoria = [];
			arrOximetria = [];
			arrSistolica = [];
			arrDiastolica = [];
			arrTemperatura = [];
			arrDolor = [];
			arrGlicemia = [];
			arrEstatura = [];
			arrPeso = [];
			arrImc = [];

			longitud=data.length;
			for (i=0; i<longitud; i++) {	
			
				arrCategorias.push(data[i].nombre);							
				data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
				data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
				data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
				data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
				data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
				data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
				data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
				data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
				data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
				data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
				data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
				data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
				
	
			} 
			Highcharts.chart('chart',{
				chart: {
					// renderTo: "chart",
					type: "spline",   
					marginLeft: 50,
					 scrollablePlotArea: {
						minWidth: 3000,
						scrollPositionX: 0
					}
				},
				  
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function() {
									abrir_visita(this.id);
								}
							}
						}
					}
				},
	  
				credits: {
					enabled: false
				},
				title: {
					text: "Visitas de Pacientes"
				},
				xAxis: {
					type: 'linear',
					categories: arrCategorias,
					pointStart: 0,
					pointInterval: 0,
					// range: 15,
					min: 0,
					max: longitud,					
					tickLength: 0 ,
					labels: {
						overflow: 'justify',
						zIndex:100
					}					
				},
				yAxis: {
					title: {
						text: null
					},
					plotLines: [{
						width: 1,
						color: "#808080"
					}],
					max: null,
					min:0,
				},
				tooltip: {
					valueDecimals: 1, 
					valuePrefix: null,
					valueSuffix: null
				},
				
				legend: {
					enable: true,
					verticalAlign: 'top', 
					// y: 100, 
					align: 'right' 
					
				},
				series: [{ 
					data: arrFrecuenciaCardiaca,
					name: "Frec. Cardíaca",
					color: "#D50000",
				},{ 
					data: arrFrecuenciaRespiratoria,
					name: "Frec. Respiratoria",
					color: "#AA00FF"
				},{ 
					data: arrOximetria,
					name: "Sat. Oxigeno",
					color: "#304FFE",
				},{ 
					data: arrSistolica,
					name: "Sistólica",
					color: "#2962FF",
				},{ 
					data: arrDiastolica,
					name: "Diastólica",
					color: "#00BFA5",
				},{ 
					data: arrTemperatura,
					name: "Temperatura",
					color: "#00C853",
				},{ 
					data: arrDolor,
					name: "Nivel de Dolor",
					color: "#AEEA00",
				},{ 
					data: arrGlicemia,
					name: "Glicemia Capilar",
					color: "#FFAB00",
				}
				]
			});
			
			// var chart = new Highcharts.Chart(signosvitales);
			// $(".highcharts-background").attr('chart','');
		});
	}
			
			
			
	
 
	grafica_todos();
	var tpacientes = $('#tpacientes').DataTable({ 
		"searching": true,
		"lengthChange": false,
		// "responsive" :true,
		//order: [[13,14,15,16,17]],
		"paging": false, 

		"ajax"		: {
			"url"		:"controller/dashboardback.php?oper=listadovisitasapp&hogar=-1",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "acciones"
			},
			{ 	"data": "nombre" },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			 },
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			 },
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			 },
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			 },
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl); 
				} 
			 },
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc); 
				} 
			 },
			 { 	"data": "imc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rim);
				} 
			 },
			 { 	"data": "fecha" },
			{ 	"data": "id" },
			{ 	"data": "rfc" },
			{ 	"data": "rfr" },
			{ 	"data": "rox" },
			{ 	"data": "rsi" },
			{ 	"data": "rdi" },
			{ 	"data": "rtm" },
			{ 	"data": "rdl" },
			{ 	"data": "rgc" },
			{ 	"data": "res" },
			{ 	"data": "rpe" },
			{ 	"data": "rim" },
			{ 	"data": "condicion" }
			],
		"rowId": 'id',
		"columnDefs": [		
			{
				"targets"	:  0,
				"width"		:  "10%"
			},{
				"targets"	:  1,
				"width"		:  "10%"
			},{
				"targets"	: [10,12,13,14,15,16,17,18,19,20,21,22,23,24],
				"visible"	: false,
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}	
	});
	
	
	//ORDENAR DATATABLE POR MULTIPLES COLUMNAS
	tpacientes.order( [ 14, 'asc' ], [ 15, 'asc' ],[ 16, 'asc' ],[ 17, 'asc' ],
					[ 18, 'asc' ],[ 19, 'asc' ],[ 20, 'asc' ],[ 21, 'asc' ],
					[ 24, 'asc' ] ).draw();
	$("#tpacientes").on('draw.dt',function(){
		$('.ver-examenfisico').each(function(){
			$(this).on('click',function(){
				var nTr = $(this).closest('tr');
				var row = tpacientes.row( nTr );
				var id  = tpacientes.row( row ).id();
				// DIBUJO
				//$('#valores-dibujo').prop('src','pacientes/'+idpacientes+'/examenfisico/'+id+'/dibujo.png');
				
				// OBTENER VALORES 
				$.when($('#signos-vitales').html("")).done(function(){
					$.get('controller/dashboardback.php',{
						oper:'getvaloresvisita',
						idvisita:id 
					},function(item){
						var grid = '';
						grid += '<tr><td><label class="">Frecuencia cardíaca</label></td>';
						grid += '<td><label class="">'+item.fc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Frecuencia respiratoria</label></td>';
						grid += '<td><label class="">'+item.fr+'</label></td></tr>';
							
						grid += '<tr><td><label class="">Saturación de Oxígeno (Oximetría)</label></td>';
						grid += '<td><label class="">'+item.so+'</label></td></tr>';	
						
						grid += '<tr><td><label class="">Sistólica</label></td>';
						grid += '<td><label class="">'+item.paa+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Diastólica</label></td>';
						grid += '<td><label class="">'+item.pab+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Temperatura corporal</label></td>';
						grid += '<td><label class="">'+item.tc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Nivel de dolor</label></td>';
						grid += '<td><label class="">'+item.dolor+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Glicemia capilar</label></td>';
						grid += '<td><label class="">'+item.gc+'</label></td></tr>';
						
						$('#signos-vitales').html(grid);
						
						
						grid = '';
						grid += '<tr><td><label class="">Recurso: </label>&nbsp;<b>'+item.recurso+'</b></td></tr>';
						grid += '<tr><td><label class="">Fecha: </label>&nbsp;<b>'+item.fecha+'</b></td></tr>';
						grid += '<tr><td><label class="">Hora: </label>&nbsp;<b>'+item.horallegada+'</b></td></tr>';
						grid += '<tr><td><label class="">Estatura: </label>&nbsp;<b>'+item.e+'</b></td></tr>';
						grid += '<tr><td><label class="">Peso: </label>&nbsp;<b>'+item.peso+'</b></td></tr>';
						grid += '<tr><td><label class="">IMC: </label>&nbsp;<b>'+item.imc+'</b></td></tr>';
						grid += '<tr><td><label class="">Comentario: </label>&nbsp;<b>'+item.comentario+'</b></td></tr>';
						grid += '<tr><td><label class="">Evaluación: &nbsp;<b></label>';
						
						switch (item.evaluacion){
							case '2':
								grid += 'Muy satisfecho';
							break;
							case '1':
								grid += 'Satisfecho';
							break;
							case '0':
								grid += 'Insatisfecho';
							break;
						}
						
						grid += '</b></td></tr>';
						
						grid += '<tr><td><label class="">Comentarios de la evaluacion:</label>&nbsp;<b>'+item.evaluacioncomentario+'</b></td></tr>';
						
						$('#visita-general').html(grid);
						
						
						
						var canvas = document.getElementById("sig-canvas");
						var ctx = canvas.getContext("2d");
						var myImage = new Image();
						myImage.onload = function() {
							ctx.drawImage(myImage, 0, 0);
						};
						myImage.src = item.firma;
						
						
					},'json');
				});	
				
				$('#modalVisita').modal('show');
			});
		});
		$(".ver-historial").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				location.href="pacientedatos.php?idpaciente="+id;
			});
		});
		$(".boton-reporte-diario").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				var fecha = $(this).attr('data-fecha');
				window.open("controller/reportediariopaciente.php?id="+id+"&fecha="+fecha);
			});
		});
		$('[data-toggle="tooltip"]').tooltip();

	});
	
	//RECARGAR TABLA PACIENTES CADA MINUTO
	
	setInterval(function(){
		var fecha = $("#fecha").val();
		if(fecha == ''){
			grafica_todos();
			tpacientes.ajax.url("controller/dashboardback.php?oper=listadovisitasapp&hogar=-1").load();  
		}
	}, 300000);
	
	var tpacientes_hogar = $('#tpacientes_hogar').DataTable({ 
		"searching": true,
		"lengthChange": false,
		//order: [[13,14,15,16,17]],
		"paging": false,

		"ajax"		: {
			"url"		:"controller/dashboardback.php?oper=listadovisitasapp",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "acciones"},
			{ 	"data": "hogar"},
			{ 	"data": "nombre" },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			 },
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			 },
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			 },
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			 },
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl); 
				} 
			 },
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc); 
				} 
			 },
			 { 	"data": "imc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rim);
				} 
			 },
			{ 	"data": "fecha" },			
			{ 	"data": "id" },
			{ 	"data": "rfc" },
			{ 	"data": "rfr" },
			{ 	"data": "rox" },
			{ 	"data": "rsi" },
			{ 	"data": "rdi" },
			{ 	"data": "rtm" },
			{ 	"data": "rdl" },
			{ 	"data": "rgc" },
			{ 	"data": "res" },
			{ 	"data": "rpe" },
			{ 	"data": "rim" },
			{ 	"data": "condicion" },
			],
		"rowId": 'id',
		"columnDefs": [		
			{
				"targets"	:  0,
				"width"		:  "10%"
			},{
				"targets"	:  1,
				"width"		:  "15%"
			},{
				"targets"	: [1,13,14,15,16,17,18,19,20,21,22,23,24],
				"visible"	: false,
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}	
	});
	//ORDENAR DATATABLE POR MULTIPLES COLUMNAS
	tpacientes_hogar.order( [ 14, 'asc' ], [ 15, 'asc' ],[ 16, 'asc' ],[ 17, 'asc' ],
					[ 18, 'asc' ],[ 19, 'asc' ],[ 20, 'asc' ],[ 21, 'asc' ],
					[ 24, 'asc' ] ).draw();
	$("#tpacientes_hogar").on('draw.dt',function(){
		$('.ver-examenfisicoHogar').each(function(){
			$(this).on('click',function(){
				var nTr = $(this).closest('tr');
				var row = tpacientes.row( nTr );
				var id  = tpacientes.row( row ).id();
				// DIBUJO
				//$('#valores-dibujo').prop('src','pacientes/'+idpacientes+'/examenfisico/'+id+'/dibujo.png');
				
				// OBTENER VALORES 
				$.when($('#signos-vitales').html("")).done(function(){
					$.get('controller/dashboardback.php',{
						oper:'getvaloresvisita',
						idvisita:id 
					},function(item){
						var grid = '';
						grid += '<tr><td><label class="">Frecuencia cardíaca</label></td>';
						grid += '<td><label class="">'+item.fc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Frecuencia respiratoria</label></td>';
						grid += '<td><label class="">'+item.fr+'</label></td></tr>';
							
						grid += '<tr><td><label class="">Saturación de Oxígeno (Oximetría)</label></td>';
						grid += '<td><label class="">'+item.so+'</label></td></tr>';	
						
						grid += '<tr><td><label class="">Sistólica</label></td>';
						grid += '<td><label class="">'+item.paa+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Diastólica</label></td>';
						grid += '<td><label class="">'+item.pab+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Temperatura corporal</label></td>';
						grid += '<td><label class="">'+item.tc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Nivel de dolor</label></td>';
						grid += '<td><label class="">'+item.dolor+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Glicemia capilar</label></td>';
						grid += '<td><label class="">'+item.gc+'</label></td></tr>';
						
						$('#signos-vitales').html(grid);
						
						
						grid = '';
						grid += '<tr><td><label class="">Recurso: </label>&nbsp;<b>'+item.recurso+'</b></td></tr>';
						grid += '<tr><td><label class="">Fecha: </label>&nbsp;<b>'+item.fecha+'</b></td></tr>';
						grid += '<tr><td><label class="">Hora: </label>&nbsp;<b>'+item.horallegada+'</b></td></tr>';
						grid += '<tr><td><label class="">Estatura: </label>&nbsp;<b>'+item.e+'</b></td></tr>';
						grid += '<tr><td><label class="">Peso: </label>&nbsp;<b>'+item.peso+'</b></td></tr>';
						grid += '<tr><td><label class="">IMC: </label>&nbsp;<b>'+item.imc+'</b></td></tr>';
						grid += '<tr><td><label class="">Comentario: </label>&nbsp;<b>'+item.comentario+'</b></td></tr>';
						grid += '<tr><td><label class="">Evaluación: &nbsp;<b></label>';
						
						switch (item.evaluacion){
							case '2':
								grid += 'Muy satisfecho';
							break;
							case '1':
								grid += 'Satisfecho';
							break;
							case '0':
								grid += 'Insatisfecho';
							break;
						}
						
						grid += '</b></td></tr>';
						
						grid += '<tr><td><label class="">Comentarios de la evaluacion:</label>&nbsp;<b>'+item.evaluacioncomentario+'</b></td></tr>';
						
						$('#visita-general').html(grid);
						
						
						
						var canvas = document.getElementById("sig-canvas");
						var ctx = canvas.getContext("2d");
						var myImage = new Image();
						myImage.onload = function() {
							ctx.drawImage(myImage, 0, 0);
						};
						myImage.src = item.firma;
						
						
					},'json');
				});	
				
				$('#modalVisita').modal('show');
			});
		});
		$(".ver-historial").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				location.href="pacientedatos.php?idpaciente="+id;
			});
		});
		$(".boton-reporte-diario").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				var fecha = $(this).attr('data-fecha');
				window.open("controller/reportediariopaciente.php?id="+id+"&fecha="+fecha);
			});
		});
		$('[data-toggle="tooltip"]').tooltip();

	});
	
	
	
	function rangos(data,type,row,val){
		//console.log(row);
		var color = '';
		var simbolo = '';
		if(data != ''){
			switch(val){
				case 'normal':
					color 	= 'green';
					simbolo = 'fa-thumbs-up';
					break;
				case 'Normal':
					color 	= 'green';
					simbolo = 'fa-thumbs-up';
					break;
				case 'alta':
					color 	= 'red';
					simbolo = 'fa-arrow-up';
					break;
				case 'Alta':
					color 	= 'red';
					simbolo = 'fa-arrow-up';
					break;
				case 'baja':
					color 	= 'red';
					simbolo	= 'fa-arrow-down';
					break;
				case 'Baja':
					color 	= 'red';
					simbolo	= 'fa-arrow-down';
					break;
				default:
					color 	= 'gray';
					simbolo	= 'fa fa-minus';
					break;
				
			}
		}else{
			color 	= 'silver';
			simbolo	= 'fa fa-minus';
		}
		
		var 	img = "<div style='float:left;margin-right:10px;' class='ui-pg-div ui-inline-custom'>";
				img	+= " <i class='"+color+" fa "+simbolo+"' data-toggle='tooltip' data-original-title='"+data+"' data-placement='right'></i> "+data;
				img += '</div>';
		
		return img;
	}
	


	var tpaciente_solo = $('#tpaciente_solo').DataTable({
		"searching": true,
		"lengthChange": false,
		//order: [[13,14,15,16,17]],
		"paging": false,

		"ajax"		: {
			"url"		:"controller/dashboardback.php?oper=listarultimasemana&idpaciente=0",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "acciones"
				
			},
			{ 	"data": "fecha" },
			{ 	"data": "nombre" },
			{ 	"data": "hogar",
				render: function(data,type,row){
					return rangos(data,type,row,row.res);
				} 
			 },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			 },
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			 },
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			 },
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			 },
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl);
				} 
			 },
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc);
				} 
			 },
			 { 	"data": "imc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rim);
				} 
			 },
			
			{ 	"data": "peso",
				render: function(data,type,row){
					return rangos(data,type,row,row.rpe);
				} 
			 },
			
			{ 	"data": "id" },
			{ 	"data": "rfc" },
			{ 	"data": "rfr" },
			{ 	"data": "rox" },
			{ 	"data": "rsi" },
			{ 	"data": "rdi" },
			{ 	"data": "rtm" },
			{ 	"data": "rdl" },
			{ 	"data": "rgc" },
			{ 	"data": "res" },
			{ 	"data": "rpe" },
			{ 	"data": "rim" }
			],
		"rowId": 'id',
		"columnDefs": [		
			{
				"targets"	:  0,
				"width"		:  "10%"
			},{
				"targets"	:  1,
				"width"		:  "10%"
			},{
				"targets"	: [2,3,13,14,15,16,17,18,19,20,21,22,23,24,25],
				"visible"	: false,
			}
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		}	
	});
	$("#tpaciente_solo").on('draw.dt',function(){
		$('.ver-examenfisicoPacSolo').each(function(){
			$(this).on('click',function(){
				var nTr = $(this).closest('tr');
				var row = tpaciente_solo.row( nTr );
				var id  = tpaciente_solo.row( row ).id();
				// DIBUJO
				//$('#valores-dibujo').prop('src','pacientes/'+idpacientes+'/examenfisico/'+id+'/dibujo.png');
				
				// OBTENER VALORES 
				$.when($('#signos-vitales').html("")).done(function(){
					$.get('controller/dashboardback.php',{
						oper:'getvaloresvisita',
						idvisita:id 
					},function(item){
						var grid = '';
						grid += '<tr><td><label class="">Frecuencia cardíaca</label></td>';
						grid += '<td><label class="">'+item.fc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Frecuencia respiratoria</label></td>';
						grid += '<td><label class="">'+item.fr+'</label></td></tr>';
							
						grid += '<tr><td><label class="">Saturación de Oxígeno (Oximetría)</label></td>';
						grid += '<td><label class="">'+item.so+'</label></td></tr>';	
						
						grid += '<tr><td><label class="">Sistólica</label></td>';
						grid += '<td><label class="">'+item.paa+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Diastólica</label></td>';
						grid += '<td><label class="">'+item.pab+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Temperatura corporal</label></td>';
						grid += '<td><label class="">'+item.tc+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Nivel de dolor</label></td>';
						grid += '<td><label class="">'+item.dolor+'</label></td></tr>';
						
						grid += '<tr><td><label class="">Glicemia capilar</label></td>';
						grid += '<td><label class="">'+item.gc+'</label></td></tr>';
						
						$('#signos-vitales').html(grid);
						
						
						grid = '';
						grid += '<tr><td><label class="">Recurso: </label>&nbsp;<b>'+item.recurso+'</b></td></tr>';
						grid += '<tr><td><label class="">Fecha: </label>&nbsp;<b>'+item.fecha+'</b></td></tr>';
						grid += '<tr><td><label class="">Hora: </label>&nbsp;<b>'+item.horallegada+'</b></td></tr>';
						grid += '<tr><td><label class="">Estatura: </label>&nbsp;<b>'+item.e+'</b></td></tr>';
						grid += '<tr><td><label class="">Peso: </label>&nbsp;<b>'+item.peso+'</b></td></tr>';
						grid += '<tr><td><label class="">IMC: </label>&nbsp;<b>'+item.imc+'</b></td></tr>';
						grid += '<tr><td><label class="">Comentario: </label>&nbsp;<b>'+item.comentario+'</b></td></tr>';
						grid += '<tr><td><label class="">Evaluación: &nbsp;<b></label>';
						
						switch (item.evaluacion){
							case '2':
								grid += 'Muy satisfecho';
							break;
							case '1':
								grid += 'Satisfecho';
							break;
							case '0':
								grid += 'Insatisfecho';
							break;
						}
						
						grid += '</b></td></tr>';
						
						grid += '<tr><td><label class="">Comentarios de la evaluacion:</label>&nbsp;<b>'+item.evaluacioncomentario+'</b></td></tr>';
						
						$('#visita-general').html(grid);
						
						
						
						var canvas = document.getElementById("sig-canvas");
						var ctx = canvas.getContext("2d");
						var myImage = new Image();
						myImage.onload = function() {
							ctx.drawImage(myImage, 0, 0);
						};
						myImage.src = item.firma;
						
						
					},'json');
				});	
				
				$.get("controller/dashboardback.php",{'oper':'imagenByVisita','idvisita':id},
				function(response){		
					$("#div_evidencia_imagen").empty();
					let resp = response.data;
					if(resp){
						$("#title-evidencia").show();
						let html_imagen = ''
						resp.map ( ( imagen ) => {
							html_imagen += `<img src="${imagen.url}" alt="Imagen evidencia" style="width: 50%;">`;
						})					
						$("#div_evidencia_imagen").html(html_imagen);
					}else{
						$("#title-evidencia").hide();
					}
				},'json');
				
				
				$('#modalVisita').modal('show');
			});
		});
		
		$(".ver-historial-p").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				location.href="pacientedatos.php?idpaciente="+id;
			});
		});
		$(".boton-reporte-diario").each(function(){
			$(this).on('click',function(){
				var id = $(this).attr('data-id');
				var fecha = $(this).attr('data-fecha');
				window.open("controller/reportediariopaciente.php?id="+id+"&fecha="+fecha);
			});
		});
		
		$('[data-toggle="tooltip"]').tooltip();
		
	});
	
	$("#all-mes").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_mes').show();
		
		tpacientes.ajax.url('controller/dashboardback.php?oper=listadovisitasapp&hogar=-1&mes=true').load(); 
		// tpacientes.ajax.reload( null, false );
		if(idpaciente === '0' ){
			$.ajax({
						"url"		:"controller/dashboardback.php",
						data: { 
							'oper' 		: 'listado_pacientes',
							'mes'	: 'true'
						},
						cache: false,
						dataType: "json",
						method: "POST"
					}).done(function(response){
						var data = response.data;
						arridvisita = [];
						arrCategorias = [];
						arrFrecuenciaCardiaca = [];
						arrFrecuenciaRespiratoria = [];
						arrOximetria = [];
						arrSistolica = [];
						arrDiastolica = [];
						arrTemperatura = [];
						arrDolor = [];
						arrGlicemia = [];
						arrEstatura = [];
						arrPeso = [];
						arrImc = [];

						longitud=data.length;
						for (i=0; i<longitud; i++) {	
						
							arrCategorias.push(data[i].nombre);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
		
							
						} 
						var signosvitales = {
							chart: {
								renderTo: "chart_mes",
								defaultSeriesType: "spline",   
								backgroundColor:"rgba(255, 255, 255, 0.0)", 
								 scrollablePlotArea: {
									minWidth: 3000,
									scrollPositionX: 0
								}								
							},
							plotOptions: {
								series: {
									cursor: 'pointer',
									point: {
										events: {
											click: function() {
												abrir_visita(this.id);
											}
										}
									}
								}
							},
							credits: {
								enabled: false
							},
							title: {
								text: null
							},
							subtitle: {
								text: null
							},
							xAxis: {
								labels: {
									style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },   
								},
								categories: arrCategorias,
								range: longitud,
								min: 0,
								max: longitud, 
								tickInterva: 0,
								step: 0, 
								pointInterval: 0,
								tickPixelInterval: 0
							},
							yAxis: {
								title: {
									text: null
								},
								plotLines: [{
									width: 1,
									color: "#808080"
								}],
								max: null
							},
							tooltip: {
								valueDecimals: 1, 
								valuePrefix: null,
								valueSuffix: null
							},
							scrollbar: {
								enabled: true,
							},
							legend: {
								enable: true,
								verticalAlign: 'top', 
								// y: 100, 
								align: 'right' 
								
							},
							series: [{ 
								data: arrFrecuenciaCardiaca,
								name: "Frec. Cardíaca",
								color: "#D50000"
							},{ 
								data: arrFrecuenciaRespiratoria,
								name: "Frec. Respiratoria",
								color: "#AA00FF"
							},{ 
								data: arrOximetria,
								name: "Sat. Oxigeno",
								color: "#304FFE",
							},{ 
								data: arrSistolica,
								name: "Sistólica",
								color: "#2962FF",
							},{ 
								data: arrDiastolica,
								name: "Diastólica",
								color: "#00BFA5",
							},{  
								data: arrTemperatura,
								name: "Temperatura",
								color: "#00C853",
							},{ 
								data: arrDolor,
								name: "Nivel de Dolor",
								color: "#AEEA00",
							},{ 
								data: arrGlicemia,
								name: "Glicemia Capilar",
								color: "#FFAB00",
							}
							]
						};
						
						var chart = new Highcharts.Chart(signosvitales);
						$(".highcharts-background").attr('stroke','');
					});
		}
	
	});
	
	$("#all_trimestre").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_mes').show();
		
		tpacientes.ajax.url('controller/dashboardback.php?oper=listadovisitasapp&hogar=-1&trimestre=true').load();
		if(idpaciente === '0' ){
			$.ajax({
						"url"		:"controller/dashboardback.php",
						data: { 
							'oper' 		: 'listado_pacientes',
							'trimestre'	: 'true'
						},
						cache: false,
						dataType: "json",
						method: "POST"
					}).done(function(response){
						var data = response.data;
						arridvisita = [];
						arrCategorias = [];
						arrFrecuenciaCardiaca = [];
						arrFrecuenciaRespiratoria = [];
						arrOximetria = [];
						arrSistolica = [];
						arrDiastolica = [];
						arrTemperatura = [];
						arrDolor = [];
						arrGlicemia = [];
						arrEstatura = [];
						arrPeso = [];
						arrImc = [];

						longitud=data.length;
						for (i=0; i<longitud; i++) {	
						
							arrCategorias.push(data[i].nombre);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
							
						} 
						var signosvitales = {
							chart: {
								renderTo: "chart_mes",
								defaultSeriesType: "spline",   
								backgroundColor:"rgba(255, 255, 255, 0.0)",
								scrollablePlotArea: {
									minWidth: 3000,
									scrollPositionX: 0
								}								
							},
							plotOptions: {
								series: {
									cursor: 'pointer',
									point: {
										events: {
											click: function() {
												abrir_visita(this.id);
											}
										}
									}
								}
							},
							credits: {
								enabled: false
							},
							title: {
								text: null
							},
							subtitle: {
								text: null
							},
							xAxis: {
								labels: {
									style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },   
								},
								categories: arrCategorias,
								range: longitud,
								min: 0,
								max: longitud, 
								tickInterva: 0,
								step: 0, 
								pointInterval: 0,
								tickPixelInterval: 0
							},
							yAxis: {
								title: {
									text: null
								},
								plotLines: [{
									width: 1,
									color: "#808080"
								}],
								max: null
							},
							tooltip: {
								valueDecimals: 1, 
								valuePrefix: null,
								valueSuffix: null
							},
							scrollbar: {
								enabled: true,
							},
							legend: {
								enable: true,
								verticalAlign: 'top', 
								// y: 100, 
								align: 'right' 
								
							},
							series: [{ 
								data: arrFrecuenciaCardiaca,
								name: "Frec. Cardíaca",
								color: "#D50000"
							},{ 
								data: arrFrecuenciaRespiratoria,
								name: "Frec. Respiratoria",
								color: "#AA00FF"
							},{ 
								data: arrOximetria,
								name: "Sat. Oxigeno",
								color: "#304FFE",
							},{ 
								data: arrSistolica,
								name: "Sistólica",
								color: "#2962FF",
							},{ 
								data: arrDiastolica,
								name: "Diastólica",
								color: "#00BFA5",
							},{ 
								data: arrTemperatura,
								name: "Temperatura",
								color: "#00C853",
							},{ 
								data: arrDolor,
								name: "Nivel de Dolor",
								color: "#AEEA00",
							},{ 
								data: arrGlicemia,
								name: "Glicemia Capilar",
								color: "#FFAB00",
							}
							]
						};
						
						var chart = new Highcharts.Chart(signosvitales);
						$(".highcharts-background").attr('stroke','');
					});
		}
	
	});
	
	
	$("#all_semestre").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_mes').show();
		
		tpacientes.ajax.url('controller/dashboardback.php?oper=listadovisitasapp&hogar=-1&semestre=true').load();
		if(idpaciente === '0' ){
			$.ajax({
						"url"		:"controller/dashboardback.php",
						data: { 
							'oper' 		: 'listado_pacientes',
							'semestre'	: 'true'
						},
						cache: false,
						dataType: "json",
						method: "POST"
					}).done(function(response){
						var data = response.data;
						arridvisita = [];
						arrCategorias = [];
						arrFrecuenciaCardiaca = [];
						arrFrecuenciaRespiratoria = [];
						arrOximetria = [];
						arrSistolica = [];
						arrDiastolica = [];
						arrTemperatura = [];
						arrDolor = [];
						arrGlicemia = [];
						arrEstatura = [];
						arrPeso = [];
						arrImc = [];

						longitud=data.length;
						for (i=0; i<longitud; i++) {	
						
							arrCategorias.push(data[i].nombre);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
							
		
						} 
						var signosvitales = {
							chart: {
								renderTo: "chart_mes",
								defaultSeriesType: "spline",   
								backgroundColor:"rgba(255, 255, 255, 0.0)",  
								 scrollablePlotArea: {
									minWidth: 3000,
									scrollPositionX: 0
								}
							},
							plotOptions: {
								series: {
									cursor: 'pointer',
									point: {
										events: {
											click: function() {
												abrir_visita(this.id);
											}
										}
									}
								}
							},
							credits: {
								enabled: false
							},
							title: {
								text: null
							},
							subtitle: {
								text: null
							},
							xAxis: {
								labels: {
									style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },   
								},
								categories: arrCategorias,
								range: longitud,
								min: 0,
								max: longitud, 
								tickInterva: 0,
								step: 0, 
								pointInterval: 0,
								tickPixelInterval: 0
							},
							yAxis: {
								title: {
									text: null
								},
								plotLines: [{
									width: 1,
									color: "#808080"
								}],
								max: null
							},
							tooltip: {
								valueDecimals: 1, 
								valuePrefix: null,
								valueSuffix: null
							},
							scrollbar: {
								enabled: true,
							},
							legend: {
								enable: true,
								verticalAlign: 'top', 
								// y: 100, 
								align: 'right' 
								
							},
							series: [{ 
								data: arrFrecuenciaCardiaca,
								name: "Frec. Cardíaca",
								color: "#D50000"
							},{ 
								data: arrFrecuenciaRespiratoria,
								name: "Frec. Respiratoria",
								color: "#AA00FF"
							},{ 
								data: arrOximetria,
								name: "Sat. Oxigeno",
								color: "#304FFE",
							},{ 
								data: arrSistolica,
								name: "Sistólica",
								color: "#2962FF",
							},{ 
								data: arrDiastolica,
								name: "Diastólica",
								color: "#00BFA5",
							},{ 
								data: arrTemperatura,
								name: "Temperatura",
								color: "#00C853",
							},{ 
								data: arrDolor,
								name: "Nivel de Dolor",
								color: "#AEEA00",
							},{ 
								data: arrGlicemia,
								name: "Glicemia Capilar",
								color: "#FFAB00",
							}
							]
						};
						
						var chart = new Highcharts.Chart(signosvitales);
						$(".highcharts-background").attr('stroke','');
					});
		}
	
	});
	
	
	$("#all_year").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_mes').show();
		
		tpacientes.ajax.url('controller/dashboardback.php?oper=listadovisitasapp&hogar=-1&year=true').load();
		if(idpaciente === '0' ){
			$.ajax({
						"url"		:"controller/dashboardback.php",
						data: { 
							'oper' 		: 'listado_pacientes',
							'year'	: 'true'
						},
						cache: false,
						dataType: "json",
						method: "POST"
					}).done(function(response){
						var data = response.data;
						arridvisita = [];
						arrCategorias = [];
						arrFrecuenciaCardiaca = [];
						arrFrecuenciaRespiratoria = [];
						arrOximetria = [];
						arrSistolica = [];
						arrDiastolica = [];
						arrTemperatura = [];
						arrDolor = [];
						arrGlicemia = [];
						arrEstatura = [];
						arrPeso = [];
						arrImc = [];

						longitud=data.length;
						for (i=0; i<longitud; i++) {	
						
							arrCategorias.push(data[i].nombre);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
							
	  
						} 
						var signosvitales = {
							chart: {
								renderTo: "chart_mes",
								defaultSeriesType: "spline",   
								backgroundColor:"rgba(255, 255, 255, 0.0)",  
								 scrollablePlotArea: {
									minWidth: 3000,
									scrollPositionX: 0
								}
							},
							plotOptions: {
								series: {
									cursor: 'pointer',
									point: {
										events: {
											click: function() {
												abrir_visita(this.id);
											}
										}
									}
								}
							},
							credits: {
								enabled: false
							},
							title: {
								text: null
							},
							subtitle: {
								text: null
							},
							xAxis: {
								labels: {
									style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },   
								},
								categories: arrCategorias,
								range: longitud,
								min: 0,
								max: longitud, 
								tickInterva: 0,
								step: 0, 
								pointInterval: 0,
								tickPixelInterval: 0
							},
							yAxis: {
								title: {
									text: null
								},
								plotLines: [{
									width: 1,
									color: "#808080"
								}],
								max: null
							},
							tooltip: {
								valueDecimals: 1, 
								valuePrefix: null,
								valueSuffix: null
							},
							scrollbar: {
								enabled: true,
							},
							legend: {
								enable: true,
								verticalAlign: 'top', 
								// y: 100, 
								align: 'right' 
								
							},
							series: [{ 
								data: arrFrecuenciaCardiaca,
								name: "Frec. Cardíaca",
								color: "#D50000"
							},{ 
								data: arrFrecuenciaRespiratoria,
								name: "Frec. Respiratoria",
								color: "#AA00FF"
							},{ 
								data: arrOximetria,
								name: "Sat. Oxigeno",
								color: "#304FFE",
							},{ 
								data: arrSistolica,
								name: "Sistólica",
								color: "#2962FF",
							},{ 
								data: arrDiastolica,
								name: "Diastólica",
								color: "#00BFA5",
							},{ 
								data: arrTemperatura,
								name: "Temperatura",
								color: "#00C853",
							},{ 
								data: arrDolor,
								name: "Nivel de Dolor",
								color: "#AEEA00",
							},{ 
								data: arrGlicemia,
								name: "Glicemia Capilar",
								color: "#FFAB00",
							}
							]
						};
						
						var chart = new Highcharts.Chart(signosvitales);
						$(".highcharts-background").attr('stroke','');
					});
		}
	
	});
	
	$("#all").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_mes').show();
		
		tpacientes.ajax.url('controller/dashboardback.php?oper=listadovisitasapp&hogar=-1').load();
		if(idpaciente === '0' ){
			$.ajax({
						"url"		:"controller/dashboardback.php",
						data: { 
							'oper' 		: 'listado_pacientes',
						},
						cache: false,
						dataType: "json",
						method: "POST"
					}).done(function(response){
						var data = response.data;
						arridvisita = [];
						arrCategorias = [];
						arrFrecuenciaCardiaca = [];
						arrFrecuenciaRespiratoria = [];
						arrOximetria = [];
						arrSistolica = [];
						arrDiastolica = [];
						arrTemperatura = [];
						arrDolor = [];
						arrGlicemia = [];
						arrEstatura = [];
						arrPeso = [];
						arrImc = [];

						longitud=data.length;
						for (i=0; i<longitud; i++) {	
						
							arrCategorias.push(data[i].nombre);							
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
							
	   
						} 
						var signosvitales = {
							chart: {
								renderTo: "chart_mes",
								defaultSeriesType: "spline",   
								backgroundColor:"rgba(255, 255, 255, 0.0)",  
								 scrollablePlotArea: {
									minWidth: 3000,
									scrollPositionX: 0
								}
							},
							plotOptions: {
								series: {
									cursor: 'pointer',
									point: {
										events: {
											click: function() {
												abrir_visita(this.id);
											}
										}
									}
								}
							},
							credits: {
								enabled: false
							},
							title: {
								text: null
							},
							subtitle: {
								text: null
							},
							xAxis: {
								labels: {
									style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },   
								},
								categories: arrCategorias,
								range: longitud,
								min: 0,
								max: longitud, 
								tickInterva: 0,
								step: 0, 
								pointInterval: 0,
								tickPixelInterval: 0
							},
							yAxis: {
								title: {
									text: null
								},
								plotLines: [{
									width: 1,
									color: "#808080"
								}],
								max: null
							},
							tooltip: {
								valueDecimals: 1, 
								valuePrefix: null,
								valueSuffix: null
							},
							scrollbar: {
								enabled: true,
							},
							legend: {
								enable: true,
								verticalAlign: 'top', 
								// y: 100, 
								align: 'right' 
								
							},
							series: [{ 
								data: arrFrecuenciaCardiaca,
								name: "Frec. Cardíaca",
								color: "#D50000"
							},{ 
								data: arrFrecuenciaRespiratoria,
								name: "Frec. Respiratoria",
								color: "#AA00FF"
							},{ 
								data: arrOximetria,
								name: "Sat. Oxigeno",
								color: "#304FFE",
							},{ 
								data: arrSistolica,
								name: "Sistólica",
								color: "#2962FF",
							},{ 
								data: arrDiastolica,
								name: "Diastólica",
								color: "#00BFA5",
							},{ 
								data: arrTemperatura,
								name: "Temperatura",
								color: "#00C853",
							},{ 
								data: arrDolor,
								name: "Nivel de Dolor",
								color: "#AEEA00",
							},{ 
								data: arrGlicemia,
								name: "Glicemia Capilar",
								color: "#FFAB00",
							}
							]
						};
						
						var chart = new Highcharts.Chart(signosvitales);
						$(".highcharts-background").attr('stroke','');
					});
		}
	
	});
	
	
	$("#pac_mes").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_paciente').hide();
		$('#chart_paciente_mes').show();
		
		tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente+'&mes=true').load(); 
		
		if(idpaciente != '0'){
				$("#todos").hide();
				$("#unsolopaciente").show();
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_rango',
						'idpaciente' : idpaciente,
						'mes' : 'true'
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita = [];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente_mes", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							 scrollablePlotArea: {
								minWidth: 3000,
								scrollPositionX: 0
							}
						},
					
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											
											console.log('ID: '+this.id)
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud, 
							tickInterva: 0,
							step: 0,
							pointInterval: 0
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
	
	});
	
	$("#pac_trimestre").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_paciente').hide();
		$('#chart_paciente_mes').show();
		
		tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente+'&trimestre=true').load(); 
		
		if(idpaciente != '0'){
				$("#todos").hide();
				$("#unsolopaciente").show();
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_rango',
						'idpaciente' : idpaciente,
						'trimestre' : 'true'
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita = [];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
	  
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente_mes", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							scrollablePlotArea: {
								minWidth: 3000,
								scrollPositionX: 0
							}
						},
					
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud, 
							tickInterva: 0,
							step: 0,
							pointInterval: 0
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
	
	});
	
	
	$("#pac_semestre").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_paciente').hide();
		$('#chart_paciente_mes').show();
		
		tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente+'&semestre=true').load(); 
		
		if(idpaciente != '0'){
				$("#todos").hide();
				$("#unsolopaciente").show();
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_rango',
						'idpaciente' : idpaciente,
						'semestre' : 'true'
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita = [];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
	  
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente_mes", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							scrollablePlotArea: {
								minWidth: 3000,
								scrollPositionX: 0
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud, 
							tickInterva: 0,
							step: 0,
							pointInterval: 0
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
	
	});
	
	$("#pac_year").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_paciente').hide();
		$('#chart_paciente_mes').show();
		
		tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente+'&year=true').load(); 
		
		if(idpaciente != '0'){
				$("#todos").hide();
				$("#unsolopaciente").show();
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_rango',
						'idpaciente' : idpaciente,
						'year' : 'true'
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita = [];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
	  
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente_mes", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							scrollablePlotArea: {
								minWidth: 3000,
								scrollPositionX: 0
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud,  
							tickInterva: 0,
							step: 0,
							pointInterval: 0
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
	
	});
	
	$("#pac_all").on('click',function(){
		var idpaciente = $('#filtropaciente').val();
		$('#chart').hide();
		$('#chart_paciente').hide();
		$('#chart_paciente_mes').show();
		
		tpaciente_solo.ajax.url('controller/dashboardback.php?oper=listarultimasemana&idpaciente='+idpaciente).load(); 
		
		if(idpaciente != '0'){
				$("#todos").hide();
				$("#unsolopaciente").show();
				
				$.ajax({
					"url"		:"controller/dashboardback.php",
					data: { 
						'oper' 		: 'get_visitas_semana',
						'idpaciente' : idpaciente
					},
					cache: false,
					dataType: "json",
					method: "POST"
				}).done(function(response){
					var data = response.data;
					arridvisita = [];
					arrCategorias = [];
					arrFrecuenciaCardiaca = [];
					arrFrecuenciaRespiratoria = [];
					arrOximetria = [];
					arrSistolica = [];
					arrDiastolica = [];
					arrTemperatura = [];
					arrDolor = [];
					arrGlicemia = [];
					arrEstatura = [];
					arrPeso = [];
					arrImc = [];
					longitud=data.length;
					for (i=0; i<longitud; i++) {	
							arrCategorias.push(data[i].fecha);
							data[i].idvisita!= '0' ?  arridvisita.push(Number(data[i].idvisita)) : arridvisita.push(null);
							data[i].fc != '0' ? arrFrecuenciaCardiaca.push({'y':Number(data[i].fc),'id': data[i].idvisita})  :  arrFrecuenciaCardiaca.push({'y':null , 'id' : null});							
							data[i].fr != '0' ? arrFrecuenciaRespiratoria.push({'y':Number(data[i].fr),'id': data[i].idvisita})  :  arrFrecuenciaRespiratoria.push({'y':null , 'id' : null});
							data[i].so != '0' ? arrOximetria.push({'y':Number(data[i].so),'id': data[i].idvisita})  :  arrOximetria.push({'y':null , 'id' : null});
							data[i].paa!= '0' ? arrSistolica.push({'y':Number(data[i].paa),'id': data[i].idvisita})  :  arrSistolica.push({'y':null , 'id' : null});
							data[i].pab!= '0' ? arrDiastolica.push({'y':Number(data[i].pab),'id': data[i].idvisita})  :  arrDiastolica.push({'y':null , 'id' : null});
							data[i].tc!= '0' ? arrTemperatura.push({'y':Number(data[i].tc),'id': data[i].idvisita})  :  arrTemperatura.push({'y':null , 'id' : null});
							data[i].dolor!= '0' ? arrDolor.push({'y':Number(data[i].dolor),'id': data[i].idvisita})  :  arrDolor.push({'y':null , 'id' : null});
							data[i].gc!= '0' ? arrGlicemia.push({'y':Number(data[i].gc),'id': data[i].idvisita})  :  arrGlicemia.push({'y':null , 'id' : null});
							data[i].e!= '0' ? arrEstatura.push({'y':Number(data[i].e),'id': data[i].idvisita})  :  arrEstatura.push({'y':null , 'id' : null});
							data[i].peso!= '0' ? arrPeso.push({'y':Number(data[i].peso),'id': data[i].idvisita})  :  arrPeso.push({'y':null , 'id' : null});
							data[i].imc!= '0' ? arrImc.push({'y':Number(data[i].imc),'id': data[i].idvisita})  :  arrImc.push({'y':null , 'id' : null});
					}
					var signosvitales = {
						chart: {
							renderTo: "chart_paciente_mes", 
							defaultSeriesType: "spline",
							backgroundColor:"rgba(255, 255, 255, 0.0)",
							 scrollablePlotArea: {
								minWidth: 3000,
								scrollPositionX: 0
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											abrir_visita(this.id);
										}
									}
								}
							}
						},
						credits: {
							enabled: false
						},
						title: {
							text: null
						},
						subtitle: {
							text: null
						},
						xAxis: {
							labels: {
								style: { color: "#000000" , fontSize: "14px" , overflow: 'justify', marginLeft:"30px" },
							},
							categories: arrCategorias,
							range: longitud,
							min : 0,
							max: longitud,  
							tickInterva: 0,
							step: 0,
							pointInterval: 0
						},
						yAxis: {
							title: {
								text: null
							},
							plotLines: [{
								width: 1,
								color: "#808080"
							}],
							max: null
						},
						tooltip: {
							valueDecimals: 1,
							valuePrefix: null,
							valueSuffix: null
						},
						scrollbar: {
							enabled: true,
						},
						legend: {
							enable: true,
							verticalAlign: 'top', 
							// y: 100, 
							align: 'right' 
							
						},
						series: [{ 
							data: arrFrecuenciaCardiaca,
							name: "Frec. Cardíaca",
							color: "#D50000"
						},{ 
							data: arrFrecuenciaRespiratoria,
							name: "Frec. Respiratoria",
							color: "#AA00FF"
						},{ 
							data: arrOximetria,
							name: "Sat. Oxigeno",
							color: "#304FFE",
						},{ 
							data: arrSistolica,
							name: "Sistólica",
							color: "#2962FF",
						},{ 
							data: arrDiastolica,
							name: "Diastólica",
							color: "#00BFA5",
						},{ 
							data: arrTemperatura,
							name: "Temperatura",
							color: "#00C853",
						},{ 
							data: arrDolor,
							name: "Nivel de Dolor",
							color: "#AEEA00",
						},{ 
							data: arrGlicemia,
							name: "Glicemia Capilar",
							color: "#FFAB00",
						}
						]
					};					
					var chart_paciente = new Highcharts.Chart(signosvitales);
					$(".highcharts-background").attr('stroke','');
				});
				
			}
	
	});
	
	function abrir_visita(id){
		$.get('controller/dashboardback.php',{
			oper:'getvaloresvisita',
			idvisita:id 
		},function(item){
			var grid = '';
			grid += '<tr><td><label class="">Frecuencia cardíaca</label></td>';
			grid += '<td><label class="">'+item.fc+'</label></td></tr>';			
			grid += '<tr><td><label class="">Frecuencia respiratoria</label></td>';
			grid += '<td><label class="">'+item.fr+'</label></td></tr>';				
			grid += '<tr><td><label class="">Saturación de Oxígeno (Oximetría)</label></td>';
			grid += '<td><label class="">'+item.so+'</label></td></tr>';				
			grid += '<tr><td><label class="">Sistólica</label></td>';
			grid += '<td><label class="">'+item.paa+'</label></td></tr>';			
			grid += '<tr><td><label class="">Diastólica</label></td>';
			grid += '<td><label class="">'+item.pab+'</label></td></tr>';			
			grid += '<tr><td><label class="">Temperatura corporal</label></td>';
			grid += '<td><label class="">'+item.tc+'</label></td></tr>';			
			grid += '<tr><td><label class="">Nivel de dolor</label></td>';
			grid += '<td><label class="">'+item.dolor+'</label></td></tr>';			
			grid += '<tr><td><label class="">Glicemia capilar</label></td>';
			grid += '<td><label class="">'+item.gc+'</label></td></tr>';			
			$('#signos-vitales').html(grid);
			grid = '';
			grid += '<tr><td><label class="">Recurso: </label>&nbsp;<b>'+item.recurso+'</b></td></tr>';
			grid += '<tr><td><label class="">Fecha: </label>&nbsp;<b>'+item.fecha+'</b></td></tr>';
			grid += '<tr><td><label class="">Hora: </label>&nbsp;<b>'+item.horallegada+'</b></td></tr>';
			grid += '<tr><td><label class="">Estatura: </label>&nbsp;<b>'+item.e+'</b></td></tr>';
			grid += '<tr><td><label class="">Peso: </label>&nbsp;<b>'+item.peso+'</b></td></tr>';
			grid += '<tr><td><label class="">IMC: </label>&nbsp;<b>'+item.imc+'</b></td></tr>';
			grid += '<tr><td><label class="">Comentario: </label>&nbsp;<b>'+item.comentario+'</b></td></tr>'; 
			grid += '<tr><td><label class="">Evaluación: &nbsp;<b></label>';			
			switch (item.evaluacion){
				case '2':
					grid += 'Muy satisfecho';
				break;
				case '1':
					grid += 'Satisfecho';
				break;
				case '0':
					grid += 'Insatisfecho';
				break;
			}			
			grid += '</b></td></tr>';			
			grid += '<tr><td><label class="">Comentarios de la evaluacion:</label>&nbsp;<b>'+item.evaluacioncomentario+'</b></td></tr>';			
			$('#visita-general').html(grid);
			// var canvas = document.getElementById("sig-canvas");
			// var ctx = canvas.getContext("2d");
			// var myImage = new Image();
			// myImage.onload = function() {
				// ctx.drawImage(myImage, 0, 0);
			// };
			// myImage.src = item.firma;
			$.get("controller/dashboardback.php",{'oper':'imagenByVisita','idvisita':id},  
				function(response){		
					$("#div_evidencia_imagen").empty();
					let resp = response.data;
					if(resp){
						$("#title-evidencia").show();
						let html_imagen = ''
						resp.map ( ( imagen ) => {
							html_imagen += `<img src="${imagen.url}" alt="Imagen evidencia" style="width: 50%;">`;
						})					
						$("#div_evidencia_imagen").html(html_imagen);
					}else{
						$("#title-evidencia").hide();
					}
				},'json');
				
				
				$('#modalVisita').modal('show');
			
		},'json');
	}
	
	

});
