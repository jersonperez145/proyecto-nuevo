	// TOOLTIPS
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});

$(".dz-nota-history-back").click(function(){
    $(".dz-nota-user-box").removeClass('d-none');
    $(".dz-nota-history-box").addClass('d-none');
});

$(".mapadash").click(function(){
    var tipo = $(this).attr('id');
    mapa(tipo);
});

function mapa(tipo) {
	var styledMapType = new google.maps.StyledMapType(
            [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#ffffff"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#dadada"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#c9c9c9"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  }
			],
            {name: 'Styled Map'}
        );

	$.ajax({
		url: "controller/dashboardback.php",
		cache: false,
		dataType: "json",
		method: "POST",
		data: {
			"opcion": "MAPA",
			"tipo"	:tipo
		}
	}).done(function(data) {
		var map = new google.maps.Map(document.getElementById('mapa'), {
          center: {lat: 8.9992394, lng: -79.5055379},
          zoom: 14
        });
		
		$.map(data, function (datos) {
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
				icon: datos.icon,
				title: datos.title,
				label: {
					text: datos.label,
					fontSize: '1px',
					color: '#1f4380'
				},
				map: map
			});
			
			marker.addListener('click', function() {
				map.setZoom(8);
				map.setCenter(marker.getPosition());
				var xUnidad = marker.label.text;
				console.log(marker.title.text);
			});
		});

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
	});
}



(function($) {
    /* "use strict" */
//LISTADO DE LLAMADO DE FUNCIONES    

var screenWidth = $(window).width();
	
var options = {
	  series: [{
	  name: 'Income',
	  data: [420, 550, 850, 220, 650]
	}, {
	  name: 'Expenses',
	  data: [170, 850, 101, 90, 250]
	}],
	  chart: {
	  type: 'bar',
	  height: 350,
	  toolbar: {
		show: false
	  }
	},
	plotOptions: {
	  bar: {
		horizontal: false,
		columnWidth: '55%',
		endingShape: 'rounded'
	  },
	},
	dataLabels: {
	  enabled: false
	},
	
	legend: {
		show: true,
		fontSize: '12px',
		fontWeight: 300,
		
		labels: {
			colors: 'black',
		},
		position: 'bottom',
		horizontalAlign: 'center', 	
		markers: {
			width: 19,
			height: 19,
			strokeWidth: 0,
			radius: 19,
			strokeColor: '#fff',
			fillColors: ['#369DC9','#D45BFF'],
			offsetX: 0,
			offsetY: 0
		}
	},
	yaxis: {
		labels: {
	   style: {
		  colors: '#3e4954',
		  fontSize: '14px',
		   fontFamily: 'Poppins',
		  fontWeight: 100,
		  
		},
	  },
	},
	stroke: {
	  show: true,
	  width: 2,
	  colors: ['transparent']
	},
	xaxis: {
	  categories: ['06', '07', '08', '09', '10'],
	},
	fill: {
	  colors:['#369DC9','#D45BFF'],
	  opacity: 1
	},
	tooltip: {
	  y: {
		formatter: function (val) {
		  return "$ " + val + " thousands"
		}
	  }
	}
	};

	var chart = new ApexCharts(document.querySelector("#line-chart"), options);
	chart.render();
	
	
	var optionsArea = {
	  series: [{
		name: "Recovered Patient",
		data: [500, 230, 600, 360, 700, 890, 750, 420, 600, 300, 420, 220]
	  },
	  {
		name: "New Patient",
		data: [250, 380, 200, 300, 200, 520,380, 770, 250, 520, 300, 900]
	  }
	],
	  chart: {
	  height: 350,
	  type: 'area',
	  group: 'social',
	  toolbar: {
		show: false
	  },
	  zoom: {
		enabled: false
	  },
	},
	dataLabels: {
	  enabled: false
	},
	stroke: {
	  width: [2, 2],
	  colors:['#F46B68','#2BC155'],
	  curve: 'straight'
	},
	legend: {
	  tooltipHoverFormatter: function(val, opts) {
		return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
	  },
	  markers: {
		fillColors:['#F46B68','#2BC155'],
		width: 19,
		height: 19,
		strokeWidth: 0,
		radius: 19
	  }
	},
	markers: {
	  size: 6,
	  border:0,
	  colors:['#F46B68','#2BC155'],
	  hover: {
		size: 6,
	  }
	},
	xaxis: {
	  categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October','November','December',
		'10 Jan', '11 Jan', '12 Jan'
	  ],
	},
	yaxis: {
		labels: {
	   style: {
		  colors: '#3e4954',
		  fontSize: '14px',
		   fontFamily: 'Poppins',
		  fontWeight: 100,
		  
		},
	  },
	},
	fill: {
		colors:['#F46B68','#2BC155'],
		type:'solid',
		opacity: 0.07
	},
	grid: {
	  borderColor: '#f1f1f1',
	}
	};
	var chartArea = new ApexCharts(document.querySelector("#chartBar"), optionsArea);
	chartArea.render();



incidentes_notificacion();
chat_usuarios();
listado_notas();
top_colaboradores();




 var dzChartlist = function(){
	
	

	
	/* Function ============ */
		return {
			init:function(){
				
			},
			
			
			load:function(){
				//lineChart();
				//chartBar();
			},
			
			resize:function(){
			}
		}
	
	}();

	jQuery(document).ready(function(){
	});
		
	jQuery(window).on('load',function(){
		setTimeout(function(){
			dzChartlist.load();
		}, 1000); 
		
	});

	jQuery(window).on('resize',function(){
		
		
	});  

	function rangos(data,type,row,val){
		//console.log(row);
		var color = '';
		var simbolo = '';
		var val = val.toLowerCase();
		if(data != ''){
			switch(val){
				case 'normal':
					color 	= 'green';
					simbolo = 'fa-thumbs-up';
					break;
				case 'alta':
					color 	= 'red';
					simbolo = 'fa-arrow-up';
					break;
				case 'baja':
					color 	= 'red';
					simbolo	= 'fa-arrow-down';
					break;
				default:
					color 	= 'gray';
					simbolo	= 'fa fa-minus';
					break;
				
			}
		}else{
			color 	= 'silver';
			simbolo	= 'fa fa-minus';
		}
		
		var 	img = "<div style='float:left;margin-right:10px;' class='ui-pg-div ui-inline-custom'>";
				img	+= " <i class='"+color+" fa "+simbolo+"' data-toggle='tooltip' data-original-title='"+data+"' data-placement='right'></i> "+data;
				img += '</div>';
		
		return img;
	}

	function cargarDatos() {
		var formatNumber = {
			separador: ",", // separador para los miles
			sepDecimal: '.', // separador para los decimales
			formatear:function (num){
				num +='';
				var splitStr = num.split('.');
				var splitLeft = splitStr[0];
				var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
				var regx = /(\d+)(\d{3})/;
				while (regx.test(splitLeft)) {
					splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
				}
				return this.simbol + splitLeft +splitRight;
			},
			new:function(num, simbol){
				this.simbol = simbol ||'';
				return this.formatear(num);
			}
		}
		
		
			$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_VISITAS_RETRASO"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].retraso;
			var xvalor = data.rows[i].retraso;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalvretraso").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalvretraso").html(formatNumber.new(xvalor));
		});
		
			$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TURNOS_CANCELADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].cancelados;
			var xvalor = data.rows[i].cancelados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totaltcancelados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totaltcancelados").html(formatNumber.new(xvalor));
		});
		
				$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_PACIENTES_COVID"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].pcovid;
			var xvalor = data.rows[i].pcovid;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalpcovid").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalpcovid").html(formatNumber.new(xvalor));
		});
			
		$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TRATAMIENTOS_EDITADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].tratamientoseditados;
			var xvalor = data.rows[i].tratamientoseditados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalteditados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalteditados").html(formatNumber.new(xvalor));
		});
		
			
		$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TRATAMIENTOS_FINALIZADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].tratamientosfinalizados;
			var xvalor = data.rows[i].tratamientosfinalizados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totaltfinalizados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totaltfinalizados").html(formatNumber.new(xvalor));
		});
	}

	var tablapacientesfuerarango = $("#tablaPacientes_alerta").DataTable({ 
		//"searching": true,
		//"lengthChange": false,
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		: {
			"url"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_FUERA_RANGO&hogar=-1",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "nombre" },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			},
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			},
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			},
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			},
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl); 
				} 
			},
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc); 
				} 
			},
			{ 	"data": "fecha" },
			{ 	"data": "acciones"}
			],
		"rowId": 'id',
		"columnDefs": [		
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
	  initComplete: function() {
      $('#totalpaciente_alertas').text( this.api().data().length )
   }
	});
	
	var tablavisitasRetraso = $("#visitasRetraso").DataTable({
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_VISITAS_RETRASO",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "fecha_inicio" },
			{ 	"data": "recurso" },
			{ 	"data": "tiempo_retardo" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "estatus" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
	  initComplete: function() {
      $('#totalvretraso').text( this.api().data().length )
   }
	});
	
	var tablaturnosCanceladas = $("#turnosCancelados").DataTable({
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TURNOS_CANCELADOS",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "tipo_visita" },
			{ 	"data": "fecha_inicio" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "recurso" },
			{ 	"data": "estatus" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
        initComplete: function() {
            $('#totaltcancelados').text( this.api().data().length )
        }
	});
	
	var tablapacientescovid = $("#pacientesCovid").DataTable({
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_COVID",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "fechanac" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#totalpcovid').text( this.api().data().length )
   }
	});
	
	var tabla_tratamientos_editados = $("#tabla_tratamientos_editados").DataTable({
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TRATAMIENTOS_EDITADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "fecha_actualizacion_tarjeta" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#totalteditados').text( this.api().data().length )
        }
	});
	
    var tabla_tratamientos_finalizados = $("#tabla_tratamientos_finalizados").DataTable({
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TRATAMIENTOS_FINALIZADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "nombremedicamento" },
			{ 	"data": "via" },
			{ 	"data": "frecuencia" },
			{ 	"data": "dosis" },
			{ 	"data": "fecha_inicio" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "observaciones" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#totaltfinalizados').text( this.api().data().length )
   }
	});
	
	function incidentes_notificacion() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'INCIDENTES_NOTIFICACION',
    		},
    		success: function (response) {
    			$('#incidentesnotific').html(response);		
    		},
    		error: function () {
    		    
    		}
    	});
    }

    var tabla_incidentes = $("#tabla_incidentes").DataTable({
        //"lengthMenu": [3, 4],
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/dashboardback.php?opcion=TABLA_INCIDENTES",
        "columns"	: [
            { 	"data": "id" },
            { 	"data": "tipo" },
            { 	"data": "incidente" },
            { 	"data": "fecha" },
            { 	"data": "prioridad" },
            { 	"data": "recurso" },
            { 	"data": "adjuntos" },
            { 	"data": "acciones" }
        ],
        rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
            {
    			targets		: [6],
    			className	: "text-center"
    		}
        ],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
        initComplete: function() {
            $('#totalincidentes').text( this.api().data().length )
        }
    });

    //SCROLLTOP
    function scrollpage(contenedor){
    	var header = contenedor - $('.header').height();
        $('html, body').stop().animate({
            scrollTop: header
        }, 1000);
        return false;
    }

    $('.ancla').click(function(){
    	var name = $(this).attr('name');
        scrollpage( $('.'+name).offset().top );
    });
    
/****************************FUNCION PARA CARGAR USUARIOS************************************/
	function chat_usuarios() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'CHAT_USUARIOS',
    		},
    		success: function (response) {
    			$('#chat_usuarios').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN    
    /****************************FUNCION PARA CARGAR POR MEDIO DE UN CLICK CADA SALA DE CHAT************************************/
    $(document).ready(function() { 
    $('#chat_usuarios').on('click','li.dz-chat-user',function() {
        var id = $(this).attr("data-id");
        var idsala = $(this).attr("data-idsala");
        var nombre = $(".pn"+id).html();
        //console.log(id+'-'+idsala+'-'+nombre);
        $(".dz-chat-user-box").addClass("d-none");
        $("#sala_chat").removeClass("d-none");
        //OBTENER DATOS DE LOS CHATS
      	$.ajax({
     		type: 'post',
			url: "controller/dashboardback.php?opcion=CHAT_USUARIOS_DETALLES&id="+id+"&idsala="+idsala,
            success: function(response) {
                console.log("CHAT_USUARIOS_DETALLES");
                 $('#chatcon').html(nombre);
                 $('#idsala').val(idsala);
                 $('#chat_enfermero').html(response);
                //$('#chat_paciente').html(response);
		   }
	    });
    });
});
//FIN
 /****************************FUNCION PARA ENVIAR MENSAJE EN EL CHAT************************************/   
    $("#boton-enviar-mensaje").on("click",function(){
		enviar_mensaje($("#idsala").val());
	});
    
    function enviar_mensaje(idsala){
		//console.log("sala: " +idsala);
		var body = $("#body").val();
		//console.log("mensaje: " +body);
		
			$.ajax({
				type: 'post',
				url: 'controller/dashboardback.php',
				data: { 
					'opcion': 'CHAT_MENSAJES_ENVIAR',
				    'idsala': idsala,
					'body'  : body
				},
				beforeSend: function() {
				    //$('#overlay').css('display','block');
				},
				success: function (response) {
				    //alert("mensaje enviado");
				    //OBTENER DATOS DE LOS CHATS
                  	$.ajax({
                 		type: 'post',
            			url: "controller/dashboardback.php?opcion=CHAT_USUARIOS_DETALLES&idsala="+idsala,
                        success: function(response) {
                            console.log("MENSAJE ENVIADO");
                            $('#chat_enfermero').html(response);
                            $("#body").val('');
            		   }
            	    });
				},
				error: function () {
				}
			});
	}
//FIN	
/****************************FUNCION PARA CARGAR LISTADO DE NOTAS************************************/
	function listado_notas() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'LISTADO_NOTAS',
    		},
    		success: function (response) {
    			$('#listado_notas').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN

/****************************FUNCION PARA CARGAR POR MEDIO DE CLICK CADA NOTA************************************/

$(document).ready(function() { 
    $('#listado_notas').on('click','li.dz-nota-user',function() {
        var id = $(this).attr("data-id");
        var idvisita = $(this).attr("data-idvisita");
        var nombre = $(".pnota"+id).html();
        $(".dz-nota-user-box").addClass("d-none");
        $("#sala_nota").removeClass("d-none");
        console.log("hice clic y paso " +id);
        //OBTENER DATOS DE LOS CHATS
      	$.ajax({
     		type: 'post',
			url: "controller/dashboardback.php?opcion=NOTAS_DETALLES&id="+id+"&idvisita="+idvisita,
            success: function(response) {
                console.log("NOTA_DETALLES");
                $('#notacon').html(nombre);
                $('#nota_detalle').html(response);
		   }
	    });
    });
});
//FIN

/****************************FUNCION PARA CARGAR LISTADO DE COLABORADORES CON MAS VISITAS ATENDIDAS EN EL CARRUSEL************************************/
	function top_colaboradores() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'TOP_COLABORADORES',
    		},
    		success: function (response) {
    			$('#top_colaboradores').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN
})(jQuery);

