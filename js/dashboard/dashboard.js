	// TOOLTIPS
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();

   

    $(document).on('click', '#boton-tabla', (e)=>{

       	console.log(e.target.innerText);
       	if(e.target.innerText == "Turnos cancelados"){
            document.getElementById("boton-tabla").innerHTML = 'Control proximas visitas';
            document.getElementById("title").innerHTML = 'Turnos cancelados';
            $("#container-retraso").css("display", "none");
            $("#container-cancelados").css("display", "block");
		}else{
            document.getElementById("boton-tabla").innerHTML = 'Turnos cancelados';
            document.getElementById("title").innerHTML = 'Control proximas visitas';
            $("#container-retraso").css("display", "block");
            $("#container-cancelados").css("display", "none");
		}

        //tabla_kitinsumos.clear().ajax.url("controller/logisticaback.php?opcion=insumoskittabla&id="+id).load();
	});


});
$("#login_vitae").click(function(){
		var txtUsuario  = localStorage.getItem("user");
    	var txtClave 	= localStorage.getItem("clave");
		$.ajax({
			url:'http://pre-homecare.vitae-health.com/login.php',
			type : 'POST',
			dataType: 'json',
			data: {txtUsuario: txtUsuario, txtClave: txtClave, db : 'pre_homecarePA'},
			success: function(data){
				if ( data.error === true ) {
					$('#error').css({"display": "block", "color": "#ff0000", "font-weight": "500", "text-align": "center"});
					var error = document.getElementById('error');
					error.innerHTML = data.msg;
					
					setTimeout(function() {
						$('#error').fadeOut(500);
					},3000);
				
					setTimeout(function(){
						window.scrollTo(0, 1);
					}, 100);
				}else {
					localStorage.setItem("user",txtUsuario);
					localStorage.setItem("user_id",data.msg);
					location.href='http://pre-homecare.vitae-health.com/dashboard.php';	
					return false;
				}
			},
			error: function(data){	
				console.log(data);
			}
		});
		return false;
});
$(".dz-nota-history-back").click(function(){
    $(".dz-nota-user-box").removeClass('d-none');
    $(".dz-nota-history-box").addClass('d-none');
});

$(".mapadash").click(function(){
    var tipo = $(this).attr('id');
    mapa(tipo);
});
$.get('controller/dashboardback.php?opcion=nivel','',function(result){
	console.log(result);
	if(result != 5 && result !=2){
		$('#div_cc').show();
		$('#div_tc').show();
		$('#tabla_c').show();
		$('#tabla_t').show();
		$('#tabla_m').show();
		$(".cuadros").removeClass( "col-xl-2 col-xxl-3 col-sm-6" ).addClass( "col-xl-3 col-xxl-4 col-sm-6" );
	}else{
		//$(".cuadros").removeClass( "col-xl-2 col-xxl-3 col-sm-6" ).addClass( "col-xl-3 col-xxl-4 col-sm-6" );
	}
});
function mapa(tipo) {
	var styledMapType = new google.maps.StyledMapType(
            [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#ffffff"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#dadada"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#c9c9c9"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  }
			],
            {name: 'Styled Map'}
        );

	$.ajax({
		url: "controller/dashboardback.php",
		cache: false,
		dataType: "json",
		method: "POST",
		data: {
			"opcion": "MAPA",
			"tipo"	:tipo
		}
	}).done(function(data) {
		var latitud = parseFloat(window.localStorage.getItem("latitud"));
		var longitud = parseFloat(window.localStorage.getItem("longitud"));
        var map = new google.maps.Map(document.getElementById('mapa'), {
          center: {lat: latitud, lng: longitud},
          zoom: 10
        });
		
		$.map(data, function (datos) {
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
				icon: datos.icon,
				title: datos.title,
				label: {
					text: datos.label,
					fontSize: '1px',
					color: '#1f4380'
				},
				map: map
			});
			
			marker.addListener('click', function() {
				map.setZoom(8);
				map.setCenter(marker.getPosition());
				var xUnidad = marker.label.text;
				console.log(marker.title.text);
			});
		});

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
	});
}



(function($) {
    /* "use strict" */
//LISTADO DE LLAMADO DE FUNCIONES    
incidentes_notificacion();
chat_usuarios();
listado_notas();
top_colaboradores();

 var dzChartlist = function(){
	
	var screenWidth = $(window).width();
		
	var lineChart = function(){
		var options = {
          series: [{
          name: 'Income',
          data: [420, 550, 850, 220, 650]
        }, {
          name: 'Expenses',
          data: [170, 850, 101, 90, 250]
        }],
          chart: {
          type: 'bar',
          height: 350,
		  toolbar: {
            show: false
          }
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
		
		legend: {
			show: true,
			fontSize: '12px',
			fontWeight: 300,
			
			labels: {
				colors: 'black',
			},
			position: 'bottom',
			horizontalAlign: 'center', 	
			markers: {
				width: 19,
				height: 19,
				strokeWidth: 0,
				radius: 19,
				strokeColor: '#fff',
				fillColors: ['#369DC9','#D45BFF'],
				offsetX: 0,
				offsetY: 0
			}
		},
		yaxis: {
			labels: {
		   style: {
			  colors: '#3e4954',
			  fontSize: '14px',
			   fontFamily: 'Poppins',
			  fontWeight: 100,
			  
			},
		  },
		},
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['06', '07', '08', '09', '10'],
        },
        fill: {
		  colors:['#369DC9','#D45BFF'],
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "$ " + val + " thousands"
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#line-chart"), options);
        chart.render();
	}
	var chartBar = function(){
		var optionsArea = {
          series: [{
            name: "Recovered Patient",
            data: [500, 230, 600, 360, 700, 890, 750, 420, 600, 300, 420, 220]
          },
          {
            name: "New Patient",
            data: [250, 380, 200, 300, 200, 520,380, 770, 250, 520, 300, 900]
          }
        ],
          chart: {
          height: 350,
          type: 'area',
		  group: 'social',
		  toolbar: {
            show: false
          },
          zoom: {
            enabled: false
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [2, 2],
		  colors:['#F46B68','#2BC155'],
		  curve: 'straight'
        },
        legend: {
          tooltipHoverFormatter: function(val, opts) {
            return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
          },
		  markers: {
			fillColors:['#F46B68','#2BC155'],
			width: 19,
			height: 19,
			strokeWidth: 0,
			radius: 19
		  }
        },
        markers: {
          size: 6,
		  border:0,
		  colors:['#F46B68','#2BC155'],
          hover: {
            size: 6,
          }
        },
        xaxis: {
          categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October','November','December',
            '10 Jan', '11 Jan', '12 Jan'
          ],
        },
		yaxis: {
			labels: {
		   style: {
			  colors: '#3e4954',
			  fontSize: '14px',
			   fontFamily: 'Poppins',
			  fontWeight: 100,
			  
			},
		  },
		},
		fill: {
			colors:['#F46B68','#2BC155'],
			type:'solid',
			opacity: 0.07
		},
        grid: {
          borderColor: '#f1f1f1',
        }
        };
		//var chartArea = new ApexCharts(document.querySelector("#chartBar"), optionsArea);
        //chartArea.render();

	}

	
	/* Function ============ */
		return {
			init:function(){
				
			},
			
			
			load:function(){
				//lineChart();
				//chartBar();
			},
			
			resize:function(){
			}
		}
	
	}();

	jQuery(document).ready(function(){
	});
		
	jQuery(window).on('load',function(){
		setTimeout(function(){
			dzChartlist.load();
		}, 1000); 
		
	});

	jQuery(window).on('resize',function(){
		
		
	});  
	
	function rangos(data,type,row,val){
		//console.log(row);
		var color = '';
		var simbolo = '';
		var val = val.toLowerCase();
		if(data != ''){
			switch(val){
				case 'normal':
					color 	= 'green';
					simbolo = 'fa-thumbs-up';
					break;
				case 'alta':
					color 	= 'red';
					simbolo = 'fa-arrow-up';
					break;
				case 'baja':
					color 	= 'red';
					simbolo	= 'fa-arrow-down';
					break;
				default:
					color 	= 'gray';
					simbolo	= 'fa fa-minus';
					break;
				
			}
		}else{
			color 	= 'silver';
			simbolo	= 'fa fa-minus';
		}
		
		var 	img = "<div style='float:left;margin-right:10px;' class='ui-pg-div ui-inline-custom'>";
				img	+= " <i class='"+color+" fa "+simbolo+"' data-toggle='tooltip' data-original-title='"+data+"' data-placement='right'></i> "+data;
				img += '</div>';
		
		return img;
	}

	function cargarDatos() {
		var formatNumber = {
			separador: ",", // separador para los miles
			sepDecimal: '.', // separador para los decimales
			formatear:function (num){
				num +='';
				var splitStr = num.split('.');
				var splitLeft = splitStr[0];
				var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
				var regx = /(\d+)(\d{3})/;
				while (regx.test(splitLeft)) {
					splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
				}
				return this.simbol + splitLeft +splitRight;
			},
			new:function(num, simbol){
				this.simbol = simbol ||'';
				return this.formatear(num);
			}
		}
		
		
			$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_VISITAS_RETRASO"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].retraso;
			var xvalor = data.rows[i].retraso;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalvretraso").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalvretraso").html(formatNumber.new(xvalor));
		});
		
			$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TURNOS_CANCELADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].cancelados;
			var xvalor = data.rows[i].cancelados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totaltcancelados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totaltcancelados").html(formatNumber.new(xvalor));
		});
		
				$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_PACIENTES_COVID"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].pcovid;
			var xvalor = data.rows[i].pcovid;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalpcovid").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalpcovid").html(formatNumber.new(xvalor));
		});
			
		$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TRATAMIENTOS_EDITADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].tratamientoseditados;
			var xvalor = data.rows[i].tratamientoseditados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totalteditados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totalteditados").html(formatNumber.new(xvalor));
		});
		
			
		$.ajax({
			url: "controller/dashboardback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "DATOS_TRATAMIENTOS_FINALIZADOS"
			}
		}).done(function(data) {
			total = 100;
			var i=0; 
			var total  = data.rows[i].tratamientosfinalizados;
			var xvalor = data.rows[i].tratamientosfinalizados;
			if (total == 0 || total == null) {
				total = 1;
			} 
			$("#totaltfinalizados").empty();
			if (xvalor == null) {
				xvalor = 0;
			}
			$("#totaltfinalizados").html(formatNumber.new(xvalor));
		});
	}

	var tablapacientesfuerarango = $("#tablaPacientes_alerta").DataTable({"scrollX": true, 
		//"searching": true,
		//"lengthChange": false,
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		: {
			"url"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_FUERA_RANGO&hogar=-1",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "nombre" },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			},
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			},
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			},
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			},
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl); 
				} 
			},
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc); 
				} 
			},
			{ 	"data": "condicion2" },
			{ 	"data": "fecha" },
			{ 	"data": "recurso" },
			{ 	"data": "acciones"}
			],
		"rowId": 'id',
		"columnDefs": [		
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
	  initComplete: function() {
      $('#totalpaciente_alertas').text( this.api().data().length )
   }
	});
	var tablapacientesfuerarango2 = $("#tablaPacientes_condicion").DataTable({"scrollX": true, 
		//"searching": true,
		//"lengthChange": false,
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		: {
			"url"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_FUERA_RANGO2&hogar=-1",
			"dataSrc"	: "data"
		},		
		"columns"	: [
			{ 	"data": "nombre" },
			{ 	"data": "fc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfc);
				}
			},
			{ 	"data": "fr",
				render: function(data,type,row){
					return rangos(data,type,row,row.rfr);
				} 
			},
			{ 	"data": "so",
				render: function(data,type,row){
					return rangos(data,type,row,row.rox);
				} 
			},
			{ 	"data": "paa",
				render: function(data,type,row){
					return rangos(data,type,row,row.rsi);
				} 
			},
			{ 	"data": "pab",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdi);
				} 
			},
			{ 	"data": "tc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rtm);
				} 
			},
			{ 	"data": "dolor",
				render: function(data,type,row){
					return rangos(data,type,row,row.rdl); 
				} 
			},
			{ 	"data": "gc",
				render: function(data,type,row){
					return rangos(data,type,row,row.rgc); 
				} 
			},
			{ 	"data": "condicion2" },
			{ 	"data": "fecha" },
			{ 	"data": "recurso" },
			{ 	"data": "acciones"}
			],
		"rowId": 'id',
		"columnDefs": [		
		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
	  initComplete: function() {
      $('#condiciones').text( this.api().data().length )
   }
	});
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
		$("#tablaPacientes_condicion").on('draw.dt',function(){
		$('.ver-notas').each(function(){
			$(this).on('click',function(){			
				var id = $(this).attr('data-id');
				$.get('controller/dashboardback.php?opcion=get_nota&id='+id,'',function(result){
					var datos = JSON.parse(result);
					
					$("#nota_enfermeria").html("<b>Paciente: "+datos.data[0].paciente+"</b><br><br>    "+datos.data[0].comentario);
					$("#nota_recurso").html(datos.data[0].usuario);
				});
			}); 
		}); 
	});
	//setInterval(function(){ $('#tablaPacientes_alerta').DataTable().ajax.reload(); }, 15000);
	
	var tablavisitasRetraso = $("#visitasRetraso").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_VISITAS_RETRASO",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "fecha_inicio" },
			{ 	"data": "recurso" },
			{ 	"data": "tiempo_retardo" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "estatus" },
			//{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"order": [[2, "asc"]],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
	  initComplete: function() {
      $('#totalvretraso').text( this.api().data().length )
   }
	});
	
	var tablaturnosCanceladas = $("#turnosCancelados").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TURNOS_CANCELADOS",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			
			{ 	"data": "fecha_inicio" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "cancela" },
			{ 	"data": "recurso" },
			
			{ 	"data": "estatus" },
			//{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
        initComplete: function() {
            $('#totaltcancelados').text( this.api().data().length )
        }
	});
	$("#turnosCancelados").on('draw.dt',function(){
		$('.detalles').each(function(){
			$(this).on('click',function(){			
				var id = $(this).attr('data-id');
				$.get('controller/dashboardback.php?opcion=datos_visita&id='+id,'',function(result){
					//var datos = JSON.parse(result);
					//console.log(datos.data[0].asignado);
					$("#detalles").html(result);
				});
			}); 
		}); 
	});
	var tablapacientescovid = $("#pacientesCovid").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_COVID",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "edad" },
			{ 	"data": "medico" },
			{ 	"data": "condicion" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#totalpcovid').text( this.api().data().length )
   }
	});
	
	var tablapacientesact = $("#pacientesact").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_PACIENTES_ACTIVOS",
		"columns"	: [
			{ 	"data": "cedula" },
			{ 	"data": "paciente" },
			{ 	"data": "edad" },
			//{ 	"data": "enfermedades" },
			{ 	"data": "medico" },
			{ 	"data": "condicion" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#totalpact').text( this.api().data().length )
   }
	});
	
	var tabla_tratamientos_editados = $("#tabla_tratamientos_editados").DataTable({"scrollX": true,
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TRATAMIENTOS_EDITADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "fecha_actualizacion_tarjeta" },
			{ 	"data": "estado" },
			{ 	"data": "creado_por" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
            $('#totalteditados').text( this.api().data().length )
        }
	});
	$("#tabla_tratamientos_editados").on('draw.dt',function(){
		$('.imprimir').each(function(){
			$(this).on('click',function(){
				var id = $(this).attr("data-id");
				$.get('controller/dashboardback.php?opcion=imprimir_tratamiento&id='+id,'',function(result){
					if(result == 1){
						var url = 'controller/reporte_tarjeta_medicamentos.php?id='+id;
						window.open(url, '_blank');
					} else if(result != '') {
						swal('ERROR!','El usuario '+result+' se encuentra editando este tratamiento','error');
					}
				});
			});
		}); 
		$('.activar').each(function(){
			
			$(this).on('click',function(){
				var id = $(this).attr("data-id");
				swal({
				  title: "Confirmar",
				  text: "¿Estas seguro de Activar este Tratamiento?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
					  $.get( "controller/dashboardback.php?opcion=activar_medicamento&id="+id, 
							{ 
								id: id
							}, function(result){
								if(result == 1){
									swal("Tratamiento Activado!", {icon: "success",});
									$('#tabla_tratamientos_editados').DataTable().ajax.reload();
								} else if(result != '') {
									swal('ERROR!','El usuario '+result+' se encuentra editando este tratamiento, aun no se puede Activar','error');
								}
						});
					
				  } else {
					swal("Este tratamiento no se puede Activar");
					$('#tabla_tratamientos_editados').DataTable().ajax.reload();
				  }
				});
			}); 
		}); 
	});
    var tabla_tratamientos_finalizados = $("#tabla_tratamientos_finalizados").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_TRATAMIENTOS_FINALIZADOS",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "nombremedicamento" },
			{ 	"data": "via" },
			{ 	"data": "frecuencia" },
			{ 	"data": "dosis" },
			{ 	"data": "fecha_inicio" },
			{ 	"data": "fecha_fin" },
			{ 	"data": "observaciones" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#totaltfinalizados').text( this.api().data().length )
   }
	});
	
	var tabla_planes = $("#tabla_planes").DataTable({"scrollX": true,
		//"lengthMenu": [3, 4],
		responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
		"ajax"		:"controller/dashboardback.php?opcion=TABLA_PLANES",
		"columns"	: [
			{ 	"data": "paciente" },
			{ 	"data": "fecha_actualizacion_tarjeta" },
			{ 	"data": "estado" },
			{ 	"data": "creado_por" },
			{ 	"data": "acciones" }
		],
		rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
		"columnDefs": [ //OCULTAR LA COLUMNA ID

		],
		"language": {
			"url": "js/Spanish.json",
			"info": "Mostrando página _PAGE_ de _PAGES_"
		},
		initComplete: function() {
      $('#planesdecuidados').text( this.api().data().length )
   }
	});
	$("#tabla_planes").on('draw.dt',function(){
		$('.imprimir_plan').each(function(){
			$(this).on('click',function(){
				var id = $(this).attr("data-id");
				$.get('controller/dashboardback.php?opcion=imprimir_plan&id='+id,'',function(result){
					if(result == 1){
						var url = 'controller/reporte_planes.php?id='+id;
						window.open(url, '_blank');
					} else if(result != '') {
						swal('ERROR!','El usuario '+result+' se encuentra editando este plan de cuidado','error');
					}
				});
			});
		}); 
		$('.activar_plan').each(function(){
			
			$(this).on('click',function(){
				var id = $(this).attr("data-id");
				swal({
				  title: "Confirmar",
				  text: "¿Estas seguro de Activar este Plan de cuidados?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
					  $.get( "controller/dashboardback.php?opcion=activar_plan&id="+id, 
							{ 
								id: id
							}, function(result){
								if(result == 1){
									swal("Tratamiento Activado!", {icon: "success",});
									$('#tabla_planes').DataTable().ajax.reload();
								} else if(result != '') {
									swal('ERROR!','El usuario '+result+' se encuentra editando este Plan de cuidados, aun no se puede Activar','error');
									$('#tabla_planes').DataTable().ajax.reload();
								}
						});
					
				  } else {
					swal("Este Plan de cuidados no se puede Activar");
					$('#tabla_planes').DataTable().ajax.reload();
				  }
				});
			}); 
		}); 
	});
	setInterval(function(){
	incidentes_notificacion();
	}, 60000);   
	function incidentes_notificacion() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'INCIDENTES_NOTIFICACION',
    		},
    		success: function (response) {
    			$('#incidentesnotific').html(response);		
    		},
    		error: function () {
    		    
    		}
    	});
    }

     var tabla_incidentes = $("#tabla_incidentes").DataTable({"scrollX": true,
        //"lengthMenu": [3, 4],
        responsive: true,
		scrollCollapse: true,
		"scrollY": "100%",
        "ajax"		:"controller/dashboardback.php?opcion=TABLA_INCIDENTES",
        "columns"	: [
			{ 	"data": "fecha" },
            { 	"data": "prioridad" },
            { 	"data": "tipo" },
			{ 	"data": "paciente" },
            { 	"data": "incidente" },
           
									 
            { 	"data": "recurso" },
            //{ 	"data": "adjuntos" },
            { 	"data": "acciones" }
        ],
        rowId: 'id', // CAMPO DE LA DATA QUE RETORNARÁ EL MÉTODO id()
        "columnDefs": [ //OCULTAR LA COLUMNA ID
            {
    			targets		: [6],
    			className	: "text-center"
    		}
        ],
		"order": [[0, "desc"]],
        "language": {
            "url": "js/Spanish.json",
            "info": "Mostrando página _PAGE_ de _PAGES_"
        },
        initComplete: function() {
            $('#totalincidentes').text( this.api().data().length )
        }
    });
	$("#tabla_incidentes").on('draw.dt',function(){
		$(".evid").click(function(){
			var id = $(this).attr("data-id");
			console.log(id);
			$.get('controller/dashboardback.php?opcion=evidencias&id='+id,'',function(result){
				console.log(result);
				
			});
		});
	});
    //SCROLLTOP
    function scrollpage(contenedor){
    	var header = contenedor - $('.header').height();
        $('html, body').stop().animate({
            scrollTop: header
        }, 1000);
        return false;
    }

    $('.ancla').click(function(){
    	var name = $(this).attr('name');
        scrollpage( $('.'+name).offset().top );
    });
    
/****************************FUNCION PARA CARGAR USUARIOS************************************/
	function chat_usuarios() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'CHAT_USUARIOS',
    		},
    		success: function (response) {
    			$('#chat_usuarios').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN    
    /****************************FUNCION PARA CARGAR POR MEDIO DE UN CLICK CADA SALA DE CHAT************************************/
    $(document).ready(function() { 
    $('#chat_usuarios').on('click','li.dz-chat-user',function() {
        var id = $(this).attr("data-id");
        var idsala = $(this).attr("data-idsala");
        var nombre = $(".pn"+id).html();
        //console.log(id+'-'+idsala+'-'+nombre);
        $(".dz-chat-user-box").addClass("d-none");
        $("#sala_chat").removeClass("d-none");
        //OBTENER DATOS DE LOS CHATS
      	$.ajax({
     		type: 'post',
			url: "controller/dashboardback.php?opcion=CHAT_USUARIOS_DETALLES&id="+id+"&idsala="+idsala,
            success: function(response) {
                console.log("CHAT_USUARIOS_DETALLES");
                 $('#chatcon').html(nombre);
                 $('#idsala').val(idsala);
                 $('#chat_enfermero').html(response);
                //$('#chat_paciente').html(response);
		   }
	    });
    });
});
//FIN
 /****************************FUNCION PARA ENVIAR MENSAJE EN EL CHAT************************************/   
    $("#boton-enviar-mensaje").on("click",function(){
		enviar_mensaje($("#idsala").val());
	});
    
    function enviar_mensaje(idsala){
		//console.log("sala: " +idsala);
		var body = $("#body").val();
		//console.log("mensaje: " +body);
		
			$.ajax({
				type: 'post',
				url: 'controller/dashboardback.php',
				data: { 
					'opcion': 'CHAT_MENSAJES_ENVIAR',
				    'idsala': idsala,
					'body'  : body
				},
				beforeSend: function() {
				    //$('#overlay').css('display','block');
				},
				success: function (response) {
				    //alert("mensaje enviado");
				    //OBTENER DATOS DE LOS CHATS
                  	$.ajax({
                 		type: 'post',
            			url: "controller/dashboardback.php?opcion=CHAT_USUARIOS_DETALLES&idsala="+idsala,
                        success: function(response) {
                            console.log("MENSAJE ENVIADO");
                            $('#chat_enfermero').html(response);
                            $("#body").val('');
            		   }
            	    });
				},
				error: function () {
				}
			});
	}
//FIN	
/****************************FUNCION PARA CARGAR LISTADO DE NOTAS************************************/
	function listado_notas() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'LISTADO_NOTAS',
    		},
    		success: function (response) {
    			$('#listado_notas').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN

/****************************FUNCION PARA CARGAR POR MEDIO DE CLICK CADA NOTA************************************/

$(document).ready(function() { 
    $('#listado_notas').on('click','li.dz-nota-user',function() {
        var id = $(this).attr("data-id");
        var idvisita = $(this).attr("data-idvisita");
        var nombre = $(".pnota"+id).html();
        $(".dz-nota-user-box").addClass("d-none");
        $("#sala_nota").removeClass("d-none");
        console.log("hice clic y paso " +id);
        //OBTENER DATOS DE LOS CHATS
      	$.ajax({
     		type: 'post',
			url: "controller/dashboardback.php?opcion=NOTAS_DETALLES&id="+id+"&idvisita="+idvisita,
            success: function(response) {
                console.log("NOTA_DETALLES");
                $('#notacon').html(nombre);
                $('#nota_detalle').html(response);
		   }
	    });
    });
});
//FIN

/****************************FUNCION PARA CARGAR LISTADO DE COLABORADORES CON MAS VISITAS ATENDIDAS EN EL CARRUSEL************************************/
	function top_colaboradores() {
    	$.ajax({
    		type: 'post',
    		url: 'controller/dashboardback.php',
    		data: { 
    			'opcion'	: 'TOP_COLABORADORES',
    		},
    		success: function (response) {
    			$('#top_colaboradores').html(response);	
    		},
    		error: function () {
    		    
    		}
    	});
    }
//FIN
})(jQuery);

