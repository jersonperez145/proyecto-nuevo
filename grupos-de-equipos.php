<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
	verificarLogin();
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - Grupos de equipos </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Ajustes -->
    <link href="./css/ajustes.css<?php autoVersiones().''.$nombre; ?>" rel="stylesheet">

</head>
<body>
    <!--*******
        ORVERLAY
    ********-->
    <div id="overlay">
		<div id="text"><strong>Procesando...</strong></div>
    </div>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper" class="show">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="#top" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header" name="top">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <a  href="#top">
							<a href="#" class="btn btn-primary ir-arriba"><i class="las la-arrow-up"></i></a>
                            <div class="dashboard_bar">
                                Grupos de equipos
                            </div>
                          </a>  
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
                                    <i class="fas fa-bell text-success"></i>
									<!--<span class="badge light text-white bg-primary" id="totalincidentes">0</span>-->
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
										<ul class="timeline" id="incidentesnotific">
										    
										</ul>
									</div>
                                    <a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
                                </div>
                            </li>
							<li class="nav-item dropdown notification_dropdown" style="display: none;">
                                <a class="nav-link bell bell-link" href="javascript:;">
                                    <i class="fas fa-comments text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown notification_dropdown" style="display:none;">
                                <a class="nav-link bell config-link" href="javascript:;">
                                    <i class="fas fa-cogs text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                                    <!--<img src="images/logo.png" width="20" alt=""/>-->
                                    <div class="round-header"><?php echo $inombre; ?></div>
									<div class="header-info">
										<span><?php echo $nombre; ?></span>
									</div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right"><!--
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>-->
                                    <a href="index.php" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Cerrar Sesion </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php menu(); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid" style="display:padding-top: 0px !important">
				<div class="form-head d-flex mb-3 mb-md-4 align-items-start">
					<div class="mr-auto d-none d-lg-block" style="display: none !important">
						<h3 class="text-black font-w600">Bienvenido a Vitae!</h3>
						<p class="mb-0 fs-18">Tu aliado de salud en casa</p>
					</div>
					<!--
					<div class="input-group search-area ml-auto d-inline-flex">
						<input type="text" class="form-control" placeholder="Buscar...">
						<div class="input-group-append">
							<button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
						</div>
					</div>
					-->
				</div>
				
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Listado</h4>
                                <div>
                                    <!-- button type="button" class="btn btn-primary mb-2" id="btn_excel_global" style="background: white;color: #36C95F;border-color: 36C95F;"><i class="fas fa-file-excel" aria-hidden="true"></i> Excel</button-->
                                    <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target=".nuevo-grupo-equipos"><i class="fas fa-plus" aria-hidden="true"></i> Nuevo</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tablaequipos" class="display min-w850 table" width="100%">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Acciones</th>
                                                <th>Nombre</th>
                                                <th>Actualizado</th>
                                                <th>Fecha actualización</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
        <!-- MODAL NUEVO -->
        <div class="modal fade nuevo-grupo-equipos" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo grupo de equipos</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="nuevo-grupo">
                            <div class="col-md-12 col-xs-12 col-sm-12">		
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label" for=""><span style="color:red">* </span> Nombre</label>
                                    <input type="text" class="form-control" id="nombre" autocomplete="off">
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-xs-12 col-sm-12">		
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label" for="">Equipo</label>
                                    <select class="form-control" id="equipos_" style="width:100%"></select>
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                                <button type="button" id="anadir" class="btn btn-info btn-md">AÑADIR</button>
                            </div>
                            <div class="col-12">
                                <div id="tablaTemporal"></div>
                            </div> 
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="boton-guardar">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL NUEVO-->
        <!-- MODAL EDITAR -->
        <!-- <div class="modal fade " id="editar-grupo-equipos" data-backdrop="static" style="opacity:1 !important;"> -->
        <div id="editar-grupo-equipos" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Editar grupo de equipos</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row col-12">
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">		
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label" for=""><span style="color:red">* </span> Nombre</label>
                                    <input type="text" class="form-control" id="nombre_editar" autocomplete="off">
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12 col-xs-12 col-sm-12">		
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label" for="">Equipo</label>
                                    <select class="form-control" id="equipos_editar" style="width:100%"></select>
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-xs-12 col-sm-12 text-center">
                                <button type="button" id="anadir_editar" class="btn btn-info btn-md">AÑADIR</button>
                            </div>  
                        </div>
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tabla_editar_grupo" class="display min-w850" style="width:100%">
                                    <thead>
                                        <tr>
                                                <th>itemid</th>
                                                <th>Quitar</th>
                                                <th>Item</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="boton-guardar-editar">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL EDITAR-->
    </div>	
    <!--**********************************
        Main wrapper end
    ***********************************-->



    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>
	<script src="./vendor/owl-carousel/owl.carousel.js"></script>
	
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<script src="./js/grupos_de_equipos/grupos-de-equipos.js<?php autoVersiones(); ?>" ></script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://kit.fontawesome.com/7f9e31f86a.js" crossorigin="anonymous"></script>	
</body>
</html>