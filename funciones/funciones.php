<?php 
// nivel de riesgo del paciente 
// parametro : id de paciente
function get_nivel_riesgo($idpaciente){
    global $mysqli;
    $q="SELECT SUM(riesgo_a+riesgo_dx) as enfermedades 
                FROM (
                    SELECT distinct e.codigo, 'antecedente' as tipo,pa.idenfermedad as id, IFNULL(ge.riesgo_antecedente,0) as riesgo_a, '0' as riesgo_dx
                    FROM  pacienteenfermedadespasadas pa
                    LEFT JOIN enfermedades e ON e.id = pa.idenfermedad
                    LEFT JOIN grupos_enfermedades ge ON ge.id= e.grupo
                    WHERE pa.idpaciente = $idpaciente AND e.codigo is not null
                    UNION all
                    SELECT distinct e2.codigo,'diagnostico' as tipo,pa.idenfermedad as id, '0' as riesgo_a, IFNULL(ge2.riesgo_diagnostico,0) as riesgo_dx
                    FROM pacienteenfermedadesactuales pa 
                    LEFT JOIN enfermedades e2 ON e2.id = pa.idenfermedad
                    LEFT JOIN grupos_enfermedades ge2 ON ge2.id = e2.grupo				
                    WHERE pa.idpaciente = $idpaciente AND e2.codigo is not null
                ) as t";
    $r = $mysqli->query($q);
    $row = $r->fetch_assoc();
    $dx = $row['enfermedades'];
    
    if($dx <=2)
            $riesgo = "Nivel 1";
    if( $dx >=3){
            $riesgo = "Nivel 2";
    }
    if($dx >=5){
            $riesgo = "Nivel 3";
    }
    return $riesgo;
}

?>