<?php
    include("../conexion.php");
	// ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);	



	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){
		case "LISTAR_BODEGAS":
			LISTAR_BODEGAS();
		break;
		case "LISTAR_ITEMS":
			LISTAR_ITEMS();
		break;
		case "cargarInventario": 
			  cargarInventario();
			  mysqli_close($mysqli);
			  break;
		case "get_ajuste":
				get_ajuste();
				mysqli_close($mysqli);
				break;
		case "guardar_entrada":
				guardar_entrada();
				mysqli_close($mysqli);
				break;
		case "cargar_movimientos":
				cargar_movimientos();
				mysqli_close($mysqli);
				break;
		case "cargar_movimientos_todos":
			  cargar_movimientos_todos();
			  mysqli_close($mysqli);
				break;	
		case "cargar_detalle_ajuste":
				cargar_detalle_ajuste();
				mysqli_close($mysqli);
				break;
		case "cargar_bodegas":
				cargar_bodegas();
				mysqli_close($mysqli);
				break;
		case "cargar_comprometido":
			cargar_comprometido();
			mysqli_close($mysqli);
			break;
		case "cargar__todo_el_comprometido":
			cargar__todo_el_comprometido();
			mysqli_close($mysqli);
			break;							  
		case "mover_inventario":
				mover_inventario();
				mysqli_close($mysqli);
				break;
		case "getOrden":
				getOrden();
				mysqli_close($mysqli);
				break;
		case "eliminarMovimiento":
				eliminarMovimiento();
				mysqli_close($mysqli);
				break;
		case "listar_acuses":
				listar_acuses();
				mysqli_close($mysqli);
				break;
		case "cargar_por_pacientes":
			  cargar_por_pacientes();
			  mysqli_close($mysqli);
			  break;
		case "getConteo":
			  getConteo();			
mysqli_close($mysqli);			  
			  break;
		case "getConteo2":
			  getConteo2();
			  mysqli_close($mysqli);
			  break;
		case "inventario_por_paciente":
			  inventario_por_paciente();
mysqli_close($mysqli);			  
			  break;
		case "movimientos_por_bodega":
			movimientos_por_bodega();
			mysqli_close($mysqli);
			break;		
		case "generarExcel":
			generarExcel();
			mysqli_close($mysqli);
			break;		
		case "actualizar_inventario":
			actualizar_inventario();
			mysqli_close($mysqli);
			break;		
		case "actualizar_inventario_acuses":
			actualizar_inventario_acuses();
			mysqli_close($mysqli);
			break;			
		case "actualizar_inventario_2":
			actualizar_inventario_2();
			mysqli_close($mysqli);
			break;					
		case "actualizar_inventario_ajuste":
			actualizar_inventario_ajuste();
			mysqli_close($mysqli);
			break;		
		case "actualizar_inventario_un_item":
			actualizar_inventario_un_item();
			mysqli_close($mysqli);
			break;
		case "isActive":
			  isActive();
			  mysqli_close($mysqli);
			break;
		default:
			  echo "{failure:true}";
			  break;
	}	
	function LISTAR_BODEGAS(){
		global $mysqli;
		$query = "SELECT id, nombre FROM bodegas";
		$respuesta = '<option value="0">Seleccione</option>';
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc()){
			$respuesta.='<option value="'.$row['id'].'">'.$row['nombre'].'</option>';
		}
		echo $respuesta;
	}
	function LISTAR_ITEMS(){
		global $mysqli;
		$bodega = $_GET['bodega'];
		$query ="SELECT DISTINCT 
				ci.iditem as id, 
				(CASE ci.tipo 
					WHEN 'I' THEN ins.codigo 
					WHEN 'M' THEN med.codigo
					WHEN 'E' THEN equi.codigo
				END) as codigo,
				ci.tipo  as tipo,
				(CASE ci.tipo 
					WHEN 'I' THEN ins.nombre 
					WHEN 'M' THEN med.nombre
					WHEN 'E' THEN equi.descripcion
				END) as nombre,				
				(ci.total - ci.comprometido) as disponible
				FROM cantidades_inventario ci 
				LEFT JOIN insumos ins ON ins.id = ci.iditem AND ci.tipo = 'I' 
				LEFT JOIN equipos equi ON equi.id = ci.iditem AND ci.tipo = 'E' 
				LEFT JOIN medicamentos med ON med.id = ci.iditem AND ci.tipo = 'M'
				WHERE  ci.activo = 1 AND ci.inventario = $bodega ";
		$result = $mysqli->query($query);
		$respuesta = '<option value="0" selected>Seleccione</option>';
		while($row = $result->fetch_assoc()){
			$respuesta.='<option value="'.$row['id'].'" data-tipo="'.$row['tipo'].'" data-disponible="'.$row['disponible'].'">'.$row['codigo'].' | '.$row['nombre'].' ('.$row['disponible'].')</option>';
		}
		echo $respuesta;
	}
	
	function cargarInventario(){
		global $mysqli;
		$resultado = array();
		$tipoinventario = $_REQUEST['tipoinventario'];
		$item = $_REQUEST['item'];
		$tipo = $_REQUEST['tipo'];
		$fecha = isset($_REQUEST['fecha'])? $_REQUEST['fecha'] : '';
		$query ="
			SELECT DISTINCT 
			ci.iditem as iditem, 
			(CASE ci.tipo 
				WHEN 'I' THEN ins.codigo 
				WHEN 'M' THEN med.codigo
				WHEN 'E' THEN equi.codigo
			END) as codigo,
			(CASE ci.tipo 
				WHEN 'I' THEN 'Ins'
				WHEN 'M' THEN 'Med'
				WHEN 'E' THEN 'Equ'
			END) as tipo,
			(CASE ci.tipo 
				WHEN 'I' THEN ins.nombre 
				WHEN 'M' THEN med.nombre
				WHEN 'E' THEN equi.descripcion
			END) as nombre,
			IFNULL((CASE ci.tipo 
				WHEN 'I' THEN ins.costounitario 
				WHEN 'M' THEN med.costounitario
				WHEN 'E' THEN equi.costounitario
			END),0) as costounitario,
			IFNULL(ci.total,inv_total(ci.iditem,ci.tipo,ci.inventario)) as total_i,
			ci.transito as transito,
			ci.comprometido as comprometido,
			(SELECT sum(can_inv.total) FROM cantidades_inventario can_inv WHERE can_inv.iditem = ci.iditem AND can_inv.tipo = ci.tipo) as total

			FROM cantidades_inventario ci 
			LEFT JOIN insumos ins ON ins.id = ci.iditem AND ci.tipo = 'I' 
			LEFT JOIN equipos equi ON equi.id = ci.iditem AND ci.tipo = 'E' 
			LEFT JOIN medicamentos med ON med.id = ci.iditem AND ci.tipo = 'M'
			WHERE  1
			";
			
			if( $_COOKIE['nombre_bd'] == 'homecareRD' ){
				$query ="
				SELECT DISTINCT 
				ci.iditem as iditem, 
				(CASE ci.tipo 
					WHEN 'I' THEN ins.codigo 
					WHEN 'M' THEN med.codigo
					WHEN 'E' THEN equi.codigo
				END) as codigo,
				(CASE ci.tipo 
					WHEN 'I' THEN 'Insumos'
					WHEN 'M' THEN 'Medicamentos'
					WHEN 'E' THEN 'Equipos'
				END) as tipo,
				(CASE ci.tipo 
					WHEN 'I' THEN ins.nombre 
					WHEN 'M' THEN med.nombre
					WHEN 'E' THEN equi.descripcion
				END) as nombre,
				IFNULL((CASE ci.tipo 
					WHEN 'I' THEN ins.costounitario 
					WHEN 'M' THEN med.costounitario
					WHEN 'E' THEN equi.costounitario
				END),0) as costounitario,
				IFNULL(ci.total_alegra,inv_total(ci.iditem,ci.tipo,ci.inventario)) as total_i,
				ci.transito as transito,
				ci.comprometido as comprometido,
				(SELECT sum(can_inv.total_alegra) FROM cantidades_inventario can_inv WHERE can_inv.iditem = ci.iditem AND can_inv.tipo = ci.tipo) as total

				FROM cantidades_inventario ci 
				LEFT JOIN insumos ins ON ins.id = ci.iditem AND ci.tipo = 'I' 
				LEFT JOIN equipos equi ON equi.id = ci.iditem AND ci.tipo = 'E' 
				LEFT JOIN medicamentos med ON med.id = ci.iditem AND ci.tipo = 'M'
				WHERE  1
				";
				
			}
			
			if ($tipoinventario	!= 0){
				$query.= " AND ci.inventario = '$tipoinventario' ";	
			}
			if($tipo != -1 && $tipo != 0 && $tipo != ""){
				$query.= " AND ci.tipo = '$tipo' ";	
			}
			if($item != '-1' && $item != 0 && $item != ""){
				$query.= " AND ci.iditem = '$item' ";	
			}
		$query.=" GROUP BY ci.iditem,ci.tipo ORDER BY nombre";
		//die($query);
		if($tipoinventario != 0){
			if($result = $mysqli->query($query)){		
				if($result->num_rows > 0){
					while($row = $result->fetch_assoc()){
						// CALCULAR COMPROMETIDO					
						$cantidad_disponible = intval($row['total_i']) - intval($row['comprometido']);
						$ver_bodegas = '<span class="icon-col fa fa-eye blue boton-ver-bodegas"  data-item="'.$row['iditem'].'" data-cantidad="'.$cantidad_disponible.'" data-tipo="'.ucfirst(substr($row['tipo'],0,1)).'" data-toggle="tooltip" data-original-title="Ver en todas las bodegas" data-placement="right"></span>';
						$ver_pacientes = '<span class="icon-col fa fa-eye blue boton-ver-pacientes"  data-item="'.$row['iditem'].'" data-cantidad="'.$row['comprometido'].'" data-tipo="'.ucfirst(substr($row['tipo'],0,1)).'" data-toggle="tooltip" data-original-title="Ver cantidad por paciente" data-placement="right"></span>';
						$mover_bodega = "<span class='icon-col fa fa-arrows-h blue boton-mover-bodega' data-id='".$row['iditem']."' data-toggle='tooltip' data-tipo='".ucfirst(substr($row['tipo'],0,1))."' data-original-title='Mover entre inventarios' data-placement='right'></span>";
						$ver_movimientos_bodega ="<span class='icon-col fa fa-eye blue boton-movimientos-bodega' data-id='".$row['iditem']."' data-toggle='tooltip' data-tipo='".ucfirst(substr($row['tipo'],0,1))."'  data-placement='right'></span>";					
						// $acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>";
						// $acciones .= '<span class="icon-col fa fa-cart-plus blue boton-oc-producto"  data-item="'.$row['iditem'].'" data-tipo="'.ucfirst(substr($row['tipo'],0,1)).'" data-toggle="tooltip" data-original-title="Órdenes de compra" data-placement="right"></span>';
						// $acciones .= "<span class='icon-col green fa fa-building boton-inventario-producto' data-item='".$row['iditem']."' data-tipo='".ucfirst(substr($row['tipo'],0,1))."' data-toggle='tooltip' data-original-title='Inventario' data-placement='right'></span>";
						// $acciones .= "$mover_bodega"; 
						// $acciones .= "<span class='icon-col blue fa fa-pencil boton-editar-producto' data-item='".$row['iditem']."' data-tipo='".substr($row['tipo'],0,1)."' data-toggle='tooltip' data-original-title='Editar item en maestro' data-placement='right'></span>";
						// $acciones .= "<span class='icon-col blue fa fa-refresh boton-calculat-cantidad' data-id='".$row['iditem']."' data-tipo='".substr($row['tipo'],0,1)."' data-toggle='tooltip' data-original-title='Actaualizar cantidades de inventario' data-placement='right'></span>";
						// $acciones .= "</div>";
						$valor = $row['costounitario'] * $row['total_i'];
						$resultado['data'][] = array(
							'id' 		=>	$row['iditem'],
							// 'acciones' 	=>	$acciones,
							'tipo' 		=>	ucfirst($row['tipo']),
							'codigo'	=> $row['codigo'],
							'nombre' 	=>	$row['nombre'],
							'costounitario' =>number_format($row['costounitario'],2),
							'valor' => number_format($valor,2),
							'cantidad' 	=>	$ver_movimientos_bodega.' '.$row['total_i'],
							'cantidad_disponible' => $cantidad_disponible,
							'cantidad_comprometido' => $row['comprometido'],
							'cantidad_total' =>  $row['total']
						);
					}
				} 
			}else{
				die($mysqli->error);
			}
		}
		if(empty($resultado)){
			$resultado['data'][] = array(
						'id' 		=>	'',
						'acciones' 	=>	'',
						'tipo' 		=>	'',
						'codigo'	=> '',
						'nombre' 	=>	'',
						'costounitario' =>'',
						'valor' =>'',
						'cantidad' 	=>	'',
						'cantidad_disponible' => '',
						'cantidad_comprometido' => '',
						'cantidad_transito' => '',
						'cantidad_total' =>''
				);
		}
		echo json_encode($resultado);
	}
	function guardar_entrada(){
		global $mysqli;
		$fecha = $_REQUEST['fecha'];
		$idproveedor =$_REQUEST['proveedor'];
		$idorden = $_REQUEST['idorden'];
		// $idpic = (!empty($_REQUEST['pic'])) ? $_REQUEST['pic'] : 0;
		// $paciente = (!empty($_REQUEST['paciente'])) ? $_REQUEST['paciente'] : 0;
		$items_array = $_REQUEST['items'];
		$idusuario = $_SESSION['user_id'];

		$tipo = 'orden de compra';
		$movimiento = 'entrada';
		 $i = 0;
 
		$q = "INSERT INTO entradas (idorigen,fecha,idpaciente,idpic,idproveedor,usuario,estado,tipo,movimiento)
			  VALUES ('$idorden','$fecha','0','0','$idproveedor','$idusuario',34,'$tipo','$movimiento')";
		
		if($mysqli->query($q)){
			$identrada =  $mysqli->insert_id;			
			$qdetalle = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,costo,inventario,costounitario,costounitarioactual,cuenta,itbms) VALUES ";
			foreach($items_array as $item){
				if($item['cantidad_final'] != 0){
					// COSTO UNITARIO
					switch($item['tipo']){
						case 'I':
							$tabla = 'insumos';
						break;
						case 'M':
							$tabla = 'medicamentos';
						break;
						case 'E':
							$tabla = 'equipos';
						break;
					}
					$qcantidaddisponible = "SELECT 
												IFNULL(SUM((CASE entradas.movimiento WHEN 'salida' THEN CONCAT('-',edet.cantidad) ELSE edet.cantidad END)),0) as disponible
												,med.costounitario
											FROM $tabla med 
											LEFT JOIN `entradasdetalle` edet ON edet.tipoproducto = 'I' AND edet.idproducto = ".$item['item']." 
											LEFT JOIN entradas ON entradas.id = edet.identrada WHERE med.id = ".$item['item']." ";
					if($r_ = $mysqli->query($qcantidaddisponible)){
						// CALCULAR COSTO UNITARIO NUEVO
						$fetch = $r_->fetch_assoc();
						$disponible = $fetch['disponible'];
						$costounitarioactual = $fetch['costounitario'];
						$costounitariodocumento = $item['costo_unitario'];
						$ingreso = $item['cantidad_final'];
						
						$costounitarionuevo = ($disponible / ($disponible + $ingreso)) * ( $costounitarioactual );
						$costounitarionuevo += ($ingreso / ($disponible + $ingreso)) * ($costounitariodocumento);
						
						$print = array(
							'disponible' 	=> $disponible,
							'ingreso'		=> $ingreso,
							'costounitarioactual' => $costounitarioactual,
							'costounitariodocumento' => $costounitariodocumento
						);
						
						//print_r($print);
						//die();
						
						// ACTUALIZAR COSTO UNITARIO NUEVO
						$q = "UPDATE $tabla SET costounitario = $costounitarionuevo WHERE id = ".$item['item']." ";
						
						if($mysqli->query($q)){
							//
						}
					}
					

					$cantidadfinal =$item['cantidad_final'] ;
					$qdetalle .= "('".$identrada."','".$item['tipo']."',".$item['item'].",".$cantidadfinal.",'".$item['costo_actual']."','1','".$item['costo_unitario']."','".$costounitarioactual."','".$item['cuentacontable']."','".$item['itbms']."'),";

				}
				
			}			
			$qdetalle = substr($qdetalle,0,-1);
			if($mysqli->query($qdetalle)){
				echo $identrada;
			} else {
				echo $qdetalle;
				echo $mysqli->error;
			}
		} else {
			echo $q;
			echo $mysqli->error;
		}	
	}
	function cargar_movimientos(){
		global $mysqli;
		$iditem = $_REQUEST['iditem'];
		$tipo =  $_REQUEST['tipo'];
		$movimiento = $_REQUEST['movimiento'];
		$inventario = $_REQUEST['inventario'];
		if($inventario == 0) $inventario = 1;
		$query = "	SELECT 		en.id as id, en.fecha, SUM(ed.cantidad) as cantidad,en.movimiento as movimiento,UPPER(en.tipo) as tipo,
								IFNULL(p.nombre,'') AS paciente, b.nombre as inventario,
                                
                                (SELECT (CASE en.tipo 
									WHEN 'Orden de compra' THEN (SELECT numero FROM ordendecompra WHERE id = en.idorigen LIMIT 1) 
									WHEN 'Caja menuda' THEN (SELECT numero FROM ordendecompra WHERE id = en.idorigen LIMIT 1) 
									WHEN 'Cierre' THEN (SELECT GROUP_CONCAT(numero) FROM cierres LEFT JOIN acuses acu ON FIND_IN_SET(acu.id,cierres.acuses) WHERE cierres.id = en.idorigen LIMIT 1) 
								END)) as numero
             
					FROM 		entradasdetalle ed
					INNER JOIN 	entradas en ON en.id = ed.identrada
					LEFT JOIN	pacientes p ON p.id = en.idpaciente
					LEFT JOIN 	medicamentos m ON m.id = ed.idproducto AND ed.tipoproducto = 'M' 
					LEFT JOIN 	insumos i ON i.id = ed.idproducto AND ed.tipoproducto = 'I' 
					LEFT JOIN 	equipos e ON e.id = ed.idproducto AND ed.tipoproducto = 'E'
					LEFT JOIN 	bodegas b ON b.id = ed.inventario
					WHERE 		ed.idproducto = '$iditem' AND ed.tipoproducto = '$tipo' and ed.inventario = '$inventario' AND en.tipo != 'acuse de entrega'
				";
		
		
		if($movimiento != ''){
			$query.= " AND en.movimiento = '$movimiento'";
		}
		
		$query.= " GROUP BY en.id";
		//echo $query;
		$resultado = array();
		if($iditem != ''){	
			$result = $mysqli->query($query); 
			while($row = $result->fetch_assoc()){
				$acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>";
				$acciones .= "<span class='icon-col fa fa-print blue imprimir-movimiento' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir Ajuste' data-placement='right'></span>";
				$acciones .= "<span class='icon-col fa fa-print green imprimir-movimientoInterno' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir Ajuste Interno' data-placement='right'></span>";
				if($row['tipo'] == 'ajuste')
				$acciones .= '<span class="icon-col fa fa-pencil blue boton-editar-movimiento"  data-id="'.$row['id'].'" data-tipo="'.$row['tipo'].'" data-usuario="'.$row['usuario'].'" data-fecha="'.$row['fecha'].'" data-toggle="tooltip" data-original-title="Editar" data-placement="right"></span>';
				
				$acciones .= "<span class='icon-col fa fa-trash red borrar-movimiento' data-id='".$row['id']."' data-tipo='".$row['tipo']."' data-numero='".$row['numero']."' data-toggle='tooltip' data-original-title='Eliminar movimiento' data-placement='right'></span>";
				

				$resultado['data'][] = array(
					'acciones'		=> $acciones, 	
					'id'			=> $row['id'], 	
					'fecha'			=> $row['fecha'],
					'paciente'		=> $row['paciente'],
					'pic'			=> '',
					'cantidad'		=> $row['cantidad'],
					'movimiento'	=> $row['movimiento'],
					'tipo'			=> $row['tipo'],
					'inventario'	=> $row['inventario']
				);
			}
		}
		if(empty($resultado)){
			$resultado['data'][] = array(					
				'acciones'		=> '', 	
				'id'			=> '', 	
				'fecha'			=> '',
				'paciente'		=> '',
				'pic'			=> '',
				'cantidad'		=> '',
				'movimiento'	=> '',
				'tipo' 			=> '',
				'inventario'	=> ''
				
			);
		}
		echo json_encode($resultado);			
	}
	 
	function get_ajuste(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$query_det = "SELECT 
					(CASE 
						WHEN ed.tipoproducto ='M' THEN 'Med'
						WHEN ed.tipoproducto ='I' THEN 'Ins'
						WHEN ed.tipoproducto ='E' THEN 'Equ' 
					END) AS tipo,
					(CASE 
						WHEN ed.tipoproducto ='M' THEN m.nombre
						WHEN ed.tipoproducto ='I' THEN i.nombre
						WHEN ed.tipoproducto ='E' THEN e.descripcion 
					END) AS nombre,		
					(CASE 
						WHEN ed.tipoproducto ='M' THEN m.codigo
						WHEN ed.tipoproducto ='I' THEN i.codigo
						WHEN ed.tipoproducto ='E' THEN e.codigo 
					END) AS codigo,					
					b.nombre AS inventario,
					ed.cantidad AS cantidad, 
					ed.costounitario AS costo
				FROM entradasdetalle ed				
				INNER JOIN bodegas b ON b.id = ed.inventario
				LEFT JOIN 	medicamentos m ON m.id = ed.idproducto AND ed.tipoproducto = 'M' 
				LEFT JOIN 	insumos i ON i.id = ed.idproducto AND ed.tipoproducto = 'I' 
				LEFT JOIN 	equipos e ON e.id = ed.idproducto AND ed.tipoproducto = 'E'
				WHERE ed.identrada ='$id'
				GROUP BY ed.id";
		$detalle = array();
		$result_det = $mysqli->query($query_det); 
		while($row_det = $result_det->fetch_assoc()){
			$total = $row_det['cantidad']*$row_det['costo'];
			$resultado['data'][] = array(
				'tipo'			=> $row_det['tipo'], 	
				'codigo'		=> $row_det['codigo'], 	
				'nombre'		=> $row_det['nombre'], 	
				'cantidad'		=> $row_det['cantidad'],
				'costo'	=> number_format($row_det['costo'],2),
				'total'			=> number_format($total,2),
				'inventario'	=> $row_det['inventario']
			);
		}	
		if(empty($resultado)){
			$resultado['data'] = array(					
					'tipo'			=> '', 	
					'codigo'		=> '', 	
					'nombre'		=> '', 	
					'cantidad'		=> '',
					'inventario'	=> '',
					'costo'	=> '',
					'total'			=> ''
			);
		}
		echo json_encode($resultado);	
	}
	
	function cargar_movimientos_todos(){
		global $mysqli;
		
		$desde = $_REQUEST['desde'];
		$hasta =  $_REQUEST['hasta'];
		$paciente =  $_REQUEST['paciente'];
			
		$query = "	SELECT en.id as id, 
						en.tipo as tipo, 
						en.fecha as fecha, 
						en.movimiento as movimiento,
						u.nombre as responsable, u.id as usuario,
						(CASE en.tipo WHEN 'orden de compra' THEN oc.numero WHEN 'caja menuda' THEN oc.numero WHEN 'cierre' THEN GROUP_CONCAT(a.numero) ELSE '' END) as numero,
						en.movimiento as movimiento,UPPER(en.tipo) as tipo
					FROM entradas en
					LEFT JOIN usuarios u on u.id = en.usuario
					LEFT JOIN 	ordendecompra oc ON oc.id = en.idorigen
					LEFT JOIN	cierres c ON c.id = en.idorigen
					LEFT JOIN acuses_cierres ac ON ac.idcierre = c.id
					LEFT JOIN	acuses a ON a.id = ac.idacuse
					WHERE 1 ";
		
		if($desde != ''){
			$query.= " AND en.fecha >= '$desde' ";
		}
		
		if($hasta != ''){
			$query.= " AND en.fecha <= '$hasta' ";
		}
		
		if($paciente != '' && $paciente != '-1'){
			$query.= " AND en.idpaciente = '$paciente' ";
		}
		
		$query.= "	GROUP BY en.id";
		$result = $mysqli->query($query); 

		$resultado = array();
		while($row = $result->fetch_assoc()){
			$resultado['data'][] = array(
				'id'			=> $row['id'], 	
				'fecha'			=> $row['fecha'],
				'tipo'			=> $row['tipo'],
				'movimiento'	=> $row['movimiento'],
				'responsable'	=> $row['responsable']
			);
		}	
		if(empty($resultado)){
			$resultado['data'][] = array(					
				'id'			=> '', 	
				'fecha'			=> '',
				'tipo'			=> '',
				'movimiento'	=> '',
				'responsable'	=> ''
			);
		}
		echo json_encode($resultado);			
	}
	
	function cargar_detalle_ajuste(){
			global $mysqli;
			$identrada = $_REQUEST['identrada'];
			
			$query = "SELECT 
					(CASE 
						WHEN ed.tipoproducto ='M' THEN 'MEDICAMENTOS'
						WHEN ed.tipoproducto ='I' THEN 'INSUMOS'
						WHEN ed.tipoproducto ='E' THEN 'EQUIPOS' 
					END) AS tipo,
					(CASE 
						WHEN ed.tipoproducto ='M' THEN m.nombre
						WHEN ed.tipoproducto ='I' THEN i.nombre
						WHEN ed.tipoproducto ='E' THEN e.descripcion 
					END) AS nombre,		
					(CASE 
						WHEN ed.tipoproducto ='M' THEN m.codigo
						WHEN ed.tipoproducto ='I' THEN i.codigo
						WHEN ed.tipoproducto ='E' THEN e.codigo 
					END) AS codigo,					
					(CASE 
						WHEN ed.inventario ='1' THEN 'Inventario General' ELSE 'Inventario en Panel' 
					END) AS inventario,
					(CASE 
						en.movimiento
						WHEN 'entrada' THEN CONCAT('+',ed.cantidad) 
						WHEN 'salida' THEN CONCAT('-',ed.cantidad) 
					END) AS cantidad, ed.costounitario AS costo
				FROM 		entradasdetalle ed
				LEFT JOIN	entradas en ON en.id = $identrada
				LEFT JOIN 	medicamentos m ON m.id = ed.idproducto AND ed.tipoproducto = 'M' 
				LEFT JOIN 	insumos i ON i.id = ed.idproducto AND ed.tipoproducto = 'I' 
				LEFT JOIN 	equipos e ON e.id = ed.idproducto AND ed.tipoproducto = 'E'
				WHERE ed.identrada = '$identrada'";

			
			$resultado = array();
			if($identrada != 0){
				$result = $mysqli->query($query); 

				while($row = $result->fetch_assoc()){
					$total = $row['cantidad']*$row['costo'];
					$resultado['data'][] = array(
						'tipo'			=> $row['tipo'], 	
						'codigo'		=> $row['codigo'], 	
						'nombre'		=> $row['nombre'], 	
						'cantidad'		=> $row['cantidad'],
						'costounitario'	=> number_format($row['costo'],2),
						'total'			=> number_format($total,2),
						'inventario'	=> $row['inventario']
					);
				}	
			}
			if(empty($resultado)){
				$resultado['data'][] = array(					
					'tipo'			=> '', 	
					'codigo'		=> '', 	
					'nombre'		=> '', 	
					'cantidad'		=> '',
					'inventario'	=> '',
					'costounitario'	=> '',
					'total'			=> ''
				);
			}
			echo json_encode($resultado);
		}
		
	
	function cargar_bodegas(){
		global $mysqli;
		$id= $_REQUEST['iditem'];
		$tipo = $_REQUEST['tipo'];
		$fecha = isset($_REQUEST['fecha'])? $_REQUEST['fecha'] : '';
		
		$resultado = array();
		$query = "SELECT 	iditem, 
				tipo, 
				inventario, 
				comprometido, 
				transito,
				total,
				b.nombre
				FROM 
				cantidades_inventario c
				INNER JOIN bodegas b ON c.inventario = b.id
				WHERE iditem = '$id' AND tipo = '$tipo'";
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc()){

			$resultado['data'][] = array(
				'nombre'			=> $row['nombre'], 	
				'cantidad'			=> $row['total'],
				'comprometido'			=> $row['comprometido'],
				'real'			=> $row['total'] - $row['comprometido']
			);

		}
		if(empty($resultado)){
			$resultado['data'] = array(					
				'nombre'			=> '', 	
				'cantidad'			=> '',
				'comprometido'		=> '',
				'real'		=> '',
			);
		}
		echo json_encode($resultado);	
	}
		
	function cargar_comprometido(){
		global $mysqli;
		$iditem = $_REQUEST['iditem'];
		$tipo = $_REQUEST['tipo'];
		$query ="SELECT DISTINCT p.nombre as paciente, a.fecha as fecha, 
				 ad.idpic as prepic, p.id as idpaciente , b.nombre as bodega, b.id as idinventario
				 
				 FROM acuses_detalle ad 
				 LEFT JOIN acuses a ON a.id = ad.idacuse 
				 LEFT JOIN pacientes p ON p.id = a.idpaciente
				 LEFT JOIN entradas e ON e.idpaciente = p.id AND e.movimiento ='salida'
				 LEFT JOIN entradasdetalle ed ON ed.identrada = e.id AND ed.tipoproducto ='$tipo' AND ed.idproducto = '$iditem'
				 LEFT JOIN bodegas b ON b.id = ad.inventario
				 WHERE ad.iditem ='$iditem' AND ad.tipo ='$tipo' AND a.estatus != 49 
				 GROUP BY a.idpaciente, b.nombre ORDER BY paciente, fecha";
		//die($query);
		$resultado = array();
		if($iditem != ''){
			$result = $mysqli->query($query); 
			while($row = $result->fetch_assoc()){
				// CALCULAR COMPROMETIDO
				$idpaciente = $row['idpaciente'];
				$idinventario = $row['idinventario'];
				
				$qc = "SELECT(
							IFNULL((
									SELECT
										SUM(ad.cantidad)
									FROM
										acuses a
									INNER JOIN acuses_detalle ad ON
										ad.idacuse = a.id
									WHERE
										a.estatus AND ad.tipo = '$tipo' AND ad.iditem = '$iditem' AND a.estatus NOT IN(48, 49) AND ad.inventario = '$idinventario' and a.idpaciente = '$idpaciente'
								),0) 
								+ 
								IFNULL((
										SELECT sum(cd.conteo) FROM cierres c
										INNER JOIN cierredetalle cd ON cd.idcierre = c.id AND cd.tipo = '$tipo' AND cd.iditem = '$iditem' AND cd.inventario = '$idinventario'
										WHERE c.id in   (	
											SELECT MAX(c1.id)  FROM cierres c1 
											INNER JOIN cierredetalle cd1 ON cd1.idcierre = c1.id AND cd1.tipo = '$tipo' AND cd1.iditem = '$iditem' and cd1.inventario = '$idinventario'
											INNER JOIN acuses a1 ON FIND_IN_SET(a1.id,c1.acuses) AND a1.estatus = 48 
											INNER JOIN acuses_detalle ad1 ON ad1.idacuse = a1.id AND ad1.inventario = '$idinventario'
											WHERE c1.idpaciente = '$idpaciente'
											GROUP BY c1.idpaciente
										) AND c.idpaciente = '$idpaciente'
								),0)
							) as comprometido
				"; 
				// if($idinventario ==2)
					// die($qc);
				$rc = $mysqli->query($qc);
				$fc = $rc->fetch_assoc();
				$comprometido = $fc['comprometido'];
				// $comprometido = $row['comprometido'];
				if($comprometido!=0){	
					if($row['prepic'] == '')
						$prepic = 'Sin asignar';
					else 
						$prepic = $row['prepic'];
					$acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>
									<span class='icon-col fa fa-eye blue boton-ver-acuses' data-idpaciente='".$row['idpaciente']."' data-iditem='".$iditem."' data-inventario='".$idinventario."' data-tipo='".$tipo."'data-toggle='tooltip' data-original-title='Ver detalle' data-placement='right'></span>
								</div>";
					$resultado['data'][] = array(
						'acciones'			=> $acciones,
						'paciente'			=> $row['paciente'],
						'prepic'			=> $prepic,
						'fecha'				=> $row['fecha'],
						'cantidad'			=> $comprometido,
						'bodega'			=> $row['bodega']
					);
				}
			}	
		}
		if(empty($resultado)){
			$resultado['data'] = array(		
				'acciones'			=> '',
				'paciente'			=> '',
				'prepic'			=> '',
				'fecha'				=> '',
				'cantidad'			=> '',
				'bodega'			=> ''
			);
		}
		echo json_encode($resultado);	
	}
	function cargar__todo_el_comprometido(){
		global $mysqli;
		$mysqli->autocommit(FALSE);
		$query ="SELECT p.nombre as paciente, a.fecha as fecha, ad.tipo,ad.iditem,iv.nombre,iv.codigo,ad.costounitario,ad.itbms,ad.inventario,
				 ad.idpic as prepic, p.id as idpaciente 
				 FROM acuses_detalle ad 
				 LEFT JOIN acuses a ON a.id = ad.idacuse 
				 LEFT JOIN pacientes p ON p.id = a.idpaciente
				 LEFT JOIN itemsinventario iv ON iv.tipo = ad.tipo AND iv.id = ad.iditem
				 LEFT JOIN entradas e ON e.idpaciente = p.id AND e.movimiento ='salida'
				 LEFT JOIN entradasdetalle ed ON ed.identrada = e.id AND ed.tipoproducto = ad.tipo AND ed.idproducto = ad.iditem
				 WHERE a.estatus != 49 
				 GROUP BY a.idpaciente,ad.tipo,ad.iditem ORDER BY paciente, fecha";
		//die($query);
		$result = $mysqli->query($query); 
		$resultado = array();

		$html = '<table><tr><th>Paciente</th><th>Codigo</th><th>Item</th><th>Comprometido</th></tr>';
		while($row = $result->fetch_assoc()){
			// CALCULAR COMPROMETIDO
			$idpaciente = $row['idpaciente'];
			$tipo = $row['tipo'];
			$iditem = $row['iditem'];
			$nombre = $row['nombre'];
			$costounitario = $row['costounitario'];
			$itbms = $row['itbms'];
			$inventario = $row['inventario'];
			

			$qc = "
			SELECT (
			IFNULL((SELECT SUM(ad.cantidad)
						FROM acuses a
						INNER JOIN acuses_detalle ad ON ad.idacuse = a.id
						WHERE a.estatus AND ad.tipo = '$tipo' AND ad.iditem = '$iditem' AND a.estatus NOT IN (48,49) AND a.idpaciente = $idpaciente )
						,0)
						
						+ 
						IFNULL((SELECT sum(cd.conteo)
						FROM cierres c
						INNER JOIN cierredetalle cd ON cd.idcierre = c.id
						AND cd.tipo = '$tipo' AND cd.iditem = '$iditem'
						WHERE 	c.id IN (	SELECT MAX(c1.id) 
											FROM cierres c1 
											INNER JOIN cierredetalle cd1 ON cd1.idcierre = c1.id AND cd1.tipo = '$tipo' AND cd1.iditem = '$iditem'
											INNER JOIN acuses a1 ON FIND_IN_SET(a1.id,c1.acuses) AND a1.estatus = 48
											WHERE c1.idpaciente = $idpaciente
											GROUP BY c1.idpaciente
										) AND c.idpaciente = $idpaciente
						) 
						,0)
				) as comprometido
			
			";
			//die($qc);
			$rc = $mysqli->query($qc);
			$fc = $rc->fetch_assoc();
			$comprometido = $fc['comprometido'];
			
			if($comprometido > 0){
				$querys = "	INSERT INTO entradas (fecha,idpaciente,idorigen,idpic,idproveedor,usuario,estado,tipo,movimiento)
							VALUES (now(),'$idpaciente',0,'0','0','1',34,'COMPROMETIDO INICIAL','salida')";
				
				$querye = "	INSERT INTO entradas (fecha,idpaciente,idorigen,idpic,idproveedor,usuario,estado,tipo,movimiento)
							VALUES (now(),'$idpaciente',0,'0','0','1',34,'COMPROMETIDO INICIAL','entrada')";
							
				// if($mysqli->query($querys)){
					// $idsalida =	$mysqli->insert_id;
					// if($mysqli->query($querye)){
						// $identrada = $mysqli->insert_id;
						// // ENTRADA DETALLE
						// $qdetalle_entrada = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,costo,inventario,itbms) VALUES ";	
						// $qdetalle_entrada .= "($identrada,'$tipo',$iditem,$comprometido,$costounitario,10,$itbms)";	
						// //SALIDA DETALLE
						// $qdetalle_salida = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,costo,inventario,itbms) VALUES ";	
						// $qdetalle_salida .= "($idsalida,'$tipo',$iditem,$comprometido,$costounitario,$inventario,$itbms)";
						
						// if(!$mysqli->query($qdetalle_entrada)){
							// die($qdetalle_entrada);
						// }
						
						
						// if(!$mysqli->query($qdetalle_salida)){
							// die($qdetalle_salida);
						// }
					// } else {
						// echo 0;
						// die($querye);
					// }
				// } else {
					// echo 1;
					// die($querys);
				// }
				
			}
			
			
			
			if($comprometido!=0){	
				if($row['prepic'] == '')
					$prepic = 'Sin asignar';
				else 
					$prepic = $row['prepic'];
				$acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>
								<span class='icon-col fa fa-eye blue boton-ver-acuses' data-idpaciente='".$row['idpaciente']."' data-iditem='".$iditem."' data-tipo='".$tipo."'data-toggle='tooltip' data-original-title='Ver detalle' data-placement='right'></span>
							</div>";
				$resultado['data'][] = array(
					'acciones'			=> $acciones,
					'paciente'			=> $row['paciente'],
					'prepic'			=> $prepic,
					'fecha'				=> $row['fecha'],
					'cantidad'			=> $comprometido
				);
				
				$html .= '<tr><td>'.$row['paciente'].'</td><td>'.$row['codigo'].'</td><td>'.$row['nombre'].'</td><td>'.$comprometido.'</td></tr>';
			}
		}	
		if(empty($resultado)){
			$resultado['data'] = array(		
				'acciones'			=> '',
				'paciente'			=> '',
				'prepic'			=> '',
				'fecha'				=> '',
				'cantidad'			=> ''
			);
		}
		//echo json_encode($resultado);	
		
		$html .= '</table>';
		
		$mysqli->commit();
		echo $html;
	}			 
	
	function mover_inventario(){
		global $mysqli;
		$idusuario = $_SESSION['user_id'];
		$iditem = $_REQUEST['iditem'];
		$tipoproducto = $_REQUEST['tipoproducto'];
		$inventario_origen = $_REQUEST['inventario_origen'];
		$inventario_destino = $_REQUEST['inventario_destino'];
		$cantidad = $_REQUEST['cantidad'];
		$fecha = $_REQUEST['fecha'];
		switch($tipoproducto){
			case "I":
				$tabla = "insumos";
			break;
			case "E":
				$tabla = "equipos";
			break;
			case "M":
				$tabla = "medicamentos";
			break;
		}
		$itbms = getValor('itbms',$tabla,$iditem);
		
		$tipo_entrada = 'movimiento de bodegas';		 

		$querys = "INSERT INTO entradas (fecha,idpaciente,idpic,idproveedor,usuario,estado,tipo,movimiento)
			  VALUES ('$fecha','0','0','0','$idusuario',34,'$tipo_entrada','salida')";
		
		if($mysqli->query($querys)){
			$idsalida =  $mysqli->insert_id;
			$qdetalles = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,costo,inventario,itbms) VALUES 
						('$idsalida', '$tipoproducto', '$iditem','$cantidad','0','$inventario_origen','$itbms')";		
			if($mysqli->query($qdetalles)){
				$querye = "INSERT INTO entradas (idorigen,fecha,idpaciente,idpic,idproveedor,usuario,estado,tipo,movimiento)
					  VALUES ('$idsalida','$fecha','0','0','0','$idusuario',34,'$tipo_entrada','entrada')";				
				if($mysqli->query($querye)){
					$identrada =  $mysqli->insert_id;
					$qdetallee = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,costo,inventario,itbms) VALUES 
								('$identrada', '$tipoproducto', '$iditem','$cantidad','0','$inventario_destino','$itbms')";		
					if($mysqli->query($qdetallee)){
						echo 1;
					} else {
						echo $qdetallee;
						echo $mysqli->error;
					}
				} else {
					echo $querye;
					echo $mysqli->error;
				}	
			} else {
				echo $qdetalles;
				echo $mysqli->error;
			}
		} else {
			echo $querys;
			echo $mysqli->error;
		}	
	}
	
	function getOrden(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$query ="SELECT odc.id as id,odc.estado as estado_orden, odc.fecha as fecha, odc.idproveedor as proveedor,pro.empresa as nombreproveedor, p.nombre as 	  nombre_paciente, odc.idpic as prepic, u.nombre as nombre_colaborador,
				odc.idusuario as colaborador, p.id as paciente, odc.formapago as formapago, odc.observaciones as observaciones,
				odcp.fechaentrega as fechaentrega, 
				(CASE WHEN odcp.formapago = '1' THEN 'CONTADO' ELSE 'CRÉDITO' END )as formapago_prov, 
				(CASE 	WHEN odcp.mediopago ='1' THEN 'Efectivo' 
						WHEN odcp.mediopago ='2' THEN 'Cheque' 
						WHEN odcp.mediopago ='3' THEN 'Transferencia' 
				 END) as mediopago, 
				(CASE WHEN odcp.modoentrega = '1' THEN 'Trasporte por parte de proveedor' ELSE 'Recoger en bodega de proveedor' END)as modoentrega,
				odcp.comentarios as comentarios 
				FROM ordendecompra odc 				
				LEFT JOIN prepic pic ON odc.idpic = pic.numero
				LEFT JOIN pacientes p ON pic.idpacientes = p.id
				LEFT JOIN proveedores pro ON pro.id = odc.idproveedor
				LEFT JOIN usuarios u ON u.id = odc.idusuario
				LEFT JOIN ordendecompraproveedor odcp ON odcp.idorden = odc.id
				WHERE odc.id = '$id'";
		if ($result = $mysqli->query($query)){
			$respuesta = array();
			while($row = $result->fetch_assoc()){
				$respuesta['data'] = array(
					'id' => $row['id'],
					'fecha' => $row['fecha'],
					'prepic' => $row['prepic'],	
					'paciente' => $row['paciente'],
					'proveedor' => $row['proveedor'],
					'colaborador' => $row['colaborador'],
					'formapago' => $row['formapago'],
					'observaciones' => $row['observaciones'],
					'nombre_paciente' => $row['nombre_paciente'],
					'nombreproveedor' => $row['nombreproveedor'],
					'nombre_colaborador' => $row['nombre_colaborador'],
					'estado_orden' => $row['estado_orden'],
					'fechaentrega' => $row['fechaentrega'],
					'modopago' => $row['formapago_prov'],
					'mediopago' => $row['mediopago'],
					'modoentrega' => $row['modoentrega'],
					'comentarios' => $row['comentarios']
				);

				$respuesta['detalle'] = array();
			}
			$query_detalle ="SELECT odc.id as id, odc.tipo as tipo, odc.itbms as itbms,
								(CASE 
									WHEN odc.tipo ='M' THEN 'Medicamentos' 
									WHEN odc.tipo ='E' THEN 'Equipos' 
									WHEN odc.tipo ='I' THEN 'Insumos' 
								END) as tipotext,  odc.cantidad as cantidad, odc.costo as costo,
								concat(c.cuenta,' | ',c.nombre) as cuentacontabletext, 
								odc.item as item,
								(CASE 
									WHEN odc.tipo ='M' THEN med.cuentacostoventa 
									WHEN odc.tipo ='E' THEN equi.cuentacostoventa 
									WHEN odc.tipo ='I' THEN ins.cuentacostodeventa 
								END) as cuentacontable,
								(CASE 
									WHEN odc.tipo ='M' THEN med.nombre 
									WHEN odc.tipo ='E' THEN equi.descripcion 
									WHEN odc.tipo ='I' THEN ins.nombredecompra 
								END) as itemtext,
								IFNULL((CASE 
									WHEN odc.tipo ='M' THEN med.unidadcompra 
									WHEN odc.tipo ='E' THEN equi.unidadcompra 
									WHEN odc.tipo ='I' THEN ins.unidadcompra 
								END),'') as unidad,
								IFNULL(odcp.cantidad_disponible,odc.cantidad) as cantidad_disponible, IFNULL(odcp.precio,odc.costo) as precionuevo, odcp.observaciones  as observaciones
							FROM ordendecompradetalle odc 
								LEFT JOIN medicamentos med ON med.id = odc.item 
								LEFT JOIN equipos equi ON equi.id = odc.item 
								LEFT JOIN insumos ins ON ins.id = odc.item 
								LEFT JOIN cuentascontables c ON c.id = odc.cuenta							
								LEFT JOIN ordendecompradetalleproveedor odcp ON odcp.iddetalleodc = odc.id
							WHERE odc.idorden = '$id'
					";
			$result_detalle = $mysqli->query($query_detalle);
			while($row_detalle = $result_detalle->fetch_assoc()){
				if( $row_detalle['unidad'] != 0 &&  $row_detalle['unidad'] != '')
					$costounitario =  ($row_detalle['precionuevo'] / $row_detalle['unidad']);
				else
					$costounitario = $row_detalle['precionuevo']/1;
				$respuesta['detalle'][] = array(
					'id' => $row_detalle['id'],
					'tipo' => $row_detalle['tipo'],
					'tipotxt' => $row_detalle['tipotext'],
					'item' => $row_detalle['item'],
					'itemtxt' => $row_detalle['itemtext'],
					'cuentacontable' => $row_detalle['cuentacontable'],
					'cuentacontabletxt' => $row_detalle['cuentacontabletext'],
					'cantidad_solicitada' => $row_detalle['cantidad'],
					'unidad' => $row_detalle['unidad'],
					'costo' => $row_detalle['costo'],
					'cantidad_extra' => '0',
					'itbms' => $row_detalle['itbms'],
					'costo_unitario' => $costounitario,
					'cantidad_recibida' => $row_detalle['cantidad_disponible'],
					'cantidad_final' => $row_detalle['cantidad_disponible']*$row_detalle['unidad'],
					'costo_actual' => $row_detalle['precionuevo'],
					'total' => ($row_detalle['cantidad'] * $row_detalle['precionuevo']),
					'observaciones' => $row_detalle['observaciones'],
				);
			}
			echo json_encode($respuesta);
		}else{
			die($mysqli->error);
		}

	}
	
	function eliminarMovimiento(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$tipo = strtoupper($_REQUEST['tipo']);
		// OBTENER ID ORIGEN
		$qo = "SELECT idorigen,movimiento FROM entradas WHERE id = $id LIMIT 1";
		
		$ro = $mysqli->query($qo);
		$fo = $ro->fetch_assoc();
		$idorigen = $fo['idorigen'];
		$movimiento = $fo['movimiento'];
		if($idorigen != 0){
			switch($tipo){
				case 'ORDEN DE COMPRA':
					// LLEVAR ESTATUS DE OC A 39
					$idoc = $idorigen;
					$qu = "UPDATE ordendecompra SET estado = 39 WHERE id = $idoc";
					if($mysqli->query($qu)){
						//
					} else {
						die('Error al actualizar la OC: '.$mysqli->error);
					}
				break;
				case 'CAJA MENUDA':
					// ANULAR ORDEN DE COMPRA
					$idoc = $idorigen;
					$qu = "UPDATE ordendecompra SET estado = 40 WHERE id = $idoc";
					if($mysqli->query($qu)){
						//
					} else {
						die('Error al anular la OC: '.$mysqli->error);
					}
				break;
				case 'CIERRE':
					// LLEVAR ESTATUS DE ACUSES DEL CIERRE A 47 SI EL CIERRE ES TOTAL
					$idcierre = $idorigen;
					$idacuses = 0;
					// OBTENER id DE ACUSES
					$qs = "SELECT acuses FROM cierres WHERE id = $idcierre";
					$rs = $mysqli->query($qs);
					if($rs->num_rows > 0){
						// EL CIERRE FUE TOTAL
						$fs = $rs->fetch_assoc();
						$idacuses = $fs['acuses'];
						
						// ELIMINAR CIERRE
						$qd = "DELETE FROM cierres WHERE id = $idcierre LIMIT 1 ";
						$mysqli->query($qd);
						
						$qde = "DELETE FROM cierredetalle WHERE idcierre = $idcierre ";
						$mysqli->query($qde);
						
						// UPDATE ACUSES
						$qs = "UPDATE acuses SET estatus = 47 WHERE FIND_IN_SET(id,'$idacuses')";
						if($mysqli->query($qs)){
							//
						} else {
							die('Error al actualizar los acuses: '.$mysqli->error);
						}
					}
					$query_detalle_cantidades="SELECT DISTINCT iditem,tipo FROM acuses_detalle WHERE idacuse in ($idacuses)";
					$arreglo = array();
					$arr = $mysqli->query($query_detalle_cantidades);
					while($registro = $arr->fetch_assoc()){
						$arreglo[]=array(
							'id' => $registro['iditem'],
							'tipo' => $registro['tipo']
						);
					}
					if(!empty($arreglo)){
						foreach($arreglo as $item){										
							$query ="select distinct id as inventario From bodegas";
							$r = $mysqli->query($query);
							while($row = $r->fetch_assoc()){
								$q = "UPDATE cantidades_inventario SET 
										comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
										total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
									WHERE
										iditem = '".$item['id']."' AND
										tipo = '".$item['tipo']."' AND
										inventario = '".$row['inventario']."'
									";
								$mysqli->query($q);
							}
						}
					};
				break;
				case 'AJUSTE POR CONTEO':
					$query_detalle_cantidades="SELECT DISTINCT iditem,tipo FROM conteodetalle WHERE idconteo = $idorigen ";
					$arreglo = array();
					$arr = $mysqli->query($query_detalle_cantidades);
					while($registro = $arr->fetch_assoc()){
						$arreglo[]=array(
							'id' => $registro['iditem'],
							'tipo' => $registro['tipo']
						);
					}
					// ELIMINAR CONTEO
					$qd = "DELETE FROM conteos WHERE id = $idorigen LIMIT 1 ";
					$mysqli->query($qd);				
					
					$qde = "DELETE FROM conteodetalle WHERE idconteo = $idorigen ";
					$mysqli->query($qde);
					if(!empty($arreglo)){
						foreach($arreglo as $item){										
							$query ="select distinct id as inventario From bodegas";
							$r = $mysqli->query($query);
							while($row = $r->fetch_assoc()){
								$q = "UPDATE cantidades_inventario SET 
										comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
										total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
									WHERE
										iditem = '".$item['id']."' AND
										tipo = '".$item['tipo']."' AND
										inventario = '".$row['inventario']."'
									";		
								$mysqli->query($q);	
							}

						}
					};

				break;
				case 'MOVIMIENTO DE BODEGA':
					// BUSCAR MOVIMIENTO DE ENTRADA Y DE SALIDA
					switch($movimiento){
						case 'entrada':
							// AQUI MISMO ESTA EL ID DE SALIDA
							$q = "SELECT idorigen FROM entradas WHERE id = $id LIMIT 1";
							$r = $mysqli->query($q);
							$f = $r->fetch_assoc();
							$idsalida = $f['idorigen'];
							$query_detalle_cantidades="SELECT DISTINCT idproducto as iditem,tipoproducto as tipo FROM entradasdetalle WHERE identrada = '$idsalida'";
							$arreglo = array();
							$arr = $mysqli->query($query_detalle_cantidades);
							while($registro = $arr->fetch_assoc()){
								$arreglo[]=array(
									'id' => $registro['iditem'],
									'tipo' => $registro['tipo']
								);
							}
							$mysqli->query("DELETE FROM entradasdetalle WHERE identrada = $idsalida");
							if(!$mysqli->query("DELETE FROM entradas WHERE id = $idsalida LIMIT 1")){
								die('Error al eliminar la entrada: '.$mysqli->error);
			 
		
							}
						break;
						case 'salida':
							// BUSCAR DONDE TIPO SEA ENTRADA Y ID ORIGEN SEA ESTE
							$q = "SELECT id FROM entradas WHERE idorigen = $id LIMIT 1";
							$r = $mysqli->query($q);
							$f = $r->fetch_assoc();
							$identrada = $f['id'];
							$query_detalle_cantidades="SELECT DISTINCT idproducto as iditem,tipoproducto as tipo FROM entradasdetalle WHERE identrada = '$identrada'";
							$arreglo = array();
							$arr = $mysqli->query($query_detalle_cantidades);
							while($registro = $arr->fetch_assoc()){
								$arreglo[]=array(
									'id' => $registro['iditem'],
									'tipo' => $registro['tipo']
								);
							}
							if(!$mysqli->query("DELETE FROM entradas WHERE id = $identrada LIMIT 1")){
								die('Error al eliminar la entrada: '.$mysqli->error);
							}
							$mysqli->query("DELETE FROM entradasdetalle WHERE identrada = $identrada");
						break;
					}
					if(!empty($arreglo)){
						foreach($arreglo as $item){										
							$query ="select distinct id as inventario From bodegas";
							$r = $mysqli->query($query);
							while($row = $r->fetch_assoc()){
								$q = "UPDATE cantidades_inventario SET 
										comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
										total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
									WHERE
										iditem = '".$item['id']."' AND
										tipo = '".$item['tipo']."' AND
										inventario = '".$row['inventario']."'
									";
								$mysqli->query($q);									
							}

						}
					};
				break;
			}
			
			// ELIMINAR MOVIMIENTO
			$qd = "DELETE FROM entradas WHERE idorigen = '$idorigen' AND UPPER(tipo) = '$tipo' LIMIT 2";
			$qdd = "DELETE FROM entradasdetalle WHERE identrada IN (SELECT id FROM entradas WHERE idorigen = '$idorigen' AND UPPER(tipo) = '$tipo') ";
			
			$query_detalle_cantidades="SELECT DISTINCT idproducto as iditem,tipoproducto as tipo FROM entradasdetalle WHERE identrada IN (SELECT id FROM entradas WHERE idorigen = '$idorigen' AND UPPER(tipo) = '$tipo') ";
			$arreglo = array();
			$arr = $mysqli->query($query_detalle_cantidades);
			while($registro = $arr->fetch_assoc()){
				$arreglo[]=array(
					'id' => $registro['iditem'],
					'tipo' => $registro['tipo']
				);
			}			
			if($mysqli->query($qdd) && $mysqli->query($qd)){
				if(!empty($arreglo)){
					foreach($arreglo as $item){										
						$query ="select distinct id as inventario From bodegas";
						$r = $mysqli->query($query);
						while($row = $r->fetch_assoc()){
							$q = "UPDATE cantidades_inventario SET 
									comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
									total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
								WHERE
									iditem = '".$item['id']."' AND
									tipo = '".$item['tipo']."' AND
									inventario = '".$row['inventario']."'
								";
							$mysqli->query($q);
	
						}

					}
				};
				echo 1;
			} else {
				die('Error al eliminar el movimiento: '.$mysqli->error);
			}
		} else {
			// ELIMINAR MOVIMIENTO
			$qd = "DELETE FROM entradas WHERE id = $id LIMIT 1";
			$qdd = "DELETE FROM entradasdetalle WHERE identrada = $id";
				$query_detalle_cantidades="SELECT DISTINCT idproducto as iditem,tipoproducto as tipo FROM entradasdetalle WHERE identrada = $id";
			$arreglo = array();
			$arr = $mysqli->query($query_detalle_cantidades);
			while($registro = $arr->fetch_assoc()){
				$arreglo[]=array(
					'id' => $registro['iditem'],
					'tipo' => $registro['tipo']
				);
			}
			if($mysqli->query($qd) && $mysqli->query($qdd)){
				if(!empty($arreglo)){
					foreach($arreglo as $item){										
						$query ="select distinct id as inventario From bodegas";
						$r = $mysqli->query($query);
						while($row = $r->fetch_assoc()){
							$q = "UPDATE cantidades_inventario SET 
									comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
									total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
								WHERE
									iditem = '".$item['id']."' AND
									tipo = '".$item['tipo']."' AND
									inventario = '".$row['inventario']."'
								";
								$mysqli->query($q);								
						}

					}
				};
				echo 1;
			} else {
				die('Error al eliminar el movimiento: '.$mysqli->error);
			}
		}
		
	}
	
	function listar_acuses(){
		global $mysqli;
		$idpaciente = $_REQUEST['idpaciente'];		
		$iditem = $_REQUEST['iditem'];
		$tipo= $_REQUEST['tipo'];
		$inventario= $_REQUEST['inventario'];
		$query= "SELECT a.id as id,'ACUSE DE ENTREGA' as tipo, a.numero as numero, a.fecha as fecha, ad.cantidad as cantidad, a.estatus as estatus, es.nombre as estado
				FROM acuses a 
				LEFT JOIN acuses_detalle ad ON ad.idacuse = a.id AND a.idpaciente = '$idpaciente'
				LEFT JOIN estados es ON es.id = a.estatus
				WHERE a.idpaciente = '$idpaciente' AND ad.iditem = '$iditem' AND ad.tipo = '$tipo' AND a.estatus != 49 AND ad.inventario ='$inventario'
				UNION
				SELECT e.id as id, UPPER(e.tipo) AS tipo, e.id as numero, e.fecha as fecha,  CONCAT('-',ed.cantidad) as cantidad ,'-' as estatus,'-' as estado
				FROM entradas e
				LEFT JOIN cierres c ON c.id = e.idorigen
				LEFT JOIN entradasdetalle ed ON ed.identrada = e.id AND e.idpaciente = '$idpaciente'
				WHERE e.idpaciente = '$idpaciente' AND ed.idproducto = '$iditem' AND ed.tipoproducto = '$tipo' AND c.tipo = 'parcial' AND  e.movimiento ='salida' AND ed.inventario ='$inventario'";
				
		//echo $query; die(); 
		$result = $mysqli->query($query);
		$resultado = array();
		
		$tabla='<table id="tabla_acuses_paciente" class="table table-striped table-bordered" style="margin-left:5%; width:90%">
			<thead>
				<tr>
					<th style="width:90px">Acciones</th>
					<th>Tipo</th>
					<th>Estado</th>
					<th>Nro</th>
					<th>Fecha</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>';
		$eciste = 0;
		while($row = $result->fetch_assoc()){
			$existe =1;
			$acciones ="<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>"; 
			
			if($row['tipo'] == 'ACUSE DE ENTREGA'){
				$imprimir = "<a target='_blank' href='controller/imprimirAcuse.php?id=".$row['id']."' ><span class='icon-col blue fa fa-print boton-imprimir-acuse' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir Acuse' data-placement='right'></span></a>";
				$imprimirInterno = "<a href='controller/imprimirAcuseInterno.php?id=".$row['id']."' target='_blank'><span class='icon-col green fa fa-print boton-imprimir-acuse-interno' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir Acuse Interno' data-placement='right'></span></a>";
				if($row['estatus'] == 48 || $row['estatus'] == 49){ 
					$editar= "<span class='icon-col blue fa fa-pencil boton-editar-acuse-gris' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Editar Acuse' data-placement='right'  style='background:#D5DBDB'></span>";		
					$eliminar = "<span class='icon-col red fa fa-trash boton-eliminar-acuse-gris' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Eliminar acuse' data-placement='right' style='background:#D5DBDB'></span>";						
				}else{
					$editar= "<span class='icon-col blue fa fa-pencil boton-editar-acuse' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Editar Acuse' data-placement='right'></span>";
					if(1){
						$eliminar = "<span class='icon-col red fa fa-trash boton-eliminar-acuse' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Eliminar acuse' data-placement='right'></span>";
					} else {
						$eliminar = "";
					}
				}
			}else{
				$imprimir = "<a target='_blank' href='controller/imprimirAjuste.php?id=".$row['id']."'><span class='icon-col blue fa fa-print boton-imprimir-cierre' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir cierre' data-placement='right'></span></a>";
				$imprimirInterno = "<a target='_blank' href='controller/imprimirAjusteInterno.php?id=".$row['id']."'><span class='icon-col green fa fa-print boton-imprimir-cierre-interno' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Imprimir cierre Interno' data-placement='right'></span></a>";
				$editar= "<span  class='icon-col blue fa fa-pencil boton-editar-cierre-gris' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Editar cierre' style='background:#D5DBDB' data-placement='right'></span>";
				$eliminar = "<span class='icon-col red fa fa-trash boton-eliminar-cierre-gris' data-id='".$row['id']."' data-toggle='tooltip' data-original-title='Eliminar cierre' data-placement='right' style='background:#D5DBDB'></span>";

			}
			
			//if($_SESSION['usuario'] == 'maxia'){
			
			$acciones .= $imprimir.
							$imprimirInterno.
							$editar.
							$eliminar.
					 "</div>";
		
			$tabla.="<tr>
						<td>".$acciones."</td>
						<td>".$row['tipo']."</td>
						<td>".$row['estado']."</td>
						<td>".$row['numero']."</td>
						<td>".$row['fecha']."</td>
						<td>".$row['cantidad']."</td>
					</tr>";
		}		
		$tabla.='</tbody></table>';
		if($existe == 1)
			echo $tabla;
		else{
			echo "No se encontró registros";
		}
	}
	
	function cargar_por_pacientes(){
		global $mysqli;
		$idpaciente = $_GET['idpaciente'];
		$empresa = isset($_COOKIE['empresa'])?$_COOKIE['empresa']:0;
		$organizacion = isset($_COOKIE['organizacion'])?$_COOKIE['organizacion']:0;
		$nivel_usuario = $_SESSION['nivel'];
		$id_usuario = $_SESSION['user_id'];
		$query ="SELECT p.id,p.nombre,p.cedula FROM pacientes p
				   LEFT JOIN empresas emp ON emp.id = p.empresa_id
				   LEFT JOIN organizaciones o ON o.id = emp.idorganizacion
				   LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id	
				   WHERE 1 
		";
		
		if($idpaciente != 0){
			$query.=" AND id = '$idpaciente' ";
		}
			   
		if ($nivel_usuario == 2 || $nivel_usuario == 5){
			  $idmedico = $mysqli->query("Select id from medicos where idusuario = $id_usuario")->fetch_assoc()['id'];
			  $query .= " AND (pu.usuario_id = '$id_usuario' OR p.idmedicotratante = $idmedico )";
		} else{
			if( $empresa != 0 ){
				$query .= " AND p.empresa_id = '".$empresa."' ";
			}else{
			if($organizacion != 0){
				$query .= " AND o.id = '".$organizacion."' ";
			}
			}		
		}
		$query .= " GROUP BY p.id ";
		
		$result = $mysqli->query($query);
		$resultado = array();
		while($row = $result->fetch_assoc()){
			$acciones ="<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>"; 
			$ver_items = '<span class="icon-col fa fa-eye blue boton-ver-items"  data-idpaciente="'.$row['id'].'"  data-toggle="tooltip" data-original-title="Ver histórico" data-placement="right"></span>';
					
			$acciones .= $ver_items.
					 "</div>";
			$resultado['data'][] = array(
				'id' => $row['id'],
				'acciones' => $acciones,
				'nombre'  =>$row['nombre'],
				'cedula' => $row['cedula']
			);
		}
		echo json_encode($resultado);
	}
	
	function getConteo(){
		global $mysqli;
		
		$idpaciente = $_REQUEST['idpaciente'];
		$desde =  isset($_REQUEST['desde'])?$_REQUEST['desde']: '';
		$hasta =  isset($_REQUEST['hasta'])?$_REQUEST['hasta']: '';
		
		$resultado = array();
		// AND (a.fecha BETWEEN '$fechamin' AND '$fechamax')
		$q = "	SELECT 		a.id as idacuse,a.numero as numero, a.fecha, ad.iditem, ad.tipo, SUM(ad.cantidad) as cantidad,
							(CASE 
									WHEN ad.tipo ='M' THEN m.nombre 
									WHEN ad.tipo ='I' THEN i.nombre 
									WHEN ad.tipo ='E' THEN e.descripcion 
							END) as itemtxt,
							(CASE 
									WHEN ad.tipo ='M' THEN m.costo 
									WHEN ad.tipo ='I' THEN i.costo 
									WHEN ad.tipo ='E' THEN e.costo 
							END) as costo,
							(CASE 
									WHEN ad.tipo ='M' THEN m.cuentacostoventa 
									WHEN ad.tipo ='I' THEN i.cuentacostodeventa 
									WHEN ad.tipo ='E' THEN e.cuentacostoventa 
							END) as cuenta,
							(CASE 
									WHEN ad.tipo ='M' THEN m.codigo 
									WHEN ad.tipo ='I' THEN i.codigo
									WHEN ad.tipo ='E' THEN e.codigo 
							END) as codigo,
							ad.costo as costounitario, 
							IFNULL(pd.cantidad,0) as cantidad_facturada,
							IFNULL(SUM(ed.cantidad),0) as consumido,
							IFNULL(ad.idpic, '') as idpic, ad.facturable
				FROM 		acuses a
				INNER JOIN 	acuses_detalle ad ON ad.idacuse = a.id
				LEFT JOIN 	medicamentos m ON m.id = ad.iditem AND ad.tipo = 'M' 
				LEFT JOIN 	insumos i ON i.id = ad.iditem  AND ad.tipo = 'I' 
				LEFT JOIN 	equipos e ON e.id = ad.iditem  AND ad.tipo = 'E'
				LEFT JOIN   prepicdetalle pd ON pd.idprepic = ad.idpic AND pd.idactividad = ad.iditem AND pd.tipo = (CASE WHEN ad.tipo ='M' THEN 'medicamentos'
									WHEN ad.tipo ='I' THEN 'insumos'
									WHEN ad.tipo ='E' THEN 'equipos' END)
				LEFT JOIN	cierres c ON FIND_IN_SET(a.id,c.acuses)
                LEFT JOIN   entradas en ON en.idpaciente = a.idpaciente AND en.tipo = 'cierre' AND en.movimiento = 'salida' AND en.idorigen = c.id
				LEFT JOIN   entradasdetalle ed ON ed.identrada = en.id AND ed.idproducto = ad.iditem AND ed.tipoproducto = ad.tipo
				WHERE 		a.idpaciente = $idpaciente AND a.estatus <> 49 ";
				if($desde != ''){
					$q.= "AND a.fecha >= '$desde' ";
				}
				if($hasta != ''){
					$q.= "AND a.fecha <= '$hasta' ";	
				} 
		$q .= "		
				GROUP BY	ad.iditem
				ORDER BY	tipo,itemtxt";
		if($idpaciente != 0){
			if($r = $mysqli->query($q)){
				while($fetch = $r->fetch_assoc()){
					$imprimir = "<a target='_blank' href='controller/imprimirAcuse.php?id=".$fetch['idacuse']."' ><span class='icon-col blue fa fa-print boton-imprimir-acuse' data-id='".$fetch['idacuse']."' data-toggle='tooltip' data-original-title='Imprimir Acuse' data-placement='right'></span></a>";
					$imprimirInterno = "<a href='controller/imprimirAcuseInterno.php?id=".$fetch['idacuse']."' target='_blank'><span class='icon-col green fa fa-print boton-imprimir-acuse-interno' data-id='".$fetch['idacuse']."' data-toggle='tooltip' data-original-title='Imprimir Acuse Interno' data-placement='right'></span></a>";
					switch($fetch['tipo']){
						case 'I':
							$tipotxt = 'Insumos';
						break;
						case 'M':
							$tipotxt = 'Medicamentos';
						break;
						case 'E':
							$tipotxt = 'Equipos';
						break;
					}
					
					setlocale(LC_TIME,"es_ES");
					
					$resultado['data'][] = array(
						'acciones' => '<div>'.$imprimir.$imprimirInterno.'</div>',
						'acuse' => $fetch['idacuse'],
						'numero' => $fetch['numero'],
						'codigo' => $fetch['codigo'],
						'tipo' => $fetch['tipo'],
						'tipotxt' => $tipotxt,
						'fecha' => date('M-d',strtotime($fetch['fecha'])),
						'itemid' => $fetch['iditem'],
						'consumido' => $fetch['consumido'],
						'idpic' => $fetch['idpic'],
						'facturable' => $fetch['facturable'],
						'cantidad_facturada' => $fetch['cantidad_facturada'],
						'itemtxt' => $fetch['itemtxt'],
						'costounitario' => getCostoPromedio($fetch['iditem'],$fetch['tipo']),//$fetch['costounitario'],
						'costo' => floatval(getCostoPromedio($fetch['iditem'],$fetch['tipo']))*intval($fetch['cantidad']),
						'cuenta' => $fetch['cuenta'],
						'cantidad' => $fetch['cantidad']
					);
				}
			} else {
				die($mysqli->error);
			}
		}
		if(empty($resultado)){
			$resultado['data'] = array(		
				'acuse' => '',
				'numero' => '',
				'tipo' => '',
				'codigo' => '',
				'tipotxt' => '',
				'fecha' => '',
				'itemid' => '',
				'consumido' => '',
				'idpic' => '',
				'facturable' => '',
				'cantidad_facturada' => '',
				'itemtxt' => '',
				'costounitario' => '',
				'costo' => '',
				'cuenta' => '',
				'cantidad' => '',
			);
		}
		echo json_encode($resultado);
			
	}
	
	
	
	function getConteo2(){
		global $mysqli;
		
		$idpaciente = $_REQUEST['idpaciente'];
		$desde =  isset($_REQUEST['desde'])?$_REQUEST['desde']: '';
		$hasta =  isset($_REQUEST['hasta'])?$_REQUEST['hasta']: '';
		
		$resultado = array();
		// AND (a.fecha BETWEEN '$fechamin' AND '$fechamax')
		$q = "	SELECT 		a.id as idacuse,a.numero as numero, a.fecha, ad.iditem, ad.tipo, ad.cantidad as cantidad,
							(CASE 
									WHEN ad.tipo ='M' THEN m.nombre 
									WHEN ad.tipo ='I' THEN i.nombre 
									WHEN ad.tipo ='E' THEN e.descripcion 
							END) as itemtxt,
							(CASE 
									WHEN ad.tipo ='M' THEN m.costo 
									WHEN ad.tipo ='I' THEN i.costo 
									WHEN ad.tipo ='E' THEN e.costo 
							END) as costo,
							(CASE 
									WHEN ad.tipo ='M' THEN m.cuentacostoventa 
									WHEN ad.tipo ='I' THEN i.cuentacostodeventa 
									WHEN ad.tipo ='E' THEN e.cuentacostoventa 
							END) as cuenta,
							(CASE 
									WHEN ad.tipo ='M' THEN m.codigo 
									WHEN ad.tipo ='I' THEN i.codigo
									WHEN ad.tipo ='E' THEN e.codigo 
							END) as codigo,
							ad.costo as costounitario, 
							IFNULL(pd.cantidad,0) as cantidad_facturada,
							IFNULL(SUM(ed.cantidad),0) as consumido,
							IFNULL(ad.idpic, '') as idpic, ad.facturable
				FROM 		acuses a
				INNER JOIN 	acuses_detalle ad ON ad.idacuse = a.id
				LEFT JOIN 	medicamentos m ON m.id = ad.iditem AND ad.tipo = 'M' 
				LEFT JOIN 	insumos i ON i.id = ad.iditem  AND ad.tipo = 'I' 
				LEFT JOIN 	equipos e ON e.id = ad.iditem  AND ad.tipo = 'E'
				LEFT JOIN   prepicdetalle pd ON pd.idprepic = ad.idpic AND pd.idactividad = ad.iditem AND pd.tipo = (CASE WHEN ad.tipo ='M' THEN 'medicamentos'
									WHEN ad.tipo ='I' THEN 'insumos'
									WHEN ad.tipo ='E' THEN 'equipos' END)
				LEFT JOIN	cierres c ON FIND_IN_SET(a.id,c.acuses)
                LEFT JOIN   entradas en ON en.idpaciente = a.idpaciente AND en.tipo = 'cierre' AND en.movimiento = 'salida' AND en.idorigen = c.id
				LEFT JOIN   entradasdetalle ed ON ed.identrada = en.id AND ed.idproducto = ad.iditem AND ed.tipoproducto = ad.tipo
				WHERE 		a.idpaciente = $idpaciente AND a.estatus <> 49 ";
				if($desde != ''){
					$q.= "AND a.fecha >= '$desde' ";
				}
				if($hasta != ''){
					$q.= "AND a.fecha <= '$hasta' ";	
				} 
				$q.=" GROUP BY a.id, ad.iditem, ad.tipo ORDER BY tipo,itemtxt";
	
		if($r = $mysqli->query($q)){
			while($fetch = $r->fetch_assoc()){
				$imprimir = "<a target='_blank' href='controller/imprimirAcuse.php?id=".$fetch['idacuse']."' ><span class='icon-col blue fa fa-print boton-imprimir-acuse' data-id='".$fetch['idacuse']."' data-toggle='tooltip' data-original-title='Imprimir Acuse' data-placement='right'></span></a>";
				$imprimirInterno = "<a href='controller/imprimirAcuseInterno.php?id=".$fetch['idacuse']."' target='_blank'><span class='icon-col green fa fa-print boton-imprimir-acuse-interno' data-id='".$fetch['idacuse']."' data-toggle='tooltip' data-original-title='Imprimir Acuse Interno' data-placement='right'></span></a>";
				switch($fetch['tipo']){
					case 'I':
						$tipotxt = 'Insumos';
					break;
					case 'M':
						$tipotxt = 'Medicamentos';
					break;
					case 'E':
						$tipotxt = 'Equipos';
					break;
				}
				
				setlocale(LC_TIME,"es_ES");
				
				$resultado['data'][] = array(
					'acciones' => '<div>'.$imprimir.$imprimirInterno.'</div>',
					'acuse' => $fetch['idacuse'],
					'numero' => $fetch['numero'],
					'codigo' => $fetch['codigo'],
					'tipo' => $fetch['tipo'],
					'tipotxt' => $tipotxt,
					'fecha' => date('M-d',strtotime($fetch['fecha'])),
					'itemid' => $fetch['iditem'],
					'consumido' => $fetch['consumido'],
					'idpic' => $fetch['idpic'],
					'facturable' => $fetch['facturable'],
					'cantidad_facturada' => $fetch['cantidad_facturada'],
					'itemtxt' => $fetch['itemtxt'],
					'costounitario' => getCostoPromedio($fetch['iditem'],$fetch['tipo']),//$fetch['costounitario'],
					'costo' => floatval(getCostoPromedio($fetch['iditem'],$fetch['tipo']))*intval($fetch['cantidad']),
					'cuenta' => $fetch['cuenta'],
					'cantidad' => $fetch['cantidad']
				);
			}
			if(empty($resultado)){
				$resultado['data'] = array(		
					'acuse' => '',
					'numero' => '',
					'tipo' => '',
					'codigo' => '',
					'tipotxt' => '',
					'fecha' => '',
					'itemid' => '',
					'consumido' => '',
					'idpic' => '',
					'facturable' => '',
					'cantidad_facturada' => '',
					'itemtxt' => '',
					'costounitario' => '',
					'costo' => '',
					'cuenta' => '',
					'cantidad' => '',
				);
			}
			echo json_encode($resultado);
		} else {
			die($mysqli->error);
		}
	}
	
	
	
	
	
	
	function inventario_por_paciente(){
		global $mysqli;		
		$idpaciente = isset($_REQUEST['idpaciente'])?$_REQUEST['idpaciente']: 0;
		$resultado = array();
		$query = "SELECT ad.iditem, ad.tipo,
					(CASE 
						WHEN ad.tipo ='M' THEN m.nombre 
						WHEN ad.tipo ='I' THEN i.nombre 
						WHEN ad.tipo ='E' THEN e.descripcion 
					END) as itemtxt,
					(CASE 
						WHEN ad.tipo ='M' THEN m.cuentacostoventa 
						WHEN ad.tipo ='I' THEN i.cuentacostodeventa 
						WHEN ad.tipo ='E' THEN e.cuentacostoventa 
					END) as cuenta,
					(CASE 
					WHEN ad.tipo ='M' THEN m.codigo 
					WHEN ad.tipo ='I' THEN i.codigo
					WHEN ad.tipo ='E' THEN e.codigo 
					END) as codigo,
					ad.costo as costounitario, 
					IFNULL(
						(select SUM(ad2.cantidad)from acuses a2 
							inner join acuses_detalle ad2 ON ad2.idacuse = a2.id
							WHERE a2.idpaciente = a.idpaciente AND ad2.iditem = ad.iditem AND ad2.tipo = ad.tipo
						   ),0) as cantidad,
					IFNULL(
						(SELECT SUM(cd2.conteo) FROM cierres c2 
							 LEFT JOIN cierredetalle cd2 ON cd2.idcierre = c2.id WHERE 
							c2.idpaciente = a.idpaciente AND cd2.iditem = ad.iditem AND cd2.movimiento = 'salida'
						),0) as conteo
					FROM 		acuses a
					INNER JOIN 	acuses_detalle ad ON ad.idacuse = a.id
					LEFT JOIN 	medicamentos m ON m.id = ad.iditem AND ad.tipo = 'M' 
					LEFT JOIN 	insumos i ON i.id = ad.iditem  AND ad.tipo = 'I' 
					LEFT JOIN 	equipos e ON e.id = ad.iditem  AND ad.tipo = 'E'
					WHERE 		a.idpaciente = '$idpaciente'
					GROUP BY	ad.iditem
					ORDER BY	tipo,itemtxt";
					//echo $query; die();
		if($r = $mysqli->query($query)){
			while($fetch = $r->fetch_assoc()){

				switch($fetch['tipo']){
					case 'I':
						$tipotxt = 'Insumos';
					break;
					case 'M':
						$tipotxt = 'Medicamentos';
					break;
					case 'E':
						$tipotxt = 'Equipos';
					break;
				}
				$consumido = $fetch['cantidad'] - $fetch['conteo'];
				
				setlocale(LC_TIME,"es_ES");
				
				$resultado['data'][] = array(
					'codigo' => $fetch['codigo'],
					'tipo' => $fetch['tipo'],
					'tipotxt' => $tipotxt,
					'itemid' => $fetch['iditem'],
					'itemtxt' => $fetch['itemtxt'],
					'costounitario' => getCostoPromedio($fetch['iditem'],$fetch['tipo']),
					'costo' => floatval(getCostoPromedio($fetch['iditem'],$fetch['tipo']))*intval($fetch['cantidad']),
					'cantidad' => $fetch['cantidad'],
					'consumido' => $consumido,
				);
			}
			if(empty($resultado)){
				$resultado['data'] = array(		
					'codigo' => '',
					'tipo' => '',
					'tipotxt' => '',					
					'itemid' => '',
					'itemtxt' => '',
					'costounitario' => '',
					'costo' => '',
					'cantidad' => '',
					'consumido' => '',
				);
			}
			echo json_encode($resultado);
		} else {
			die($mysqli->error);
		}
	}

	function movimientos_por_bodega(){
		global $mysqli;
		$iditem = $_REQUEST['iditem'];
		$tipoitem = $_REQUEST['tipo'];
		$inventario = $_REQUEST['inventario'];
		if($inventario==0)$inventario =1;
		// $query = "SELECT id,iditem,tipoitem,movimiento,tipo,paciente,fecha,cantidad FROM (
					// SELECT 
						// e.id as id,
						// UPPER(e.tipo) as tipo,
						// UPPER(e.movimiento) as movimiento, 
						// (CASE e.movimiento WHEN 'salida' THEN CONCAT('-',ed.cantidad) ELSE ed.cantidad END) as cantidad, 
						// ed.idproducto as iditem, 
						// ed.tipoproducto as tipoitem,
						// e.fecha as fecha,
						// p.nombre as paciente 
					// FROM entradas e
					// LEFT JOIN entradasdetalle ed ON ed.identrada = e.id
					// LEFT JOIN pacientes p ON p.id = e.idpaciente
					// WHERE     ed.idproducto ='$iditem' AND  ed.tipoproducto = '$tipoitem' AND ed.inventario= '$inventario' 
					// UNION				
					// SELECT 
						// a.id as id,
						// UPPER('acuse') as tipo,
						// UPPER('salida') as movimiento,
						// CONCAT('-',ad.cantidad) as cantidad,
						// ad.iditem as iditem,
						// ad.tipo as tipoitem,
						// a.fecha as fecha,
						// p.nombre as paciente
					// FROM acuses a
					// LEFT JOIN acuses_detalle ad ON ad.idacuse = a.id
					// LEFT JOIN pacientes p ON p.id = a.idpaciente
					// WHERE ad.iditem = '$iditem' AND ad.tipo = '$tipoitem' AND ad.inventario= '$inventario'  AND a.estatus in (46,47,48)
				// ) AS movimientos WHERE iditem = '$iditem' AND tipoitem = '$tipoitem' ORDER BY fecha desc";
		$query = "SELECT 
						e.id as id,
						UPPER(e.tipo) as tipo,
						UPPER(e.movimiento) as movimiento, 
						(CASE e.movimiento WHEN 'salida' THEN CONCAT('-',ed.cantidad) ELSE ed.cantidad END) as cantidad, 
						ed.idproducto as iditem, 
						ed.tipoproducto as tipoitem,
						e.fecha as fecha,
						p.nombre as paciente 
					FROM entradas e
					LEFT JOIN entradasdetalle ed ON ed.identrada = e.id
					LEFT JOIN pacientes p ON p.id = e.idpaciente
					WHERE     ed.idproducto ='$iditem' AND  ed.tipoproducto = '$tipoitem' AND ed.inventario= '$inventario' AND e.tipo != 'ACUSE DE ENTREGA'";
			
		$resultado = array();
		if($iditem != ''){
			if($r = $mysqli->query($query)){
				while($fetch = $r->fetch_assoc()){	
					if($fetch['tipo'] == 'ACUSE'){
						$imprimir = "<a target='_blank' href='controller/imprimirAcuse.php?id=".$fetch['id']."' ><span class='icon-col blue fa fa-print boton-imprimir-movimiento-boega' data-id='".$fetch['id']."' data-toggle='tooltip' data-original-title='Imprimir movimiento' data-placement='right'></span></a>";
					}else{
						$imprimir = "<a target='_blank' href='controller/imprimirAjuste.php?id=".$fetch['id']."'><span class='icon-col blue fa fa-print boton-imprimir-movimiento-boega' data-id='".$fetch['id']."' data-toggle='tooltip' data-original-title='Imprimir movimiento' data-placement='right'></span></a>";

					}
					$resultado['data'][] = array(
						'id' => $fetch['id'],
						'acciones' => '<div>'.$imprimir.'</div>',
						'movimiento' => $fetch['movimiento'],
						'paciente' => $fetch['paciente'],
						'tipo' => $fetch['tipo'],
						'fecha' => $fetch['fecha'],
						'cantidad' => $fetch['cantidad']				
					);
				}
			} else {
				die($mysqli->error);
			}
		}
		if(empty($resultado)){
			$resultado['data'] = array(		
				'id' => '',
				'acciones' => '',
				'movimiento' =>  '',
				'paciente' =>  '',
				'tipo' =>  '',
				'fecha' =>  '',
				'cantidad' => ''
			);
		}
		echo json_encode($resultado);
		
	}
	
	function generarExcel(){
		global $mysqli;
		
		$data 	 = '';
		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

		/** Include PHPExcel */
		//require_once '../../repositorio-lib/xls/Classes/PHPExcel.php';
		require_once 'xls/Classes/PHPExcel.php';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Maxia Latam")
		->setLastModifiedBy("Maxia Latam")
		->setTitle("Inventario comprometido")
		->setSubject("Inventario comprometido")
		->setDescription("Inventario comprometido")
		->setKeywords("Inventario comprometido")
		->setCategory("Inventario comprometido");

		//ESTILOS
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$fontColor = new PHPExcel_Style_Color();
		$fontColor->setRGB('ffffff');		

		$fontGreen = new PHPExcel_Style_Color();
		$fontGreen->setRGB('00b355');

		$fontRed = new PHPExcel_Style_Color();
		$fontRed->setRGB('ff0000');

		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			)
		);
		$style2 = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);
		$style3 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_HAIR 
				)
			),		
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);
		$azulOscuro = '1F3D7B';
		$azulClaro = '599fbc';
		$gris = 'cfcfcf';
		$valoreslistas = array();


		// ************************************************ SERVICIOS ******************************************
		$hoja = $objPHPExcel->createSheet(1);

		$hoja->setCellValue('A1', 'Tipo')
			 ->setCellValue('B1', 'Codigo')
			 ->setCellValue('C1', 'Nombre')
			 ->setCellValue('D1', 'Costo unitario')
			 ->setCellValue('E1', 'Valor')
			 ->setCellValue('F1', 'Inventario disponible')			 
			 ->setCellValue('G1', 'Inventario comprometido')
			 ->setCellValue('H1', 'Inventario en transito')
			 ->setCellValue('I1', 'Inventario total')
			 ->setCellValue('J1', 'Id item');

		//LETRA
		$hoja->getStyle('A1:J1')->getFont()->setBold(true)->setSize(11)->setColor($fontColor);
		$hoja->getStyle('A1:J1')->applyFromArray($style2);
		//FONDO
		$hoja->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($azulOscuro);
		// CONSULTA SQL

		$resultado = array();
		$tipoinventario = isset($_REQUEST['tipoinventario']) ? $_REQUEST['tipoinventario'] : 0;
		$item = isset($_REQUEST['item']) ? $_REQUEST['item']: 0;
		$tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo'] : '';
		$fecha = isset($_REQUEST['fecha'])? $_REQUEST['fecha'] : '';
		$query="
			SELECT DISTINCT 
			ci.iditem as iditem, 
			(CASE ci.tipo 
				WHEN 'I' THEN ins.codigo 
				WHEN 'M' THEN med.codigo
				WHEN 'E' THEN equi.codigo
			END) as codigo,
			(CASE ci.tipo 
				WHEN 'I' THEN 'Insumos'
				WHEN 'M' THEN 'Medicamentos'
				WHEN 'E' THEN 'Equipos'
			END) as tipo,
			(CASE ci.tipo 
				WHEN 'I' THEN ins.nombre 
				WHEN 'M' THEN med.nombre
				WHEN 'E' THEN equi.descripcion
			END) as nombre,
			(CASE ci.tipo 
				WHEN 'I' THEN ins.costounitario 
				WHEN 'M' THEN med.costounitario
				WHEN 'E' THEN equi.costounitario
			END) as costounitario,
			ci.total as total_i,
			ci.transito as transito,
			ci.comprometido as comprometido,
			(SELECT sum(can_inv.total) FROM cantidades_inventario can_inv WHERE can_inv.iditem = ci.iditem AND can_inv.tipo = ci.tipo) as total

			FROM cantidades_inventario ci 
			LEFT JOIN insumos ins ON ins.id = ci.iditem AND ci.tipo = 'I' 
			LEFT JOIN equipos equi ON equi.id = ci.iditem AND ci.tipo = 'E' 
			LEFT JOIN medicamentos med ON med.id = ci.iditem AND ci.tipo = 'M'
			WHERE  1
			";
			if ($tipoinventario	!= 0){
				$query.= " AND ci.inventario = '$tipoinventario' ";	
			}
			if($tipo != -1 && $tipo != 0){
				$query.= " AND ci.tipo = '$tipo' ";	
			}
			if($item != '-1' && $item != 0){
				$query.= " AND ci.iditem = '$item' ";	
			}
		$query.=" GROUP BY ci.iditem,ci.tipo ORDER BY nombre";
		
			//echo $query;die();
		$result = $mysqli->query($query);
		
		$i = 2;

		while($row = $result->fetch_assoc()){
			$id = $row['iditem'];
			$tipo = substr($row['tipo'],0,1);
				$comprometido = $row['comprometido'];
				$disponible = intval($row['total_i']) - intval($comprometido);
			$hoja->setCellValue('A'.$i, $row['tipo'])
			     ->setCellValue('B'.$i, $row['codigo'])
				 ->setCellValue('C'.$i, $row['nombre'])
				 ->setCellValue('D'.$i, round($row['costounitario'],2)) 
				 ->setCellValue('E'.$i, round($row['costounitario']*$row['total_i'],2))
				 ->setCellValue('F'.$i, $disponible)
				 ->setCellValue('G'.$i, $comprometido)
				 ->setCellValue('H'.$i, $row['transito'])
				 ->setCellValue('I'.$i, $row['total_i'])
				 ->setCellValue('J'.$i, $row['iditem']);
			$i++;			
			 
			
		}

		$hoja->setTitle('Inventario Comprometido VITAE');




		//Anchos
		$hoja->getColumnDimension('A')->setWidth(15);
		$hoja->getColumnDimension('B')->setWidth(25);
		$hoja->getColumnDimension('C')->setWidth(95);
		$hoja->getColumnDimension('D')->setWidth(25);
		$hoja->getColumnDimension('E')->setWidth(25);
		$hoja->getColumnDimension('F')->setWidth(25);
		$hoja->getColumnDimension('G')->setWidth(25);
		$hoja->getColumnDimension('H')->setWidth(25);
		$hoja->getColumnDimension('I')->setWidth(25);
		$hoja->getColumnDimension('J')->setWidth(25);


		// *************** FINAL

		$objPHPExcel->removeSheetByIndex(
			$objPHPExcel->getIndex(
				$objPHPExcel->getSheetByName('Worksheet')
			) 
		);

		$objPHPExcel->setActiveSheetIndex(0);


		//Redirigir la salida al navegador del cliente
		$hoy = date('dmY');
		$nombreArc = 'Inventario_comprometido_Vitae_'.$hoy.'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$nombreArc.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		
		$objWriter->save('php://output');
		
		exit();		
	}
	
	function actualizar_inventario(){
		global $mysqli;
		$items = $_REQUEST['items'];
		foreach($items as $item){										
			$query ="select distinct id as inventario From bodegas";
			$r = $mysqli->query($query);
			while($row = $r->fetch_assoc()){
				$q = "UPDATE cantidades_inventario SET 
						comprometido = inv_comprometido(".$item['id'].",'".$item['tipo']."',".$row['inventario'].",0),				
						total = inv_total(".$item['id'].",'".$item['tipo']."',".$row['inventario'].")
					WHERE
						iditem = '".$item['id']."' AND
						tipo = '".$item['tipo']."' AND
						inventario = '".$row['inventario']."'
					";
				if ($mysqli->query($q)){
					echo "<br> EXITO: iditem : '".$item['id']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}else{
					echo "<br> ERROR:  iditem : '".$item['id']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}
			}
		}
	}	
	function actualizar_inventario_acuses(){
		global $mysqli;
		$items = $_REQUEST['items'];
			foreach($items as $item){			
				$query ="select distinct id as inventario From bodegas";
				$r = $mysqli->query($query);
				while($row = $r->fetch_assoc()){
					$q = "UPDATE cantidades_inventario SET 
							comprometido = inv_comprometido(".$item['itemid'].",'".$item['tipo']."',".$row['inventario'].",0),				
							total = inv_total(".$item['itemid'].",'".$item['tipo']."',".$row['inventario'].")
						WHERE
							iditem = '".$item['itemid']."' AND
							tipo = '".$item['tipo']."' AND
							inventario = '".$row['inventario']."'
						";
					if ($mysqli->query($q)){
						echo "<br> EXITO: iditem : '".$item['itemid']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
					}else{
						echo "<br> ERROR:  iditem : '".$item['itemid']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
					}
				}
			}
		
	}		
	function actualizar_inventario_2(){
		global $mysqli;
		$items = $_REQUEST['items'];
		foreach($items as $item){										
			$query ="select distinct id as inventario From bodegas";
			$r = $mysqli->query($query);
			while($row = $r->fetch_assoc()){
				$q = "UPDATE cantidades_inventario SET 
						comprometido = inv_comprometido(".$item['item'].",'".$item['tipo']."',".$row['inventario'].",0),				
						total = inv_total(".$item['item'].",'".$item['tipo']."',".$row['inventario'].")
					WHERE
						iditem = '".$item['item']."' AND
						tipo = '".$item['tipo']."' AND
						inventario = '".$row['inventario']."'
					";
				if ($mysqli->query($q)){
					echo "<br> EXITO: iditem : '".$item['item']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}else{
					echo "<br> ERROR:  iditem : '".$item['item']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}
			}
		}
	}	
	function actualizar_inventario_ajuste(){
		global $mysqli;
		$items = $_REQUEST['items'];
		foreach($items as $item){										
			$query ="select distinct id as inventario From bodegas";
			$r = $mysqli->query($query);
			while($row = $r->fetch_assoc()){
				$q = "UPDATE cantidades_inventario SET 
						comprometido = inv_comprometido(".$item['itemid'].",'".$item['tipo']."',".$row['inventario'].",0),				
						total = inv_total(".$item['itemid'].",'".$item['tipo']."',".$row['inventario'].")
					WHERE
						iditem = '".$item['itemid']."' AND
						tipo = '".$item['tipo']."' AND
						inventario = '".$row['inventario']."'
					";
				if ($mysqli->query($q)){
					echo "<br> EXITO: iditem : '".$item['itemid']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}else{
					echo "<br> ERROR:  iditem : '".$item['itemid']."' - tipo : '".$item['tipo']."' - inventario : '".$row['inventario']."'";
				}
			}
		}
	}	
	function actualizar_inventario_un_item(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$tipo = $_REQUEST['tipo'];
		
		$query ="select distinct id as inventario From bodegas";
		$r = $mysqli->query($query);
		while($row = $r->fetch_assoc()){
			$q = "UPDATE cantidades_inventario SET 
					comprometido = inv_comprometido(".$id.",'".$tipo."',".$row['inventario'].",0),				
					total = inv_total(".$id.",'".$tipo."',".$row['inventario'].")
				WHERE
					iditem = '".$id."' AND
					tipo = '".$tipo."' AND
					inventario = '".$row['inventario']."'
				";
			if ($mysqli->query($q)){
				echo "<br> EXITO: iditem : '".$id."' - tipo : '".$tipo."' - inventario : '".$row['inventario']."'";
			}else{
				echo "<br> ERROR:  iditem : '".$tipo."' - tipo : '".$tipo."' - inventario : '".$row['inventario']."'";
			}
		}
		
	}	
	function isActive(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$tipo = $_REQUEST['tipo'];	
		switch ($tipo){
			case 'I':
				$query = "select activo from insumos where id = $id limit 1;";
			break;
			case 'M':
				$query = "select activo from medicamentos where id = $id limit 1;";
			break;case 'E':
				$query = "select activo from equipos where id = $id limit 1;";
			break;
		}
		$r = $mysqli->query($query);
		$res = $r->fetch_assoc()['activo'];
		echo $res;							
	}
		
?>