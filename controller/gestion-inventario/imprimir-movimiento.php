<?php


include_once("../conexion.php");
//ini_set('display_errors', 1);ini_set('display_startup_errors', 1);

global $mysqli;
$config = unserialize(urldecode($_COOKIE['configuraciones']));
$conf = $config['data'][0];

$vitrae = '';
$moneda = $conf['moneda'];
$impuesto= $conf['nombre_impuesto'];
$vitae = $conf['nombre'].'<br>';
$vitae.= $conf['tipo_identificacion'].': '.$conf['identificacion'].'<br>';
$vitae.= $conf['direccion'].'<br>';
$vitae.= 'Telefono: '.$conf['telefono'].'<br>';
$vitae.= 'Whatsapp: '.$conf['whatsapp'];
$porcentaje_impuesto = $conf['porcentaje_impuesto'];
$orientacion = '';

$id = (!empty($_REQUEST['id']) ? $_REQUEST['id'] : 0);

$query  = " SELECT 
            a.id as id, 
            a.fecha as fecha,
            UPPER(a.movimiento) as movimiento,				
            UPPER(a.tipo) as tipo,				
            p.id as paciente, 
            p.nombre as nombre_paciente, 						
            p.cedula as cedula,
            p.fechanac as fecha_nacimiento,
            p.direccion as direccion,
            p.telefonocasa as telefono_paciente,
            u.nombre as nombre_colaborador,
            u.correo as correo_colaborador,
            u.telefono as telefono_colaborador
            FROM entradas a 
            LEFT JOIN pacientes p ON a.idpaciente = p.id
            LEFT JOIN usuarios u ON u.id = a.usuario
            WHERE a.id = '$id' ";
//debug($query);

$r = $mysqli->query($query);
$cabecera = '';
$colaborador = '';
$paciente = '';
if($p = $r->fetch_assoc()){
    $paciente.='
					Nombre: <b>'.utf8_decode($p['nombre_paciente']).'</b><br>';
    if($_COOKIE['region'] == 'El Salvador'){
        $paciente.='NIT: <b>'.$p['cedula'].'</b><br>';
    }else{
        $paciente.='C&eacute;dula: <b>'.$p['cedula'].'</b><br>';
    }

    $paciente.='Tel&eacute;fono: <b>'.$p['telefono_paciente'].'</b><br>
					E-mail: <b>'.$p['email'].'</b><br>
				';
    $paciente.='Direcci/oacute;n: <b>'.utf8_decode($p['direccion']).'</b><br>
					E-mail: <b>'.$p['email'].'</b><br>
				';
    $colaborador.='Nombre: <b>'.utf8_decode($p['nombre_colaborador']).'</b><br>';
    $colaborador.='Correo: <b>'.utf8_decode($p['correo_colaborador']).'</b><br>';
    $colaborador.='Tel/eactue;fono: <b>'.utf8_decode($p['telefono_colaborador']).'</b><br>';


    $nombre = $p['nombre'];
    $pic = $p['numero'];
    $fecha= $p['fecha'];
    $estatus= $p['estatus'];
    $comentarios = utf8_decode($p['comentario']);
    $cabecera .=$vitae.'<br>
                <table style="width:100%;border:1;">
                    <tbody>
                        <tr>
                            <th style="border:1;width:50% "><b>RESPONSABLE<b></th>
                            <th style="border:1;width:50%"><b>PACIENTE<b></th>
                        </tr>
                        <tr>							
                            <td style="border:1; line-height:20px">'.$colaborador.'</td>
                            <td  style="border:1; line-height:20px">'.$paciente.'</td>
                        </tr>
                    </tbody>
				</table> 
				<br>';
}

$html = '
		<!-- DEFINE HEADERS & FOOTERS -->
				<style>
					h1,h2,h3,h4,h5,h6,a,p,div,span,li,td,th {
						font-family: Arial, sans-serif;
					}
					table{
						  border-collapse: collapse;
					}
					td {
						font-family: Arial, sans-serif;
						font-size: 12px;
						text-align: justify;
						border: none;
						heigth: 15px;
					}
					h1,h2,h3,h4,h5,h6 {
						/*color: rgb(21, 98, 158);*/
						text-align:center;
						margin-top: -35px;
						margin-bottom: 35px;
					}
					.blue {
						color: rgb(21, 98, 158);
					}
					th {
						font-size: 11px;
						background-color: #0a5897;
						color: white
					}									
					.center {
						text-align:center;
					}
					.subtitulo_tabla{
						background-color: #d6dfe2;
						color: black;
						border: none;
						font-weight: bold; 
						
					}
					.detalle{
						font-family: Arial, sans-serif;
						font-size: 12px;
						text-align: justify;
						border-top: 1px solid #96B9EF;
						line-height: 25px
					}
					.footer{
						font-family: Arial, sans-serif;
						font-size: 12px;
						text-align: right;
						border-bottom: 1px solid #96B9EF;
						line-height: 25px;
						font-weight: bold; 
					}
				</style>
		';


// Imprimir detalle

$tabla_detalle ='';
$tabla_detalle .='<table style="width:100%">
                        <thead>
                            <tr>
                                <th style="border-right:1px solid black; ">Codigo</th>
                                <th style="border-right:1px solid black; ">Descripción</th>
                                <th style="border-right:1px solid black;">Inventario</th>
                                <th style="border-right:1px solid black;">Cantidad</th>
                            </tr>
                            <tbody>
                                ';
                

$query = array();

// MEDICAMENTOS
$query['MEDICAMENTOS']  = "	SELECT  m.nombre AS nombre,	m.codigo AS codigo, b.nombre AS inventario,					
        (CASE WHEN ed.inventario ='1' THEN 'Inventario General' ELSE 'Inventario en Panel' 	END) AS inventario,ed.itbms as itbms,
        ed.cantidad AS cantidad, ed.costounitario AS costounitario, u.nombre as unidad	
        FROM entradasdetalle ed
        INNER JOIN 	medicamentos m ON m.id = ed.idproducto AND ed.tipoproducto = 'M' 
        LEFT JOIN tiposdeunidad u ON u.id = m.unidadcompra
        LEFT JOIN bodegas b  ON b.id = ed.inventario

        WHERE ed.identrada = '$id'";
//INSUMOS
$query['INSUMOS']  = "	SELECT  m.nombre AS nombre,	m.codigo AS codigo, b.nombre AS inventario,					
        (CASE WHEN ed.inventario ='1' THEN 'Inventario General' ELSE 'Inventario en Panel' END) AS inventario,ed.itbms as itbms,
        ed.cantidad AS cantidad, ed.costounitario AS costounitario, u.nombre as unidad	
        FROM entradasdetalle ed
        INNER JOIN 	insumos m ON m.id = ed.idproducto AND ed.tipoproducto = 'I' 
        LEFT JOIN tiposdeunidad u ON u.id = m.unidadcompra
        LEFT JOIN bodegas b  ON b.id = ed.inventario


        WHERE ed.identrada = '$id'";

//EQUIPOS
$query['EQUIPOS']  = "SELECT  m.descripcion AS nombre,	m.codigo AS codigo,	b.nombre AS inventario,				
        (CASE WHEN ed.inventario ='1' THEN 'Inventario General' ELSE 'Inventario en Panel' END) AS inventario, ed.itbms as itbms,
        ed.cantidad AS cantidad, ed.costounitario AS costounitario, u.nombre as unidad	
        FROM entradasdetalle ed
        INNER JOIN 	equipos m ON m.id = ed.idproducto AND ed.tipoproducto = 'E' 
        LEFT JOIN tiposdeunidad u ON u.id = m.unidadcompra	
        LEFT JOIN bodegas b  ON b.id = ed.inventario
        WHERE ed.identrada = '$id'";


$acum 		= 0;
$acumitbms 	= 0;

$base_de_impuesto = 0;
$total_excento = 0;
$subtotal 	= 0;
foreach($query as $i => $q){
    //debug($query);
    $result = $mysqli->query($q);
    $nbrows = $result->num_rows;	
    $cantserv 	= 0;
    $total_x_tipo = 0;
    while($registro	= $result->fetch_assoc()){												
        $total_x_item = floatval($registro['cantidad'] * $registro['costounitario']);
        $subtotal += $total_x_item ;
        $total_x_tipo += $total_x_item;
        $cantserv += $registro['cantidad'];
        $itbms ='';
        if($registro['itbms'] == 0){
            $itbms = 'Exento';
            $total_excento += $total_x_item;
        }else{
            $base_de_impuesto += $total_x_item;
        }
        $tabla_detalle.=' <tr>
                            <td style=" solid black;text-align:center">'.$registro['codigo'].'</td>
                            <td style=" solid black;">'.$registro['nombre'].'</td>
                            <td style=" solid black;font-size: 10px">'.$registro['inventario'].'</td>
                            <td style=" solid black; text-align:center" class="center">'.$registro['cantidad'].'</td>
                        </tr>';
    }
    $tabla_detalle.=' <tr>
                        <td style="background:#D6DFE2; text-align:right" colspan="3"><b>TOTAL '.$i.':</b>
                        <td style="background:#D6DFE2;text-align:center"><b>'.$cantserv.'</b></td>
                    </tr>';
}


$tabla_detalle.='
        </tbody>
    </thead>
';


$total_desc_final = 0;
$monto_impuesto_final	= 0;
$subtotal_final = 0;
$total_final = 0;
$tabla_detalle.='</table>';

$footer='';


$imagehtml = ($image != "") ? '<img src="'.$image.'" alt="" style="width: 220px;" />' : "";
$footer.='<br><br>
			<table style="width:100%">
				<tr>
					<td style="width:15%"><b>Firma responsable:</b>:</td><td style="width:35%;border-bottom:1px solid"></td>
					<td style="width:15%"><b>Firma paciente:</b>:</td><td style="width:35%;border-bottom:1px solid">'.$imagehtml.'<br>'.utf8_decode($nombre_firma).$cedula_firma.' '.utf8_decode($parentesco_firma).'</td>					
				</tr>
				<tr> 
					<td colspan="4" style="text-align:center">
						<br>
						<b>Vitae Homecare.</b>
					</td>
				</tr> 
			</table>
		';
$header='<table style="border:none;width:100%:display:block" ><tr>
				<td style="border:none;align:left ;width:33%"><img src="../images/logo-hc.png"></td>

				<td style="border:none;text-align:right;width:33%; line-height:25px"><br>
					<h3 style="border:none;color:red ;align:right">Movimiento Nro:'.$id.'</h3>
					<h3 style="border:none;align:right">Fecha:'.$fecha.'</h3>
					<h3 style="align:right">Tipo:'.utf8_decode($tipo).'</h3>
					<h3 style="align:right">Movimiento:'.utf8_decode($movimiento).'</h3>
				</td>			
			</tr></table>	
			<table width="100%">
				<tr>
					<td class="center" style="border:none;align:center;width:100%"><h2></h2></td>
				</tr>
			</table>';
$footer_doc ='
			<table width="100%">
				<tr>
					<td style="width:100%;text-align:center;font-style: oblique;">Vitae Home Care</td>
				</tr>
			</table>';

$nombre_archivo = 'Ajuste_de_inventario_'.$id.'.pdf';
include("../mpdf/mpdf.php");
$mpdf=new mpdf();
$mpdf->mirrorMargins = true;
$mpdf->SetDisplayMode('fullpage','two');
$mpdf->mirrorMargins = 1 ;// Use different Odd/Even headers and footers and mirror margins
$mpdf->SetHTMLFooter($footer_doc,'O');
$mpdf->SetHTMLFooter($footer_doc,'E');
$mpdf->addPage($orientacion, 'O', 0, '', '', 15, 15, 10, 15, 8,8);
//die(utf8_encode($html.$header.$cabecera));
$mpdf->WriteHTML(utf8_encode($html.$header.$cabecera));
//die($mpdf->y);

if($mpdf->y >= 230){
    $mpdf->WriteHTML('<pagebreak>');
}
$mpdf->WriteHTML($tabla_detalle);
$mpdf->WriteHTML('<br>');
if($mpdf->y >= 245){
    $mpdf->WriteHTML('<pagebreak>');
}
$mpdf->WriteHTML($footer);
$mpdf->setTitle('Ajuste_de_inventario_'.$id);
$mpdf->Output($nombre_archivo,'I');
mysqli_close($mysqli);
exit;

?>