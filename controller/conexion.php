<?php
	require('config.php');
	$mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
	if ($mysqli->connect_error) {
		echo "Fallo al conectar a MySQL: (" . $mysqli->connect_error . ") " . $mysqli->connect_error;
		echo "<br>".$DB_HOST."<br>".$DB_USER."<br>".$DB_PASS."<br>".$DB_NAME;
		die();
	}
	$mysqli->query("SET NAMES utf8"); 
	$mysqli->query("SET CHARACTER SET utf8"); 
	
	// PARA MANEJAR GET,POST,COOKIES
	ini_set ( 'request_order' , 'GP' );
	if (session_status() !== PHP_SESSION_ACTIVE) {
		// Change db to db_new
		if(isset($_SESSION['db_name'])){
			$mysqli-> select_db($_SESSION['db_name']);
			session_start();
		}else{
			if(isset($_COOKIE['db_name'])){
				$mysqli-> select_db($_COOKIE['db_name']);
				session_start();
			}
		}
	}
	
    $mysqli->set_charset("utf8");
    //session_start();
	
	function getValor($campo,$tabla,$id){
		global $mysqli;
		
		if($id != ''){
			$q = "SELECT $campo FROM $tabla WHERE id = $id LIMIT 1";

			$r = $mysqli->query($q);
			if($val = $r->fetch_assoc()){
				$valor = $val[$campo];				
			}else{
				die($campo.' '.$tabla.' '.$id);
			}
		}else{
			$valor = '';
		}	
		return $valor;
	}
	
	function guardar_bitacora($usuario,$idregistro,$modulo,$descripcion,$sentencia){
		global $mysqli;
		$query = "INSERT INTO bitacora (usuario, fecha, idregistro, modulo, accion, sentencia) 
			  VALUES('$usuario',CURRENT_TIMESTAMP,'$idregistro','$modulo','$descripcion','$sentencia');";
		
			if($mysqli->query($query)){
				return 1;
			}else{
				return $query;
			}
	}
	
	function generar_sentencia($tabla, $id){
		global $mysqli;		
		$campos = '("';
		$q = "SELECT * FROM $tabla WHERE id ='$id';";
		$r = $mysqli->query($q);
		$val = $r->fetch_assoc();
		$campos .= implode('","', $val);
		$campos	.='")';
		$respuesta = "INSERT INTO $tabla VALUES $campos";
		return $respuesta;
	
	}
	
	function generar_Numero($tabla,$campo,$prefijo){
		global $mysqli;		
		//NUMERO
		$codok = 0;
		while ($codok == 0):
			$key 	= $prefijo.'-'.generar_Codigo_L(2).''.generar_Codigo_N(6);			
			$query  = " SELECT * FROM $tabla WHERE $campo = '$key' ";
			$result = $mysqli->query($query);
			$count = $result->num_rows;		
			if( $count > 0 ){				
				$codok = 0;
			}else {
				$codok = 1;
			}
		endwhile;
		return $key;
	}
	
	function generar_Codigo_L($longitud) {
		$key = '';
		$pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$max = strlen($pattern)-1;
		for($i=0;$i < $longitud;$i++) $key .= $pattern[rand(0,$max)];
		return $key;
	}
	
	function generar_Codigo_N($longitud) {
		$key = '';
		$pattern = '1234567890';
		$max = strlen($pattern)-1;
		for($i=0;$i < $longitud;$i++) $key .= $pattern[rand(0,$max)];
		return $key;
	}
	
	function nuevoRegistro($modulo,$idregistro,$campos,$query){
	    $descripcion = "Fue creado un registro en el módulo de $modulo con el id #$idregistro. <br/>";
		$descripcion .= "<ul>";
		foreach($campos as $campo => $valor){
			if($valor != ''){
				$descripcion .= "<li><b>".ucfirst($campo)."</b>: $valor.</li>";
			}
		}
		$descripcion .= "</ul>";
		$res = guardar_bitacora((isset($_SESSION['usuario'])?$_SESSION['usuario']:0),$idregistro, $modulo, $descripcion, $query);
		//return $res;
	}

?>
