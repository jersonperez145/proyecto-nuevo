<?php
    include("../conexion.php");
	/** Error reporting */
	
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	
	global $mysqli;
	require '../phpspreadsheet/vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\IOFactory;
        // Create new PHPExcel object
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
	$sheet->setTitle('KIT DE INSUMOS');
	// Set document properties
	$fontColor = new \PhpOffice\PhpSpreadsheet\Style\Color();
	$fontColor->setRGB('ffffff');
	$style = array(
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
	);
	$style2 = array(
			'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
			)
	);
	$spreadsheet->getProperties()->setCreator("VITAE")
	->setLastModifiedBy("VITAE")
	->setTitle("Pacientes")
	->setSubject("Pacientes")
	->setDescription("Pacientes")
	->setKeywords("Pacientes")
	->setCategory("Reportes");

	//TITULO	
	$spreadsheet->getActiveSheet()->setCellValue('A1', 'Lista de pacientes');
	$spreadsheet->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(14);
	$spreadsheet->getActiveSheet()->getStyle("A1")->applyFromArray($style);
	
    // ENCABEZADO 
     //--phpexcel--//        $hoja            
                                        //->setCellValue('A1', 'Id')

        //--spreadsheet//       $spreadsheet->getActiveSheet()          
                                        //->setCellValue('A1', 'Id')
	$spreadsheet->getActiveSheet()
        ->setCellValue('A1', 'Id')
        ->setCellValue('B1', 'Nombre')
        ->setCellValue('C1', 'Código')
        ->setCellValue('D1', 'Tipo')
        ->setCellValue('E1', 'Insumos')
        ->setCellValue('F1', 'Descripcion')
        ->setCellValue('G1', 'cantidad');
	
	//LETRA
	$spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true)->setSize(12)->setColor($fontColor);
	$spreadsheet->getActiveSheet()->getStyle("A1:G1")->applyFromArray($style);
	//FONDO
    $spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('293F76');	
    //SENTENCIA BASE
    $query  ="SELECT * FROM vista_kit_insumos;";
    $result = $mysqli->query($query);
    $i = 2;    
    //Definir fuente
	$spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);					
	
    while($row = $result->fetch_assoc()){  
        //--phpexcel--//        $hoja            
                                        //->setCellValue('A'.$i, $row['id'])

        //--spreadsheet//       $spreadsheet->getActiveSheet()          
                                        //->setCellValue('A'.$i, $row['id'])
        $spreadsheet->getActiveSheet()          
            ->setCellValue('A'.$i, $row['id'])
            ->setCellValue('B'.$i, $row['nombre'])
            ->setCellValue('C'.$i, $row['codigo'])
            ->setCellValue('D'.$i, $row['tipo'])
            ->setCellValue('E'.$i, $row['insumos'])
            ->setCellValue('F'.$i, $row['descripcion'])
            ->setCellValue('G'.$i, $row['cantidad']);
        $i++;		
    }
    //--phpexcel--//                              $hoja->getColumnDimension('A')->setAutoSize(true);
    //--spreadsheet//    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    
    //Renombrar hoja de Excel
	$spreadsheet->getActiveSheet()->setTitle('Kit de insumos');
    //Redirigir la salida al navegador del cliente
    $hoy = date('dmY');
    $nombreArc = 'DATA-KIT-INSUMOS - '.$hoy.'.xlsx';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename='.$nombreArc);
    header('Cache-Control: max-age=0');	
    $writer = IOFactory::createWriter($spreadsheet,'Xlsx');
    //save into php output
    $writer->save('php://output');
	mysqli_close($mysqli);
    exit();
?>