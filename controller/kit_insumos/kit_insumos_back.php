<?php
    include("../conexion.php");

	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){
		case "cargarkits": 
			  cargarkits();
			  mysqli_close($mysqli);
			  break;
		case "combokitinsumos": 
			  combokitinsumos();
			  mysqli_close($mysqli);
			  break;
		case "getkit": 
			  getkit();
			  mysqli_close($mysqli);
			  break;
		case "create": 
			  create();
			  mysqli_close($mysqli);
			  break;
		case "updatekit": 
			  updatekit();
			  mysqli_close($mysqli);
			  break;
		case "deletekit": 
			  deletekit();
			  mysqli_close($mysqli);
			  break;
		case "updateitbmsjubilado": 
			  updateitbmsjubilado();
			  mysqli_close($mysqli);
			  break;
		case "existecodigo":
			  existecodigo();
			  mysqli_close($mysqli);
			  break;
		case "duplicar":
			  duplicar();
			  mysqli_close($mysqli);
			  break;
		case "COMBO_ITEMS":
				COMBO_ITEMS();
			  mysqli_close($mysqli);
			  break;
		default:
			  echo "{failure:true}";
			  break;
	}	
	
	function cargarkits(){
		global $mysqli;
		
		$query 	= "	SELECT 
                        kit.id, 
                        kit.nombre, 
                        IFNULL(kit.costo_promedio, 0) AS costo_promedio, 
                        DATE(kit.updated_at) AS fecha_actualizacion,
                        u.nombre AS actualizado_por 
                    FROM insumoskit kit 
                    INNER JOIN usuarios u ON u.id = kit.updated_by
                    WHERE kit.status = 1";
		$result = $mysqli->query($query);
		
		while($row = $result->fetch_assoc()){
			$items = array();
			$query 	= "	SELECT 	k.id, k.iditem,k.tipo,k.cantidad,
								IFNULL((CASE 
									WHEN k.tipo ='M' THEN CONCAT(med.codigo,' | ',med.nombre)
									WHEN k.tipo ='E' THEN CONCAT(equi.codigo,' | ',equi.descripcion)
									WHEN k.tipo ='I' THEN CONCAT(ins.codigo,' | ',ins.nombre)
								END),'') as itemtxt
					FROM 	insumoskit_detalle k
					LEFT JOIN 	medicamentos med ON med.id = k.iditem and k.tipo = 'M'
					LEFT JOIN 	equipos equi ON equi.id = k.iditem and k.tipo = 'E'
					LEFT JOIN 	insumos ins ON ins.id = k.iditem and k.tipo = 'I'
					WHERE 		k.idkit = '".$row['id']."' 
					GROUP BY 	k.id";
			$result_i = $mysqli->query($query);
			if($result_i->num_rows > 0){
				while($row_items = $result_i->fetch_assoc()){
					$items[] = array(
						'id' => $row_items['id'],
						'itemid' => $row_items['iditem'],
						'tipo' => $row_items['tipo'],
						'itemtxt' => $row_items['itemtxt'],
						'cantidad' => $row_items['cantidad']
					);
				}
				$resultado['data'][] = array(
					'id' 		=>	$row['id'],				
					'nombre' 		=>	$row['nombre'],
					'costo_promedio' 	=>	$row['costo_promedio'],
					'fecha_actualizacion' => $row['fecha_actualizacion'],
					'actualizado_por' => $row['actualizado_por'],
					'items' => $items
				);
			}
		}
		
		echo json_encode($resultado);
	}	
	
	function combokitinsumos(){
		global $mysqli;		
  
		$query 	= "	SELECT CONCAT('K-',id) as id, CONCAT('KIT: ',nombre) as nombre FROM insumoskit";
		  
		$query2 ="SELECT DISTINCT m.id as id, 'I' as tipo, m.codigo as codigo, m.nombre as nombre, m.itbms as itbms, 
							m.costounitario as costounitario, m.cuentacostodeventa as cuenta,
							IFNULL((SUM(ci.total)-SUM(ci.comprometido)),0) as disponible,
							m.ubicacion
					FROM insumos m	
					LEFT JOIN cantidades_inventario ci ON ci.iditem = m.id AND ci.tipo='I'
					WHERE m.activo = 1 GROUP BY m.id ORDER BY m.nombre ASC ";
				
		$resultado = "";		 
		$resultado .= "<option value=''></option>";	
		
		$result = $mysqli->query($query);		
		while($row1 = $result->fetch_assoc()){
			$resultado .= "<option value='".$row1['id']."'>".$row1['nombre']."</option>";
		}

		$result2 = $mysqli->query($query2);
		while($row = $result2->fetch_assoc()){			
			$cantidad = $row['disponible'];
			$resultado .= "<option value='".$row['id']."' data-itbms='".$row['itbms']."' data-tipo='".$row['tipo']."' data-nombre='".$row['nombre']."' data-cuenta='".$row['cuenta']."' data-costounitario='".$row['costounitario']."' data-cantidad='".$cantidad."' data-ubicacion='".$row['ubicacion']."'>".$row['codigo']." | ".$row['nombre']." (".$cantidad.")</option>";
		}
		if( $resultado != ""){
			echo $resultado;
		} else {
			echo 0;
		}
	}	
	
	function getkit(){
		global $mysqli;
		
		$resultado = array();
		$resultadodetalle = array();
		$id = $_GET['id'];
		$query 	= "	SELECT 	k.id,	i.nombre,i.updated_at as fecha_modificacion, u.nombre as modificado_por, k.iditem,k.tipo,k.cantidad,
								IFNULL((CASE 
									WHEN k.tipo ='M' THEN CONCAT(med.codigo,' | ',med.nombre)
									WHEN k.tipo ='E' THEN CONCAT(equi.codigo,' | ',equi.descripcion)
									WHEN k.tipo ='I' THEN CONCAT(ins.codigo,' | ',ins.nombre)
								END),'') as itemtxt
					FROM 		insumoskit i
					INNER JOIN usuarios u ON u.id = i.updated_by
					LEFT JOIN 	insumoskit_detalle k ON k.idkit = i.id
					LEFT JOIN 	medicamentos med ON med.id = k.iditem 
					LEFT JOIN 	equipos equi ON equi.id = k.iditem 
					LEFT JOIN 	insumos ins ON ins.id = k.iditem 
					WHERE 		i.id = '$id' 
					GROUP BY 	k.id";
		$result = $mysqli->query($query);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$nombre =	$row['nombre'];
				$modificado_por =	$row['modificado_por'];
				$fecha_modificacion =	$row['fecha_modificacion'];
				switch($row['tipo']){
					case 'I':
						$tipotxt = 'Insumos';
					break;
					case 'M':
						$tipotxt = 'Medicamentos';
					break;
					case 'E':
						$tipotxt = 'Equipos';
					break;
				}
				$resultadodetalle[] = array(
					'id' => $row['id'],
					'tipo' => $row['tipo'],
					'tipotxt' => $tipotxt,
					'itemid' => $row['iditem'],
					'itemtxt' => $row['itemtxt'],
					'cantidad' => $row['cantidad']
				);
			}
			
			$resultado = array(
				'nombre' => $nombre,
				'modificado_por' => $modificado_por,
				'fecha_modificacion' => $fecha_modificacion,
				'items' => $resultadodetalle
			);
		}
		
		
		echo json_encode($resultado);
	}	
	
	
	function deletekit(){
		global $mysqli;			
		$id = $_POST['id'];
		$fecha = $_POST['fecha'];
		$user_id = $_POST['user_id'];
		$query 	= "	UPDATE insumoskit 
						SET status = 0, updated_at ='$fecha', updated_by ='$user_id'
					WHERE id = '$id'";
		if($result = $mysqli->query($query)){
			echo 1;
		}else{
			die($mysqli->error);
		}		
	}
	
	function updatekit(){
		global $mysqli;
		
		$id 		= $_POST['id'];
		$fecha 		= $_POST['fecha'];
		$user_id 	= $_POST['user_id'];
		$nombre 	= $_POST['nombre'];
		$items 	= $_POST['items'];
		
		$query 	= "	UPDATE 	insumoskit SET 	
						nombre = '$nombre',
						updated_at = '$fecha',
						updated_by = '$user_id'
					WHERE 	id = '$id'";
		if($result = $mysqli->query($query)){
			$qd = "INSERT INTO insumoskit_detalle (idkit, iditem, tipo, cantidad) VALUES ";
			
			foreach($items as $detalle){
				$qd .= "($id,".$detalle['itemid'].",'".$detalle['tipo']."',".$detalle['cantidad']."),";
			}
			
			$qd = substr($qd,0,-1);
			
			$qdelete = "DELETE FROM insumoskit_detalle WHERE idkit = $id";
			$mysqli->query($qdelete);
			
			if($mysqli->query($qd)){
				echo 1;
			} else {
				die($mysqli->error);
			}
		} else {
			die($mysqli->error);
		}
	}
	
	function create(){
		global $mysqli;
		$items = $_POST['items'];
		$nombre = $_POST['nombre'];
		$fecha = $_POST['fecha'];
		$user_id = $_POST['user_id'];
		
		
		$query 	= "	INSERT INTO	insumoskit (nombre,created_at,updated_at,created_by,updated_by,status) VALUES ('$nombre','$fecha','$fecha','$user_id','$user_id',1)";
		
		if($mysqli->query($query)){
			$idkit = $mysqli->insert_id;
			
			$qd = "INSERT INTO insumoskit_detalle (idkit, iditem, tipo, cantidad) VALUES ";
			
			foreach($items as $detalle){
				$qd .= "($idkit,".$detalle['itemid'].",'".$detalle['tipo']."',".$detalle['cantidad']."),";
			}
			
			$qd = substr($qd,0,-1);
			if($mysqli->query($qd)){
				echo 1;
			} else {
				die($mysqli->error);
			}
		} else {
			die($mysqli->error);
		}		
	}
	
	function updateitbmsjubilado(){
		global $mysqli;
		
		if(isset ($_REQUEST['data_i'])){
			// UPDATE ITBMS
			$data_i 			= $_REQUEST['data_i'];
			$i = 0;
			$query 			= "	UPDATE 	insumos	SET itbms =(case";
			foreach ($data_i as $itbms){
				foreach ($itbms as $itbms_) {
					$query	   	.= " when id ='".$itbms_['id']."' then '".$itbms_['valor']."' ";
					$i++;
				}
				
			} 
			$query			.= " END ) WHERE id in (";
			foreach ($data_i as $itbms){
				foreach ($itbms as $itbms_) {
					$query	   	.= " '".$itbms_['id']."',";
					$i++;
				}
			} 
			$query = substr($query, 0, -1); 
			$query			.= ")";
			
			$result = $mysqli->query($query);
		}
		
		if(isset ($_REQUEST['data_j'])){
			$data_j 			= $_REQUEST['data_j'];
			$i = 0;
			// UPDATE JUBILADO
			$query 			= "	UPDATE 	insumos	SET jubilado =(case";
			foreach ($data_j as $jub){
				foreach ($jub as $jub_) {
					$query	   	.= " when id ='".$jub_['id']."' then '".$jub_['valor']."' ";
					$i++;
				}
				
			} 
			$query			.= " END ) WHERE id in (";
			foreach ($data_j as $jub){
				foreach ($jub as $jub_) {
					$query	   	.= " '".$jub_['id']."',";
					$i++;
				}
			} 
			$query = substr($query, 0, -1); 
			$query			.= ")";
			
			$result = $mysqli->query($query);		
		}
		echo 1;	
	}
	
	function existecodigo(){
		global $mysqli;
		$codigo = $_REQUEST['codigo'];
		$count = 0;
		$query = "SELECT codigo FROM insumos WHERE codigo = '$codigo'";
		$result = $mysqli->query($query);
		$count = $result->num_rows;
		echo $count;
	}
	
	function duplicar(){
		global $mysqli;
		$id = $_REQUEST['id'];
		
		$query = " 	INSERT INTO insumoskit (nombre, insumos)
					SELECT CONCAT(nombre,' - COPIA') as nombre,insumos FROM insumoskit WHERE id = $id
					LIMIT 1";
		if($result = $mysqli->query($query)){
			$idkit = $mysqli->insert_id;
				if($idkit > 0){
					$query2 = "INSERT INTO insumoskit_detalle (idkit, iditem, tipo, cantidad) 
						SELECT $idkit, iditem, tipo, cantidad
						FROM insumoskit_detalle 
						WHERE idkit = '$id' "; 
					$resultado2 = $mysqli->query($query2);													
					if ($resultado2){					
						echo 1;
					}else{
						die($query2);
					}
				} else {
					echo 'id kit:'.$idkit;
				}
		}else {
			die($mysqli->error);
		}
	}

	function COMBO_ITEMS(){
		global $mysqli;
		$query  = " SELECT id,unidad,codigo,nombre,tipo,precio,itbms,nombredeventa,unidadcompra, unidadventa FROM (
		 
						SELECT 	DISTINCT i.id as id,i.itbms as itbms, IFNULL(i.presentacion,'Unidad') as unidad, i.unidadcompra as unidadcompra, i.unidadventa as unidadventa,
							i.codigo as codigo, i.nombredecompra as nombre,i.nombre as nombredeventa, 'I' as tipo,
							i.costo as precio
								FROM insumos i where i.activo = 1
								UNION
						SELECT 	DISTINCT m.id as id,m.itbms as itbms,  IFNULL(m.unidadcompra,'Unidad') as unidad, m.unidadcompra as unidadcompra, m.unidadventa as unidadventa,
								m.codigo as codigo, m.nombre as nombre, m.nombre as nombredeventa, 'M' as tipo,
								m.costo as precio
							FROM medicamentos m where m.activo = 1
								UNION
						SELECT 	DISTINCT e.id as id,e.itbms as itbms, 'Unidad' as unidad,e.unidadcompra as unidadcompra, e.unidadventa as unidadventa,
							e.codigo as codigo, e.descripcion as nombre,e.descripcion as nombredeventa,'E' as tipo,
							e.costo as precio FROM equipos e WHERE e.activo = 1
					) as resultado ORDER BY precio desc ";

		$result = $mysqli->query($query);
		$resultado = "";
		$resultado .= "<option value='' selected></option>";
		while($row = $result->fetch_assoc()){
			$resultado .= "<option 
								data-precio='".number_format($row['precio'],2)."' 
								data-tipo ='".strtoupper($row['tipo'])."' 
								value='".$row['id']."'>".$row['codigo']." | ".$row['nombre']."
							</option>";
		}
		if( $resultado != ""){

		echo $resultado;

		} else {
		echo 0;
		}
		mysqli_close($mysqli);

	}

?>