<?php
		include("../conexion.php");
		global $mysqli;
		include("../mpdf/mpdf.php");
		// ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
		$idreporte = $_REQUEST['idreporte'];
		$strTarjeta = '';
		$datospaciente = '';
		$recursos = '';
		$queryreporte = "select r.fecha as fecha, u.nombre as enfermera, r.* from reporte_notas_enfermeria r 
						left join usuarios u on u.id = r.idenfermera 
						where r.idreporte = '$idreporte'";
		$rreporte = $mysqli->query($queryreporte);
		$preporte = $rreporte->fetch_assoc();
		$fecha = $preporte['fecha'];
		$fecha_r = date('Y-m-d',  strtotime($fecha));
		$idusuario = $preporte['idusuario'];
		$query  = "select nombre from usuarios where id = '$idusuario'";
		$r = $mysqli->query($query);
		$p = $r->fetch_assoc();
		$fecha = date('Y/m/d h:i a', strtotime($preporte['fecha']));
			$datospaciente .= '<table autosize="1" repeat_header="1" style="width:100%">';
			$datospaciente .= '<tr>
									<th style="border:1;width:33%; text-align:left;"><b>Reporte Diario por</b>:</th>
									<th style="border:1;width:33%;text-align:left;"><b>Reporte Preparado por</b>: <br> </th>
									
								</tr>
								<tr>							
									<td style="border:1;width:33%"><b>'.$preporte['enfermera'].' - ENFERMERA</b></td>
									<td style="border:1;width:33%"><b>'.$p['nombre'].'</b><br>
									<b>Fecha: </b>'.$fecha.'</td>
								</tr>';	
			$datospaciente .= '</table> <br>';  
		
		//DETALLE
		$strTarjeta .= $datospaciente;
		$strTarjeta .= '<table autosize="1" repeat_header="1" style="width:100%">';
		$strTarjeta .= '<thead>
							<tr>
								<th style="width:20%">Paciente</th>
								<th style="width:80%"><b>Nota de enfermería  </b></th>
							</tr>
						</thead>';		
	
			$queryvisitas = "select DISTINCT(idvisita), idreporte from reporte_notas_enfermeria_detalles where idreporte = '$idreporte' ";
			
			$res = $mysqli->query($queryvisitas);
			if($res->num_rows > 0){
				while($row = $res->fetch_assoc()){
					$visita = $row['idvisita'];
					$qidp = "select v.id as id,u.nombre as enfermera , p.nombre as paciente,p.id as idp, v.comentario as comentario
					from visitas v 
					INNER join usuarios u on u.id = v.idusuario 					
					INNER join pacientes p on p.id = v.idpaciente 
					where v.id = '$visita' and u.nivel = 3 ";
					
					$resa = $mysqli->query($qidp);
					$rowa = $resa->fetch_assoc();
					if($row['enfermera'] != ''){
						$recursos .= $row['enfermera'];
					}
					$img = base64_encode($tes);
					
					$querySV =" SELECT
						MAX(CASE WHEN vs.name = 'Frecuencia cardíaca' THEN vvs.value ELSE NULL END) 'FC',
						MAX(CASE WHEN vs.name = 'Frecuencia respiratoria' THEN vvs.value ELSE NULL END) 'FR',
						MAX(CASE WHEN vs.name = 'Oximetria' THEN vvs.value ELSE NULL END) 'SO2',
						MAX(CASE WHEN vs.name = 'Sistólica' THEN vvs.value ELSE NULL END) 'Sistolica',
						MAX(CASE WHEN vs.name = 'Diastólica' THEN vvs.value ELSE NULL END) 'Diastolica',
						MAX(CASE WHEN vs.name = 'Temperatura' THEN vvs.value ELSE NULL END) 'Temp',
						MAX(CASE WHEN vs.name = 'Dolor' THEN vvs.value ELSE NULL END) 'Dolor',
						MAX(CASE WHEN vs.name = 'Glicemia' THEN vvs.value ELSE NULL END) 'Glicemia'
						FROM visit_vital_sign vvs 
						LEFT JOIN vital_signs vs ON vs.id = vvs.vital_sign_id
						WHERE vvs.visit_id ='$visita' ";
								
					$resultSV=  $mysqli->query($querySV);
					//$textoSignos = "";
					while($rowSV = $resultSV->fetch_assoc()){
						$textoSignos = "FC:". $rowSV["FC"] . " FR:" . $rowSV["FR"] . " SO2:" . $rowSV["SO2"] . " Sistólica:" . $rowSV["Sistolica"]. " Diastólica:" . $rowSV["Diastolica"] . " Temperatura:" . $rowSV["Temp"]. " Dolor:". $rowSV["Dolor"]. " Glicemia:". $rowSV["Glicemia"];
					}	
					
					$strTarjeta .= '<tr>							
										<td class="center"><b >'.$rowa['paciente'].'</b></td>
										<td>'.$rowa['comentario'] . " ". $textoSignos . '<br> <br>';
										$query =' SELECT * FROM documentoshospitalizacion WHERE idvisita = '.$visita.'  ';
										$res1 = $mysqli->query($query);
										
										while($row1 = $res1->fetch_assoc()){
											
											$tes = "../../../homecare/pacientesbudget/".$rowa['idp']."/".$row1['codigo']."/".$row1['idvisita']."/".$row1['nombre'].".".$row1['tipoarchivo'];
											$strTarjeta .= '<span style="text-align: center;"><img style="height:375px;max-width:200px;width: expression(this.width > 300 ? 300: true); margin: 0 30px" src="'.$tes.'"></span>';
										}
									$strTarjeta .= '</td></tr>';	
					
				}
			}else{
					$qidp = "select v.id as id,u.nombre as enfermera , p.nombre as paciente,p.id as idp, v.comentario as comentario, ev.id as idevidencia
					from visitas v 
					LEFT join usuarios u on u.id = v.idusuario 					
					LEFT join pacientes p on p.id = v.idpaciente
					LEFT JOIN evidenciavisitas ev ON ev.idvisita = v.id
					where v.fecha = CURRENT_DATE  and u.nivel = 3 and p.estado_reporte_notas = 1 GROUP BY v.id ORDER BY ev.id asc";
					
				$res = $mysqli->query($qidp);
				
				
				
				
				while($row = $res->fetch_assoc()){

					$visita = $row['id'];
					$querySV =" SELECT
						MAX(CASE WHEN vs.name = 'Frecuencia cardíaca' THEN vvs.value ELSE NULL END) 'FC',
						MAX(CASE WHEN vs.name = 'Frecuencia respiratoria' THEN vvs.value ELSE NULL END) 'FR',
						MAX(CASE WHEN vs.name = 'Oximetria' THEN vvs.value ELSE NULL END) 'SO2',
						MAX(CASE WHEN vs.name = 'Sistólica' THEN vvs.value ELSE NULL END) 'Sistolica',
						MAX(CASE WHEN vs.name = 'Diastólica' THEN vvs.value ELSE NULL END) 'Diastolica',
						MAX(CASE WHEN vs.name = 'Temperatura' THEN vvs.value ELSE NULL END) 'Temp',
						MAX(CASE WHEN vs.name = 'Dolor' THEN vvs.value ELSE NULL END) 'Dolor',
						MAX(CASE WHEN vs.name = 'Glicemia' THEN vvs.value ELSE NULL END) 'Glicemia'
						FROM visit_vital_sign vvs 
						LEFT JOIN vital_signs vs ON vs.id = vvs.vital_sign_id
						WHERE vvs.visit_id ='$visita' ";
								
					$resultSV=  $mysqli->query($querySV);
					//$textoSignos = "";
					while($rowSV = $resultSV->fetch_assoc()){
						$textoSignos = "FC:". $rowSV["FC"] . " FR:" . $rowSV["FR"] . " SO2:" . $rowSV["SO2"] . " Sistólica:" . $rowSV["Sistolica"]. " Diastólica:" . $rowSV["Diastolica"] . " Temperatura:" . $rowSV["Temp"]. " Dolor:". $rowSV["Dolor"]. " Glicemia:". $rowSV["Glicemia"];
					}

					$img = base64_encode($tes);
					$strTarjeta .= '<tr>							
										<td><b>'.$row['paciente'].'</b></td>
										<td>'.$row['comentario']. " ".$textoSignos .'<br> <br>';
										$query =' SELECT * FROM documentoshospitalizacion WHERE idvisita = '.$row['id'].'  ';
										$res1 = $mysqli->query($query);
										while($row1 = $res1->fetch_assoc()){	
											$tes = "../../../homecare/pacientesbudget/".$row['idp']."/".$row1['codigo']."/".$row1['idvisita']."/".$row1['nombre'].".".$row1['tipoarchivo'];
											$strTarjeta .= '<span><img style="height:375px;max-width:300px;width: expression(this.width > 300 ? 300: true);" src="'.$tes.'"></span>';
										}
									$strTarjeta .= '</td></tr>';	
					
				}
			}
		
		$strTarjeta .= '</table><br/></br/>';
		
		$queryseguimientos = "select p.nombre as paciente, c.descripcion as descripcion 
								from reporte_notas_enfermeria_seguimiento c
								LEFT JOIN pacientes p on p.id = c.idpaciente where c.idreporte = '$idreporte'";
		
		$ss = $mysqli->query($queryseguimientos);
		
		if($ss->num_rows > 0){
			$strTarjeta .= '<table autosize="1" repeat_header="1" style="width:100%">';
				$strTarjeta .= '<thead>
							<tr>
								<th style="width:20%">Pacientes</th>
								<th style="width:80%"><b>Seguimientos</b></th>
							</tr>
						</thead>';
			while($row= $ss->fetch_assoc()){
							
					$strTarjeta .= 	'<tr>								
								<td class="center"><b>
									'.$row['paciente'].'
								</b></td>
								<td>
									<b>'.$row['codigo'].'</b><br>'.$row['descripcion'].'
								</td>';
		
			}
			$strTarjeta .= '</tr></table><br/></br/>';
		}
		$querycotizaciones = "select p.nombre as paciente, c.numero as codigo, c.descripcion as descripcion 
								from reporte_notas_enfermeria_cotizacion c
								LEFT JOIN pacientes p on p.id = c.idpaciente where c.idreporte = '$idreporte'";
		
		$cc = $mysqli->query($querycotizaciones);
		
		if($cc->num_rows > 0){
			$strTarjeta .= '<table autosize="1" repeat_header="1" style="width:100%">';
				$strTarjeta .= '<thead>
							<tr>
								<th style="width:20%">Pacientes</th>
								<th style="width:80%"><b>Cotizaciones </b></th>
							</tr>
						</thead>';
			while($row= $cc->fetch_assoc()){
							
					$strTarjeta .= 	'<tr>								
								<td class="center"><b>
									'.$row['paciente'].'
								</b></td>
								<td>
									<b>'.$row['codigo'].'</b><br>'.$row['descripcion'].'
								</td>';
		
			}
			$strTarjeta .= '</tr></table><br/></br/>';
		}
		
		$querylaboratorios = "select p.nombre as paciente, c.descripcion as descripcion 
								from reporte_notas_enfermeria_laboratorio c
								LEFT JOIN pacientes p on p.id = c.idpaciente where c.idreporte = '$idreporte'";
		
		$ll = $mysqli->query($querylaboratorios);
		
		if($ll->num_rows > 0){
			$strTarjeta .= '<table autosize="1" repeat_header="1" style="width:100%">';
				$strTarjeta .= '<thead>
							<tr>
								<th style="width:20%">Pacientes</th>
								<th style="width:80%"><b>Laboratorios</b></th>
							</tr>
						</thead>';
			while($row= $ll->fetch_assoc()){
							
					$strTarjeta .= 	'<tr>								
								<td class="center"><b>
									'.$row['paciente'].'
								</b></td>
								<td>
									<b>'.$row['codigo'].'</b><br>'.$row['descripcion'].'
								</td>';
		
			}
			$strTarjeta .= '</tr></table><br/></br/>';
		}
		
		
		
		if($preporte['descripcion_plataforma'] != ''){
		
		$strTarjeta .= '<table autosize="1" repeat_header="1" style="width:100%">';
		$strTarjeta .= '<thead>			
							<tr>
								<th style="width:100%"><b>Actualizaciones </b></th>
							</tr>
						</thead>';
		$strTarjeta .= 		'<tr>								
								<td>
									'.$preporte['descripcion_plataforma'].'
								</td>	
							</tr>';	
		$strTarjeta .= '</table><br/></br/>';
		
		}
		
		$html = '
		<!-- DEFINE HEADERS & FOOTERS -->
				<style>
					h1,h2,h3,h4,h5,h6,a,p,div,span,li,td,th {
						font-family: Arial, sans-serif;
					}
					table{
						  border-collapse: collapse;
					}
					td{
						font-family: Arial, sans-serif;
						font-size: 16px;
						text-align: justify;
						 border: 1px solid #000;
					}
					
					tr{
						font-family: Arial, sans-serif;
						font-size: 12px;
						text-align: center;
						 border: 1px solid #000;
					}
					h1,h2,h3,h4,h5,h6 {
						/*color: rgb(21, 98, 158);*/
						text-align:center;
						margin-top: 5px;
						margin-bottom: 5px;
					}
					.blue {
						color: rgb(21, 98, 158);
					}
					th,p{
						background-color: #0a5897;
						color: white
					}									
					.center {
						text-align:center;
					}
				</style>
		
		<div align="left"><img src="../../images/logo-hc.png"><h3>REPORTE DIARIO - ENFERMERÍA ESPECIALIZADA
		<br>PREPARADO PARA: HOGAR COMO EN SU CASA</h3></div>
		<br>
		';
		
		$html .= $strTarjeta;
		//die($html);
		$mpdf= new mpdf(); 
		$mpdf->mirrorMargins = true;
		$mpdf->SetDisplayMode('fullpage','two');	
		$mpdf->addPage('L');
		$mpdf->WriteHTML($html); 
	
		
		
        $mpdf->Output("Reporte_diario_".$fecha_r.".pdf",'I');
			
		//$mpdf->Output();
		
		echo true;
?>