<?php
    include_once("../conexion.php");

    // ini_set('display_errors', 1); ini_set('display_startup_errors', 1);
    
    $opcion = '';
    if (isset($_REQUEST['opcion'])) {
        $opcion = $_REQUEST['opcion'];   
    }
    
    switch($opcion){
        case "PACIENTES_CON_ALERTAS":
            pacientes_con_alertas();
        break;
        case "TABLA_TRATAMIENTOS_POR_FINALIZAR":
            datos_tratamientos_por_finalizar();
        break;
        case "LISTAR_REPORTES_DIARIOS":
            listar_reportes_diarios();
        break;
        case "TABLA_TRATAMIENTOS_EDITADOS":
            tabla_tratamientos_editados();
        break;        
        case "TABLA_PLAN_EDITADOS":
            tabla_plan_editados();        
        break;
        case "TABLA_REPOSICION_MEDICAMENTOS":
            tabla_reposicion_medicamentos();
        break;
        case "GUARDAR_REPOSICION_MEDICAMENTOS":
            guardar_reposicion_medicamentos();
        break;
        case "HISTORICO_REPOSICION_MEDICAMENTOS":
            historico_reposicion_medicamentos();
        break;
		case "VER_TARJETA":
            ver_tarjeta();
        break;
		case "VER_PLAN":
            ver_plan();
        break;
        default: 
            echo json_encode(array(
                "error"=>true,
                "message" => "opción no válida",
                "opcion" => $opcion
            ));
        break;
    }
	function pacientes_con_alertas(){
		global $mysqli;
		$idpaciente = isset($_GET['idpaciente'])?$_GET['idpaciente']:0;	
		$fecha_inicio = isset( $_GET['fecha_inicio'] )?$_GET['fecha_inicio']: "";
		$fecha_fin = isset($_GET['fecha_fin'])?$_GET['fecha_fin']:'';
		$empresa = isset($_COOKIE['empresa'])?$_COOKIE['empresa']:0;
		$organizacion = isset($_COOKIE['organizacion'])?$_COOKIE['organizacion']:2;
		$nivel_usuario = $_SESSION['nivel'];
		$id_usuario = $_SESSION['user_id'];
		$resultado = array();
		$arrCategorias = array();
		$arrFrecuenciaCardiaca = array();
		$arrFrecuenciaRespiratoria = array();
		$arrOximetria = array();
		$arrSistolica = array();
		$arrDiastolica = array();
		$arrTemperatura = array();
		$arrDolor = array();
		$arrGlicemia = array();
		if($idpaciente == -1 ) $idpaciente = 0;
		$q = "SELECT max(v.id) AS idvisita,  p.id as id, p.condicion_id as condicion, h.nombre as hogar		
				FROM pacientes p
				INNER join visitas v ON p.id = v.idpaciente	
				LEFT JOIN empresas emp ON emp.id = p.empresa_id
				LEFT JOIN organizaciones o ON o.id = emp.idorganizacion
				LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id		
				LEFT JOIN hogares h ON h.id = p.hogar
				WHERE 1  ";
				
		if ($nivel_usuario == 2 || $nivel_usuario == 5){
			$idmedico = $mysqli->query("Select id from medicos where idusuario = $id_usuario")->fetch_assoc()['id'];
			$q .= " AND (pu.usuario_id = '$id_usuario' OR p.idmedicotratante = $idmedico )";
		} else{
			if( $empresa != 0 ){
				$q .= " AND p.empresa_id = '".$empresa."' ";
			}else{
				if($organizacion != 0){
					$q .= " AND o.id = '".$organizacion."' ";
				}
			}		
		}
		if($fecha_inicio  == '' && $idpaciente == 0 ){
			$q.= " AND v.fecha >= DATE_SUB(NOW(), INTERVAL 1 DAY) ";
			$q.=" GROUP BY p.id ORDER BY v.fecha desc";	
		}else{
			if($idpaciente != 0 && $idpaciente != -1){
				$q.="AND p.id = '$idpaciente' ";				
				if($fecha_inicio != ''){
					if ($fecha_fin != ''){
						$q.=" AND v.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' ";
					}else{
						$q.=" AND v.fecha >= '$fecha_inicio' ";
					}
				}					
				$q.=" group by v.id ORDER BY v.fecha desc";			
			}else{
				if($fecha_inicio != ''){
					$q.=" AND v.fecha = '$fecha_inicio' ";				
				}					
				$q.=" group by p.id ORDER BY v.fecha desc";			
			}
		}
											 
		$res = $mysqli->query($q);
		while ($ro = $res->fetch_assoc()){
			$idvisita = $ro['idvisita'];
			

			$query = "
				SELECT distinct(p.nombre) as paciente, p.condicion_id as condicion,	CONCAT(v.fecha,' ',v.horainicioplan) as fecha,p.id as idpaciente, u.nombre as recurso, 
				(CASE WHEN u.nivel = 3 THEN 'Enfermera' WHEN u.nivel = 9 THEN 'Técnico de enfermería' WHEN 2 THEN 'Médico' WHEN u.nivel = 5 THEN 'Médico' ELSE '' END) as tipo_recurso, 
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='$idvisita' LIMIT 1) ,'') as frecuenciacardiaca,
				resultado_examen('Frecuencia cardiaca',IFNULL(p.fechanac,now()),(SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='$idvisita' LIMIT 1) ) as resultado_frecuenciacardiaca,
				IFNULL( (SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='$idvisita' LIMIT 1),'') as frecuenciarespiratoria,
				resultado_examen('Frecuencia respiratoria',IFNULL(p.fechanac,now()), (SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='$idvisita' LIMIT 1) ) as resultado_frecuenciarespiratoria,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='$idvisita' LIMIT 1) ,'') as oximetria,
				resultado_examen('Saturación de oxígeno',IFNULL(p.fechanac,now()), (SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='$idvisita' LIMIT 1)) as resultado_oximetria,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='$idvisita' LIMIT 1),'') as sistolica,
				resultado_examen('Presión sistolica',IFNULL(p.fechanac,now()),(SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='$idvisita' LIMIT 1) ) as resultado_sistolica,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='$idvisita' LIMIT 1),'') as diastolica,
				resultado_examen('Presión diastolica',IFNULL(p.fechanac,now()),(SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='$idvisita' LIMIT 1) ) as resultado_diastolica,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='$idvisita' LIMIT 1) ,'') as temperatura,
				resultado_examen('Temperatura',IFNULL(p.fechanac,now()),(SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='$idvisita' LIMIT 1  )) as resultado_temperatura,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='$idvisita' LIMIT 1  ),'') as dolor,
				resultado_examen('Dolor',IFNULL(p.fechanac,now()), (SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='$idvisita' LIMIT 1  ) ) as resultado_dolor,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='$idvisita' LIMIT 1  ),'') as glicemia,
				resultado_examen('Glicemia capilar',IFNULL(p.fechanac,now()), (SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='$idvisita' LIMIT 1  ) ) as resultado_glicemia,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=11 AND t.visit_id='$idvisita' LIMIT 1  ) ,'') as estatura,
				IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=11 AND t.visit_id='$idvisita' LIMIT 1  ) ,'') as peso,
				IFNULL(v.imc,'') as imc,
				resultado_examen('IMC',IFNULL(p.fechanac,now()),v.imc) as resultado_imc,
				IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_cardiaca,
				IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_respiratoria,
				IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_oximetria,
				IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_sistolica,
				IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_diastolica, 
				IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_temp,
				IFNULL(resultado_examen_valor('Dolor',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_dolor,
				IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='$idvisita' LIMIT 1),0)),'baja') as condicion_glicemia
				FROM visitas v 
				INNER JOIN pacientes p ON p.id = v.idpaciente
				LEFT JOIN visit_vital_sign vs ON v.id = vs.visit_id
				INNER JOIN usuarios u ON u.id = v.idusuario
				WHERE v.id = '$idvisita' AND vs.id is not null
			";
			$query.=" 
				GROUP BY p.id
				ORDER BY p.id DESC
			";
			$r 	= $mysqli->query($query);
			while($row= $r->fetch_assoc()){	
                $querycondicion = "SELECT minimo,maximo, (case when idsignov = 1 then 'dfc' when idsignov = 2 then 'dfr'  
                when idsignov = 3 then 'dox'  when idsignov = 4 then 'dsi'  when idsignov = 5 then 'ddi'  
                when idsignov = 6 then 'dtc'  when idsignov = 7 then 'ddolor'  when idsignov = 8 then 'dgc' end) as signo 
                FROM patient_vital_sign_condition  
                where idpaciente = '".$row['idpaciente']."' and idsignov != 9 and estado = 1
                group by idsignov ORDER BY id desc";
                $resultcondicion = $mysqli->query($querycondicion);                
                if($resultcondicion->num_rows > 0){
                    while($filad = $resultcondicion->fetch_assoc()){
                        $fc2 = $row['glicemia'];                        
                        if (isset($filad['signo'])) { 
                            $signo = $filad['signo'];
                            if ($signo=='dfc'){
                                if($row['fc'] > $filad['maximo']){$dfc = 'alta';}
                                elseif($row['fc'] < $filad['minimo']){$dfc = 'baja';}
                            }elseif ($signo=='dfr'){
                                if($row['fr'] > $filad['maximo']){$dfr = 'alta';}
                                elseif($row['fr'] < $filad['minimo']){$dfr = 'baja';}
                            }elseif ($signo=='dox'){
                                if($row['oximetria'] > $filad['maximo']){$dox = 'alta';}
                                elseif($row['oximetria'] < $filad['minimo']){$dox = 'baja';}
                            }elseif ($signo=='dsi'){ 
                                if($row['sistolica'] > $filad['maximo']){$dsi = 'alta';}
                                elseif($row['sistolica'] < $filad['minimo']){$dsi = 'baja';} 
                            }elseif ($signo=='ddi'){
                                if($row['diastolica'] > $filad['maximo']){$ddi = 'alta';}
                                elseif($row['diastolica'] < $filad['minimo']){$ddi = 'baja';}
                            }elseif ($signo=='dtc'){
                                if($row['temperatura'] > $filad['maximo']){$dtc = 'alta';}
                                elseif($row['temperatura'] < $filad['minimo']){$dtc = 'baja';}
                            }elseif ($signo=='ddolor'){
                                if($row['dolor'] > $filad['maximo']){$ddolor = 'alta';}
                                elseif($row['dolor'] < $filad['minimo']){$ddolor = 'baja';}
                            }elseif ($signo=='dgc'){
                                if($row['glicemia'] > $filad['maximo']){$dgc = 'alta';}
                                elseif($row['glicemia'] < $filad['minimo']){$dgc = 'baja';}
                            }
                        }
                    }
                }








				// if($row['condicion'] == 1){
				// 	$condicion = '<span class="icon-col fa fa-heartbeat green" data-toggle="tooltip" data-original-title="Paciente estable" data-placement="left"></span>';
				// }
				// elseif($row['condicion'] == 2){
				// 	$condicion = '<span class="icon-col fa fa-heartbeat yellow" data-toggle="tooltip" data-original-title="Paciente requiere atención"  data-placement="left"></span>';
				// }elseif($row['condicion'] == 3){
				// 	$condicion = '<span class="icon-col fa fa-heartbeat red" data-toggle="tooltip" data-original-title="Paciente requiere atención inmediata"  data-placement="left"></span>';
				// }else{
				// 	$condicion = '<span class="icon-col fa fa-heartbeat blue" data-toggle="tooltip" data-original-title="Valor no registrado"  data-placement="left"></span>';
				// }
				
				$resultado['data'][] = array(
					'idpaciente' => $row['idpaciente'],
					'id'	=>  $idvisita,
					'acciones'	=>  "<div style='float:left;margin-right:10px;' class='ui-pg-div ui-inline-custom'>
										<span class='icon-col blue fa fa-eye ver-visita' data-id='".$idvisita."'data-toggle='tooltip' data-original-title='Ver valores' data-placement='right'></span>
										<span class='icon-col blue fa fa-folder-open boton-historia ver-historial-p' data-id='".$ro['id']."' data-toggle='tooltip' data-original-title='Ir al historial' data-placement='right'></span>
										<span class='icon-col green fa fa-print boton-reporte-diario1 ver-reporte-diario' data-id='".$ro['id']."' data-fecha='".$row['fecha']."' data-toggle='tooltip' data-original-title='Imprimir reporte diario' data-placement='right'></span>
									 </div>",
					'hogar'	=>  $ro['hogar'],
					'nombre'	=>  $row['paciente'],
					'recurso'	=>  $row['tipo_recurso'].':'.$row['recurso'],
					'condicion'	=>  $row['condicion'],
					'riesgo'	=>  get_nivel_riesgo($row['idpaciente']),
					'fecha'	    =>  $row['fecha'],
					'idvisita'	=>  $idvisita,
					'fc'		=>	$row['frecuenciacardiaca'],
					'rfc'		=>  $row['resultado_frecuenciacardiaca'],
					'fr'		=>	$row['frecuenciarespiratoria'],
					'rfr'     	=>  $row['resultado_frecuenciarespiratoria'],
					'so'		=>	$row['oximetria'],
					'rox'		=>  $row['resultado_oximetria'],
					'paa'		=>	$row['sistolica'],
					'rsi'		=>  $row['resultado_sistolica'],
					'pab'		=>	$row['diastolica'],
					'rdi'		=>  $row['resultado_diastolica'],
					'tc'		=>	$row['temperatura'],
					'rtm'		=>  $row['resultado_temperatura'],
					'dolor'		=>	$row['dolor'],
					'rdl'		=>  $row['resultado_dolor'],
					'gc'		=>	$row['glicemia'],
					'rgc'		=>  $row['resultado_glicemia'],
					'e'			=>	$row['estatura'],
					'peso'		=>	$row['peso'],
					'imc'		=>	$row['imc'],
					'rim'		=>  $row['resultado_imc'],
					'condicion_cardiaca' => $row['condicion_cardiaca'],
					'condicion_respiratoria' => $row['condicion_respiratoria'],
					'condicion_oximetria' => $row['condicion_oximetria'],
					'condicion_sistolica' => $row['condicion_sistolica'],
					'condicion_diastolica' => $row['condicion_diastolica'],
					'condicion_temp' => $row['condicion_temp'],
					'condicion_dolor' => $row['condicion_dolor'],			
					'condicion_glicemia' => $row['condicion_glicemia'],
                    'dfc'		=>  !empty($dfc)?$dfc : "",	                    
                    'dfr'		=>  !empty($dfr)?$dfr : "",	
                    'dox'		=>  !empty($dox)?$dox : "",	
                    'dsi'		=>  !empty($dsi)?$dsi : "",	
                    'ddi'		=>  !empty($ddi)?$ddi : "",	
                    'dtc'		=>  !empty($dtc)?$dtc : "",	
                    'ddolor'	=>  !empty($ddolor)?$ddolor : "",	
                    'dgc'		=>  !empty($dgc)?$dgc : "",				
				);		
                // se carga la información para la gráfica
                if($idpaciente != 0){
                    $arrCategorias[] = $row['fecha'];
                }else{
                    $arrCategorias[] = $row['paciente'];
                }
                $arrIdVisita[] =  $idvisita;
                $arrFrecuenciaCardiaca[] = array(
                    'id' => $idvisita,
                    'y' =>  $row['frecuenciacardiaca'],
                );
                $arrFrecuenciaRespiratoria[] = array(
                    'id' => $idvisita,
                    'y'  => $row['frecuenciarespiratoria']
                );
                $arrOximetria[] = array(
                    'id' => $idvisita,
                    'y'  => $row['oximetria']
                );
                $arrSistolica[] = array(
                    'id' => $idvisita,
                    'y'  => $row['sistolica']
                );
                $arrDiastolica[] = array(
                    'id' => $idvisita,
                    'y'  => $row['diastolica']
                );
                $arrTemperatura[] = array(
                    'id' => $idvisita,
                    'y'  => $row['temperatura']
                );
                $arrDolor[] = array(
                    'id' => $idvisita,
                    'y'  => $row['dolor']
                );
                $arrGlicemia[] = array(
                    'id' => $idvisita,
                    'y'  => $row['glicemia']
                );
                
			}
		}
        $series = array(
            'arrFrecuenciaCardiaca' => array( 
                                'data' => $arrFrecuenciaCardiaca,
                                'name' => 'Frec. Cardíaca',
                                'color' => '#D50000'
                            ),
            'arrFrecuenciaRespiratoria'=> array( 
                                'data' => $arrFrecuenciaRespiratoria,
                                'name' => 'Frec. Respiratoria',
                                'color' => '#AA00FF'
                            ),
            'arrOximetria' => array( 
                                'data' => $arrOximetria,
                                'name' => 'Sat. Oxigeno',
                                'color' => '#304FFE',
                            ),
            'arrSistolica' => array( 
                                'data' => $arrSistolica,
                                'name' => 'Sistólica',
                                'color' => '#2962FF',
                            ),
            'arrDiastolica' => array( 
                                'data' => $arrDiastolica,
                                'name' => 'Diastólica',
                                'color' => '#00BFA5',
                            ),
            'arrTemperatura'=> array( 
                                'data' => $arrTemperatura,
                                'name' => 'Temperatura',
                                'color' => '#00C853',
                            ),
            'arrDolor' => array( 
                                'data' => $arrDolor,
                                'name' => 'Nivel de Dolor',
                                'color' => '#AEEA00',
                            ),
            'arrGlicemia' => array(  
                                'data' => $arrGlicemia,
                                'name' => 'Glicemia Capilar',
                                'color' => '#FFAB00',
                            )
        );
		if(!empty($resultado)){
            $resultado['grafica'] = array(
                'series' => $series,
                'arrCategorias' => $arrCategorias
            );
		}else{
			$resultado = array(
				'data' => array(
					'id' => '',
					'acciones' => '',
					'nombre' => '',
					'condicion' => '',
					'riesgo' => '',
					'fecha' => '',
					'idvisita' => '',
					'fc' => '',
					'rfc' => '',
					'fr' => '',
					'rfr' => '',
					'so' => '',
					'rox' => '',
					'paa' => '',
					'rsi' => '',
					'pab' => '',
					'rdi' => '',
					'tc' => '',
					'rtm' => '',
					'dolor' => '',
					'rdl' => '',
					'gc' => '',
					'rgc' => '',
					'e' => '',
					'peso' => '',
					'imc' => '',
					'rim' => '',
					'condicion_cardiaca' => '',
					'condicion_respiratoria' => '',
					'condicion_oximetria' => '',
					'condicion_sistolica' => '',
					'condicion_diastolica' => '',
					'condicion_temp' => '',
					'condicion_dolor' => '',
					'condicion_glicemia' => ''
				),
				'grafica' => array(
                    'series' => $series,
                    'arrCategorias' => $arrCategorias
                )
			);
		}
		echo json_encode($resultado);
		mysqli_close($mysqli);
	}
    function datos_tratamientos_por_finalizar() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
        $empresa = isset($_COOKIE['empresa'])?$_COOKIE['empresa']:0;
		$organizacion = isset($_COOKIE['organizacion'])?$_COOKIE['organizacion']:2;
		$nivel_usuario = $_SESSION['nivel'];
		$id_usuario = $_SESSION['user_id'];

		$consulta ="SELECT 
        p.nombre as paciente, m.nombre as nombremedicamento, td.via, td.frecuencia, td.dosis, td.fecha_inicio, td.fecha_fin, td.observaciones, td.estado
        FROM tarjetamedicamentodetalle td
        INNER JOIN tarjetamedicamento t ON t.id = td.idtarjeta
        INNER JOIN pacientes p ON p.id = t.idpaciente
        INNER JOIN  medicamentos m ON m.id = td.idmedicamento
        LEFT JOIN empresas emp ON emp.id = p.empresa_id
        LEFT JOIN organizaciones o ON o.id = emp.idorganizacion
        LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id		
        LEFT JOIN hogares h ON h.id = p.hogar
        WHERE fecha_fin BETWEEN NOW() AND DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 3 DAY) AND td.estado = '1' AND t.estatus = '1'";																
		if ($nivel_usuario == 2 || $nivel_usuario == 5){
			$idmedico = $mysqli->query("Select id from medicos where idusuario = $id_usuario")->fetch_assoc()['id'];
			$consulta .= " AND (pu.usuario_id = '$id_usuario' OR p.idmedicotratante = $idmedico )";
		} else{
			if( $empresa != 0 ){
				$consulta .= " AND p.empresa_id = '".$empresa."' ";
			}else{
				if($organizacion != 0){
					$consulta .= " AND o.id = '".$organizacion."' ";
				}
			}		
		}
        $consulta .=" GROUP BY p.id,m.id ";
		if($result = $mysqli->query($consulta)){
            
        }else{
            die($mysqli->error);
        }
		
		$response = array();	
		while($row = $result->fetch_assoc()){
			$response['data'][]= array(
            "paciente" => $row["paciente"],
            "nombremedicamento" => $row["nombremedicamento"],
            "via" => $row["via"],
            "frecuencia" => $row["frecuencia"],
            "dosis" => $row["dosis"],
            "fecha_inicio" => $row["fecha_inicio"],
            "fecha_fin" => $row["fecha_fin"],
            "observaciones" => $row["observaciones"],
            "estado" => $row["estado"],
            "acciones" => ''
            );        
		}

		echo json_encode($response);
	}
    function listar_reportes_diarios(){
        $servidor = "https://pmc.vitae-health.com";
        global $mysqli;
		$query="SELECT r.id,u.nombre as preparado_por,r.fecha as fecha, r.idreporte as reporte, (CASE WHEN r.estado = 0 THEN 'No enviado' ELSE 'Enviado' END) as estado 
				FROM reporte_notas_enfermeria r 
				LEFT JOIN usuarios u on u.id = r.idusuario 
                WHERE r.estado = 1 ORDER BY r.fecha desc LIMIT 6";

		$res = $mysqli->query($query);
		$resultado = array();
		while($row= $res->fetch_assoc()){
            $acciones = '<div class="dropdown ml-auto text-right">
                            <div class="btn-link" data-toggle="dropdown">
                                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                            </div>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item boton-imprimir-rep-diario" href="'.$servidor.'/controller/phm/imprimir_reporte_diario.php?idreporte='.$row['reporte'].'" target="blank_">Imprimir</a>
                            </div>
                        </div>';
            
            $resultado['data'][] = array(
                'id'	=> 	$row['id'], 
                'acciones'	=> 	$acciones, 
                'fecha'	=> 	$row['fecha'],
                'preparado_por'	=>  $row['preparado_por'],
                'estado' => $row['estado']
            );		
        }
        if(!empty($resultado)){
            echo json_encode($resultado);
        }else{
            $resultado['data'][] = array(
                    'id'	=> 	'-',
                    'acciones'	=> 	'-',
                    'fecha'	=> 	'-',
                    'usuario'	=>  '-',
                    'estado'	=>  '-'
                );
                echo json_encode($resultado);
        }
    }
    function tabla_tratamientos_editados(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$empresa = isset($_COOKIE['empresa'])?$_COOKIE['empresa']:0;
		$organizacion = isset($_COOKIE['organizacion'])?$_COOKIE['organizacion']:2;
		$nivel_usuario = $_SESSION['nivel'];
		$id_usuario = $_SESSION['user_id'];
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		$query  = "SELECT DISTINCT t.id as idreporte,DATE_FORMAT(t.fecha, '%Y-%m-%d %h:%i:%p') AS fecha_actualizacion_tarjeta, 
					p.nombre AS paciente, (case when t.estatus = 1 then 'Tarjeta Enviada' when t.estatus = 0 then 'Por revisar' end) as estado, u.nombre as creado_por, t.tarjeta_simple
                    FROM  tarjetamedicamento t 
                    INNER JOIN pacientes p ON p.id = t.idpaciente
                    INNER JOIN usuarios u ON u.id = t.creadopor
                    LEFT JOIN empresas emp ON emp.id = p.empresa_id
                    LEFT JOIN organizaciones o ON o.id = emp.idorganizacion
                    LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id		
                    LEFT JOIN hogares h ON h.id = p.hogar
                    WHERE t.fecha >= now() - INTERVAL 2 DAY  AND t.estatus <> '2'";
		if ($nivel_usuario == 2 || $nivel_usuario == 5){
			$idmedico = $mysqli->query("Select id from medicos where idusuario = $id_usuario")->fetch_assoc()['id'];
			$query .= " AND (pu.usuario_id = '$id_usuario' OR p.idmedicotratante = $idmedico )";
		} else{
			if( $empresa != 0 ){
				$query .= " AND p.empresa_id = '".$empresa."' ";
			}else{
				if($organizacion != 0){
					$query .= " AND o.id = '".$organizacion."' ";
				}
			}		
		}
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		$diagnostico = '';
		while($row = $result->fetch_assoc()){
			if($row['diagnosticos'] != ''){
				$separada = explode(',', $row['diagnosticos']);
				foreach($separada as $id){
					$diagn = getValor('nombre','enfermedades',$id);
					$diagnostico = ' '.$diagn.',';
				}
				$diagnostico = substr($diagnostico, 0, -1);	
			}else{
				$diagnostico = '-';
			}
			if($row['estado'] == 'Inactivo'){
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item imprimir" data-id='.$row['idreporte'].'>Imprimir</a>
							<a class="dropdown-item activar" data-id='.$row['idreporte'].'>Activar</a>
							<a class="dropdown-item ver-tarjeta" data-id='.$row['idreporte'].' data-simple="'.$row['tarjeta_simple'].'" >Ver</a>
						</div>
					</div>';
			}else{
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="controller/reporte_tarjeta_medicamentos.php?id='.$row['idreporte'].'" target="_blank">Imprimir</a>
							<a class="dropdown-item ver-tarjeta" data-id='.$row['idreporte'].' data-simple="'.$row['tarjeta_simple'].'">Ver</a>
						</div>
					</div>';
			}
			
			$response['data'][] = array(
				'paciente'     	    =>	$row['paciente'],
				'fecha_actualizacion_tarjeta' 	        =>	$row['fecha_actualizacion_tarjeta'],
				'estado' => $row['estado'],
				'creado_por' => $row['creado_por'],
				'acciones' 	    => $acciones
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}


	function tabla_plan_editados(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$empresa = isset($_COOKIE['empresa'])?$_COOKIE['empresa']:0;
		$organizacion = isset($_COOKIE['organizacion'])?$_COOKIE['organizacion']:2;
		$nivel_usuario = $_SESSION['nivel'];
		$id_usuario = $_SESSION['user_id'];
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		$query  = "SELECT DISTINCT t.id as idreporte,DATE_FORMAT(t.fecha, '%Y-%m-%d %h:%i:%p') AS fecha_actualizacion_tarjeta, 
					p.nombre AS paciente, (case when t.estatus = 1 then 'Plan enviado' when t.estatus = 0 then 'Por revisar' end) as estado, u.nombre as creado_por
                    FROM  plancuidado t 
                    INNER JOIN pacientes p ON p.id = t.idpaciente
                    INNER JOIN usuarios u ON u.id = t.creadopor
                    LEFT JOIN empresas emp ON emp.id = p.empresa_id
                    LEFT JOIN organizaciones o ON o.id = emp.idorganizacion
                    LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id		
                    LEFT JOIN hogares h ON h.id = p.hogar
                    WHERE t.fecha >= now() - INTERVAL 2 DAY  AND t.estatus <> '2'";
		if ($nivel_usuario == 2 || $nivel_usuario == 5){
			$idmedico = $mysqli->query("Select id from medicos where idusuario = $id_usuario")->fetch_assoc()['id'];
			$query .= " AND (pu.usuario_id = '$id_usuario' OR p.idmedicotratante = $idmedico )";
		} else{
			if( $empresa != 0 ){
				$query .= " AND p.empresa_id = '".$empresa."' ";
			}else{
				if($organizacion != 0){
					$query .= " AND o.id = '".$organizacion."' ";
				}
			}		
		}
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		$diagnostico = '';
		while($row = $result->fetch_assoc()){
			if($row['diagnosticos'] != ''){
				$separada = explode(',', $row['diagnosticos']);
				foreach($separada as $id){
					$diagn = getValor('nombre','enfermedades',$id);
					$diagnostico = ' '.$diagn.',';
				}
				$diagnostico = substr($diagnostico, 0, -1);	
			}else{
				$diagnostico = '-';
			}
			if($row['estado'] == 'Inactivo'){
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item imprimir" data-id='.$row['idreporte'].'>Imprimir</a>
							<a class="dropdown-item activar" data-id='.$row['idreporte'].'>Activar</a>
							<a class="dropdown-item ver-plan" data-id='.$row['idreporte'].' >Ver</a>
						</div>
					</div>';
			}else{
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="controller/reporte_planes.php?id='.$row['idreporte'].'"   target="_blank">Imprimir</a>
							<a class="dropdown-item ver-plan" data-id='.$row['idreporte'].' >Ver</a>
						</div>
					</div>';
			}
			
			$response['data'][] = array(
				'paciente'     	    =>	$row['paciente'],
				'fecha_actualizacion' 	        =>	$row['fecha_actualizacion_tarjeta'],
				'estado' => $row['estado'],
				'creado_por' => $row['creado_por'],
				'acciones' 	    => $acciones
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}

	function tabla_reposicion_medicamentos(){
		global $mysqli;
		$idpaciente =isset($_GET['idpaciente'])?$_GET['idpaciente']:0;
		$where = '';
		if($idpaciente !=0){
			$where = " AND p.id = $idpaciente ";
		}
		$query = "SELECT p.id as idpaciente, t.id as idtarjeta, m.id as idmedicamento, p.nombre AS paciente, m.nombre AS medicamento, DATE(IFNULL(rm.fecha_prox_reposicion,td.fecha_inicio)) AS proxima_reposicion, IFNULL(IFNULL(rm.contacto,CONCAT(c.nombre,' ',c.apellido)),'') AS contacto, IFNULL(IFNULL(rm.telefono_contacto, IFNULL(c.celular,c.telefonocasa)),'') AS telefono_contacto,  IFNULL(rm.comentarios,'') AS comentarios 
					FROM tarjetamedicamentodetalle td 
					INNER JOIN tarjetamedicamento t ON t.id = td.idtarjeta
					INNER JOIN medicamentos m ON m.id = td.idmedicamento
					INNER JOIN pacientes p ON p.id = t.idpaciente 
					LEFT JOIN reposicion_medicamentos rm ON rm.id = (SELECT max(id)  FROM reposicion_medicamentos WHERE idpaciente = p.id AND idmedicamento = td.idmedicamento )
					LEFT JOIN pacientecontactos pc ON pc.idpaciente = p.id 
					LEFT JOIN contactos c ON c.id = pc.idcontacto
					WHERE t.estatus = 1 AND td.estado = 1 AND td.fecha_fin >= CURDATE() AND p.empresa_id = 9 AND (rm.fecha_prox_reposicion IS NULL or rm.fecha_prox_reposicion BETWEEN NOW() AND NOW() + INTERVAL 3 DAY   ) $where GROUP BY p.id,m.id ORDER BY td.fecha_fin ASC";
		$respuesta = array();
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc()){
			$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item registrar_reposicion" data-idpaciente="'.$row['idpaciente'].'" data-idmedicamento ="'.$row['idmedicamento'].'">Reasignar fecha de reposición</a>
							<a class="dropdown-item historico_reposicion" data-idpaciente="'.$row['idpaciente'].'" data-idmedicamento ="'.$row['idmedicamento'].'" data-paciente="'.$row['paciente'].'" data-medicamento ="'.$row['medicamento'].'">Ver histórico de reposición</a>
							<a class="dropdown-item imprimir-tarjeta"  href="controller/reporte_tarjeta_medicamentos.php?id='.$row['idtarjeta'].'" data-id='.$row['idtarjeta'].'>Imprimir Tarjeta de medicamentos</a>

						</div>
					</div>';
			if($idpaciente != 0){
				$respuesta['paciente'] = $row['paciente'];
			}
			$respuesta['data'][] = array(
				"idpaciente" => $row['idpaciente'],
				"paciente" => $row['paciente'],
				"idmedicamento" => $row['idmedicamento'],
				"medicamento" => $row['medicamento'],
				"proxima_reposicion" => $row['proxima_reposicion'],
				"contacto" => $row['contacto'],
				"telefono_contacto" => $row['telefono_contacto'],
				"comentarios" => $row['comentarios'],
				"acciones" => $acciones
			);
		}
		if(empty($respuesta)){
			$respuesta['data'][] = array(
				"idpaciente" => '',
				"paciente" => '',
				"idmedicamento" => '',
				"medicamento" => '',
				"proxima_reposicion" => '',
				"contacto" => '',
				"telefono_contacto" => '',
				"comentarios" => '',
				"acciones" => ''
			);
		}
		echo json_encode($respuesta);
	}

	function guardar_reposicion_medicamentos(){
		global $mysqli;
		$data = isset($_POST['data'])?$_POST['data']:'';
		$idpaciente = isset($_POST['idpaciente'])?$_POST['idpaciente']:0;
		$contacto = isset($_POST['contacto'])?$_POST['contacto']:0;
		$tlf_contacto = isset($_POST['tlf_contacto'])?$_POST['tlf_contacto']:0;
		$idusuario = $_SESSION['user_id'];
		$respuesta = array();
		$idregistros = array();
		if(!empty($data)){
			if($idpaciente != 0){
				foreach($data as $row){


					$query = "INSERT INTO reposicion_medicamentos (idpaciente, idmedicamento,cantidad,fecha,fecha_estimada, fecha_prox_reposicion,contacto,telefono_contacto,idusuario,comentarios) VALUES ($idpaciente,'".$row['idmedicamento']."','".$row['cantidad']."','".$row['fecha_reposicion']."','".$row['fecha_estimada']."','".$row['proxima_reposicion']."','".$contacto."','".$tlf_contacto."','$idusuario','".$row['comentarios']."')";
					if(!$mysqli->query($query)){
						$respuesta = array(
							"error" => true,
							"message" => "Error al ejecutar consulta",
							"query" => $query,
							"mysqli_error" =>$mysqli->error 
						);
						if(!empty($idregistros)){
							$ids = implode($idregistros);
							$q = "DELETE FROM reposicion_medicamentos WHERE id IN($ids)";
							if($mysqli->query($q)){
								$respuesta = array(
									"error" => true,
									"message" => "Error al ejecutar consulta, no se almacenó registro",
									"query" => $q,
									"mysqli_error" =>$mysqli->error 
								);
							}else{
								$respuesta = array(
									"error" => true,
									"message" => "Error al ejecutar consulta, se almacenaron algunos registros ID: $ids",
									"query" => $q,
									"mysqli_error" =>$mysqli->error 
								);
							}
						}
					}else{
						$idregistros[] = $mysqli->insert_id;
					}
				}
			}else{
				$respuesta = array(
					"error" => true,
					"message" => "ID de paciente no fue recibido",					
				);
			}			
		}else{
			$respuesta = array(
				"error" => true,
				"message" => "arreglo de datos no fue recibido",					
			);
		}
		if(!isset($respuesta['error'])){
			$respuesta=array(
				"succes" => true
			);
		}
		echo json_encode($respuesta);
	}

	function historico_reposicion_medicamentos(){
		global $mysqli;
		$idpaciente = isset($_GET['idpaciente'])?$_GET['idpaciente']:0;
		$idmedicamento = isset($_GET['idmedicamento'])?$_GET['idmedicamento']:0;
		$respuesta =array();
		if($idpaciente != 0 && $idmedicamento != 0){
			$query = "SELECT rm.id as id, DATE(rm.fecha) as fecha_reposicion,DATE(rm.fecha_estimada) as fecha_estimada, rm.cantidad, DATE(rm.fecha_prox_reposicion) as proxima_reposicion, rm.contacto, rm.telefono_contacto, rm.comentarios, u.nombre as usuario FROM reposicion_medicamentos rm 
						INNER JOIN usuarios u ON u.id = rm.idusuario 
						WHERE rm.idmedicamento = '$idmedicamento' AND rm.idpaciente = '$idpaciente'";
			$result = $mysqli->query($query);
			while($row = $result->fetch_assoc()){
				$respuesta[] =array(
					"id" => $row["id"],
					"cantidad" => $row["cantidad"],
					"comentarios" => $row["comentarios"],
					"fecha_estimada"=> $row["fecha_estimada"],
					"fecha_reposicion" => $row["fecha_reposicion"],
					"proxima_reposicion" => $row["proxima_reposicion"],
					"contacto" => $row["contacto"],
					"telefono_contacto" => $row["telefono_contacto"],
					"usuario" =>$row["usuario"]
				);
			}
		}
		echo json_encode($respuesta);
	}
	
	
	
	function ver_tarjeta(){
		global $mysqli;
		$id = (!empty($_REQUEST['id']) ? $_REQUEST['id'] : 0);
		$query  = "SELECT t.id as id,
				p.nombre as paciente, 
				p.cedula as cedula, 
				p.fechanac as fechanac,
				h.nombre as hogar, 
				FLOOR(TIMESTAMPDIFF(DAY , p.fechanac, CURDATE() ) /365 ) AS edad,
				t.id,t.fecha, 
				GROUP_CONCAT(d.nombre) as diagnostico, 
				UPPER(t.estatus) as estado
				FROM tarjetamedicamento t
				INNER JOIN pacientes p ON p.id = t.idpaciente
				LEFT JOIN hogares h ON h.id = p.hogar
				LEFT JOIN enfermedades d ON FIND_IN_SET(d.id, t.diagnosticos)
				WHERE t.id = '$id'"; 
		$resultado =array();

			$q= "	SELECT m.nombre as medicamento, t.id, t.idtarjeta,t.idmedicamento,TIME_FORMAT(t.hora,'%I:%i %p') as hora,t.horario,t.via,
					t.frecuencia,t.dosis,t.fecha_inicio,t.fecha_fin,t.observaciones,t.estado, tmdh.*
					FROM tarjetamedicamentodetalle t 
					LEFT JOIN medicamentos m ON m.id = t.idmedicamento 
					LEFT JOIN tarjetamedicamentodetallehorarios tmdh ON tmdh.id_tarjetamedicamentodetalle = t.id 
					WHERE idtarjeta = '$id' ORDER BY t.idmedicamento";
								
			
		$r = $mysqli->query($query);
		$row = $r->fetch_assoc();
		$resultado['tarjeta'] = array(
				'id'=> $row['id'],
				'paciente'=> $row['paciente'], 
				'cedula'=> $row['cedula'], 
				'fechanac'=> $row['fechanac'],
				'hogar'=> $row['hogar'], 
				'edad'=> $row['edad'],
				'fecha'=> $row['fecha'], 
				'diagnostico'=> $row['diagnostico'], 
				'estado'=> $row['estado']
        );

		
        $r2 = $mysqli->query($q);
		while($row2 = $r2->fetch_assoc()){
				
			if ($row2['estado'] == 0)
				$estado = 'Espera de aprobación';
			if($row2['estado'] == 1)
				$estado = 'Aprobado';
			if ($row2['estado'] == 2)
				$estado = 'Rechazado';
			
			
			$horario = '';
			if( strtoupper($row2['horario']) == 'PRN'){
				$horario = 'PRN';
				$hora = '';
			}else{
				$horario = '';			
				$hora = $row2['hora'];
			}
			
            $resultado['detalle'][] = array(
				'id' => $row2['id'],
				'estado' => $estado,
                'medicamento' => $row2['medicamento'],
                'hora' => $hora,
                'horario' => $horario,
                'dosis' => $row2['dosis'],
                'via' => $row2['via'],
                'frecuencia' => $row2['frecuencia'] ,
                'fecha_inicio' => $row2['fecha_inicio'],
                'fecha_fin' => $row2['fecha_fin'],
                'observaciones' => $row2['observaciones'],
                'al_levantarse' => ($row2['al_levantarse'] == "") ? "Nada" : $row2['al_levantarse'],
                'desayuno' => ($row2['desayuno'] == "") ? "Nada" : $row2['desayuno'],
                'almuerzo' => ($row2['almuerzo'] == "") ? "Nada" : $row2['almuerzo'],
                'cena' => ($row2['cena'] == "") ? "Nada" : $row2['cena'],
                'al_acostarse' => ($row2['al_acostarse'] == "") ? "Nada" : $row2['al_acostarse']
            );
        }
				
		if( !empty($resultado) ){
		    echo json_encode($resultado);
		}else{
		   echo 0;
		}
		
                    
	}
	
	
	function ver_plan(){
		
		global $mysqli;
		$id = $_REQUEST['id'];
		$query  = "SELECT pa.id as idpaciente,
				pa.nombre as paciente, 
				pa.cedula as cedula, 
				pa.fechanac as fechanac,
				h.nombre as hogar, 
				FLOOR(TIMESTAMPDIFF(DAY , pa.fechanac, CURDATE() ) /365 ) AS edad,
				p.id,p.fecha, 
				GROUP_CONCAT(pe.idenfermedad) as diagnostico, 
				UPPER(p.estatus) as estado
				FROM plancuidado p
				INNER JOIN pacientes pa ON pa.id = p.idpaciente
				LEFT JOIN hogares h ON h.id = pa.hogar
				LEFT JOIN pacienteenfermedadesactuales pe ON pe.idpaciente = pa.id
				WHERE p.id = '$id'"; 
				
		$resultado =array();
		// // DETALLE
			$q= "SELECT TIME_FORMAT(hora,'%H:%i') as hora,horario, 
					tipocuidado, estrategiacuidado,frecuencia, fecha_inicio, fecha_fin 
					FROM plancuidadodetalle WHERE idplancuidado = $id AND estado = '1' order by horario,hora ASC";
		$r = $mysqli->query($query);
		$row = $r->fetch_assoc();
		$resultado['datos'] = array(
				'id'=> $row['idpaciente'],
				'paciente'=> $row['paciente'], 
				'cedula'=> $row['cedula'], 
				'fechanac'=> $row['fechanac'],
				'hogar'=> $row['hogar'], 
				'edad'=> $row['edad'],
				'fecha'=> $row['fecha'], 
				'diagnostico'=> $row['diagnostico'], 
				'estado'=> $row['estado']
        );
        $r2 = $mysqli->query($q);
		while($row2 = $r2->fetch_assoc()){
			$hora = date("g:i a",strtotime($row2['hora']));
			if( strtoupper($row2['frecuencia']) == 'PRN'){
				$hora_imprimir = '';
			
			}elseif($row2['frecuencia'] == 'completar en el día'){
				$hora_imprimir = '';
			}elseif($row2['frecuencia'] == 'Completar en el día'){
				$hora_imprimir = '';
			}else{
				$hora_imprimir = $hora;
			}
			
			$estado = 'Aprobado';
            $resultado['detalle'][] = array(
                'hora' => $hora_imprimir,
                'horario' => '',
                'tipocuidado' => $row2['tipocuidado'],
                'estrategiacuidado' => $row2['estrategiacuidado'],
                'frecuencia' => $row2['frecuencia'],
                'fecha_inicio' => $row2['fecha_inicio'],
                'fecha_fin' => $row2['fecha_fin']
            );
        }
		
		if( !empty($resultado) ){
		    echo json_encode($resultado);
		}else{
		   echo 0;
   
			  
						
		}
		
	}
	
	
	function get_nivel_riesgo($idpaciente){
		global $mysqli;
		$q="SELECT SUM(riesgo_a+riesgo_dx) as enfermedades 
					FROM (
						SELECT distinct e.codigo, 'antecedente' as tipo,pa.idenfermedad as id, IFNULL(ge.riesgo_antecedente,0) as riesgo_a, '0' as riesgo_dx
						FROM  pacienteenfermedadespasadas pa
						LEFT JOIN enfermedades e ON e.id = pa.idenfermedad
						LEFT JOIN grupos_enfermedades ge ON ge.id= e.grupo
						WHERE pa.idpaciente = $idpaciente AND e.codigo is not null
						UNION all
						SELECT distinct e2.codigo,'diagnostico' as tipo,pa.idenfermedad as id, '0' as riesgo_a, IFNULL(ge2.riesgo_diagnostico,0) as riesgo_dx
						FROM pacienteenfermedadesactuales pa 
						LEFT JOIN enfermedades e2 ON e2.id = pa.idenfermedad
						LEFT JOIN grupos_enfermedades ge2 ON ge2.id = e2.grupo				
						WHERE pa.idpaciente = $idpaciente AND e2.codigo is not null
					) as t";
		$r = $mysqli->query($q);
		$row = $r->fetch_assoc();
		$dx = $row['enfermedades'];
		
		if($dx <=2)
				$riesgo = "Nivel 1";
		if( $dx >=3){
				$riesgo = "Nivel 2";
		}
		if($dx >=5){
				$riesgo = "Nivel 3";
		}
		return $riesgo;
	}
	
	
	
?>