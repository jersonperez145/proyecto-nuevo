<?php
    include("../conexion.php");

	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){
		case "listado": 
			listado();
			break;
		case "comboproveedores": 
			comboproveedores();
			break;
		case "itemProveedor": 
			itemProveedor();
			break;
		case "presentacionItem": 
			presentacionItem();
			break;

		default:
			  echo "{failure:true}";
			  break;
	}

	function listado(){
		global $mysqli;
		
		$query 	= "	SELECT c.id,c.tipo,fecha,total,numerofactura, c.numero, p.empresa as proveedor, e.nombre as estado
					FROM compras c
					LEFT JOIN proveedores p ON p.id = c.idproveedor
					LEFT JOIN estados e ON e.id = c.estado";
		$result = $mysqli->query($query);
		
		while($row = $result->fetch_assoc()){
			$acciones = '<div class="dropdown ml-auto text-center">
						<button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
							<svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item boton-ver" data-id="'.$row['id'].'" href="#">Ver</a>
							<a class="dropdown-item boton-duplicar" data-id="'.$row['id'].'" data-numero="'.$row['numero'].'" href="#">Copiar</a>
							<a class="dropdown-item boton-imprimir" data-id="'.$row['id'].'" href="#">Imprimir</a>
							<a class="dropdown-item boton-imprimir" data-id="'.$row['id'].'" href="#">Aprobar</a>
							<a class="dropdown-item boton-imprimir" data-id="'.$row['id'].'" href="#">Anular</a>
							<a class="dropdown-item boton-eliminar disabled bg-light" data-id="'.$row['id'].'" data-numero="'.$row['numero'].'" href="#">Eliminar</a>
						</div>
					</div>';
			$i = 0;
			while($i < 1000){
				$resultado['data'][] = array(
					'id' 		=>	$row['id'],
					'acciones' 		=>	$acciones,
					'categoria' 	=>	($row['tipo'] == 'OC') ? 'Orden de compra' : 'Caja menuda',
					'numero' 		=>	$row['numero'].'-'.$i,
					'proveedor' 	=>	$row['proveedor'],
					'total' 		=>	'$ '.number_format(floatval($row['total']),2),
					'factura' 		=>	$row['numerofactura'],
					'fecha' 		=>	date('d/m/Y',strtotime($row['fecha'])),
					'estado' 		=>	$row['estado']
				);
				$i++;	
			}
			
		}
		
		echo json_encode($resultado);
		mysqli_close($mysqli);
	}	
	
	function comboproveedores(){
		
		global $mysqli; 
		
		if(isset($_REQUEST['onlydata'])){ $odata = $_REQUEST['onlydata']; }else{ $odata = ''; }
		
		$query  = " SELECT id, ruc,empresa 
					FROM proveedores
					WHERE 1=1 ";
					
		$result = $mysqli->query($query);
		
		$resultado = "";
		 
			$resultado .= "<option value=''> Proveedor </option>";
			
		while($row = $result->fetch_assoc()){
			
			$resultado .= "<option value='".$row['id']."'>".$row['ruc']." | ".$row['empresa']."</option>";
		}
		if( $resultado != ""){
			
			echo $resultado;
			
		} else {
			echo 0;
		}
		 mysqli_close($mysqli);
	}

	function itemProveedor(){
		global $mysqli; 
		
		if(isset($_REQUEST['onlydata'])){ $odata = $_REQUEST['onlydata']; }else{ $odata = ''; }
		//$tipo = $_REQUEST['tipoItem'];
		$idproveedor = $_REQUEST['idproveedor'] !='' ? $_REQUEST['idproveedor'] : '0';
		//$unidad_item= $_REQUEST['unidad_item'] !='' ? $_REQUEST['unidad_item'] : '1';
		// switch ($tipo) {
			// case 'I':
				// $item = 'insumos';
				// $campo = 'nombre';
				// break;
			// case 'E':
				// $item = 'equipos';
				// $campo = 'descripcion';
				// break;
			// case 'M':
				// $item = 'medicamentos';
				// $campo = 'nombre';
				// break;
			// default:
	
				// break;
		// }
		// $query  = " 	SELECT 		DISTINCT i.id as id, IFNULL(i.unidadcompra,'') as unidad, 
									// i.codigo as codigo, i.".$campo." as nombre,
									// i.costo as precio
						// FROM 		".$item." i ";
		
		// if($idproveedor != '0'){
			// $query .= " INNER JOIN 	item_proveedor ip ON i.id = ip.iditem
						// WHERE 		ip.tipo ='$tipo' AND ip.idproveedor = '$idproveedor' ";
		// }
		 $query  = " SELECT id,unidad,codigo,nombre,tipo,precio,itbms,nombredeventa,unidadcompra, unidadventa FROM (
		 
						SELECT 	DISTINCT i.id as id,i.itbms as itbms, IFNULL(i.presentacion,'Unidad') as unidad, i.unidadcompra as unidadcompra, i.unidadventa as unidadventa,
							i.codigo as codigo, i.nombredecompra as nombre,i.nombre as nombredeventa, 'I' as tipo,
						";
		if($idproveedor != '0'){				
			$query .=	"IFNULL(ip.costo,IFNULL((SELECT ip3.costo FROM item_proveedor ip3 WHERE ip3.iditem=i.id AND ip3.tipo='I' AND tipo_proveedor =1 limit 1),(IFNULL(CAST((SELECT AVG(ip2.costo) FROM item_proveedor ip2 WHERE ip2.iditem=i.id AND ip2.tipo='I') AS DECIMAL(10,2)),0)))) as precio
						FROM insumos i 
						LEFT JOIN item_proveedor ip ON i.id = ip.iditem and ip.tipo = 'I' AND ip.idproveedor = '$idproveedor'
						where i.activo = 1 
						";
		}else{
			$query .=	"i.costo as precio
						FROM insumos i where i.activo = 1 ";
		}
		$query .=	"UNION
						SELECT 	DISTINCT m.id as id,m.itbms as itbms,  IFNULL(m.unidadcompra,'Unidad') as unidad, m.unidadcompra as unidadcompra, m.unidadventa as unidadventa,
									 m.codigo as codigo, m.nombre as nombre, m.nombre as nombredeventa, 'M' as tipo,
						 ";
		if($idproveedor != '0'){								 
			$query .=	"IFNULL(ip.costo,IFNULL((SELECT ip3.costo FROM item_proveedor ip3 WHERE ip3.iditem=m.id AND ip3.tipo='M' AND tipo_proveedor =1 limit 1),(IFNULL(CAST((SELECT AVG(ip2.costo) FROM item_proveedor ip2 WHERE ip2.iditem=m.id AND ip2.tipo='M') AS DECIMAL(10,2)),0)))) as precio

						FROM 	medicamentos m  
						LEFT JOIN item_proveedor ip ON m.id = ip.iditem and ip.tipo = 'M'  AND ip.idproveedor = '$idproveedor'
						where m.activo = 1 ";
		}else{
					$query .="  m.costo as precio
								FROM medicamentos m where m.activo = 1 ";
		}
		$query .=	"UNION
						SELECT 	DISTINCT e.id as id,e.itbms as itbms, 'Unidad' as unidad,e.unidadcompra as unidadcompra, e.unidadventa as unidadventa,
							e.codigo as codigo, e.descripcion as nombre,e.descripcion as nombredeventa,'E' as tipo,
						
						 ";
		if($idproveedor != '0'){				
			$query .=	"IFNULL(ip.costo,IFNULL((SELECT ip3.costo FROM item_proveedor ip3 WHERE ip3.iditem=e.id AND ip3.tipo='E' AND tipo_proveedor =1 limit 1),(IFNULL(CAST((SELECT AVG(ip2.costo) FROM item_proveedor ip2 WHERE ip2.iditem=e.id AND ip2.tipo='E') AS DECIMAL(10,2)),0)))) as precio
							FROM equipos e	
							LEFT JOIN item_proveedor ip ON e.id = ip.iditem and ip.tipo = 'E'  AND ip.idproveedor = '$idproveedor'
							WHERE e.activo = 1 
							";
		}else{
			$query .=	"	e.costo as precio FROM equipos e WHERE e.activo = 1 ";
		}
		$query.= "	) as resultado ORDER BY precio desc";
		
		$result = $mysqli->query($query);
		
		$resultado = "";
		//die($query);
		$resultado .= "<option value=''></option>";
		while($row = $result->fetch_assoc()){
			if($row['tipo'] == 'M'){
				$nombre = $row['nombredeventa'];
			}else{
				$nombre = $row['nombre'];
			}

			$resultado .= "<option 
								data-tipo ='".strtoupper($row['tipo'])."' 
								value='".$row['id']."'>".$row['codigo']." | ".$nombre."</option>";
									// data-presentacion ='".strtoupper($row['presentacion'])."' 
		}
		if( $resultado != ""){
			
			echo $resultado;
			
		} else {
			echo 0;
		}
		 mysqli_close($mysqli);
	}


	function presentacionItem(){
		global $mysqli; 
		
		if(isset($_REQUEST['onlydata'])){ $odata = $_REQUEST['onlydata']; }else{ $odata = ''; }
		$idproveedor = $_REQUEST['idproveedor'] !='' ? $_REQUEST['idproveedor'] : '0';
		$iditem= $_REQUEST['iditem'] !='' ? $_REQUEST['iditem'] : '1';
		$tipo= $_REQUEST['tipo'] !='' ? $_REQUEST['tipo'] : '1';

	
		$query = "SELECT i.id,i.descripcion_presentacion, i.costo_presentacion as precio, i.cantidad_presentacion
		FROM proveedores_presentaciones i 
		WHERE i.id_item = '$iditem'  AND i.id_proveedor = '$idproveedor' AND i.tipo = '$tipo'";
		
		$result = $mysqli->query($query);
		
		$resultado = "";
		//die($query);
		$resultado .= "<option value=''></option>";
		while($row = $result->fetch_assoc()){
			$resultado .= "<option 
								data-precio='".$row['precio']."' 
								data-cantidad='".$row['cantidad_presentacion']."' 
								value='".$row['id']."'>".$row['descripcion_presentacion']." (".$row['cantidad_presentacion'].")</option>";
									// data-presentacion ='".strtoupper($row['presentacion'])."' 
		}
		if( $resultado != ""){
			
			echo $resultado;
			
		} else {
			echo 0;
		}
		 mysqli_close($mysqli);
	}



?>