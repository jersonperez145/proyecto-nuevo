<?php
    include("../conexion.php");

	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){
		case "cargarkits": 
			  cargarkits();
			  break;
		case "combokitinsumos": 
			  combokitinsumos();
			  break;
		case "getkit": 
			  getkit();
			  break;
		case "create": 
			  create();
			  break;
		case "updatekit": 
			  updatekit();
			  break;
		case "deletekit": 
			  deletekit();
			  break;
		case "updateitbmsjubilado": 
			  updateitbmsjubilado();
			  break;
		case "existecodigo":
			  existecodigo();
			  break;
		case "duplicar":
			  duplicar();
			  break;
		case "comboequipos":
			  comboequipos();
			  break;
		default:
			  echo "{failure:true}";
			  break;
	}	
	
	function cargarkits(){
		global $mysqli;
		
		$query 	= "	SELECT a.id, a.nombre, a.equipos, u.nombre as updated_by, a.updated_at
					FROM gruposdeequipos a
					LEFT JOIN usuarios u ON u.id = a.updated_by";
		$result = $mysqli->query($query);
		
		while($row = $result->fetch_assoc()){
			$query_equipos = "SELECT codigo, descripcion FROM equipos WHERE id in (".$row['equipos'].")";
			$re = $mysqli->query($query_equipos);
			$lista_equipos = array();
			while($eq = $re->fetch_assoc()){
				//print_r($row['id']);
				$lista_equipos[] = $eq['codigo'] . ' | ' .$eq['descripcion'];
			}

			$acciones = '<div class="dropdown ml-auto text-center">
						<button type="button" class="btn btn-success light sharp" data-toggle="dropdown" aria-expanded="false">
							<svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item boton-editar" data-id="'.$row['id'].'" href="#" data-toggle="modal" data-target="#editar-grupo-equipos">Editar</a>
							<a class="dropdown-item boton-duplicar" data-id="'.$row['id'].'" data-nombre="'.$row['nombre'].'" href="#">Copiar</a>
							<a class="dropdown-item boton-eliminar text-danger" data-id="'.$row['id'].'" data-nombre="'.$row['nombre'].'" href="#">Eliminar</a>
						</div>
					</div>';
			$resultado['data'][] = array(
				'id' 		=>	$row['id'],
				'acciones' 		=>	$acciones,
				'nombre' 		=>	$row['nombre'],
				'updated_at' 	=>	$row['updated_at'],
				'updated_by' 	=>	$row['updated_by'],
				'insumos' 	=>	$lista_equipos
			);
		}
		
		echo json_encode($resultado);
		mysqli_close($mysqli);
	}	
	
	function combokitinsumos(){
		global $mysqli;
		
		$query 	= "	SELECT CONCAT('G-',id) as id, CONCAT('GRUPO: ',nombre) as nombre FROM gruposdeequipos
					";
		$result = $mysqli->query($query);
		
		$combo = '';
		
		while($row = $result->fetch_assoc()){
			$combo .= '<option value="'.$row['id'].'">'.$row['nombre'].'</option>';
		}
		
		echo $combo;
		mysqli_close($mysqli);
	}	
	
	
	function getkit(){
		global $mysqli;
		
		$resultado = array();
		$resultadodetalle = array();
		$id = $_REQUEST['id'];
		$query 	= "	SELECT 		i.nombre, equi.codigo, equi.id, equi.descripcion as itemtxt
					FROM 		gruposdeequipos i
					LEFT JOIN 	equipos equi ON FIND_IN_SET(equi.id,i.equipos)
					WHERE 		i.id = '$id' ";
		$result = $mysqli->query($query);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$resultado['nombre'] = $row['nombre'];
				$resultado['equipos'][] = array(
					'id' => $row['id'],
					'nombre' => $row['codigo']. ' | ' .$row['itemtxt'],
				);
			}
			
		}
		
		
		echo json_encode($resultado);
		mysqli_close($mysqli);
	}	
	
	
	function deletekit(){
		global $mysqli;
		
		$id = $_REQUEST['id'];
		$query 	= "	DELETE
					FROM gruposdeequipos 
					WHERE id = '$id'";
		$result = $mysqli->query($query);
		
		echo 1;
		mysqli_close($mysqli);
	}
	
	function updatekit(){
		global $mysqli;
		
		$id 		= $_REQUEST['id'];
		$nombre 	= $_REQUEST['nombre'];
		$insumos 	= $_REQUEST['insumos'];
		$idequiposarray = array();
		$user_id = $_COOKIE['user_id'];
		
		foreach($insumos as $detalle){
			$idequiposarray[] = $detalle['itemid'];
		}
		
		$idequipos = implode(',',$idequiposarray);
		
		$query 	= "	UPDATE 	gruposdeequipos
					SET 	nombre = '$nombre',
							equipos = '$idequipos',
							updated_by = '$user_id',
							updated_at = NOW()
					WHERE 	id = '$id'";
		if($result = $mysqli->query($query)){
			echo 1;
		} else {
			die($mysqli->error);
		}
		mysqli_close($mysqli);
	}
	
	function create(){
		global $mysqli;
		
		$insumos 		= $_REQUEST['insumos'];
		$nombre 		= $_REQUEST['nombre'];
		$idequiposarray = array();
		$user_id = $_COOKIE['user_id'];
		
		foreach($insumos as $detalle){
			$idequiposarray[] = $detalle['itemid'];
		}
		
		$idequipos = implode(',',$idequiposarray);
		
		$query 	= "	INSERT INTO	gruposdeequipos (nombre, equipos,updated_by) VALUES ('$nombre','$idequipos','$user_id')";
		
		if($mysqli->query($query)){
			$idkit = $mysqli->insert_id;
			
			echo $idkit;
		} else {
			die($mysqli->error);
		}	
mysqli_close($mysqli);		
	}
	
	
	function duplicar(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$user_id = $_COOKIE['user_id'];
		
		$query = " 	INSERT INTO gruposdeequipos (nombre, equipos,updated_by)
					SELECT CONCAT(nombre,' - COPIA') as nombre,equipos,updated_by FROM gruposdeequipos WHERE id = $id
					LIMIT 1";
		if($result = $mysqli->query($query)){
			$idkit = $mysqli->insert_id;
			echo 1;
		}else {
			die($mysqli->error);
		}
		mysqli_close($mysqli);
	}

	function comboequipos() {
		global $mysqli;
		$combo = '';
		if(isset($_REQUEST['onlydata'])){ $odata = $_REQUEST['onlydata']; }else{ $odata = ''; }
		
		$query  = "SELECT 	DISTINCT m.id as id, 'E' as tipo, m.codigo as codigo, m.descripcion as nombre,  m.itbms as itbms,
							m.costounitario as costounitario, m.cuentacostoventa as cuenta,
							IFNULL((SUM(ci.total)-SUM(ci.comprometido)),0) as disponible,
							m.ubicacion
					FROM equipos m	
					LEFT JOIN cantidades_inventario ci ON ci.iditem = m.id AND ci.tipo='E'
					WHERE m.activo = 1 AND m.categoria = 'Equipo Medico'  
					GROUP BY ci.iditem,ci.tipo";
								   
				
		$resultado = "";		 
																	   
		$resultado .= "<option value=''></option>";	
		$result = $mysqli->query($query);
												   
   
		while($row = $result->fetch_assoc()){			
			$cantidad = $row['disponible'];
			$resultado .= "<option value='".$row['id']."' data-itbms='".$row['itbms']."' data-tipo='".$row['tipo']."' data-nombre='".$row['nombre']."' data-cuenta='".$row['cuenta']."' data-costounitario='".$row['costounitario']."' data-cantidad='".$cantidad."' data-ubicacion='".$row['ubicacion']."'>".$row['codigo']." | ".$row['nombre']." (".$cantidad.")</option>";
		}
		if( $resultado != ""){
			
			echo $resultado;
			
		} else {
			echo 0;
		}
		mysqli_close($mysqli);	  
	}

?>