<?php
include('config.php');
function autoVersiones(){
	echo '?v='.rand(1000, 9999);
}

function verificarLogin() {
	if($_SESSION["usuario"] != ''){
		return 1;
	}else{
		header('Location: index.php');
	}
}

function cargar_header(){
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
	echo '
		<!--*******************
			Preloader start
		********************-->
			<div id="preloader">
				<div class="sk-three-bounce">
					<div class="sk-child sk-bounce1"></div>
					<div class="sk-child sk-bounce2"></div>
					<div class="sk-child sk-bounce3"></div>
				</div>
			</div>
		<!--*******************
		Preloader end
		********************-->
	';
	echo '
		<!--**********************************
			Main wrapper start
		***********************************-->
		<div id="main-wrapper">';
	echo '
			<!--**********************************
				Nav header start
			***********************************-->
			<div class="nav-header">
				<a href="#top" class="brand-logo">
					<img class="logo-abbr" src="./images/logo.png" alt="">
					<img class="logo-compact" src="./images/logo-text.png" alt="">
					<img class="brand-title" src="./images/logo-text.png" alt="">
				</a>

				<div class="nav-control">
					<div class="hamburger">
						<span class="line"></span><span class="line"></span><span class="line"></span>
					</div>
				</div>
			</div>
			<!--**********************************
				Nav header end
			***********************************-->
		/*	<!--**********************************
				Chat box start
			***********************************-->
			<div class="chatbox">
				<div class="chatbox-close"></div>
				<div class="custom-tab-1">
					<ul class="nav nav-tabs">
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#notes">Notas</a>
						</li><!--
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#alerts">Alertas</a>
						</li>-->
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#chat">Chat</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active show" id="chat" role="tabpanel">
							<div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box">
								<div class="card-header">
									<div>
										<h6 class="mb-1">Lista de Chats</h6>
									</div>
								</div>
								<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body">
									<ul class="contacts" id="chat_usuarios">
									</ul>
								</div>
							</div>
							<div class="card chat dz-chat-history-box d-none" id="sala_chat">
								<div class="card-header chat-list-header text-center">
									<a href="javascript:;" class="dz-chat-history-back">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
									</a>
									<div>
										<h6 class="mb-1">Chat con <span id="chatcon"></span></h6>
										<p class="mb-0 text-success">Paciente</p>
									</div>							
									<div class="dropdown">
									<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
											<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
											<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
											<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
										</ul> -->
									</div>
								</div>
								<div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3">
									<div id="chat_enfermero"></div>
								</div>
								<div class="card-footer type_msg">
									<div class="input-group">
										<input type="hidden" class="form-control" name="idsala" id="idsala" autocomplete="off">
										<textarea class="form-control" id="body" placeholder="Escribir Mensaje..."></textarea>
										<div class="input-group-append">
											<button type="button" class="btn btn-primary" id="boton-enviar-mensaje"><i class="fa fa-location-arrow"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="notes">
							<div class="card mb-sm-3 mb-md-0 contacts_card dz-nota-user-box">
								<div class="card-header">
									<div>
										<h6 class="mb-1">Lista de Notas</h6>
									</div>
								</div>
								<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Notas_Body">
									<ul class="contacts" id="listado_notas">
									</ul>
								</div>
							</div>
							<div class="card chat dz-nota-history-box d-none" id="sala_nota">
								<div class="card-header chat-list-header text-center">
									<a href="javascript:;" class="dz-nota-history-back">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
									</a>
									<div>
										<h6 class="mb-1">Nota - <span id="notacon"></h6>
										<p class="mb-0 text-info">Paciente</p>
									</div>							
									<div class="dropdown">
									<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
											<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
											<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
											<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
										</ul> -->
									</div>
								</div>
								<div class="card-body msg_card_body dz-scroll" id="DZ_W_Notas_Body3">
									<div id="nota_detalle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--**********************************
				Chat box End
			***********************************-->

			<!--**********************************
				Header start
			***********************************-->
			<div class="header" name="top">
				<div class="header-content">
					<nav class="navbar navbar-expand">
						<div class="collapse navbar-collapse justify-content-between">
							<div class="header-left">
								<a  href="#top">
								<div class="dashboard_bar">
									Historial
								</div>
							</a>  
							</div>

							<ul class="navbar-nav header-right">
								<li class="nav-item dropdown notification_dropdown">
									<a class="nav-link ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
										<i class="fas fa-bell text-success"></i>
										<!--<span class="badge light text-white bg-primary" id="totalincidentes">0</span>-->
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
											<ul class="timeline" id="incidentesnotific">
												
											</ul>
										</div>
										<a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
									</div>
								</li>
								<li class="nav-item dropdown notification_dropdown">
									<a class="nav-link bell bell-link" href="javascript:;">
										<i class="fas fa-comments text-success"></i>
										<!--<span class="badge light text-white bg-primary">5</span>-->
									</a>
								</li>
								<li class="nav-item dropdown notification_dropdown">
									<a class="nav-link bell config-link" href="javascript:;">
										<i class="fas fa-cogs text-success"></i>
										<!--<span class="badge light text-white bg-primary">5</span>-->
									</a>
								</li>
								<li class="nav-item dropdown header-profile">
									<a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
										<!--<img src="images/logo.png" width="20" alt=""/>-->
										<div class="round-header">'.$inombre.'</div>
										<div class="header-info">
											<span><?php echo $nombre; ?></span>
										</div>
									</a>
									<div class="dropdown-menu dropdown-menu-right"><!--
										<a href="./app-profile.html" class="dropdown-item ai-icon">
											<svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
											<span class="ml-2">Profile </span>
										</a>
										<a href="./email-inbox.html" class="dropdown-item ai-icon">
											<svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
											<span class="ml-2">Inbox </span>
										</a>-->
										<a href="index.php" class="dropdown-item ai-icon">
											<svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
											<span class="ml-2">Cerrar Sesion </span>
										</a>
									</div>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
			<!--**********************************
				Header end ti-comment-alt
			***********************************-->';
	echo'
			<!--**********************************
				Sidebar start
			***********************************-->
			'.menu().'
			<!--**********************************
				Sidebar end
			***********************************-->';
	echo'</div>';
}

function menu() {
	echo ' 
		<div class="deznav">
            <div class="deznav-scroll">
				<ul class="metismenu" id="menu">
					<li>
						<a class="ai-icon mm-active" href="dashboard.php" aria-expanded="true">
							<i class="flaticon-381-radar"></i>
							<span class="nav-text">Dashboard</span>
						</a>
					</li>
					
					<li>
						<a class="ai-icon mm-active" href="kit_insumos.php" aria-expanded="true">
							<i class="flaticon-381-radar"></i>
							<span class="nav-text">Kit de insumos</span>
						</a>
					</li>
					
					<li>
						<a class="ai-icon mm-active" href="grupos-de-equipos.php" aria-expanded="true">
							<i class="flaticon-381-radar"></i>
							<span class="nav-text">Grupos de equipos</span>
						</a>
					</li>
					
					<li>
						<a class="ai-icon mm-active" href="gestion-inventario.php" aria-expanded="true">
							<i class="flaticon-381-radar"></i>
							<span class="nav-text">Gestión de inventario</span>
						</a>
					</li>
					
					<li>
						<a class="ai-icon mm-active" href="compras.php" aria-expanded="true">
							<i class="flaticon-381-radar"></i>
							<span class="nav-text">Compras</span>
						</a>
					</li>
					
					<li>
						<a class="ai-icon mm-active" href="http://homecare.vitae-health.com/" aria-expanded="true">
							<img src="images/logo-blanco.png" class="icon-vitae">
							<span class="nav-text">VITAE</span>
						</a>
					</li>
                </ul>
				<div class="copyright">
					<p class="fs-14 font-w200"><strong class="font-w400">Vitae Health</strong> © 2021 All Rights Reserved</p>
				</div>
			</div>
		</div>            
    ';
    
}

function linksFooter(){
	echo '<script src="js/funciones.js"></script>';
}

?>