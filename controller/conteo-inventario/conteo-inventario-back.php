<?php
    include("../conexion.php");
	//error_reporting(E_ALL); ini_set('display_errors', 1);
	
	
	
	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){
		case "cargar": 
			  cargar();
		break;
		case "cambiarcuenta":
			  cambiarcuenta();
		break;
		case "guardarConteo":
			  guardarConteo();
		break;
		case "conteos":
			  conteos();
		break;
		case "updateEstado":
			  updateEstado();
		break;
		case "insert_conteo_tmp":
			  insert_conteo_tmp();
		break;
		case "update_conteo_tmp":
			  update_conteo_tmp();
		break;	  
		case "delete_conteo_tmp":
			  delete_conteo_tmp();
		break;	  
		case "limpiar_conteo_tmp":
			  limpiar_conteo_tmp();
		break;	  
		default:
			  echo "{failure:true}";
		break;
	}	
	function cargar(){
		global $mysqli;
		$resultado = array();
		
		$idconteo = $_REQUEST['idconteo'];
		$fecha = $_REQUEST['fecha'];
		$bodega = $_REQUEST['bodega'];
		
					   
				  
   
		
		if($idconteo == '' && $fecha == ''){
			$resultado['data'][] = array(
				'id' 				=>	'',
				'check' 			=>	'',
				'acciones' 			=>	'',
				'codigo'			=>	'',
				'nombre' 			=>	'',
				'ubicacion' 		=>	'',
				'disponible' 		=>	'',
				'contado'	    	=>	'',
				'diferencia' 		=>	'',
				'costounitario' 	=>	'',
				'valorajuste' 		=>	'',
				'cuentacontable' 	=>	'',
				'estatus' 			=>	'',
				'tipo' 				=>	'',
				'codigof' 			=>	''
			);
		} else {
			$query ="SELECT DISTINCT 
                    ci.iditem as id, 
                    ci.tipo as tipo,
                    (CASE ci.tipo 
                        WHEN 'I' THEN ins.codigo 
                        WHEN 'M' THEN med.codigo
                        WHEN 'E' THEN equi.codigo
                    END) as codigo,
                    (CASE ci.tipo 
                        WHEN 'I' THEN ins.nombre 
                        WHEN 'M' THEN med.nombre
                        WHEN 'E' THEN equi.descripcion
                    END) as nombre,
                    (CASE ci.tipo 
                        WHEN 'I' THEN ins.ubicacion 
                        WHEN 'M' THEN med.ubicacion
                        WHEN 'E' THEN equi.ubicacion
                    END) as ubicacion,
                    (CASE ci.tipo 
                        WHEN 'I' THEN ins.costounitario 
                        WHEN 'M' THEN med.costounitario
                        WHEN 'E' THEN equi.costounitario
                    END) as costounitario,
                    (ci.total - ci.comprometido) as cantidad,
                    (CASE(CASE ci.tipo 
                                WHEN 'I' THEN ins.cuentacostodeventa 
                                WHEN 'M' THEN med.cuentacostoventa
                                WHEN 'E' THEN equi.cuentacostoventa
                            END)	 
                        WHEN 0 THEN cce.nombre WHEN '' THEN cce.nombre ELSE cc.nombre END) as cuentatxt,
                    (CASE (CASE ci.tipo 
                                WHEN 'I' THEN ins.cuentacostodeventa 
                                WHEN 'M' THEN med.cuentacostoventa
                                WHEN 'E' THEN equi.cuentacostoventa
                            END)	
                        WHEN 0 THEN cce.id WHEN '' THEN cce.id ELSE (
                            CASE ci.tipo 
                                WHEN 'I' THEN ins.cuentacostodeventa 
                                WHEN 'M' THEN med.cuentacostoventa
                                WHEN 'E' THEN equi.cuentacostoventa
                            END)	
                    END) as cuenta,
                    (CASE cd.registrado WHEN 0 THEN 'Por registrar' WHEN 1 THEN 'Registrado' ELSE '' END) as estatus,
					IFNULL(cd.conteo,'') as conteo

                FROM cantidades_inventario ci 
                LEFT JOIN insumos ins ON ins.id = ci.iditem AND ci.tipo = 'I' 
                LEFT JOIN equipos equi ON equi.id = ci.iditem AND ci.tipo = 'E' 
                LEFT JOIN medicamentos med ON med.id = ci.iditem AND ci.tipo = 'M'
                LEFT JOIN cuentascontables cc ON cc.id = 
                    (CASE ci.tipo 
                        WHEN 'I' THEN ins.cuentacostodeventa 
                        WHEN 'M' THEN med.cuentacostoventa
                        WHEN 'E' THEN equi.cuentacostoventa
                    END)					
                LEFT JOIN 		entradasdetalle ed ON ed.tipoproducto = ci.tipo AND ed.idproducto = ci.iditem
                LEFT JOIN 		entradas e ON e.id = ed.identrada 
                LEFT JOIN		cuentascontables cce ON cce.id = ed.cuenta
                LEFT JOIN		conteos c ON c.id = '$idconteo'
			    LEFT JOIN		conteodetalle cd ON cd.idconteo = c.id AND cd.tipo = ci.tipo and cd.iditem = ci.iditem
                WHERE  ci.inventario = '$bodega'  GROUP BY ci.iditem, ci.tipo
			";
				
			// if($paciente != ''){
				// $query.= " AND p.id ='$paciente' ";
			// }
			// if($pic != ''){
				// $query.= " AND ed.idpic ='$pic' ";
			// }
			//die($query);
			$result = $mysqli->query($query);

			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					$acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>";
					$acciones .= "</div>";
					
					$nombre = $row['nombre'];
					$nombre = str_replace('"','\"',$nombre);					
					$codigof = str_replace(',','_',$row['codigo']);
					$codigof = str_replace('+','_',$codigof);
					$codigof = trim($codigof);
					$cantidad  =$row['cantidad'];
					if($row['conteo'] != ''){
						//editando
						$resultado['data'][] = array(
							'id' 				=>	$row['id'],
							'check' 			=>	'',
							'acciones' 			=>	$acciones,
							'codigo'			=>	$row['codigo'],
							'nombre' 			=>	$row['nombre'],
							'ubicacion' 		=>	$row['ubicacion'],
							'disponible' 		=>	$row['cantidad'],
							'contado'	    	=>	'<input style="width: 70px;" value="'.$row['conteo'].'" data-registrado="'.$row['estatus'].'" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="contado" min="0" max="99999" />',
							'diferencia' 		=>	'',
							'costounitario' 	=>	'<input disabled="true" style="width: 80px;" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="costounitarioedit" value="'.$row['costounitario'].'" min="0" max="99999" />',
							'valorajuste' 		=>	'',
							'cuentacontable' 	=>	$row['cuentatxt'],
							'estatus' 			=>	$row['estatus'],
							'tipo' 				=>	$row['tipo'],
							'codigof' 			=>	$codigof
						);
					} else {
						// nuevo
						$resultado['data'][] = array(
							'id' 				=>	$row['id'],
							'check' 			=>	'',
							'acciones' 			=>	$acciones,
							'codigo'			=>	$row['codigo'],
							'nombre' 			=>	$row['nombre'],
							'ubicacion' 		=>	$row['ubicacion'],
							'disponible' 		=>	$row['cantidad'],
							'contado'	    	=>	'<input style="width: 70px;" data-nombre="'.$nombre.'" data-registrado="'.$row['estatus'].'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="contado" min="0" max="99999" />',
							'diferencia' 		=>	'',
							'costounitario' 	=>	'<input disabled="true" style="width: 80px;" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="costounitarioedit" value="'.$row['costounitario'].'" min="0" max="99999" />',
							'valorajuste' 		=>	'',
							'cuentacontable' 	=>	$row['cuentatxt'],
							'estatus' 			=>	$row['estatus'],
							'tipo' 				=>	$row['tipo'],
							'codigof' 			=>	$codigof
						);
					}
					
				}
			} else {
				// echo $query; die();
				$resultado['data'][] = array(
					'id' 				=>	'',
					'check' 			=>	'',
					'acciones' 			=>	'',
					'codigo'			=>	'',
					'nombre' 			=>	'',
					'ubicacion' 		=>	'',
					'disponible' 		=>	'',
					'contado'	    	=>	'',
					'diferencia' 		=>	'',
					'costounitario' 	=>	'',
					'valorajuste' 		=>	'',
					'cuentacontable' 	=>	'',
					'estatus' 			=>	'',
					'tipo' 				=>	'',
					'codigof' 			=>	''
				);

			}
		}
		echo json_encode($resultado);
		mysqli_close($mysqli);
	}	
	// function cargar(){
		// global $mysqli;
		// $resultado = array();
		
		// $idconteo = $_REQUEST['idconteo'];
		// $fecha = $_REQUEST['fecha'];
		// $bodega = $_REQUEST['bodega'];
		
					   
				  
   
		
		// if($idconteo == '' && $fecha == ''){
			// $resultado['data'][] = array(
				// 'id' 				=>	'',
				// 'check' 			=>	'',
				// 'acciones' 			=>	'',
				// 'codigo'			=>	'',
				// 'nombre' 			=>	'',
				// 'ubicacion' 		=>	'',
				// 'disponible' 		=>	'',
				// 'contado'	    	=>	'',
				// 'diferencia' 		=>	'',
				// 'costounitario' 	=>	'',
				// 'valorajuste' 		=>	'',
				// 'cuentacontable' 	=>	'',
				// 'estatus' 			=>	'',
				// 'tipo' 				=>	'',
				// 'codigof' 			=>	''
			// );
		// } else {
			// $query ="
			// SELECT DISTINCT		m.id as id, 'M' as tipo,
								// (CASE m.cuentacostoventa WHEN 0 THEN cce.nombre WHEN '' THEN cce.nombre ELSE cc.nombre END) as cuentatxt,
								// (CASE m.cuentacostoventa WHEN 0 THEN cce.id WHEN '' THEN cce.id ELSE m.cuentacostoventa END) as cuenta,
								// m.ubicacion,m.costounitario,
								// m.codigo as codigo, m.nombre as nombre,IFNULL(cd.conteo,'') as conteo,
								// (CASE cd.registrado WHEN 0 THEN 'Por registrar' WHEN 1 THEN 'Registrado' ELSE '' END) as estatus,
								
								// (
								   // SELECT IFNULL(SUM((CASE entradas.movimiento WHEN 'salida' THEN CONCAT('-',edet.cantidad) ELSE edet.cantidad END)),0)
								   // FROM medicamentos med 
								   // LEFT JOIN `entradasdetalle` edet ON edet.tipoproducto = 'M' AND edet.idproducto = med.id 
								   // LEFT JOIN entradas ON entradas.id = edet.identrada 
								   // WHERE med.id = m.id AND entradas.idorigen <> '$idconteo' AND entradas.fecha <= '$fecha'
								// ) as cantidad_disponible
								
				// FROM 			medicamentos m 
				// LEFT JOIN 		entradasdetalle ed ON ed.tipoproducto = 'M' AND ed.idproducto = m.id
				// LEFT JOIN 		entradas e ON e.id = ed.identrada 
				// LEFT JOIN		cuentascontables cc ON cc.id = m.cuentacostoventa
				// LEFT JOIN		cuentascontables cce ON cce.id = ed.cuenta

				// LEFT JOIN		conteos c ON c.id = '$idconteo'
				// LEFT JOIN		conteodetalle cd ON cd.idconteo = c.id AND cd.tipo = 'M' and cd.iditem = m.id
				// GROUP BY 		m.id  
				
			// UNION ALL
			
			// SELECT DISTINCT	m.id as id, 'I' as tipo,
							// (CASE m.cuentacostodeventa WHEN 0 THEN cce.nombre WHEN '' THEN cce.nombre ELSE cc.nombre END) as cuentatxt,
							// (CASE m.cuentacostodeventa WHEN 0 THEN cce.id WHEN '' THEN cce.id ELSE m.cuentacostodeventa END) as cuenta,
							// m.ubicacion,m.costounitario,
							// m.codigo as codigo, m.nombre as nombre, IFNULL(cd.conteo,'') as conteo,
							// (CASE cd.registrado WHEN 0 THEN 'Por registrar' WHEN 1 THEN 'Registrado' ELSE '' END) as estatus,
							// (
							   // SELECT IFNULL(SUM((CASE entradas.movimiento WHEN 'salida' THEN CONCAT('-',edet.cantidad) ELSE edet.cantidad END)),0)
							   // FROM insumos med 
							   // LEFT JOIN `entradasdetalle` edet ON edet.tipoproducto = 'I' AND edet.idproducto = med.id 
							   // LEFT JOIN entradas ON entradas.id = edet.identrada 	
							   // WHERE med.id = m.id AND entradas.idorigen <> '$idconteo' AND entradas.fecha <= '$fecha'
							 
							// ) as cantidad_disponible
			// FROM 		insumos m 
			// LEFT JOIN 	entradasdetalle ed ON ed.tipoproducto = 'I' AND ed.idproducto = m.id
			// LEFT JOIN 	entradas e ON e.id = ed.identrada
			// LEFT JOIN	cuentascontables cc ON cc.id = m.cuentacostodeventa
			// LEFT JOIN	cuentascontables cce ON cce.id = ed.cuenta

			// LEFT JOIN		conteos c ON c.id = '$idconteo'
			// LEFT JOIN		conteodetalle cd ON cd.idconteo = c.id AND cd.tipo = 'I' and cd.iditem = m.id
			// GROUP BY 	m.id  
						
			// UNION ALL
			
			// SELECT DISTINCT	m.id as id, 'E' as tipo,
							// (CASE m.cuentacostoventa WHEN 0 THEN cce.nombre WHEN '' THEN cce.nombre ELSE cc.nombre END) as cuentatxt,
							// (CASE m.cuentacostoventa WHEN 0 THEN cce.id WHEN '' THEN cce.id ELSE m.cuentacostoventa END) as cuenta,
							// m.ubicacion,m.costounitario,
							// m.codigo as codigo, m.descripcion as nombre,IFNULL(cd.conteo,'') as conteo,
							// (CASE cd.registrado WHEN 0 THEN 'Por registrar' WHEN 1 THEN 'Registrado' ELSE '' END) as estatus,
							// (
							   // SELECT IFNULL(SUM((CASE entradas.movimiento WHEN 'salida' THEN CONCAT('-',edet.cantidad) ELSE edet.cantidad END)),0)
							   // FROM equipos med 
							   // LEFT JOIN `entradasdetalle` edet ON edet.tipoproducto = 'E' AND edet.idproducto = med.id 
							   // LEFT JOIN entradas ON entradas.id = edet.identrada 
							   // WHERE med.id = m.id AND entradas.idorigen <> '$idconteo' AND entradas.fecha <= '$fecha'
							// ) as cantidad_disponible
   
			// FROM 			equipos m 
			// LEFT JOIN 		entradasdetalle ed ON ed.tipoproducto = 'E' AND ed.idproducto = m.id
			// LEFT JOIN 		entradas e ON e.id = ed.identrada 
			// LEFT JOIN		cuentascontables cc ON cc.id = m.cuentacostoventa
			// LEFT JOIN		cuentascontables cce ON cce.id = ed.cuenta
			// LEFT JOIN		conteos c ON c.id = '$idconteo'
			// LEFT JOIN		conteodetalle cd ON cd.idconteo = c.id AND cd.tipo = 'E' and cd.iditem = m.id
			// GROUP BY 		m.id
			
			// ";
				
			// // if($paciente != ''){
				// // $query.= " AND p.id ='$paciente' ";
			// // }
			// // if($pic != ''){
				// // $query.= " AND ed.idpic ='$pic' ";
			// // }
			// //die($query);
			// $result = $mysqli->query($query);

			// if($result->num_rows > 0){
				// while($row = $result->fetch_assoc()){
					// $acciones = "<div style='float:left;margin-left:0px;' class='ui-pg-div ui-inline-custom'>";
					// $acciones .= "</div>";
					
					// // CALCULAR COMPROMETIDO
					// $id = $row['id'];
					// $tipo = substr($row['tipo'],0,1);
					
					// $qc = "
					// SELECT (
					// IFNULL((SELECT SUM(ad.cantidad)
								// FROM acuses a
								// INNER JOIN acuses_detalle ad ON ad.idacuse = a.id
								// WHERE a.estatus AND ad.tipo = '$tipo' AND ad.iditem = '$id' AND a.estatus NOT IN (48,49))
								// ,0)
								
								// + 
								// IFNULL((SELECT sum(cd.conteo)
								// FROM cierres c
								// INNER JOIN cierredetalle cd ON cd.idcierre = c.id
								// AND cd.tipo = '$tipo' AND cd.iditem = '$id'
								// WHERE 	c.id IN (	SELECT MAX(c1.id) 
													// FROM cierres c1 
													// INNER JOIN cierredetalle cd1 ON cd1.idcierre = c1.id AND cd1.tipo = '$tipo' AND cd1.iditem = '$id'
													// INNER JOIN acuses a1 ON FIND_IN_SET(a1.id,c1.acuses) AND a1.estatus = 48
													// GROUP BY c1.idpaciente
												// ) 
								// )
								// ,0)
						// ) as comprometido
					
					// ";
					
					// $rc = $mysqli->query($qc);
					// $fc = $rc->fetch_assoc();
					// $comprometido = $fc['comprometido'];
					
					
					
					// $nombre = $row['nombre'];
					// $nombre = str_replace('"','\"',$nombre);
					// $cantidad = intval($row['cantidad_disponible']) - intval($comprometido);
					// $codigof = str_replace(',','_',$row['codigo']);
					// $codigof = str_replace('+','_',$codigof);
					// $codigof = trim($codigof);
					
					// if($row['conteo'] != ''){
						// //editando
						// $resultado['data'][] = array(
							// 'id' 				=>	$row['id'],
							// 'check' 			=>	'',
							// 'acciones' 			=>	$acciones,
							// 'codigo'			=>	$row['codigo'],
							// 'nombre' 			=>	$row['nombre'],
							// 'ubicacion' 		=>	$row['ubicacion'],
							// 'disponible' 		=>	$cantidad,
							// 'contado'	    	=>	'<input style="width: 70px;" value="'.$row['conteo'].'" data-registrado="'.$row['estatus'].'" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="contado" min="0" max="99999" />',
							// 'diferencia' 		=>	'',
							// 'costounitario' 	=>	'<input disabled="true" style="width: 80px;" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="costounitarioedit" value="'.$row['costounitario'].'" min="0" max="99999" />',
							// 'valorajuste' 		=>	'',
							// 'cuentacontable' 	=>	$row['cuentatxt'],
							// 'estatus' 			=>	$row['estatus'],
							// 'tipo' 				=>	$row['tipo'],
							// 'codigof' 			=>	$codigof
						// );
					// } else {
						// // nuevo
						// $resultado['data'][] = array(
							// 'id' 				=>	$row['id'],
							// 'check' 			=>	'',
							// 'acciones' 			=>	$acciones,
							// 'codigo'			=>	$row['codigo'],
							// 'nombre' 			=>	$row['nombre'],
							// 'ubicacion' 		=>	$row['ubicacion'],
							// 'disponible' 		=>	$cantidad,
							// 'contado'	    	=>	'<input style="width: 70px;" data-nombre="'.$nombre.'" data-registrado="'.$row['estatus'].'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="contado" min="0" max="99999" />',
							// 'diferencia' 		=>	'',
							// 'costounitario' 	=>	'<input disabled="true" style="width: 80px;" data-nombre="'.$nombre.'" data-tipo="'.$row['tipo'].'" data-cuenta="'.$row['cuenta'].'" data-cuentatxt="'.$row['cuentatxt'].'" data-disponible="'.$cantidad.'" data-id="'.$row['id'].'" data-codigo="'.$codigof.'" data-costo="'.$row['costounitario'].'" type="number" class="costounitarioedit" value="'.$row['costounitario'].'" min="0" max="99999" />',
							// 'valorajuste' 		=>	'',
							// 'cuentacontable' 	=>	$row['cuentatxt'],
							// 'estatus' 			=>	$row['estatus'],
							// 'tipo' 				=>	$row['tipo'],
							// 'codigof' 			=>	$codigof
						// );
					// }
					
				// }
			// } else {
				// // echo $query; die();
				// $resultado['data'][] = array(
					// 'id' 				=>	'',
					// 'check' 			=>	'',
					// 'acciones' 			=>	'',
					// 'codigo'			=>	'',
					// 'nombre' 			=>	'',
					// 'ubicacion' 		=>	'',
					// 'disponible' 		=>	'',
					// 'contado'	    	=>	'',
					// 'diferencia' 		=>	'',
					// 'costounitario' 	=>	'',
					// 'valorajuste' 		=>	'',
					// 'cuentacontable' 	=>	'',
					// 'estatus' 			=>	'',
					// 'tipo' 				=>	'',
					// 'codigof' 			=>	''
				// );

			// }
		// }
		// echo json_encode($resultado);
	// }	
	
	function cambiarcuenta(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$tipo = $_REQUEST['tipo'];
		$cuenta = $_REQUEST['cuenta'];
		if($tipo == 'Acuse')
			$tabla = 'acuses_detalle';
		else
			$tabla = 'entradasdetalle';
		$query = "UPDATE $tabla SET cuenta = '$cuenta' WHERE id = '$id';"; 
		if($mysqli->query($query)){
			echo 1;
		}else{
			echo 0;
		}
		mysqli_close($mysqli);
	}

	function guardarConteo(){
		global $mysqli;
		$id = $_REQUEST['id'];
		
																		  
																	
		$fecha = $_REQUEST['fecha'];
		$nombre = $_REQUEST['nombre'];
		$bodega = $_REQUEST['bodega'];
		$idusuario = isset($_REQUEST['idusuario']) ? $_REQUEST['idusuario'] : $_SESSION['user_id'];
		$hacerEntrada = 0;
		$hacerSalida = 0;
		$movimientos = array();
		$qdc = 'INSERT INTO conteodetalle (idconteo,tipo,iditem,disponible,conteo,costo,movimiento) VALUES ';
		$qdch = 'INSERT INTO conteodetalle_historico (nombreconteo,tipo,iditem,disponible,conteo,costo,movimiento,fechahora) VALUES ';
		
		$usuario = $_SESSION['user_id'];
		
		$consulta = "SELECT * FROM conteo_tmp WHERE idconteo = $id AND inventario = $bodega AND idusuario = $usuario";
		$rc = $mysqli->query($consulta);
		$entradas =array();
		$salidas = array();
		$ceros =array();
		while($r = $rc->fetch_assoc()){
			switch($r['movimiento']){
				case 'entrada':
					$entradas[] = array(
						"id"=>$r['iditem'],
						"codigo"=>$r['codigo'],
						"tipo"=>$r['tipo'],
						"nombre"=>$r['nombre'],
						"cantidad"=>$r['cantidad'],
						"costounitario"=>$r['costounitario'],
						"cuenta"=>$r['cuenta'],
						"cuentatxt"=>$r['cuentatxt'],
						"conteo"=>$r['conteo'],
						"registrado"=>$r['registrado'],
						"disponible"=>$r['disponible'],
						"movimiento"=>$r['movimiento'],
					);
				break;
				case 'salida':
					$salidas[] = array(
						"id"=>$r['iditem'],
						"codigo"=>$r['codigo'],
						"tipo"=>$r['tipo'],
						"nombre"=>$r['nombre'],
						"cantidad"=>$r['cantidad'],
						"costounitario"=>$r['costounitario'],
						"cuenta"=>$r['cuenta'],
						"cuentatxt"=>$r['cuentatxt'],
						"conteo"=>$r['conteo'],
						"registrado"=>$r['registrado'],
						"disponible"=>$r['disponible'],
						"movimiento"=>$r['movimiento'],
					);
				break;
				case 'cero':
					$ceros[] = array(
						"id"=>$r['iditem'],
						"codigo"=>$r['codigo'],
						"tipo"=>$r['tipo'],
						"nombre"=>$r['nombre'],
						"cantidad"=>$r['cantidad'],
						"costounitario"=>$r['costounitario'],
						"cuenta"=>$r['cuenta'],
						"cuentatxt"=>$r['cuentatxt'],
						"conteo"=>$r['conteo'],
						"registrado"=>$r['registrado'],
						"disponible"=>$r['disponible'],
						"movimiento"=>$r['movimiento'],
					);
				break;
			}
		}
		
		if($id != '-1'){
			$q = "DELETE FROM conteos WHERE id = $id";
			$mysqli->query($q);
			$q1 = "DELETE FROM conteodetalle WHERE idconteo = $id";
			$mysqli->query($q1);
			$q2 = "DELETE FROM entradasdetalle WHERE identrada IN (SELECT id FROM entradas WHERE idorigen = $id)";
			$mysqli->query($q2);
			$q3 = "DELETE FROM entradas WHERE idorigen = $id";
			$mysqli->query($q3);
		}
		
		// ENTRADAS
		if(!empty($entradas)){
								 
			$hacerEntrada = 1;
	
		}
		if(!empty($salidas)){
			$hacerSalida = 1;
		}

		if($hacerEntrada == 1){
			// GUARDAR CONTEO
			$qe = "INSERT INTO conteos (idusuario,fecha,nombre,bodega)  VALUES ($idusuario,'$fecha','$nombre','$bodega') ";
			if($mysqli->query($qe)){
				$idorigen = $mysqli->insert_id;
			}
			
			
			// GUARDAR ENTRADA
			$qe = "INSERT INTO entradas (idorigen,idproveedor,fecha,usuario,idpic,idpaciente,estado,tipo,movimiento)  VALUES ($idorigen,0,'$fecha',$idusuario,0,0,34,'ajuste por conteo','entrada')";
			if($mysqli->query($qe)){
				$identrada = $mysqli->insert_id;

				$qde = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,inventario,costo,costounitario,costounitarioactual,cuenta,idpic) VALUES ";
				foreach($entradas as $entrada){
					if($entrada['id'] != ''){
						$id = $entrada['id'];						
						$tipo = $entrada['tipo'];
						$costounitario = $entrada['costounitario'];
						$cuenta = $entrada['cuenta'];
						$cantidad = $entrada['cantidad'];
						$conteo = $entrada['conteo'];
						$disponible_ = $entrada['disponible'];
						
						// CONTEO DETALLE
						$qdc .= "('$idorigen','$tipo','$id','$disponible_','$conteo','$costounitario','entrada'),";
						$qdch .= "('$nombre','$tipo','$id','$disponible_','$conteo','$costounitario','entrada',now()),";
						
						// CALCULO DE COSTO UNITARIO
						switch($entrada['tipo']){
							case 'I':
								$tabla = 'insumos';
							break;
							case 'M':
								$tabla = 'medicamentos';
							break;
							case 'E':
								$tabla = 'equipos';
							break;
						}
						// $qcantidaddisponible = "SELECT 
						// 							IFNULL(SUM((CASE entradas.movimiento WHEN 'salida' THEN CONCAT('-',edet.cantidad) ELSE edet.cantidad END)),0) as disponible
						// 							,med.costounitario
						// 						FROM $tabla med 
						// 						LEFT JOIN `entradasdetalle` edet ON edet.tipoproducto = 'I' AND edet.idproducto = ".$entrada['id']." 
						// 						LEFT JOIN entradas ON entradas.id = edet.identrada WHERE med.id = ".$entrada['id']." ";
						$qcantidaddisponible ="SELECT ci.total as disponible, t.costounitario FROM 
												cantidades_inventario ci 
												inner join $tabla t ON t.id = ci.iditem 
												WHERE ci.iditem = '".$entrada['id']."' AND ci.tipo = '".$entrada['tipo']."'
						";
						if($r_ = $mysqli->query($qcantidaddisponible)){
							// CALCULAR COSTO UNITARIO NUEVO
							$fetch = $r_->fetch_assoc();
							$disponible = intval($fetch['disponible']);
							$costounitarioactual = floatval($fetch['costounitario']);
							$costounitarionuevo =$costounitarioactual ;
							$costounitariodocumento = floatval($entrada['costounitario']);
							$ingreso = intval($entrada['cantidad']);
							
							
							
							if( ($disponible + $ingreso) != 0){
								$costounitarionuevo = ($disponible / ($disponible + $ingreso)) * ( $costounitarioactual );
								$costounitarionuevo += ($ingreso / ($disponible + $ingreso)) * ($costounitariodocumento);
								
								$print = array(
									'disponible' 	=> $disponible,
									'ingreso'		=> $ingreso,
									'costounitarioactual' => $costounitarioactual,
									'costounitariodocumento' => $costounitariodocumento
								);
								
								//print_r($print);
								//die();
								
								// ACTUALIZAR COSTO UNITARIO NUEVO
								$q = "UPDATE $tabla SET costounitario = $costounitarionuevo WHERE id = ".$entrada['id']." ";							
		   
								$mysqli->query($q);
								// ACTUALIZAR CANTIDADES DE INVENTARIO
								$qi = "UPDATE cantidades_inventario SET total = total + ".$entrada['cantidad']." 
											WHERE 
												iditem = '".$entrada['id']."'  AND
												tipo = '".$entrada['tipo']."'  AND
												inventario = '".$bodega."'
									";							
								if(!$mysqli->query($qi))
									die($qi);
							} else {
								//echo $entrada['tipo'].'-'.$entrada['id'].': '.$disponible.' / ( '.$disponible.' + '.$ingreso. ') * '.$costounitarioactual.'<br/>';
							}
						} // FIN CALCULO COSTO UNITARIO
		 
		 
		 
		 
						// ENTRADA DETALLE
						$qde .= "($identrada,'".$tipo."',".$id.",".$cantidad.",'$bodega','0','".$costounitario."','".$costounitarionuevo."','".$cuenta."',''),";
					}		
				}
				
				// EJECUTAR QUERY
				$qde = substr($qde,0,-1);
				if($mysqli->query($qde)){
					$movimientos['entrada'] = $identrada;
				} else {
					die('Detalle entrada: '.$qde);
				}
			} else {
				die($mysqli->error);
			}
			
		} // FIN ENTRADA
		
  
  
							   
								
					 
	
   
  
  
  
		if($hacerSalida == 1){
			if($hacerEntrada == 0){
				// GUARDAR CONTEO
				$qe = "INSERT INTO conteos (idusuario,fecha,nombre,bodega)  VALUES ($idusuario,'$fecha','$nombre','$bodega') ";
				if($mysqli->query($qe)){
					$idorigen = $mysqli->insert_id;
				}
			}
			// GUARDAR SALIDA
			$qe = "INSERT INTO entradas (idorigen,idproveedor,fecha,usuario,idpic,idpaciente,estado,tipo,movimiento)  VALUES ($idorigen,0,'$fecha',$idusuario,0,0,34,'ajuste por conteo','salida')";
			if($mysqli->query($qe)){
				$identrada = $mysqli->insert_id;
				

				$qde = "INSERT INTO entradasdetalle (identrada,tipoproducto,idproducto,cantidad,inventario,costo,costounitario,costounitarioactual,cuenta,idpic) VALUES ";
				foreach($salidas as $salida){
							 
					$id = $salida['id'];
					$tipo = $salida['tipo'];
					$costounitario = $salida['costounitario'];
					$cuenta = $salida['cuenta'];
					$cantidad = $salida['cantidad'];
					$conteo = $salida['conteo'];
					$disponible_ = $salida['disponible'];
					
					// CONTEO DETALLE
					$qdc .= "('$idorigen','$tipo','$id','$disponible_','$conteo','$costounitario','salida'),";
					$qdch .= "('$nombre','$tipo','$id','$disponible_','$conteo','$costounitario','salida',now()),";
					
					// SALIDA DETALLE
					$qde .= "($identrada,'".$tipo."',".$id.",".$cantidad.",'$bodega','0','".$costounitario."','".$costounitario."','".$cuenta."',''),";
					// ACTUALIZAR CANTIDADES DE INVENTARIO
					$qi = "UPDATE cantidades_inventario SET total = total - ".$salida['cantidad']." 
								WHERE 
									iditem = '".$salida['id']."'  AND
									tipo = '".$salida['tipo']."'  AND
									inventario = '".$bodega."'
						";							
					if(!$mysqli->query($qi))
						die($qi);
	  
				}
				
				// EJECUTAR QUERY
				$qde = substr($qde,0,-1);
				if($mysqli->query($qde)){
					$movimientos['salida'] = $identrada;
				} else {
					die('Detalle salida: '.$qde);
				}
			} else {
				die($mysqli->error);
			}
			
		} // FIN SALIDA
		
		// GUARDAR CEROS
		if($hacerEntrada == 0 && $hacerSalida == 0){
			// GUARDAR CONTEO
			$qe = "INSERT INTO conteos (idusuario,fecha)  VALUES ($idusuario,'$fecha') ";
			if($mysqli->query($qe)){
				$idorigen = $mysqli->insert_id;
			}
		}
		foreach($ceros as $cero){			
			if(isset($cero['codigo'])){
				$id = $cero['id'];
				$tipo = $cero['tipo'];
				$conteo = $cero['conteo'];
				$disponible_ = $cero['disponible'];
				
				// CONTEO DETALLE
				$qdc .= "('$idorigen','$tipo','$id','$disponible_','$conteo','$costounitario','ninguno'),";
				$qdch .= "('$nombre','$tipo','$id','$disponible_','$conteo','$costounitario','ninguno',now()),";
			}
		}
  
		// GUARDAR DETALLES CONTEO
		$qdc = substr($qdc,0,-1);
		if($mysqli->query($qdc)){
  
			// HISTORICO
			$qdch = substr($qdch,0,-1);
			if($mysqli->query($qdch)){
  
				limpiar_conteo_tmp();
  
				echo json_encode($movimientos);
			} 
		}else{
			die($qdc);
		}
		mysqli_close($mysqli);
	}
	
	function conteos(){
		global $mysqli;
		
		$q = "SELECT id,fecha,nombre,bodega FROM conteos WHERE 1 ";
		$res = '';
		$r = $mysqli->query($q);
		while($f = $r->fetch_assoc()){
			$res .= '<option value="'.$f['id'].'" data-fecha="'.$f['fecha'].'" data-bodega="'.$f['bodega'].'" data-nombre="'.$f['nombre'].'">'.$f['fecha'].' | '.$f['nombre'].'</option>';
		}
		
		echo $res;
		mysqli_close($mysqli);
	}
	
	function updateEstado(){
		global $mysqli;
		
		$filas = $_REQUEST['filas'];
		$estado = $_REQUEST['estado'];
		$idconteo = $_REQUEST['idconteo'];
		
		foreach ($filas as $fila){
			$id = $fila['id'];
			$tipo = $fila['tipo'];
			$q = "UPDATE conteodetalle SET registrado = '$estado' WHERE idconteo = '$idconteo' AND iditem = '$id' AND tipo = '$tipo'";
			if( !($mysqli->query($q)) ){
				die($mysqli->error);
			}
		}
		
		echo 1;
		mysqli_close($mysqli);
	}

	function insert_conteo_tmp(){
		global $mysqli;
		$idconteo = $_REQUEST['idconteo'];
		$inventario = $_REQUEST['inventario'];
		$datos = $_REQUEST['datos'][0];
		$id= $datos['id'];
		$codigo= $datos['codigo'];
		$tipo= $datos['tipo'];
		$nombre= $datos['nombre'];
		$cantidad= $datos['cantidad'];
		$costounitario= $datos['costounitario'];
		$cuenta= $datos['cuenta'];
		$cuentatxt= $datos['cuentatxt'];
		$conteo= $datos['conteo'];
		$registrado= $datos['registrado'];
		if($registrado =='') $registrado=0;
		$disponible= $datos['disponible'];
		$movimiento= $datos['movimiento'];
		$query = "INSERT INTO conteo_tmp (
											idconteo, 
											inventario,
											idusuario, 
											iditem, 
											codigo, 
											tipo,
											nombre,
											cantidad,
											costounitario,
											cuenta,
											cuentatxt,
											conteo,
											registrado,
											disponible,
											movimiento
										) VALUES (
											'".$idconteo."',
											'".$inventario."',
											'".$_SESSION['user_id']."',
											'".$id."',
											'".$codigo."',
											'".$tipo."',
											'".$nombre."',
											'".$cantidad."',
											'".$costounitario."',
											'".$cuenta."',
											'".$cuentatxt."',
											'".$conteo."',
											'".$registrado."',
											'".$disponible."',
											'".$movimiento."'
										)
		";
		if($mysqli->query($query)){
			echo 1;
		}else{
			echo $query.'<br>';
			die($mysqli->error);
		}
		mysqli_close($mysqli);
	}

	function update_conteo_tmp(){
		global $mysqli;
		$idconteo = $_REQUEST['idconteo'];
		$inventario = $_REQUEST['inventario'];
		$datos = $_REQUEST['datos'];
		$id= $datos['id'];
		$codigo= $datos['codigo'];
		$tipo= $datos['tipo'];
		$nombre= $datos['nombre'];
		$cantidad= $datos['cantidad'];
		$costounitario= $datos['costounitario'];
		$cuenta= $datos['cuenta'];
		$cuentatxt= $datos['cuentatxt'];
		$conteo= $datos['conteo'];
		$registrado= $datos['registrado'];
		if($registrado =='') $registrado=0;
		$disponible= $datos['disponible'];
		$query = "UPDATE conteo_tmp SET
					idconteo= '".$idconteo."',					 
					inventario= '".$inventario."',
					idusuario= '".$_SESSION['user_id']."', 
					iditem= '".$id."', 
					codigo= '".$codigo."', 
					tipo= '".$tipo."',
					nombre= '".$nombre."',
					cantidad= '".$cantidad."',
					costounitario= '".$costounitario."',
					cuenta= '".$cuenta."',
					cuentatxt= '".$cuentatxt."',
					conteo= '".$conteo."',
					registrado= '".$registrado."',
					disponible= '".$disponible."'
				WHERE 
					idconteo= '".$idconteo."' AND 
					inventario= '".$inventario."' AND
					idusuario= '".$_SESSION['user_id']."' AND 
					iditem= '".$id."' AND 
					tipo= '".$tipo."'
		";
		if($mysqli->query($query)){
			echo 1;
		}else{
			die($mysqli->error);
		}
		mysqli_close($mysqli);
	}
	function delete_conteo_tmp(){
		global $mysqli;
		$idconteo = $_REQUEST['idconteo'];
		$inventario = $_REQUEST['inventario'];
		$datos = $_REQUEST['datos'][0];
		$id= $datos['id'];		
		$tipo= $datos['tipo'];

		$query ="DELETE FROM conteo_tmp WHERE 
				idconteo ='".$idconteo."' AND 
				inventario= '".$inventario."' AND
				iditem = '".$id."' AND 
				tipo = '".$tipo."' AND 
				idusuario = '".$_SESSION['user_id']."' ";
		if($mysqli->query($query)){
			echo 1;
		}else{
			die($mysqli->error);
		}
		mysqli_close($mysqli);
	}
	
	function limpiar_conteo_tmp(){
		global $mysqli;
		$idconteo = isset($_REQUEST['id'])?$_REQUEST['id']:$_REQUEST['idconteo'];
		$inventario = $_REQUEST['bodega'];		
		$usuario = $_SESSION['user_id'];
		$query ="DELETE FROM conteo_tmp WHERE idconteo = '$idconteo' AND inventario = $inventario AND idusuario = '$usuario'";
		$mysqli->query($query);
		mysqli_close($mysqli);
	}

?>