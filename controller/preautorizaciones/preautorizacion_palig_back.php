<?php  
    include("../conexion.php");
	$oper = '';
	if (isset($_REQUEST['oper'])) {
		$oper = $_REQUEST['oper'];
	}
	
	switch($oper){ 
		case "getpaciente": 
			  getpaciente();
			  break;
		case "enfermedades":
			  enfermedades();
			  break;
		case "procedimientos":
			  procedimientos();
			  break;
		case "getseguro": 
			  getseguro();
			  break; 
		case "guardarpalig":
			  guardarpalig();
			  break;
		case "getpreautorizacion":
			  getpreautorizacion();
			  break;
		case "editarpalig":
			  editarpalig();
			  break;
		default:
			  echo "{failure:true} fallo";
			  break;
	}	 
	function getpaciente(){
		global $mysqli;
		$idpaciente = $_REQUEST['pacienteid'];
		$idseguro = $_REQUEST['idseguro'];
		$query 		= "	SELECT pa.id, pa.nombre,pa.apellido, pa.email, pa.celular, pa.fechanac, pa.cedula, pa.sexo,
						se.nombre as seguro, pase.numeropoliza, pase.nombreasegurado, pase.cedulaasegurado
						FROM pacienteseguros as pase
						INNER JOIN pacientes as pa on pa.id = pase.idpacientes 
						INNER JOIN seguros as se on se.id = pase.idseguros 
						WHERE pa.id = '$idpaciente' and pase.idseguros = '$idseguro';";
		$result 	= $mysqli->query($query);
		while($row = $result->fetch_assoc()){
			$resultado = array(
				'cedula'				=>  $row['cedula'],
				'nombre'	 			=>	$row['nombre'],
				'apellido'	 			=>	$row['apellido'],
				'fechanac' 				=>	$row['fechanac'],
				'correo'	 			=>	$row['email'],
				'sexo'	 				=>	$row['sexo'],
				'nombreasegurado'		=>  $row['nombreasegurado'], 
				'cedulaasegurado'		=>  $row['cedulaasegurado'],
				'telefono'	 			=>	$row['celular'],
				'numeropoliza'			=> 	$row['numeropoliza'],
				'seguro'				=>	$row['seguro']
			);
		}
		if( isset($resultado) ) {
			echo json_encode($resultado);
		} else {
			echo "0";
		}
	}

	function getseguro(){
		global $mysqli;
		
		$pacienteid = $_REQUEST['pacienteid'];
		$seguroid = $_REQUEST['seguroid'];
		$query 		= "	SELECT pa.id, pa.nombre,pa.fechanac, pa.cedula, se.nombre as seguro, pase.numeropoliza
						FROM pacienteseguros as pase
						INNER JOIN pacientes as pa on pa.id = pase.idpacientes 
						INNER JOIN seguros as se on se.id = pase.idseguros 
						WHERE pa.id = '$pacienteid' and pase.idseguros = '$seguroid'";
		$result 	= $mysqli->query($query);
		
		while($row = $result->fetch_assoc()){
			//echo $row['nombre'];
			$resultado = array(
			    
				'txt_poliza' 				=>	$row['numeropoliza'],
				
			);
		}
		
		if( isset($resultado) ) {
			echo json_encode($resultado);
		} else {
			echo "0";
		}
	}	
	
	function enfermedades() {
		global $mysqli;
		$query = "SELECT * FROM enfermedades";
		if ( isset( $_REQUEST['search'])){
			$q =  $_REQUEST['search'];
			$query .= " WHERE nombre LIKE '%".$q."%' OR codigo LIKE '%".$q."%'";
		}
		$result_num = $mysqli->query($query);
		$pag =  $_REQUEST['page'];
		if ($pag == 1) {
			$query .= " LIMIT 5";
		} else {
			$query .= " LIMIT 5 OFFSET ".(($pag - 1) * 5);
		}
		$result = $mysqli->query($query);
		$resultado['count'] = $result_num->num_rows;
		while ($row = $result->fetch_assoc()){
			$resultado['results'][] = array(
				'id'	=> $row['id'],
				'text'	=> $row['codigo']." | ".$row['nombre']
			);
		}
		echo json_encode($resultado);
	}

	function procedimientos() {
		global $mysqli;
		$query = "SELECT * FROM cpt";
		if ( isset( $_REQUEST['search'])){
			$q =  $_REQUEST['search'];
			$query .= " WHERE descripcion LIKE '%".$q."%' OR codigo LIKE '%".$q."%'";
		}
		$result_num = $mysqli->query($query);
		$pag =  $_REQUEST['page'];
		if ($pag == 1) {
			$query .= " LIMIT 5";
		} else {
			$query .= " LIMIT 5 OFFSET ".(($pag - 1) * 5);
		}
		$result = $mysqli->query($query);
		$resultado['count'] = $result_num->num_rows;
		while ($row = $result->fetch_assoc()){
			$resultado['results'][] = array(
				'id'	=> $row['id'],
				'text'	=> $row['codigo']." | ".$row['descripcion']
			);
		}
		echo json_encode($resultado);
	}


	function guardarpalig(){
		global $mysqli;
		$idpaciente = (!empty($_REQUEST['idpaciente']) ? $_REQUEST['idpaciente'] : '');
		$idaseguradora = (!empty($_REQUEST['idaseguradora']) ? $_REQUEST['idaseguradora'] : '');
		$idpoliza = (!empty($_REQUEST['idpoliza']) ? $_REQUEST['idpoliza'] : '');
		$tipo = (!empty($_REQUEST['tipo']) ? $_REQUEST['tipo'] : 0);
		$hospital = (!empty($_REQUEST['hospital']) ? $_REQUEST['hospital'] : '');
		$str = (!empty($_REQUEST['str']) ? $_REQUEST['str'] : '');
		$query 	= "	INSERT INTO	preautorizaciones (id,idpaciente,idaseguradora,poliza,hospital,idtiposervicio,formulario)
					VALUES (null,$idpaciente,$idaseguradora,'$idpoliza','$hospital','$tipo','$str')";
		$result = $mysqli->query($query);
		$idcross = $mysqli->insert_id;
		
		if($result==true){
			echo 1;
		}else{
			die ($mysqli->error);
		}
		
	}



	function editarpalig(){
		global $mysqli;
		$id = (!empty($_REQUEST['id']) ? $_REQUEST['id'] : '');
		$hospital = (!empty($_REQUEST['hospital']) ? $_REQUEST['hospital'] : '');
		$tipo = (!empty($_REQUEST['tipo']) ? $_REQUEST['tipo'] : 0);
		$str = (!empty($_REQUEST['str']) ? $_REQUEST['str'] : '');
		$query 	= "	UPDATE preautorizaciones SET  hospital='$hospital', idtiposervicio='$tipo', formulario='$str' WHERE id ='$id';";
		$result = $mysqli->query($query);
		if($result==true){
			echo 1;
		}else{
			die ($mysqli->error);
		}
		
	}
	function getpreautorizacion(){
		global $mysqli;
		$id =  (!empty($_REQUEST['id']) ? $_REQUEST['id'] : '');
		$query= "SELECT formulario FROM preautorizaciones WHERE id = $id;";
		$result = $mysqli->query($query);
		$idpalig = $result->fetch_assoc();
		echo json_encode($idpalig['formulario']);

	}

?>

