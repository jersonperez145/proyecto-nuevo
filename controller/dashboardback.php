<?php
    include("conexion.php");
	//ini_set('display_errors', 1);ini_set('display_startup_errors', 1);error_reporting(E_ALL);
	
	if (isset($_REQUEST['opcion'])) {
		$opcion = $_REQUEST['opcion'];
		if ($opcion=='MAPA')
			mapa();
		elseif ($opcion=='DATOS_PACIENTES_ALERTAS')
			datos_pacientes_alertas(); //
		elseif ($opcion=='DATOS_VISITAS_RETRASO')
			datos_visitas_retraso(); //
		elseif ($opcion=='DATOS_TURNOS_CANCELADOS')
			datos_turnos_cancelados(); //
		elseif ($opcion=='DATOS_PACIENTES_COVID')
			datos_pacientes_covid(); //
		elseif ($opcion=='DATOS_TRATAMIENTOS_EDITADOS')
			datos_tratamientos_editados(); //
		elseif ($opcion=='DATOS_PLANES')
			datos_planes(); //
		elseif ($opcion=='DATOS_TRATAMIENTOS_FINALIZADOS')
			datos_tratamientos_finalizados(); //
		elseif ($opcion=='TABLA_PACIENTES_FUERA_RANGO')
			tabla_pacientes_fuera_rango(); //
		elseif ($opcion=='TABLA_PACIENTES_FUERA_RANGO2')
			tabla_pacientes_fuera_rango2(); //
		elseif ($opcion=='TABLA_VISITAS_RETRASO')
			tabla_visitas_retraso(); //
    	elseif ($opcion=='TABLA_TURNOS_CANCELADOS')
			tabla_turnos_cancelados(); //
		elseif ($opcion=='TABLA_PACIENTES_COVID')
			tabla_pacientes_covid(); //	
		elseif ($opcion=='TABLA_PACIENTES_ACTIVOS')
			tabla_pacientes_activos(); //	
		elseif ($opcion=='TABLA_TRATAMIENTOS_EDITADOS')
			tabla_tratamientos_editados(); //
		elseif ($opcion=='TABLA_PLANES')
			tabla_planes(); //
		elseif ($opcion=='TABLA_TRATAMIENTOS_FINALIZADOS')
			tabla_tratamientos_finalizados(); //
		elseif ($opcion=='TABLA_INCIDENTES')
			tabla_incidentes(); //
		elseif ($opcion=='evidencias')
			evidencias(); //
		elseif ($opcion=='INCIDENTES_NOTIFICACION')
			incidentes_notificacion(); //
		elseif ($opcion=='CHAT_USUARIOS')
			chat_usuarios(); //
    	elseif ($opcion=='CHAT_USUARIOS_DETALLES')
			chat_usuarios_detalles(); //
		elseif ($opcion=='CHAT_MENSAJES_ENVIAR')
			chat_mensajes_enviar(); //
	    elseif ($opcion=='LISTADO_NOTAS')
			listado_notas(); //
		elseif ($opcion=='NOTAS_DETALLES')
			notas_detalles(); //
	    elseif ($opcion=='TOP_COLABORADORES')
			top_colaboradores(); //
		elseif ($opcion=='datos_visita')
			datos_visita(); //
		elseif ($opcion=='activar_medicamento')
			activar_medicamento();
		elseif ($opcion=='activar_plan')
			activar_plan();
		elseif ($opcion=='imprimir_tratamiento')
			imprimir_tratamiento(); 
		elseif ($opcion=='imprimir_plan')
			imprimir_plan();
		elseif ($opcion=='nivel')
			nivel();			//
		elseif ($opcion=='get_nota')
			get_nota();			//
		else
			return true;
	}
	function imprimir_tratamiento(){
		global $mysqli;
		$idreporte = $_REQUEST['id'];
		$url = 'tarjeta_medicamentos_editar.php?id='.$idreporte.'%';
		$queryrestriccion = " SELECT a.*, u.nombre FROM accion_usuario a INNER JOIN usuarios u ON u.id = a.idusuario WHERE url like '$url' LIMIT 1";
		//die($queryrestriccion);
		$resultrestriccion = $mysqli->query($queryrestriccion);
			if($resultrestriccion->num_rows > 0){
				$row = $resultrestriccion->fetch_assoc();
				
				echo $row['nombre'];
			}else{
				echo true;
			}	
	}
	function imprimir_plan(){
		global $mysqli;
		$idreporte = $_REQUEST['id'];
		$url = 'plan_cuidado_editar.php?id='.$idreporte.'%';
		$queryrestriccion = " SELECT a.*, u.nombre FROM accion_usuario a INNER JOIN usuarios u ON u.id = a.idusuario WHERE url like '$url' LIMIT 1";
		//die($queryrestriccion);
		$resultrestriccion = $mysqli->query($queryrestriccion);
			if($resultrestriccion->num_rows > 0){
				$row = $resultrestriccion->fetch_assoc();
				
				echo $row['nombre'];
			}else{
				echo true;
			}	
	}
	function datos_visita(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$resultado = array();
		$html = '';
		$query = "SELECT b.sentencia as json_detalle  FROM bitacora b 
                   INNER JOIN eventoscalendario e ON e.id = b.idregistro
                   INNER JOIN pacientes p ON p.id = e.idpaciente 
                   INNER JOIN usuarios u ON u.id = e.recurso
                   where modulo ='AGENDA-INSTRUCCIONES' and e.id = $id";
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc()){
			$data = (explode(",",$row['json_detalle']));
			$recurso = explode(":",$data[2]);
			$asignado = explode(":",$data[3]);
			$motivo = explode(":",$data[1]);
			$fecha = explode(" ",$data[4]);
			//print_r($asignado[1]);
			//print_r($recurso[1]);
			//print_r($motivo[1]);
			//print_r($fecha[2]);
			$html .= '	<h5>Recurso Anterior</h5>
                        <p>'.$recurso[1].'.</p>
                        <hr>
						<h5>Recurso Reasignado</h5>
                        <p>'.$asignado[1].'.</p>
                        <hr>
                        <h5>Motivo</h5>
                        <p>'.$motivo[1].'.</p>
						<hr>
                        <h5>Fecha</h5>
                        <p>'.$fecha[2].'.</p>';
			$response['data'][] = array(
				'recurso'		    =>	$recurso[1],
				'asignado'     	    =>	$asignado[1],
				'motivo' 	    	=>	$motivo[1],
				'fecha' 	        =>	$fecha[2]
			);
		}
		echo $html;
	}
	
	function mapa() {
		global $mysqli;
		
		$tipo   = (!empty($_REQUEST['tipo']) ? $_REQUEST['tipo'] : '');	
		$agno   = (int) date('Y');
		$mes    = (int) date('m');
		$objJson= array();
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		//PACIENTES
		$consultap = "  SELECT e.cedula as label, e.nombre as title, e.nombre as html, vc.latitude, vc.longitude, 'pac' AS tipomapa 
            			FROM enfermeras e
            			INNER JOIN visitas v ON e.idusuario = v.idusuario
						inner join usuarios u on e.idusuario = u.id
                        left join disponibilidades d on u.id = d.idpersonal
            			INNER JOIN visit_coordinates vc ON v.id = vc.visit_id
            			WHERE 1 ";
		
		//ENFERMEROS
		$consultae = "  SELECT p.cedula as label, p.nombre as title, p.nombre as html, pa.latitude, pa.longitude, 'enf' AS tipomapa 
            			FROM pacientes p
            			INNER JOIN patient_addresses pa ON p.id = pa.patient_id
						LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id
            			INNER JOIN hospitalizacion h ON h.paciente = p.id
            			WHERE h.estatus = 'activa' AND p.ubicaciongeografica <> ''
            			 ";
		//FILTROS
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consultae .= " and p.idmedicotratante  = $idmedico";
		}
		if($tipo == 'mapapac'){
			$consultae .= " GROUP BY p.id";
    		$consultaf = $consultae;
		}elseif($tipo == 'mapaenf'){
			$consultap .= "GROUP BY e.id";
    		$consultaf = $consultap; 
		}elseif($tipo == 'mapaenfd'){
			
			$consultap .= "and d.inicio != '' and d.inicio >= CURRENT_DATE ";
			$consultap .= "GROUP BY e.id";
			$consultaf = $consultap;
		}
		//TIPO
		if($tipo == '' || $tipo == 'mapatodos'){
			 $consultap .= "GROUP BY e.id";
		     $consultad =  $consultap.' UNION '. $consultae;
		}else{
		    $consultad = $consultaf;
		}
		
		$result = $mysqli->query($consultad);
		$i = 0;
		$map = array();
		while($rec = $result->fetch_assoc()){
		    $tipomapa = $rec['tipomapa'];
		    if ($tipomapa == 'pac') {
		        $map['label']   = $rec['label'];
        		$map['title']   = $rec['title'];
        		$map['html']    = $rec['html'];
        		$map['latitud'] = $rec['latitude'];
        		$map['longitud']= $rec['longitude'];
        		$map['icon']    = 'images/markere.png';
        		$objJson[] = $map;
			}elseif($tipomapa == 'enf'){
			    $map['label']   = $rec['label'];
        		$map['title']   = $rec['title'];
        		$map['html']    = $rec['html'];
        		$map['latitud'] = $rec['latitude'];
        		$map['longitud']= $rec['longitude'];
        		$map['icon']    = 'images/markerp.png';
        		$objJson[] = $map;
			}
		}
		
		/*
		$rec['label'] = 'CL3366';
		$rec['title'] = 'Logistica 1';
		$rec['html'] = 'Logistica 1';
		$rec['latitud'] = 8.9992394 + (rand(-5, 5) / 1000);
		$rec['longitud'] = -79.5055379  + (rand(-5, 5) / 1000);
		$rec['icon']= 'images/markerl.png';
		$objJson[] = $rec;
		
		$rec['label'] = 'AZ1020';
		$rec['title'] = 'Logistica 2';
		$rec['html'] = 'Logistica 2';
		$rec['latitud'] = 8.9892394 + (rand(-5, 5) / 1000);
		$rec['longitud'] = -79.504181  + (rand(-5, 5) / 1000);
		$rec['icon']= 'images/markerl.png';
		*/
		
		echo json_encode($objJson);
	}

	function datos_pacientes_alertas() {
		global $mysqli;		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$consulta ="SELECT SUM(CASE alerta_signos_vitales(vvs.vital_sign_id,p.fechanac,vvs.value) WHEN 'red' THEN 1 ELSE 0 END) as alertasignovital 
		FROM visit_vital_sign vvs 
		INNER JOIN visitas v ON v.id = vvs.visit_id 
		INNER JOIN pacientes p ON p.id = v.idpaciente
		LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
		WHERE vvs.created_at >= now() - INTERVAL 1 DAY GROUP BY p.id";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}		
		$consulta .= "GROUP BY p.id";
		$result = $mysqli->query($consulta);
	//	echo $consulta;
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}        
		echo json_encode($response);
	}
	
	function datos_visitas_retraso() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$consulta ="SELECT SUM(CASE WHEN e.start < now() AND e.estatus = 'pendiente' AND e.start >= now() - INTERVAL 1 DAY THEN 1 ELSE 0 END) as retraso
		FROM eventoscalendario e 
		INNER JOIN pacientes p ON p.id = e.idpaciente 
		INNER JOIN usuarios u ON u.id = e.recurso
		left join paciente_usuario pu on pu.paciente_id = p.id
		WHERE e.start < now() AND e.estatus = 'pendiente' AND e.start >= now() - INTERVAL 1 DAY";																	
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}			
		$result = $mysqli->query($consulta);
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}        
		echo json_encode($response);
	}
	
	function datos_turnos_cancelados() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
			$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$consulta ="SELECT SUM(CASE WHEN modulo ='AGENDA-INSTRUCCIONES' AND fecha  >= now() - INTERVAL 1 DAY THEN 1 ELSE 0 END) as cancelados
		FROM bitacora b 
        INNER JOIN eventoscalendario e ON e.id = b.idregistro
        INNER JOIN pacientes p ON p.id = e.idpaciente 
        INNER JOIN usuarios u ON u.id = e.recurso
		left join paciente_usuario pu on pu.paciente_id = p.id
        WHERE modulo ='AGENDA-INSTRUCCIONES' AND fecha  >= now() - INTERVAL 1 DAY";																	
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}			
		$result = $mysqli->query($consulta);
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}        
		echo json_encode($response);
	}
	
	function datos_pacientes_covid() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;	
		$consulta ="SELECT SUM(CASE WHEN e.codigo  = 'U071' THEN 1 ELSE 0 END) as pcovid
		from pacientes p 
        INNER JOIN pacienteenfermedadesactuales pe ON pe.idpaciente = p.id
        INNER JOIN enfermedades e ON e.id = pe.idenfermedad
		left join paciente_usuario pu on pu.paciente_id = p.id
        WHERE e.codigo  = 'U071'";																	
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}		
			
		$result = $mysqli->query($consulta);
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}
		echo json_encode($response);
	}

	function datos_tratamientos_editados() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;	
		$consulta ="SELECT SUM(CASE WHEN t.fecha >= now() - INTERVAL 1 DAY  AND t.estatus = '1' THEN 1 ELSE 0 END) as tratamientoseditados
		FROM  tarjetamedicamento t 
	    INNER JOIN pacientes p ON p.id = t.idpaciente
		left join paciente_usuario pu on pu.paciente_id = p.id
    	WHERE t.fecha >= now() - INTERVAL 1 DAY  AND t.estatus = '1'";																	
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}			
		$result = $mysqli->query($consulta);
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}        
		echo json_encode($response);
	}

	function datos_tratamientos_finalizados() {
		global $mysqli;
		
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');	
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;	
		$consulta ="SELECT SUM(CASE WHEN fecha_fin = (SELECT DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) AND td.estado = '1' AND t.estatus = '1' THEN 1 ELSE 0 END) as tratamientosfinalizados
		FROM tarjetamedicamentodetalle td
		INNER JOIN tarjetamedicamento t ON t.id = td.idtarjeta
		INNER JOIN pacientes p ON p.id = t.idpaciente
		left join paciente_usuario pu on pu.paciente_id = p.id
		INNER JOIN  medicamentos m ON m.id = td.idmedicamento
		WHERE fecha_fin = (SELECT DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) AND td.estado = '1' AND t.estatus = '1'";																
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$consulta .= " and p.idmedicotratante  = $idmedico";
		}			
		$result = $mysqli->query($consulta);
		
		$response = new StdClass;
		$rows = array();
		$i=0;
		while($row = $result->fetch_assoc()){
			$response->rows[$i]=$row;
			$i++;
		}        
		echo json_encode($response);
	}

	//FIN

	/****************************************************FUNCIONES PARA CARGAR LAS TABLAS****************************************************/

	function tabla_pacientes_fuera_rango(){
		global $mysqli;
		$where = array();
		$data  = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$fecha = (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		
		$draw 		= $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy 	= 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType 	= "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   	= (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);
		$length   	= (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$resultado 	= array();
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		$resultado['data'] = array();
		
		$queryI  = "	SELECT MAX(v.id) as idvisita, IFNULL(IF(p.condicion_id='',0,p.condicion_id),0) as condicion, 
						p.id as id_paciente, p.nombre as nombre, 
						YEAR(CURRENT_TIMESTAMP) - YEAR(p.fechanac) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(p.fechanac, 5)) as edad,
						h.cod_hospitalizacion, mcp.nombre as condicion2
						FROM pacientes p
						LEFT JOIN visitas v ON p.id = v.idpaciente  
						LEFT JOIN visit_vital_sign vvs ON vvs.visit_id = v.id 
						LEFT JOIN hospitalizacion h ON p.id = h.paciente
						LEFT JOIN maestro_condicion_paciente mcp ON p.condicion_id = mcp.id
						LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id
						WHERE 1 AND vvs.id is not null 
						AND h.estatus = 'activa'
                    ";
		
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$queryI .= " and p.idmedicotratante  = $idmedico";
		}
		 
		$queryI .= " GROUP BY p.id ORDER BY v.fecha DESC";		
		$resultI = $mysqli->query($queryI);
		$recordsTotal = 0;
		//die($queryI);
		while ($row = $resultI->fetch_assoc()){
			$idpaciente = $row['id_paciente'];
			$idvisita 	= $row['idvisita'];
			$paciente 	= $row['nombre'];
			$edad 		= $row['edad'];
			$condicion 	= 0;
			
			if($row['condicion'] == 1){
				$condicion = '<span class="icon-col fa fa-heartbeat green" data-toggle="tooltip" data-original-title="Paciente estable" data-placement="left"></span>';
			}
			elseif($row['condicion'] == 2){
				$condicion = '<span class="icon-col fa fa-heartbeat yellow" data-toggle="tooltip" data-original-title="Paciente requiere atención"  data-placement="left"></span>';
			}elseif($row['condicion'] == 3){
				$condicion = '<span class="icon-col fa fa-heartbeat red" data-toggle="tooltip" data-original-title="Paciente requiere atención inmediata"  data-placement="left"></span>';
			}else{
				$condicion = '<span class="icon-col fa fa-heartbeat blue" data-toggle="tooltip" data-original-title="Valor no registrado"  data-placement="left"></span>';
			}
			if ($edad != ''){
				$queryToday = "	SELECT COUNT(pa.id) as visita_today 
								FROM pacientes pa 
								LEFT JOIN hogares ho ON ho.id = pa.hogar 
								INNER JOIN visitas vi ON pa.id = vi.idpaciente ";
					
				if($fecha != ""){
					$queryToday .=" AND vi.fecha = '$fecha' ";
				}else{
					$queryToday .= "  ";
				}
				$queryToday.=" AND  pa.id = '$idpaciente'";
				$resultToday = $mysqli->query($queryToday);		   
				
				while ($rowToday = $resultToday->fetch_assoc()){
					if($rowToday['visita_today'] >= 1){
						$query =" 
							SELECT DISTINCT p.condicion_id as condicion, v.id as id, p.nombre as nombre, CONCAT(DATE_FORMAT(v.fecha, '%Y-%m-%d'),' ', DATE_FORMAT(v.horainicio, '%h:%i:%p')) as fecha,u.nombre as recurso, h.nombre as paciente_hogar,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as fc,
							IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') as rfc,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as fr,
							IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rfr,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as oximetria, 
							IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rox,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as sistolica,
							IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rsi,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as diastolica,
							IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rdi, 
							IFNULL(v.imc,0) as imc,
							IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') as rim, 
							IFNULL((SELECT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as glicemia,
							IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rgc,
							ROUND(IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id = '".$idvisita."' LIMIT 1  ) ,0),2) as temperatura,
							IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rtm,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as dolor, 
							IF(IFNULL(v.dolor,0) > 0,'alta','alta') as rdl,
							( 	IF(IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) > 0,1,0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											 IF(IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='baja',1,0)
										)
										),
								0) +					
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +					
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(
									IF(	
										IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	 
									IF(	
										IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0)+
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0) 
										)
									),
								0)
							) as prioridad
							FROM visitas v
                            LEFT JOIN visit_vital_sign vs ON v.id = vs.visit_id
							inner join pacientes p ON p.id = v.idpaciente
							inner join usuarios u ON u.id = v.idusuario
							LEFT JOIN hogares h ON h.id = p.hogar		  
							WHERE v.id = '".$idvisita."' 	 		 
						";
					}else{						
						$query =" 
							SELECT v.id as id,p.nombre as nombre, '' as fecha, h.nombre as paciente_hogar,
							'' as fc,
							'' as rfc,
							'' as fr,
							'' as rfr,
							'' as oximetria, 
							'' as rox,
							'' as sistolica,
							'' as rsi,
							'' as diastolica,
							'' as rdi, 
							'' as imc,
							'' as rim, 
							'' as glicemia,
							'' as rgc,
							'' as temperatura,
							'' as rtm,
							'' as rdl,
							'' as dolor,
							'' as recurso,
							0 as prioridad
							FROM visitas v
							inner join pacientes p ON p.id = v.idpaciente	  
							LEFT JOIN hogares h ON h.id = p.hogar		
							WHERE v.id = '".$idvisita."' 	 		
						";
					}
				}
				
				$query .= " GROUP BY v.id
							ORDER BY prioridad DESC LIMIT 1";  
				if($result2 = $mysqli->query($query)){									
					$cont_rfc = 0;
					$cont_rfr = 0; 
					$cont_rox = 0; 
					$cont_sis = 0;
					$cont_di = 0; 
					$cont_dl = 0;
					$cont_gc = 0; 
					$cont_im = 0;
					$fila = $result2->fetch_assoc();
					$acciones = '<div class="dropdown ml-auto text-right">
												<div class="btn-link" data-toggle="dropdown">
													<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
												    <a class="dropdown-item" href="historial.php?idpaciente='.$idpaciente.'" >Ver Historial</a>
													
													<!--a class="dropdown-item" href="#">Chatear con Paciente</a-->
													
												</div>
											</div>';
					$sistolica 	= !empty($fila['sistolica'])? $fila['sistolica'] : "";
					$diastolica = !empty($fila['diastolica'])? $fila['diastolica'] : "";
					$dolor 		= !empty($fila['dolor'])? $fila['dolor'] : "";
					$glicemia 	= !empty($fila['glicemia'])? $fila['glicemia'] : "";
					$temperatura = !empty($fila['temperatura'])? $fila['temperatura'] : "";
					
					if( strtolower($fila['rfc']) == 'alta' || strtolower($fila['rfc']) == 'baja' ||
						strtolower($fila['rfr']) == 'alta' || strtolower($fila['rfr']) == 'baja' ||
						strtolower($fila['rox']) == 'alta' || strtolower($fila['rox']) == 'baja' ||
						( (strtolower($fila['rsi']) == 'alta' || strtolower($fila['rsi']) == 'baja') && $sistolica != '' ) ||
						( (strtolower($fila['rdi']) == 'alta' || strtolower($fila['rdi']) == 'baja') && $diastolica != '' ) ||
						( (strtolower($fila['rtm']) == 'alta' || strtolower($fila['rtm']) == 'baja') && $temperatura != '' ) ||
						( (strtolower($fila['rdl']) == 'alta' || strtolower($fila['rdl']) == 'baja') && $dolor != '' ) ||
						( (strtolower($fila['rgc']) == 'alta' || strtolower($fila['rgc']) == 'baja') && $glicemia != '' ) )
					{
						$resultado['data'][] = array(						
							'nombre'	=>  !empty($fila['nombre']) ? $fila['nombre'] : "",
							'fc' 		=>	!empty($fila['fc']) ? $fila['fc'] : "",
							'rfc' 		=>	!empty($fila['rfc'])? $fila['rfc'] : "",
							'fr' 		=>	!empty($fila['fr'])? $fila['fr'] : "",
							'rfr' 		=>	!empty($fila['rfr'])? $fila['rfr'] : "",
							'so' 		=>	!empty($fila['oximetria'])? $fila['oximetria'] : "",
							'rox' 		=>	!empty($fila['rox'])? $fila['rox'] : "",
							'paa' 		=>	$sistolica,
							'rsi' 		=>	!empty($fila['rsi'])? $fila['rsi'] : "",
							'pab' 		=>	$diastolica,
							'rdi' 		=>	!empty($fila['rdi'])? $fila['rdi'] : "",
							'tc' 		=>	$temperatura,
							'rtm' 		=>	!empty($fila['rtm'])? $fila['rtm'] : "",
							'dolor' 	=>	$dolor,
							'rdl' 		=>	!empty($fila['rdl'] )? $fila['rdl'] : "",
							'gc' 		=>	$glicemia,
							'rgc' 		=>	!empty($fila['rgc']) ? $fila['rgc'] : "",
							'hogar' 	=>	!empty($fila['paciente_hogar']) ? $fila['paciente_hogar'] : "",
							'res' 		=>	!empty($fila['res']) ? $fila['res'] : "",
							'fecha' 	=>	!empty($fila['fecha']) ? $fila['fecha'] : "",
							'rpe' 		=>	!empty($fila['rpe']) ? $fila['rpe'] : "",
							'imc' 		=>	!empty($fila['imc']) ? $fila['imc'] : "",
							'rim' 		=>	!empty($fila['rim']) ? $fila['rim'] : "",
							'id' 		=>	!empty($fila['id']) ? $fila['id'] : "",
							'condicion'	=>	$condicion,
							'recurso'	=>	!empty($fila['recurso']) ? $fila['recurso'] : "",
							'condicion2'	=>	!empty($row['condicion2']) ? $row['condicion2'] : "",
							'acciones' 	=>  $acciones
						);
						$recordsTotal++;
					}
				}else{
					//echo $query.'<br>';
					die($mysqli->error); 
				}
			}
		}
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				$where[] = " $column like '%".$campo."%' ";
				$hayFiltros++;
			}
		}

		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		
		$queryI  .= $where;
		$resultI = $mysqli->query($queryI);
		//$recordsTotal = $resultI->num_rows;
		//$recordsFiltered = $resultI->num_rows;
		
		$resultado['draw'] = intval($draw);
		$resultado['recordsTotal'] = intval($recordsTotal);
		$resultado['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($resultado);
	}
	
	function tabla_pacientes_fuera_rango2(){
		global $mysqli;
		$where = array();
		$data  = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$fecha = (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		
		$draw 		= $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy 	= 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType 	= "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   	= (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);
		$length   	= (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$resultado 	= array();
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$id_seguro = isset($_COOKIE['seguro'])?$_COOKIE['seguro']:16;
		$resultado['data'] = array();
		
		$queryI  = "	SELECT MAX(v.id) as idvisita, IFNULL(IF(p.condicion_id='',0,p.condicion_id),0) as condicion, 
						p.id as id_paciente, p.nombre as nombre, 
						YEAR(CURRENT_TIMESTAMP) - YEAR(p.fechanac) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(p.fechanac, 5)) as edad,
						h.cod_hospitalizacion, mcp.nombre as condicion2
						FROM pacientes p
						LEFT JOIN visitas v ON p.id = v.idpaciente  
						LEFT JOIN visit_vital_sign vvs ON vvs.visit_id = v.id 
						LEFT JOIN hospitalizacion h ON p.id = h.paciente
						LEFT JOIN maestro_condicion_paciente mcp ON p.condicion_id = mcp.id
						LEFT JOIN paciente_usuario pu ON pu.paciente_id = p.id
						WHERE 1 AND vvs.id is not null 
						 and h.estatus = 'activa' AND v.fechaplan  >= now() - INTERVAL 5 DAY
                    ";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$queryI .= " and p.idmedicotratante  = $idmedico";
		}
		
		$queryI .= " GROUP BY p.id ORDER BY v.fecha DESC";	
		
		$resultI = $mysqli->query($queryI);
		$recordsTotal = 0;
		
		while ($row = $resultI->fetch_assoc()){
			$idpaciente = $row['id_paciente'];
			$idvisita 	= $row['idvisita'];
			$paciente 	= $row['nombre'];
			$edad 		= $row['edad'];
			$condicion 	= 0;
			
			if($row['condicion'] == 1){
				$condicion = '<span class="icon-col fa fa-heartbeat green" data-toggle="tooltip" data-original-title="Paciente estable" data-placement="left"></span>';
			}
			elseif($row['condicion'] == 2){
				$condicion = '<span class="icon-col fa fa-heartbeat yellow" data-toggle="tooltip" data-original-title="Paciente requiere atención"  data-placement="left"></span>';
			}elseif($row['condicion'] == 3){
				$condicion = '<span class="icon-col fa fa-heartbeat red" data-toggle="tooltip" data-original-title="Paciente requiere atención inmediata"  data-placement="left"></span>';
			}else{
				$condicion = '<span class="icon-col fa fa-heartbeat blue" data-toggle="tooltip" data-original-title="Valor no registrado"  data-placement="left"></span>';
			}
			if ($edad != ''){
				$queryToday = "	SELECT COUNT(pa.id) as visita_today 
								FROM pacientes pa 
								inner join pacienteseguros ps on ps.idpacientes = pa.id
								LEFT JOIN hogares ho ON ho.id = pa.hogar 
								INNER JOIN visitas vi ON pa.id = vi.idpaciente
								INNER JOIN visit_vital_sign vvs on vvs.visit_id = vi.id 
								where 1 ";
					
				if($fecha != ""){
					$queryToday .=" AND vi.fecha = '$fecha' ";
				}else{
					$queryToday .= "  ";
				}
				$queryToday.=" AND  pa.id = '$idpaciente' ";
				$resultToday = $mysqli->query($queryToday);		   
				
				while ($rowToday = $resultToday->fetch_assoc()){
					if($rowToday['visita_today'] >= 1){
						$query =" 
							SELECT DISTINCT p.condicion_id as condicion, v.id as id, p.nombre as nombre, CONCAT(DATE_FORMAT(v.fecha, '%Y-%m-%d'),' ', DATE_FORMAT(v.horainicio, '%h:%i:%p')) as fecha,u.nombre as recurso, h.nombre as paciente_hogar,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as fc,
							IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') as rfc,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as fr,
							IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rfr,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as oximetria, 
							IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rox,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as sistolica,
							IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rsi,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as diastolica,
							IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rdi, 
							IFNULL(v.imc,0) as imc,
							IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') as rim, 
							IFNULL((SELECT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as glicemia,
							IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rgc,
							ROUND(IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id = '".$idvisita."' LIMIT 1  ) ,0),2) as temperatura,
							IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') as rtm,
							IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) as dolor, 
							IF(IFNULL(v.dolor,0) > 0,'alta','alta') as rdl,
							( 	IF(IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=7 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0) > 0,1,0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ) ,0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Frecuencia cardiaca',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=1 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											 IF(IFNULL(resultado_examen_valor('Frecuencia respiratoria',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=2 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='baja',1,0)
										)
										),
								0) +					
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Saturación de oxígeno',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=3 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +					
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Presión sistolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=4 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,( 
											IF(IFNULL(resultado_examen_valor('Presión diastolica',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=5 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(
									IF(	
										IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('IMC',p.fechanac,IFNULL(v.imc,0)),'baja') = 'baja',1,0)
										)
									),
								0) +
								IFNULL(	 
									IF(	
										IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Glicemia capilar',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=8 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0)
										)
									),
								0)+
								IFNULL(	
									IF(	
										IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') ='alta',1,(
											IF(IFNULL(resultado_examen_valor('Temperatura',p.fechanac,IFNULL((SELECT DISTINCT t.value FROM visit_vital_sign t WHERE t.vital_sign_id=6 AND t.visit_id='".$idvisita."' LIMIT 1  ),0)),'baja') = 'baja',1,0) 
										)
									),
								0)
							) as prioridad, v.id as idvisita
							FROM visitas v
                            LEFT JOIN visit_vital_sign vs ON v.id = vs.visit_id
							inner join pacientes p ON p.id = v.idpaciente
							inner join usuarios u ON u.id = v.idusuario
							LEFT JOIN hogares h ON h.id = p.hogar		  
							WHERE v.id = '".$idvisita."' 	 		 
						";
					}else{						
						$query =" 
							SELECT v.id as id,p.nombre as nombre, '' as fecha, h.nombre as paciente_hogar,
							'' as fc,
							'' as rfc,
							'' as fr,
							'' as rfr,
							'' as oximetria, 
							'' as rox,
							'' as sistolica,
							'' as rsi,
							'' as diastolica,
							'' as rdi, 
							'' as imc,
							'' as rim, 
							'' as glicemia,
							'' as rgc,
							'' as temperatura,
							'' as rtm,
							'' as rdl,
							'' as dolor,
							'' as recurso,
							0 as prioridad,v.id as idvisita
							FROM visitas v
							inner join pacientes p ON p.id = v.idpaciente	  
							LEFT JOIN hogares h ON h.id = p.hogar		
							WHERE v.id = '".$idvisita."' 	 		
						";
					}
				}
				
				$query .= " GROUP BY v.id
							ORDER BY prioridad DESC LIMIT 1";  
				if($result2 = $mysqli->query($query)){									
					$cont_rfc = 0;
					$cont_rfr = 0; 
					$cont_rox = 0; 
					$cont_sis = 0;
					$cont_di = 0; 
					$cont_dl = 0;
					$cont_gc = 0; 
					$cont_im = 0;
					$fila = $result2->fetch_assoc();
					$acciones = '<div class="dropdown ml-auto text-right">
												<div class="btn-link" data-toggle="dropdown">
													<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
												    <a class="dropdown-item" href="historial.php?idpaciente='.$idpaciente.'" >Ver Historial</a>
													<a class="dropdown-item ver-notas" data-toggle="modal" data-id="'.$fila['idvisita'].'" data-target="#modal_vernota">Ver nota</a>
													<!--a class="dropdown-item" href="#">Chatear con Paciente</a-->
													
												</div>
											</div>';
					$sistolica 	= !empty($fila['sistolica'])? $fila['sistolica'] : "";
					$diastolica = !empty($fila['diastolica'])? $fila['diastolica'] : "";
					$dolor 		= !empty($fila['dolor'])? $fila['dolor'] : "";
					$glicemia 	= !empty($fila['glicemia'])? $fila['glicemia'] : "";
					$temperatura = !empty($fila['temperatura'])? $fila['temperatura'] : "";
					
					/*if( strtolower($fila['rfc']) == 'alta' || strtolower($fila['rfc']) == 'baja' ||
						strtolower($fila['rfr']) == 'alta' || strtolower($fila['rfr']) == 'baja' ||
						strtolower($fila['rox']) == 'alta' || strtolower($fila['rox']) == 'baja' ||
						( (strtolower($fila['rsi']) == 'alta' || strtolower($fila['rsi']) == 'baja') && $sistolica != '' ) ||
						( (strtolower($fila['rdi']) == 'alta' || strtolower($fila['rdi']) == 'baja') && $diastolica != '' ) ||
						( (strtolower($fila['rtm']) == 'alta' || strtolower($fila['rtm']) == 'baja') && $temperatura != '' ) ||
						( (strtolower($fila['rdl']) == 'alta' || strtolower($fila['rdl']) == 'baja') && $dolor != '' ) ||
						( (strtolower($fila['rgc']) == 'alta' || strtolower($fila['rgc']) == 'baja') && $glicemia != '' ) )
					{*/
						$resultado['data'][] = array(						
							'nombre'	=>  !empty($fila['nombre']) ? $fila['nombre'] : "",
							'fc' 		=>	!empty($fila['fc']) ? $fila['fc'] : "",
							'rfc' 		=>	!empty($fila['rfc'])? $fila['rfc'] : "",
							'fr' 		=>	!empty($fila['fr'])? $fila['fr'] : "",
							'rfr' 		=>	!empty($fila['rfr'])? $fila['rfr'] : "",
							'so' 		=>	!empty($fila['oximetria'])? $fila['oximetria'] : "",
							'rox' 		=>	!empty($fila['rox'])? $fila['rox'] : "",
							'paa' 		=>	$sistolica,
							'rsi' 		=>	!empty($fila['rsi'])? $fila['rsi'] : "",
							'pab' 		=>	$diastolica,
							'rdi' 		=>	!empty($fila['rdi'])? $fila['rdi'] : "",
							'tc' 		=>	$temperatura,
							'rtm' 		=>	!empty($fila['rtm'])? $fila['rtm'] : "",
							'dolor' 	=>	$dolor,
							'rdl' 		=>	!empty($fila['rdl'] )? $fila['rdl'] : "",
							'gc' 		=>	$glicemia,
							'rgc' 		=>	!empty($fila['rgc']) ? $fila['rgc'] : "",
							'hogar' 	=>	!empty($fila['paciente_hogar']) ? $fila['paciente_hogar'] : "",
							'res' 		=>	!empty($fila['res']) ? $fila['res'] : "",
							'fecha' 	=>	!empty($fila['fecha']) ? $fila['fecha'] : "",
							'rpe' 		=>	!empty($fila['rpe']) ? $fila['rpe'] : "",
							'imc' 		=>	!empty($fila['imc']) ? $fila['imc'] : "",
							'rim' 		=>	!empty($fila['rim']) ? $fila['rim'] : "",
							'id' 		=>	!empty($fila['id']) ? $fila['id'] : "",
							'condicion'	=>	$condicion,
							'recurso'	=>	!empty($fila['recurso']) ? $fila['recurso'] : "",
							'condicion2'	=>	!empty($row['condicion2']) ? $row['condicion2'] : "",
							'acciones' 	=>  $acciones
						);
						$recordsTotal++;
					//}
				}else{
					//echo $query.'<br>';
					die($mysqli->error); 
				}
			}
		}
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				$where[] = " $column like '%".$campo."%' ";
				$hayFiltros++;
			}
		}

		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		
		$queryI  .= $where;
		$resultI = $mysqli->query($queryI);
		//$recordsTotal = $resultI->num_rows;
		//$recordsFiltered = $resultI->num_rows;
		
		$resultado['draw'] = intval($draw);
		$resultado['recordsTotal'] = intval($recordsTotal);
		$resultado['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($resultado);
	}
	
	function get_nota(){
		global $mysqli;
		$id = $_REQUEST['id'];
		$response = array();
		$query = "SELECT v.comentario, p.nombre as paciente, u.nombre as usuario from visitas v inner join usuarios u on u.id = v.idusuario inner join pacientes p on p.id = v.idpaciente where v.id = $id";
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		$response['data'][] = array(
				'comentario'		    =>	$row['comentario'],
				'paciente'     	    =>	$row['paciente'],
				'usuario' 	    =>	$row['usuario']
			);
		echo json_encode($response);
	}
	
	function tabla_visitas_retraso(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);

		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		$query  = "SELECT p.cedula, p.nombre as paciente,DATE_FORMAT(e.start, '%Y-%m-%d %h:%i:%p')as fecha_inicio,DATE_FORMAT(e.start, '%h:%i:%s') as horatest,DATE_FORMAT(e.end, '%Y-%m-%d %h:%i:%p') as fecha_fin,
		    u.nombre as recurso , e.estatus, TIMEDIFF(e.start, CURRENT_TIMESTAMP + INTERVAL 2 HOUR) AS `tiempo_retardo`
		    FROM eventoscalendario e 
		    INNER JOIN pacientes p ON p.id = e.idpaciente 
		    INNER JOIN usuarios u ON u.id = e.recurso
			LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
		    WHERE e.start > subdate(curdate(), 1) AND e.start <= now() + INTERVAL 12 HOUR AND e.estatus = 'pendiente' ";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		$query .= " group by p.id order by e.start asc";
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		while($row = $result->fetch_assoc()){
			if($row['tiempo_retardo'] > 0){
				$tiemporetraso = $row['tiempo_retardo'];
			}else{
				$tiemporetraso = '<span class="red">'.$row['tiempo_retardo'].'</span>';
			}
			$response['data'][] = array(
				'cedula'		    =>	$row['cedula'],
				'paciente'     	    =>	$row['paciente'],
				'fecha_inicio' 	    =>	$row['fecha_inicio'],
				'recurso' 	        =>	$row['recurso'],
				'tiempo_retardo'    =>  $tiemporetraso,
				'fecha_fin'         =>  $row['fecha_fin'],
				'estatus'           =>	$row['estatus'],
				'acciones' 	    => '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<!--a class="dropdown-item" href="#">Abrir Teleconsulta</a-->
							<!--a class="dropdown-item" href="#">Chatear con Paciente</a-->
							<!--a class="dropdown-item" href="#">Enviar a Referente</a-->
						</div>
					</div>'
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}	

	function tabla_turnos_cancelados(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT r.answer, d.id as id, p.cedula, p.nombre as paciente,d.tipo as tipo_visita, DATE_FORMAT(d.start, '%Y-%m-%d %h:%i:%p' ) as fecha_inicio, DATE_FORMAT(d.end, '%Y-%m-%d %h:%i:%p' ) as fecha_fin, u.nombre as recurso , d.estatus, b.sentencia as json_detalle
				FROM eventoscalendario d
                INNER JOIN bitacora b ON d.id = b.idregistro
				LEFT JOIN usuarios u ON u.id = d.recurso 
                LEFT JOIN pacientes p ON p.id = d.idpaciente
                LEFT JOIN availability_requests r ON d.id=r.event_id
				LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
                WHERE  b.modulo ='AGENDA-INSTRUCCIONES' AND d.start  >= now() - INTERVAL 1 DAY";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		$query .= " group by p.id";
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		while($row = $result->fetch_assoc()){	
			$data = (explode(",",$row['json_detalle']));
			$recurso = explode(":",$data[2]);

			$response['data'][] = array(
				'cedula'		    =>	$row['cedula'],
				'paciente'     	    =>	$row['paciente'],
				'tipo_visita' 	    =>	$row['tipo_visita'],
				'fecha_inicio' 	        =>	$row['fecha_inicio'],
				'fecha_fin'         =>  $row['fecha_fin'],
				'recurso'         =>  $row['recurso'],
				'estatus'           =>	$row['estatus'],
				'detalles'     => $row['json_detalle'],
				'cancela'      => $recurso[1],
				'acciones' 	    => '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item detalles" data-id='.$row['id'].' data-toggle="modal"  data-target="#exampleModalpopover">Detalles</a>
						</div>
					</div>'
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}	
	
	function nivel(){
		global $mysqli;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$query = "select nivel from usuarios where id = $id_usuario";
		$result = $mysqli->query($query); 
		$row = $result->fetch_assoc();
		
		echo $row['nivel'];
	}
	
	function tabla_pacientes_covid(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT DISTINCT p.id as idpaciente,p.cedula, mcp.nombre as condicion, p.nombre as paciente, TIMESTAMPDIFF(YEAR, p.fechanac, CURRENT_DATE) AS edad,concat(m.nombre,' ',m.apellido) as medico
		           FROM pacientes p 
                   INNER JOIN pacienteenfermedadesactuales pe ON pe.idpaciente = p.id
                   INNER JOIN enfermedades e ON e.id = pe.idenfermedad
				   LEFT JOIN hospitalizacion h ON p.id = h.paciente
				   LEFT JOIN maestro_condicion_paciente mcp ON p.condicion_id = mcp.id
				   left JOIN medicos m ON m.id = p.idmedicotratante
				   LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
                   WHERE e.codigo  = 'U071' AND h.estatus = 'activa'"; 
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		while($row = $result->fetch_assoc()){
			if($row['medico'] != ''){
				$medico = $row['medico'];
			}else{
				$medico = '-';
			}
			
			if($row['condicion'] != ''){
				$condicion = $row['condicion'];
			}else{
				$condicion = '-';
			}
			$idpaciente = $row['idpaciente'];
			$response['data'][] = array(
				'cedula'		    =>	$row['cedula'],
				'paciente'     	    =>	$row['paciente'],
				'edad' 	        =>	$row['edad'].' Años',
				'medico' 	        =>	$medico,
				'condicion' 	        =>	$condicion,
				'acciones' 	    => '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">Agendar teleconsulta</a>
							<a class="dropdown-item" href="historial.php?idpaciente='.$idpaciente.'" >Ver Historial</a>
							<!--a class="dropdown-item" href="#">Chatear con Paciente</a-->
							<!--a class="dropdown-item" href="#">Enviar a Referente</a-->
						</div>
					</div>'
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
	function tabla_pacientes_activos(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT DISTINCT p.id as idpaciente,p.cedula, mcp.nombre as condicion, p.nombre as paciente, TIMESTAMPDIFF(YEAR, p.fechanac, CURRENT_DATE) AS edad,concat(m.nombre,' ',m.apellido) as medico
		           ,GROUP_CONCAT(e.nombre) as enfermedades
				   FROM pacientes p 
                   INNER JOIN pacienteenfermedadesactuales pe ON pe.idpaciente = p.id
                   INNER JOIN enfermedades e ON e.id = pe.idenfermedad
				   LEFT JOIN hospitalizacion h ON p.id = h.paciente
				   LEFT JOIN maestro_condicion_paciente mcp ON p.condicion_id = mcp.id
				   left JOIN medicos m ON m.id = p.idmedicotratante
				   LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
                   WHERE 1 AND h.estatus = 'activa' and e.codigo  != 'U071'"; 
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		$query .= " group by p.id";
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		while($row = $result->fetch_assoc()){
			if($row['medico'] != ''){
				$medico = $row['medico'];
			}else{
				$medico = '-';
			}
			
			if($row['condicion'] != ''){
				$condicion = $row['condicion'];
			}else{
				$condicion = '-';
			}
			$idpaciente = $row['idpaciente'];
			$response['data'][] = array(
				'cedula'		    =>	$row['cedula'],
				'paciente'     	    =>	$row['paciente'],
				'enfermedades'     	    =>	$row['enfermedades'],
				'edad' 	        =>	$row['edad'].' Años',
				'medico' 	        =>	$medico,
				'condicion' 	        =>	$condicion,
				'acciones' 	    => '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							
							<a class="dropdown-item" href="historial.php?idpaciente='.$idpaciente.'" >Ver Historial</a>
							<!--a class="dropdown-item" href="#">Chatear con Paciente</a-->
							<!--a class="dropdown-item" href="#">Enviar a Referente</a-->
						</div>
					</div>'
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
    
    function tabla_tratamientos_editados(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		
		$query  = "SELECT DISTINCT t.id as idreporte,DATE_FORMAT(t.fecha, '%Y-%m-%d %h:%i:%p') AS fecha_actualizacion_tarjeta, 
					p.nombre AS paciente, (case when t.estatus = 1 then 'Activo' when t.estatus = 0 then 'Inactivo' end) as estado, u.nombre as creado_por
	        	   FROM  tarjetamedicamento t 
	               INNER JOIN pacientes p ON p.id = t.idpaciente
                   INNER JOIN usuarios u ON u.id = t.creadopor
				    LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
	               WHERE t.fecha >= now() - INTERVAL 1 DAY  AND t.estatus <> '2'";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		$diagnostico = '';
		while($row = $result->fetch_assoc()){
			if($row['diagnosticos'] != ''){
				$separada = explode(',', $row['diagnosticos']);
				foreach($separada as $id){
					$diagn = getValor('nombre','enfermedades',$id);
					$diagnostico = ' '.$diagn.',';
				}
				$diagnostico = substr($diagnostico, 0, -1);	
			}else{
				$diagnostico = '-';
			}
			if($row['estado'] == 'Inactivo'){
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item imprimir" data-id='.$row['idreporte'].'>Imprimir</a>
							<a class="dropdown-item activar" data-id='.$row['idreporte'].'>Activar</a>
						</div>
					</div>';
			}else{
				$acciones = '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="controller/reporte_tarjeta_medicamentos.php?id='.$row['idreporte'].'"   target="_blank">Imprimir</a>
						</div>
					</div>';
			}
			
			$response['data'][] = array(
				'paciente'     	    =>	$row['paciente'],
				'fecha_actualizacion_tarjeta' 	        =>	$row['fecha_actualizacion_tarjeta'],
				'estado' => $row['estado'],
				'creado_por' => $row['creado_por'],
				'acciones' 	    => $acciones
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
	
	function tabla_tratamientos_finalizados(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		$idpaciente = $_REQUEST['idpaciente'];
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = " SELECT DISTINCT u.nombre as recurso, td.idtarjeta, t.fecha AS fechatarjeta, td.idmedicamento, td.via, td.frecuencia, td.dosis, td.fecha_inicio, td.fecha_fin, 
                	td.observaciones, td.estado, p.nombre AS paciente, m.nombre AS nombremedicamento 
                	FROM tarjetamedicamentodetalle td
                	INNER JOIN tarjetamedicamento t ON t.id = td.idtarjeta
                	INNER JOIN pacientes p ON p.id = t.idpaciente
					  LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
                	INNER JOIN  medicamentos m ON m.id = td.idmedicamento
                    inner join usuarios u on t.creadopor = u.id
                	WHERE 1";
		
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		
		if($idpaciente != ''){
			$query = " SELECT t.id as idtarjeta,t.fecha, u.nombre as creador,t.estatus as estado, t.tarjeta_simple
				FROM tarjetamedicamento t
				LEFT JOIN usuarios u ON u.id = t.creadopor
				WHERE t.idpaciente = '$idpaciente' and t.estatus <> 0 ORDER BY estado asc limit 5";
			
		}else{
			$query .= " AND fecha_fin = (SELECT DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) AND td.estado = '1' AND t.estatus = '1'";
		}
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		while($row = $result->fetch_assoc()){
				if($idpaciente != ''){
					if($row['tarjeta_simple'] == '1'){
						$tipotarjeta = "Simple";
					}else{
						$tipotarjeta = "Completa";
					}
					if($row['estado'] == '0'){
							$estado = 'Inactivo';
					}else if($row['estado'] == 2){
							$estado = 'Finalizado';
					}else{
						$estado = 'Activo';
					}
					$acciones = '<div class="dropdown ml-auto text-center">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item imprimir_tarjeta" target="_blank" href="http://pmc.vitae-health.com/controller/reporte_tarjeta_medicamentos.php?id='.$row['idtarjeta'].'" data-id='.$row['idtarjeta'].'>Imprimir</a>
						</div>
					</div>';
				}else{
					$acciones = '<div class="dropdown ml-auto text-center">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">Abrir Teleconsulta</a>
							<a class="dropdown-item" href="#">Chatear con Paciente</a>
							<a class="dropdown-item" href="#">Enviar a Referente</a>
						</div>
					</div>';
				}
				
				if($idpaciente != ''){
					$response['data'][] =array(
							'acciones'=>$acciones,
							'fecha'=>$row['fecha'],
							'creador'=>$row['creador'],
							'estado'=> $estado,
							'tipotarjeta'=>$tipotarjeta
						);
				}else{
					$response['data'][] = array(
						'paciente'     	    =>	$row['paciente'],
						'nombremedicamento' =>	$row['nombremedicamento'],
						'via' 	            =>	$row['via'],
						'frecuencia' 	    =>	$row['frecuencia'],
						'dosis' 	        =>	$row['dosis'],
						'recurso' 	        =>	$row['recurso'],
						'estado' 	        =>	$row['estado'],
						'fecha_inicio' 	    =>	$row['fecha_inicio'],
						'fecha_fin' 	    =>	$row['fecha_fin'],
						'observaciones' 	=>	$row['observaciones'],
						'acciones' 	    => $acciones
					);
					}
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
	
	function tabla_planes(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$idpaciente = $_REQUEST['idpaciente'];
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT DISTINCT p.id as idpaciente,t.id as idreporte,DATE_FORMAT(t.fecha, '%Y-%m-%d %h:%i:%p') AS fecha_actualizacion_tarjeta, 
					p.nombre AS paciente, (case when t.estatus = 1 then 'Activo' when t.estatus = '2' then 'Finalizado' 
					when t.estatus is null then 'Inactivo' end) as estado, u.nombre as creado_por 
					FROM plancuidado t 
					INNER JOIN pacientes p ON p.id = t.idpaciente
					LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
					INNER JOIN usuarios u ON u.id = t.creadopor 
					WHERE 1 ";
					
		if($idpaciente != ''){
			$query .= " AND idpaciente = $idpaciente ";
			
		}else{
			$query .= " AND t.fecha >= now() - INTERVAL 1 DAY"; 
		}
		
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico order by estado asc";
		}
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	//	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		$diagnostico = '';
		
		while($row = $result->fetch_assoc()){
			if($row['diagnosticos'] != ''){
				$separada = explode(',', $row['diagnosticos']);
				foreach($separada as $id){
					$diagn = getValor('nombre','enfermedades',$id);
					$diagnostico = ' '.$diagn.',';
				}
				$diagnostico = substr($diagnostico, 0, -1);	
			}else{
				$diagnostico = '-';
			}
			if($row['estado'] == 'Inactivo'){
				$acciones = '<div class="dropdown ml-auto text-center">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item imprimir_plan" data-id='.$row['idreporte'].'>Imprimir</a>
							<a class="dropdown-item activar_plan" data-id='.$row['idreporte'].'>Activar</a>
						</div>
					</div>';
			}else{
				$acciones = '<div class="dropdown ml-auto text-center">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="controller/reporte_planes.php?id='.$row['idreporte'].'&idpaciente='.$row['idpaciente'].'"   target="_blank">Imprimir</a>
						</div>
					</div>';
			}
			
			$response['data'][] = array(
				'paciente'     	    =>	$row['paciente'],
				'fecha_actualizacion_tarjeta' 	        =>	$row['fecha_actualizacion_tarjeta'],
				'estado' => $row['estado'],
				'creado_por' => $row['creado_por'],
				'acciones' 	    => $acciones
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
	
	function evidencias(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		$id = $_REQUEST['id'];
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT i.id, (CASE i.type WHEN 'OTHER' THEN 'Otro' WHEN 'VITAL_SIGNS' THEN 'Signos vitales' WHEN 'VISIT' THEN 'Visita' WHEN 'PATHWAY_OBSTRUCTED' THEN 'Vía obstruida' END) as tipo,
					i.body as incidente,i.created_at as fecha, (CASE i.priority WHEN 'LOW' THEN 'Baja' WHEN 'HIGHT' THEN 'Alta' WHEN 'MEDIUM' THEN 'Media' END) as prioridad,
					IFNULL(GROUP_CONCAT(f.path),0) as evidencias,u.nombre as recurso,p.nombre as paciente,p.id as idpaciente
					FROM incidents i
					INNER JOIN visitas v ON v.id = i.visit_id
					INNER JOIN eventoscalendario e ON e.idgcal = v.idgcal
					INNER JOIN usuarios u ON u.id = v.idusuario
					INNER JOIN pacientes p on p.id = v.idpaciente
					LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
					LEFT JOIN files f ON f.fileable_id = i.id and f.fileable_type like '%Incident%'
					WHERE 1 and i.id = $id";
					
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		
		$query .=  " group by p.id";
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	    //	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		function checkExternalFile($url){
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
        
            return $retCode;
        }
        
        $adjuntos = '';
		while($row = $result->fetch_assoc()){
		    //ADJUNTOS
			$ruta		= $row['evidencias'];
			$arrruta = explode(',',$ruta);
			
			foreach($arrruta AS $rutaimg){
			    $ruta2       = str_replace( 'homecare','https://homecare.vitae-health.com',$rutaimg);
                $fileExists = checkExternalFile($ruta2);
                //echo $fileExists;
                if($fileExists == 200){
                    $adjuntos  .= ' <div class="carousel-item active">
                                            <img class="d-block w-100" src="'.$ruta2.'" alt="First slide">
                                        </div> ';
                }
			}
			
			$response['data'][] = array(
				'evidencias'  	=>	$adjuntos,
			);
		}
		
		echo json_encode($response);
	}
	
	function tabla_incidentes(){
		global $mysqli;
		$data   = (!empty($_REQUEST['data']) ? $_REQUEST['data'] : '');
		$where = array();
		
		$draw = $_REQUEST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
		$orderByColumnIndex  = $_REQUEST['order'][0]['0'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy = 0;//$_REQUEST['id'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType = "DESC";//$_REQUEST['order'][0]['dir']; // ASC or DESC
	    $start   = (!empty($_REQUEST['start']) ? $_REQUEST['start'] : 0);	
		$length   = (!empty($_REQUEST['length']) ? $_REQUEST['length'] : 10);
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = "SELECT i.id,v.id as idvisita, (CASE i.type WHEN 'OTHER' THEN 'Otro' WHEN 'VITAL_SIGNS' THEN 'Signos vitales' WHEN 'VISIT' THEN 'Visita' WHEN 'PATHWAY_OBSTRUCTED' THEN 'Vía obstruida' END) as tipo,
					i.body as incidente,DATE_FORMAT(i.created_at, '%Y-%m-%d %h:%i:%p' ) as fecha, (CASE i.priority WHEN 'LOW' THEN 'Baja' WHEN 'HIGHT' THEN 'Alta' WHEN 'MEDIUM' THEN 'Media' END) as prioridad,
					IFNULL(GROUP_CONCAT(f.path),'') as evidencias,u.nombre as recurso,p.nombre as paciente,p.id as idpaciente
					FROM incidents i
					INNER JOIN visitas v ON v.id = i.visit_id
					INNER JOIN eventoscalendario e ON e.idgcal = v.idgcal
					INNER JOIN usuarios u ON u.id = v.idusuario
					INNER JOIN pacientes p on p.id = v.idpaciente
					LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
					LEFT JOIN files f ON f.fileable_id = i.id and f.fileable_type like '%Incident%'
					WHERE 1 ";

		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}
		
		$query .=  " group by p.id order by i.created_at desc";
		
		$hayFiltros = 0;
		for($i=0 ; $i<count($_REQUEST['columns']);$i++){
			$column = $_REQUEST['columns'][$i]['data'];//we get the name of each column using its index from POST request
			if ($_REQUEST['columns'][$i]['search']['value']!="") {
				
				$campo = $_REQUEST['columns'][$i]['search']['value'];
				$campo = str_replace('^','',$campo);
				$campo = str_replace('$','',$campo);
				
				$where[] = " $column like '%".$campo."%' ";
				
				$hayFiltros++;
			}
		}
		
		if ($hayFiltros > 0)
			$where = " AND ".implode(" AND " , $where)." ";// id like '%searchValue%' or name like '%searchValue%'
		else
			$where = "";
		$query  .= $where;
		$result = $mysqli->query($query); 
		$recordsTotal = $result->num_rows;
		//debug($query);
	    //	$query  .= " ORDER BY nombre ASC LIMIT $start, $length ";
		$response = array();
		$response['data'] = array();
		$result = $mysqli->query($query);
		$recordsFiltered = $result->num_rows;
		
		function checkExternalFile($url){
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
        
            return $retCode;
        }
        
        $adjuntos = '';
		while($row = $result->fetch_assoc()){
		    //ADJUNTOS
			$ruta		= $row['evidencias'];
			$arrruta = explode(',',$ruta);
			
			foreach($arrruta AS $rutaimg){
			    $ruta2       = str_replace( 'homecare','https://homecare.vitae-health.com',$rutaimg);
                $fileExists = checkExternalFile($ruta2);
                //echo $fileExists;
                if($fileExists == 200){
                    $adjuntos  .= " <a href='".$ruta2."' target='_blank'><span class='blue fa fa-picture-o fa-2x'style='text-align:center;'></span></a> ";
                }
			}
        	$idpaciente = $row['idpaciente'];
			if($row['evidencias'] != ''){
				$evidencias = '<button type="button" class="btn btn-primary mb-2 evid" data-toggle="modal" data-id="'.$row['id'].'" data-target="#exampleModalCenter">Evidencias</button>';
			}else{
				$evidencias = 'No tiene evidencias';
			}
			$response['data'][] = array(
				'id'    	    =>	$row['id'],
				'tipo'          =>	$row['tipo'],
				'incidente'     =>	$row['incidente'],
				'fecha' 	    =>	$row['fecha'],
				'prioridad' 	=>	$row['prioridad'],
				'recurso' 	    =>	$row['recurso'],
				'paciente' 	    =>	$row['paciente'],
				'adjuntos'   	=>	$evidencias,
				'acciones' 	    => '<div class="dropdown ml-auto text-right">
						<div class="btn-link" data-toggle="dropdown">
							<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
						</div>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item"href="historial.php?idpaciente='.$idpaciente.'">Historial</a>
																 
															   
						</div>
					</div>'
			);
		}
		$response['draw'] = intval($draw);
		$response['recordsTotal'] = intval($recordsTotal);
		$response['recordsFiltered'] = intval($recordsTotal);
		echo json_encode($response);
	}
	
	function incidentes_notificacion(){
		global $mysqli;
		$start	= 0;
		$limit	= 5;

		$incidente	= (!empty($_REQUEST['incidente']) ? $_REQUEST['incidente'] : '');
		$fecha  	= (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		$nivel_usuario = isset($_COOKIE['nivel'])?$_COOKIE['nivel']:0;
		$id_usuario = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:0;
		$idmedico =  isset($_COOKIE['id_medico'])?$_COOKIE['id_medico']:0;
		$query  = " SELECT i.id, (CASE i.type WHEN 'OTHER' THEN 'Otro' WHEN 'VITAL_SIGNS' THEN 'Signos vitales' WHEN 'VISIT' THEN 'Visita' WHEN 'PATHWAY_OBSTRUCTED' THEN 'Vía obstruida' END) as tipo,
					i.body as incidente,DATE(i.created_at) as fecha, (CASE i.priority WHEN 'LOW' THEN 'Baja' WHEN 'HIGHT' THEN 'Alta' WHEN 'MEDIUM' THEN 'Media' END) as prioridad,
					IFNULL(GROUP_CONCAT(f.path),0) as evidencias,u.nombre as recurso,p.nombre as paciente
					FROM incidents i
					INNER JOIN visitas v ON v.id = i.visit_id
					INNER JOIN eventoscalendario e ON e.idgcal = v.idgcal
					INNER JOIN usuarios u ON u.id = v.idusuario
					INNER JOIN pacientes p on p.id = v.idpaciente
					LEFT JOIN paciente_usuario pu on pu.paciente_id = p.id
					LEFT JOIN files f ON f.fileable_id = i.id and f.fileable_type like '%Incident%'
					WHERE 1
					";
		if($nivel_usuario == 5 || $nivel_usuario == 2){
			$query .= " and p.idmedicotratante  = $idmedico";
		}			
		$query .= " group by i.id order by i.created_at desc";
		//echo $query;
		$result = $mysqli->query($query);
		$salidahtml = '';
		while($row = $result->fetch_assoc()){
		//	$imagen = 'logo.png';
		$prioridad = $row['prioridad'];
			if($prioridad == 'Alta'){
				$iconestado = '<i class="red fa fa fa-circle fa-2x float-left"></i>';
			}elseif($prioridad == 'Media'){
				$iconestado = '<i class="orange fa fa fa-circle fa-2x float-left"></i>';
			}elseif($prioridad == 'Baja'){
				$iconestado = '<i class="green fa fa fa-circle fa-2x float-left"></i>';
			}else{
				$iconestado = '<i class="color-red-dark fa fa fa-circle fa-2x float-left"></i>';
			}
            $salidahtml .= '
			<li><a href="#tabla_incidentes" name="incidentesC">
			<div class="timeline-panel">
			<div class="media mr-2">
			'.$iconestado.'
			</div>
			<div class="media-body">
			<h8 class="mb-1"><b>'.$row['paciente'].'</b> ha presentado una nueva incidencia con prioridad '.$row['prioridad'].' <i class="fa fa-info-circle tool_tip " data-toggle="tooltip" title="'.$row['incidente'].'"></i></h8>
			<small class="d-block">Fue reportado el '.$row['fecha'].' por '.$row['recurso'].'</br></small>
			</div>
			</div>
			</a>
			</li>';
		}
		echo $salidahtml;
	}
    
    function chat_usuarios(){
		global $mysqli;
	    //	$start	= 0;
	    //	$limit	= 5;

	    //	$incidente	= (!empty($_REQUEST['incidente']) ? $_REQUEST['incidente'] : '');
	    //	$fecha  	= (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		
		$query  = " SELECT i.patient_id AS id, i.id AS idsala, p.nombre AS paciente, i.created_at AS fechacreacion, i.updated_at AS fechareciente
					FROM threads i
					INNER JOIN pacientes p ON p.id = i.patient_id
					WHERE i.updated_at >= now() - INTERVAL 1 DAY GROUP BY paciente
					";
					
	    //	$query .= " LIMIT ".$start.", ".$limit;
		//echo $query;
		$result = $mysqli->query($query);
		$salidahtml = '';
		while($row = $result->fetch_assoc()){
		    $arrpaciente = explode(' ', $row['paciente']);
		    $letra = substr($arrpaciente[0], 0, 1).''.substr($arrpaciente[1], 0, 1);
            $salidahtml .= '
			<li class="active dz-chat-user" data-id='.$row['id'].' data-idsala='.$row['idsala'].'>
    			<div class="d-flex bd-highlight">
        			<div class="">
        			    <div class="img_cont primary">'.$letra.'</div>
        			    <!--
            			<span class="badge badge-circle-chat badge-success">'.$letra.'</span>
            			<span class="online_icon"></span>
            			-->
        			</div>
        			<div class="user_info"> 
            			<span class="pn'.$row['id'].'">'.$row['paciente'].'</span>
            			<p>En línea</p>
        			</div>
        		</div>
    		</li>';
		}
		echo $salidahtml;
	}
	
	function chat_usuarios_detalles(){
		global $mysqli;
	//	$start	= 0;
	//	$limit	= 5;

		$id	= (!empty($_REQUEST['id']) ? $_REQUEST['id'] : '');
		$idsala	= (!empty($_REQUEST['idsala']) ? $_REQUEST['idsala'] : '');
		$idusuarios = $_SESSION['user_id'];
	    //$fecha  	= (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		
		$query  = " SELECT i.id, i.thread_id AS idsala, i.updated_at AS fechaactualizada, u.nombre as enfermero, i.body AS mensaje, i.user_id
                    FROM messages i 
                    INNER JOIN usuarios u ON i.user_id = u.id
                    WHERE (i.created_at >= now() - INTERVAL 1 DAY OR i.updated_at >= now() - INTERVAL 1 DAY) AND i.thread_id = $idsala 
		";
		//m.created_at >= now() - INTERVAL 1 DAY AND			
	    //$query .= " LIMIT ".$start.", ".$limit;
	    //echo $query;
		$result = $mysqli->query($query);
		$mensaje= '';
		while($row  = $result->fetch_assoc()){
		    $arrenfermero = explode(' ', $row['enfermero']);
		    $letra = substr($arrenfermero[0], 0, 1).''.substr($arrenfermero[1], 0, 1);
		    
		    if($idusuarios == $row['user_id']){
		        $mensaje .= '<div class="d-flex justify-content-end mb-4">
    							<div class="msg_cotainer_send" data-idsala='.$row['idsala'].'>
    								'.$row['mensaje'].'
    								<span class="msg_time">'.$row['fechaactualizada'].'</span>
    							</div>
    							<div class="">
    						        <div class="img_cont primary">'.$letra.'</div>
    							</div>
    						</div>';
		    }else if($row['enfermero'] == 'Mensaje del Sistema'){
		        $mensaje .= '<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="images/logo.png" class="rounded-circle user_img_msg" alt="">
								</div>
								<div class="msg_cotainer msg_container_sistema" data-idsala='.$row['idsala'].'>
								    <h6 class="mb-1" name="'.$idusuarios.'-'.$row['user_id'].'">'.$row['enfermero'].'</h6>
								    '.$row['mensaje'].'
									<span class="msg_time">'.$row['fechaactualizada'].'</span>
								</div>
							</div>';
		    }else{
		         $mensaje .= '<div class="d-flex justify-content-start mb-4">
								<div class="" data-idsala='.$row['idsala'].'>
									<div class="img_cont primary">'.$letra.'</div>
								</div>
								<div class="msg_cotainer">
								    <h6 class="mb-1" name="'.$idusuarios.'-'.$row['user_id'].'">'.$row['enfermero'].'</h6>
								    '.$row['mensaje'].'
									<span class="msg_time">'.$row['fechaactualizada'].'</span>
								</div>
							</div>';
		    }
		}
		echo $mensaje;
	}
	
	function chat_mensajes_enviar(){
		global $mysqli;
		
		$idsala	= (!empty($_REQUEST['idsala']) ? $_REQUEST['idsala'] : '');
		$body	= (!empty($_REQUEST['body']) ? $_REQUEST['body'] : '');
		$idusuarios = $_SESSION['user_id'];
	    //$fechacreacion  	= (!empty($_REQUEST['fecha']) ? $_REQUEST['fecha'] : '');
		
		$query 	= '	INSERT INTO	messages (body,type,user_id,thread_id,created_at) VALUES ( "'.$body.'", "Normal", "'.$idusuarios.'", "'.$idsala.'", NOW() ) ';		
		$result = $mysqli->query($query);
		if($result == true){
		    $id = $mysqli->insert_id;
			echo $id;
		}else{
			echo 0;
		}
    }
    
    function listado_notas(){
		global $mysqli;
		
		$query  = " select v.id AS idvisita, v.idpaciente AS id, e.start AS fecha, u.nombre AS recurso, p.nombre AS paciente, v.comentario 
		            from visitas v 
		            inner join eventoscalendario e ON e.idgcal = v.idgcal 
		            inner join pacientes p ON p.id = v.idpaciente 
		            INNER JOIN usuarios u ON u.id = v.idusuario 
		            where v.fecha >= now()- INTERVAL 1 DAY AND v.comentario != ''
					";
					
	    //$query .= " LIMIT ".$start.", ".$limit;
		//echo $query;
		$result = $mysqli->query($query);
		$salidahtml = '';
		while($row = $result->fetch_assoc()){
		    $arrpaciente = explode(' ', $row['paciente']);
		    $letra = substr($arrpaciente[0], 0, 1).''.substr($arrpaciente[1], 0, 1);
            $salidahtml .= '
			<li class="active dz-nota-user" data-id='.$row['id'].' data-idvisita='.$row['idvisita'].'>
    			<div class="d-flex bd-highlight">
        			<div class="img_cont info">'.$letra.'</div>
        			<div class="user_info"> 
            			<span class="pnota'.$row['id'].'">'.$row['paciente'].'</span>
            			<p>Ver nota</p>
        			</div>
        		</div>
    		</li>';
		}
		echo $salidahtml;
	}
    
    function notas_detalles(){
		global $mysqli;
	    //$start	= 0;
	    //$limit	= 5;
	
	    $id     	= (!empty($_REQUEST['id']) ? $_REQUEST['id'] : '');
		$idvisita	= (!empty($_REQUEST['idvisita']) ? $_REQUEST['idvisita'] : '');
		$idusuarios = $_SESSION['user_id'];
		
		$query  = " select v.id as idvisita,e.start as fecha, u.nombre as recurso, p.nombre as paciente, v.comentario 
		            from visitas v 
		            inner join eventoscalendario e ON e.idgcal = v.idgcal 
		            inner join pacientes p ON p.id = v.idpaciente 
		            INNER JOIN usuarios u ON u.id = v.idusuario 
		            where v.fecha >= now()- INTERVAL 1 DAY AND v.comentario != '' AND p.id = ".$id."
		";
		//m.created_at >= now() - INTERVAL 1 DAY AND			
	    //$query .= " LIMIT ".$start.", ".$limit;
	    //echo $query;
		$result = $mysqli->query($query);
		$mensaje= '<ul class="contacts">';
		while($row  = $result->fetch_assoc()){
		    $mensaje .= '<li>
							<div class="d-flex justify-content-start">
								<div class="">
									<strong>'.$row['recurso'].'</strong>
									<p class="mb-0">'.$row['comentario'].'</p>
									<p class="text-info text-right mb-0">'.$row['fecha'].'</span>
								</div>
							</div>
						</li>';
		    
		}
		$mensaje .= '</ul>';
		echo $mensaje;
	}
	
	function top_colaboradores(){
		global $mysqli;
		
		$query  = " select u.nombre as recurso, COUNT(v.id) as visitas from visitas v 
		            INNER JOIN usuarios u ON u.id = v.idusuario 
		            where 1 GROUP BY v.idusuario ORDER BY visitas DESC
					";
	    //$query .= " LIMIT ".$start.", ".$limit;
		//echo $query;
		$result = $mysqli->query($query);
		$salidahtml = '';
		while($row = $result->fetch_assoc()){
		    $arrrecurso = explode(' ', $row['recurso']);
		    $letra = substr($arrrecurso[0], 0, 1).''.substr($arrrecurso[1], 0, 1);
		    
		    $salidahtml .= '
    			<div class="items">
        			<div class="text-center">
        			    <div class="alert alert-light fs-34 font-w600 mb-0">'.$letra.'</div>
        			    <!--<div class="dr-star"><i class="las la-star"></i> 4.2</div>-->
        			    <h5 class="fs-16 mb-1 font-w600"><p class="text-black">Lic. '.$row['recurso'].'.</p></h5>
        			    <span class="text-primary mb-2 d-block">Colaborador</span>
        			    <p class="fs-15 font-w600 text-black">Total visitas: '.$row['visitas'].' </p>
        			</div>
    			</div>';
		}
		echo $salidahtml;
	}
		
	function activar_medicamento(){
		global $mysqli;
		$idreporte = $_REQUEST['id'];
		$url = 'tarjeta_medicamentos_editar.php?id='.$idreporte.'%';
		$queryrestriccion = " SELECT a.*, u.nombre FROM accion_usuario a INNER JOIN usuarios u ON u.id = a.idusuario WHERE url like '$url' LIMIT 1";
		//die($queryrestriccion);
		$resultrestriccion = $mysqli->query($queryrestriccion);
			if($resultrestriccion->num_rows > 0){
				$row = $resultrestriccion->fetch_assoc();
				
				echo $row['nombre'];
			}else{
				$idpaciente = getValor('idpaciente','tarjetamedicamento',$idreporte);
		$query1 ="UPDATE tarjetamedicamento SET estatus = '1' WHERE id ='".$idreporte."';";
		$query2 ="UPDATE tarjetamedicamento SET estatus = '2' WHERE estatus = '1' and idpaciente = '$idpaciente';";		
			if($r = $mysqli->query($query2)){
				if($r = $mysqli->query($query1)){
					$salida = 1;
				}else{
					$salida = 0;				
				}
			}else{
				$salida = 0;				
			}
		if($salida == 1)
			echo 1;
		else
			die($mysqli->error);
			}	
	}
	
	function activar_plan(){
		global $mysqli;
		$idreporte = $_REQUEST['id'];
		$url = 'plan_cuidado_editar.php?id='.$idreporte.'%';
		$queryrestriccion = " SELECT a.*, u.nombre FROM accion_usuario a INNER JOIN usuarios u ON u.id = a.idusuario WHERE url like '$url' LIMIT 1";
		//die($queryrestriccion);
		$resultrestriccion = $mysqli->query($queryrestriccion);
			if($resultrestriccion->num_rows > 0){
				$row = $resultrestriccion->fetch_assoc();
				
				echo $row['nombre'];
			}else{
			
			$query_a = "UPDATE plancuidado SET estatus = 1 WHERE id = '$idreporte'";			
			if($mysqli->query($query_a)){
				echo 1;
			}else{				
				die($mysqli->error);
			}

			}	
	}
	
?>