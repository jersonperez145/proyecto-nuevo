<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
	verificarLogin();
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - PHM </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" type="text/css">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Ajustes -->
    <link href="./css/ajustes.css<?php autoVersiones().''.$nombre; ?>" rel="stylesheet">

    <!-- Material color picker -->
    <link href="./vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Pick date -->
    <link rel="stylesheet" href="./vendor/pickadate/themes/default.css">
    <link rel="stylesheet" href="./vendor/pickadate/themes/default.date.css">
	
	<link rel="stylesheet" href="./vendor/select2/css/select2.min.css">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
	
	<!--link href="./vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet"-->
	
	
	<style>
		#table-lista-items * {
			padding: 2px !important;
			font-size: 12px !important;
		}
		.card{
			height: auto !important;
		}
		
		.table#tabla-tarjeta-medicamentos th, .table#tabla-plan-cuidado th{
			padding: 0 !important;
		}
		
		.table#tabla-tarjeta-medicamentos td, .table#tabla-plan-cuidado td{
			padding: 0 !important;
		}
		
		.table#tabla-tarjeta-medicamentos thead th, .table#tabla-plan-cuidado thead th{
			font-size: 11px !important;
		}
		
		.table#tabla-tarjeta-medicamentos tbody td, .table#tabla-plan-cuidado tbody td{
			font-size: 12px !important;
		}
	</style>
	
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader" style="z-index:1000000;">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper" class="show menu-toggle">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="#top" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Chat box start
        ***********************************-->
		<div class="chatbox">
			<div class="chatbox-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#notes">Notas</a>
					</li><!--
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#alerts">Alertas</a>
					</li>-->
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#chat">Chat</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="chat" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box">
							<div class="card-header">
								<div>
									<h6 class="mb-1">Lista de Chats</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body">
								<ul class="contacts" id="chat_usuarios">
								</ul>
							</div>
						</div>
						<div class="card chat dz-chat-history-box d-none" id="sala_chat">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-chat-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Chat con <span id="chatcon"></span></h6>
									<p class="mb-0 text-success">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3">
								<div id="chat_enfermero"></div>
							</div>
							<div class="card-footer type_msg">
								<div class="input-group">
								    <input type="hidden" class="form-control" name="idsala" id="idsala" autocomplete="off">
									<textarea class="form-control" id="body" placeholder="Escribir Mensaje..."></textarea>
									<div class="input-group-append">
										<button type="button" class="btn btn-primary" id="boton-enviar-mensaje"><i class="fa fa-location-arrow"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="notes">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-nota-user-box">
							<div class="card-header">
								<div>
                                      <h6 class="mb-1">Lista de Notas</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Notas_Body">
								<ul class="contacts" id="listado_notas">
								</ul>
							</div>
						</div>
						<div class="card chat dz-nota-history-box d-none" id="sala_nota">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-nota-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Nota - <span id="notacon"></h6>
									<p class="mb-0 text-info">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Notas_Body3">
								<div id="nota_detalle"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Chat box End
        ***********************************-->
        
        <!--**********************************
            Configuración start
        ***********************************-->
		<div class="config">
			<div class="config-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#filtrosconfig">Filtros</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="filtrosconfig" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0">
							<div class="card-header d-none">
								<div>
                                      <h6 class="mb-1">Filtros</h6>
								</div>
							</div>
							<div class="card-body p-0 dz-scroll" id="DZ_W_Filtros_Body">
								<div class="form-config">
                                    <form>
                                        <div class="d-block my-3">
                                             <div class="custom-control custom-radio mb-2">
                                                <input id="evitae" name="empresaConfig" type="radio" class="custom-control-input" checked="" required="">
                                                <label class="custom-control-label" for="evitae">Vitae</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="emapfre" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="emapfre">MAPFRE</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="enestle" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="enestle">Nestlé</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="ebgm" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="ebgm">BGM</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="eemg" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="eemg">EMG</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Configuración End
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header" name="top">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <a  href="#top">
							<a href="#" class="btn btn-primary ir-arriba"><i class="las la-arrow-up"></i></a>
                            <div class="dashboard_bar">
                                PHM - <span id="txt_nombre_empresa">Hogar como en su casa</span> <i class="lni lni-reload" id="reload" style="margin-left: 50px"></i>
                            </div>
                          </a>  
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
                                    <i class="fas fa-bell text-success"></i>
									<!--<span class="badge light text-white bg-primary" id="totalincidentes">0</span>-->
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
										<ul class="timeline" id="incidentesnotific">
										</ul>
									</div>
                                    <a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
                                </div>
                            </li>
							<li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link bell bell-link" href="javascript:;">
                                    <i class="fas fa-comments text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link bell config-link" href="javascript:;">
                                    <i class="fas fa-cogs text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                                    <!--<img src="images/logo.png" width="20" alt=""/>-->
                                    <div class="round-header"><?php echo $inombre; ?></div>
									<div class="header-info">
										<span><?php echo $nombre; ?></span>
									</div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right"><!--
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>-->
                                    <a href="index.php" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Cerrar Sesion </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>


        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php menu(); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
				
        <div class="content-body">

			<div class="container-fluid" style="display:padding-top: 0px !important">
			
				
				<div class="row">
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
						<a href="#div_tablaPacientes_alerta" class="ancla" name="pacientesFR">
							<div class="card gradient-bx text-white bg-success">	
								<div class="card-body">
									<div class="media align-items-center">
										<div class="media-body">
											<p class="mb-1">Residentes evaluados</p>
											<div class="d-flex flex-wrap">
												<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalpaciente_alertas">0</h2>
											</div>
										</div>
										<span class="border rounded-circle p-4">
											<svg width="32" height="32" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M23.6667 0.333311C21.1963 0.264777 18.7993 1.17744 17 2.87164C15.2008 1.17744 12.8038 0.264777 10.3334 0.333311C8.9341 0.337244 7.55292 0.649469 6.28803 1.24778C5.02315 1.84608 3.90564 2.71577 3.01502 3.79498C-0.039984 7.45998 -0.261651 13.3333 1.19668 17.5966C1.21002 17.6266 1.22002 17.6566 1.23335 17.6866C3.91168 25.3333 15.2717 33.6666 17 33.6666C18.6983 33.6666 30.025 25.5166 32.7667 17.6866C32.78 17.6566 32.79 17.6266 32.8034 17.5966C34.2417 13.4016 34.0867 7.51498 30.985 3.79498C30.0944 2.71577 28.9769 1.84608 27.712 1.24778C26.4471 0.649469 25.0659 0.337244 23.6667 0.333311ZM17 30.03C14.6817 28.5233 8.23168 24 5.30335 18.6666H12C12.2743 18.6667 12.5444 18.599 12.7863 18.4696C13.0282 18.3403 13.2344 18.1532 13.3867 17.925L14.83 15.7583L17.0867 22.525C17.1854 22.8207 17.3651 23.0829 17.6054 23.2816C17.8456 23.4803 18.1368 23.6076 18.4458 23.6491C18.7548 23.6906 19.0693 23.6446 19.3535 23.5163C19.6376 23.388 19.8801 23.1825 20.0533 22.9233L22.8917 18.6666H28.6984C25.7684 24 19.3183 28.5233 17 30.03ZM29.975 15.3333H22C21.7257 15.3333 21.4556 15.4009 21.2137 15.5303C20.9718 15.6597 20.7656 15.8468 20.6133 16.075L19.17 18.2416L16.9133 11.475C16.8146 11.1792 16.6349 10.9171 16.3947 10.7184C16.1544 10.5196 15.8632 10.3923 15.5542 10.3508C15.2452 10.3093 14.9307 10.3553 14.6466 10.4837C14.3624 10.612 14.1199 10.8174 13.9467 11.0766L11.1084 15.3333H4.02502C3.35835 12.1816 3.50502 8.41164 5.57668 5.92831C6.151 5.22081 6.87614 4.65057 7.69911 4.25927C8.52209 3.86797 9.42209 3.6655 10.3334 3.66664C15.445 3.66664 14.9117 7.16664 16.9817 7.18664H17C19.0733 7.18664 18.5483 3.66664 23.6667 3.66664C24.5785 3.665 25.4792 3.86723 26.3027 4.25855C27.1263 4.64987 27.852 5.22037 28.4267 5.92831C30.4867 8.40331 30.6467 12.1666 29.975 15.3333Z" fill="white"/>
											</svg>
										</span>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
					    <a href="#tabla_tratamientos_finalizados" class="ancla" name="tratamientosF">
						<div class="card gradient-bx text-white bg-danger">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos por finalizar</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totaltfinalizados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M22.8,13.6L20,11.5V10h1c0.6,0,1-0.4,1-1V5c0-1.7-1.3-3-3-3h-6c-1.7,0-3,1.3-3,3v4c0,0.6,0.4,1,1,1h1v1.5l-2.8,2.1C8.4,14.2,8,15.1,8,16v11c0,1.7,1.3,3,3,3h10c1.7,0,3-1.3,3-3V16C24,15.1,23.6,14.2,22.8,13.6z M12,5c0-0.6,0.4-1,1-1h6c0.6,0,1,0.4,1,1v3h-8V5z M19,22h-2v2c0,0.6-0.4,1-1,1s-1-0.4-1-1v-2h-2c-0.6,0-1-0.4-1-1s0.4-1,1-1h2v-2c0-0.6,0.4-1,1-1s1,0.4,1,1v2h2c0.6,0,1,0.4,1,1S19.6,22,19,22z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
					    <a href="#tabla_tratamientos_editados" class="ancla" name="tratamientosE">
						<div class="card gradient-bx text-white bg-tertiary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos actualizados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalteditados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M29.9,17.5C29.7,17.2,29.4,17,29,17c-2.2,0-4.3,1-5.6,2.8L22.5,21c-1.1,1.3-2.8,2-4.5,2h-3c-0.6,0-1-0.4-1-1s0.4-1,1-1h1.9
												c1.6,0,3.1-1.3,3.1-2.9c0,0,0-0.1,0-0.1c0-0.5-0.5-1-1-1l-6.1,0c-3.6,0-6.5,1.6-8.1,4.2l-2.7,4.2c-0.2,0.3-0.2,0.7,0,1l3,5
												c0.1,0.2,0.4,0.4,0.6,0.5c0.1,0,0.1,0,0.2,0c0.2,0,0.4-0.1,0.6-0.2c3.8-2.5,8.2-3.8,12.7-3.8c3.3,0,6.3-1.8,7.9-4.7l2.7-4.8
												C30,18.2,30,17.8,29.9,17.5z"/>
												<path fill="white" d="M 24.094 5.292 A 1 1 0 0 0 14.083 5.292 V 16 L 22 16 L 21 17 A 1 1 0 0 0 24 18 L 24 16 Z Z M 22 12 H 20 V 14 C 20 14.6 19.6 15 19 15 S 18 14.6 18 14 V 12 H 16 C 15.4 12 15 11.6 15 11 L 14 11 L 15 11 S 15.4 10 16 10 H 18 V 8 C 18 7.4 18.4 7 19 7 S 20 7.4 20 8 V 10 H 22 C 22.6 10 23 10.4 23 11 L 24 11 L 23 11 S 22.6 12 22 12 Z"/>
												<!-- <path fill="white" d="M12.9,15C12.9,15,12.9,15,12.9,15l6.1,0c1.6,0,3,1.3,3,2.9l0,0.2c0,0,0,0,0,0l6.2-6.4c2.4-2.5,2.4-6.4,0-8.9
												C27,1.7,25.5,1,23.9,1c-1.6,0-3.2,0.7-4.4,1.9L19,3.4l-0.5-0.5C17.3,1.7,15.8,1,14.1,1C12.5,1,11,1.7,9.8,2.9
												c-2.4,2.5-2.4,6.4,0,8.9L12.9,15z M14,9h1.6l1.7-1.7C17.5,7.1,17.8,7,18.2,7c0.3,0.1,0.6,0.3,0.7,0.5l1,2l1.2-2.9
												C21.2,6.3,21.5,6,21.9,6c0.4,0,0.7,0.1,0.9,0.4l2,3c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3l-0.9-1.4l-1.3,3.2
												C20.8,12.7,20.4,13,20,13c0,0,0,0,0,0c-0.4,0-0.7-0.2-0.9-0.6l-1.4-2.8l-1,1C16.5,10.9,16.3,11,16,11h-2c-0.6,0-1-0.4-1-1
												S13.4,9,14,9z"/> -->
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
					    <a href="#tabla_planes_editados" class="ancla" name="planesE">
						<div class="card gradient-bx text-white bg-secondary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Planes de cuidado actualizados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalPlanEditados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M29.9,17.5C29.7,17.2,29.4,17,29,17c-2.2,0-4.3,1-5.6,2.8L22.5,21c-1.1,1.3-2.8,2-4.5,2h-3c-0.6,0-1-0.4-1-1s0.4-1,1-1h1.9
												c1.6,0,3.1-1.3,3.1-2.9c0,0,0-0.1,0-0.1c0-0.5-0.5-1-1-1l-6.1,0c-3.6,0-6.5,1.6-8.1,4.2l-2.7,4.2c-0.2,0.3-0.2,0.7,0,1l3,5
												c0.1,0.2,0.4,0.4,0.6,0.5c0.1,0,0.1,0,0.2,0c0.2,0,0.4-0.1,0.6-0.2c3.8-2.5,8.2-3.8,12.7-3.8c3.3,0,6.3-1.8,7.9-4.7l2.7-4.8
												C30,18.2,30,17.8,29.9,17.5z"/>
											<path fill="white" d="M12.9,15C12.9,15,12.9,15,12.9,15l6.1,0c1.6,0,3,1.3,3,2.9l0,0.2c0,0,0,0,0,0l6.2-6.4c2.4-2.5,2.4-6.4,0-8.9
												C27,1.7,25.5,1,23.9,1c-1.6,0-3.2,0.7-4.4,1.9L19,3.4l-0.5-0.5C17.3,1.7,15.8,1,14.1,1C12.5,1,11,1.7,9.8,2.9
												c-2.4,2.5-2.4,6.4,0,8.9L12.9,15z M14,9h1.6l1.7-1.7C17.5,7.1,17.8,7,18.2,7c0.3,0.1,0.6,0.3,0.7,0.5l1,2l1.2-2.9
												C21.2,6.3,21.5,6,21.9,6c0.4,0,0.7,0.1,0.9,0.4l2,3c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3l-0.9-1.4l-1.3,3.2
												C20.8,12.7,20.4,13,20,13c0,0,0,0,0,0c-0.4,0-0.7-0.2-0.9-0.6l-1.4-2.8l-1,1C16.5,10.9,16.3,11,16,11h-2c-0.6,0-1-0.4-1-1
												S13.4,9,14,9z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
					    <a href="#historico_reporte_diario" class="ancla" name="logistica">
							<div class="card gradient-bx text-white bg-info">	
								<div class="card-body">
									<div class="media align-items-center">
										<div class="media-body">
											<p class="mb-1">Reporte diario</p>
											<div class="d-flex flex-wrap">
												<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="rep_diario"><br/>0</h2>
											</div>
										</div>
										<span class="border rounded-circle p-4">
											<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/><path d="M14 3v5h5M16 13H8M16 17H8M10 9H8"/></svg>
										</span>
									</div>
								</div>
							</div>
						</a>
					</div>
					
					<div class="col-xl-3 col-xxl-3 col-sm-6 cuadros">
					    <a href="#tabla_reposicion_medicamentos" class="ancla" name="tratamientosF">
						<div class="card gradient-bx text-white bg-warning">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Reposición de medicamentos </p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="med_reposicion"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M 8 5 L 5 9 v 18 c 0 1.1 0.9 2 2 2 h 22 a 2 2 0 0 0 2 -2 V 9 l -3 -4 H 8 z M 5.8 9 c -0.8 -1 27.2 -1 24.2 0 L 6 9 M 25 16 L 25 21 L 20 21 L 20 26 L 15 26 L 15 21 L 10 21 L 10 16 L 15 16 L 15 11 L 20 11 L 20 16 L 25 16"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
				</div>

				<!-- GRAFICO DE SIGNOS VITALES -->
				<div class="col-lg-12 mb-md-0 mb-12"  id="">
					<div class="card">
						<div class="card-header" style="display:flex">
							<h5 class="card-title" style="flex-grow: 1;">Signos vitales</h5>
							<!--select class="select-fecha" style="flex-grow: 2;">
								<option value="0">Hoy</option>
								<option value="1">Hace 1 dia</option>
								<option value="2">Hace 2 dias</option>
							</select-->
						</div>
						<div class="card-body">
							<div id="chart" style="height: 70vh;  max-width: 100%; margin: 20px auto"></div>
						</div>
						<div class="card-footer border-0 pt-0">
						</div>
					</div>
				</div>
				<!-- PACIENTES CON ALERTAS -->
				
				<div class="col-xs-12">
					<div class="card pacientesFR">
						<div class="card-header" style="display:flex">
							<h4 class="card-title" style="flex-grow: 1;" id="div_tablaPacientes_alerta">Signos vitales de residentes</h4>
							
							<select class="select-fecha" style="flex-grow: 2;">
								<option value="0">Hoy</option>
								<option value="1">Hace 1 dia</option>
								<option value="2">Hace 2 dias</option>
							</select>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="tablaPacientes_alerta" class="display min-w850">
									<thead>
										<tr>
											<th>id</th>
											<th>Hogar</th>
											<th>Paciente</th>
											<th>Riesgo</th>
											<th>Fecha</th>
											<th>Recurso</th>
											<th>FC</th>
											<th>FR</th>
											<th>OXI</th>
											<th>Sistolica</th>
											<th>Diastolica</th>
											<th>Temp</th>
											<th>Dolor</th>
											<th>Glicemia</th>
											<th>Condición</th>
											<th>IMC</th> 
											<th>rfc</th>
											<th>rfr</th>
											<th>rox</th>
											<th>rsi</th>
											<th>rdi</th>
											<th>rtm</th>
											<th>rdl</th>
											<th>rgc</th>
											<th>rpe</th>
											<th>rmi</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- TRATAMIENTOS POR FINALIZAR -->
				<div class="col-xs-12">
					<div class="card tratamientosF">
						<div class="card-header">
							<h4 class="card-title">Tratamientos por finalizar</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_finalizados"   class="table table-striped" style="width: 100%">
									<thead>
										<tr>
										    <th>Paciente</th>
											<th>Medicamento</th>
											<th>Vía</th>
											<th>Frecuencia</th>
											<th>Dosis</th>
											<th>Fecha inicio</th>
											<th>Fecha Fin</th>
											<th>Observaciones</th>
											<th>Estado</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- TRATAMIENTOS ACTUALIZADOS -->
				<div class="col-xs-12">
					<div class="card tratamientosE">
						<div class="card-header">
							<h4 class="card-title">Tratamientos Editados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_editados"  class="table table-striped" style="width: 100%">
									<thead>
										<tr>
											<th>Paciente</th>
											<th>Fecha de actualización</th>
											<th>Estado</th>
											<th>Creado por</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
		
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- PLANES ACTUALIZADOS-->
				<div class="col-xs-12">
					<div class="card plancuidadoE">
						<div class="card-header">
							<h4 class="card-title">Planes de cuidado editados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_planes_editados" class="table table-striped" style="width: 100%">
									<thead>
										<tr>
											<th>Paciente</th>
											<th>Fecha de actualización</th>
											<th>Estado</th>
											<th>Creado por</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- REPOSICIÓN DE MEDICAMENTOS -->  
				<div class="col-xs-12">
					<div class="card reposicionmedicamento">
						<div class="card-header">
							<h4 class="card-title">Reposición de medicamentos</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="tabla_reposicion_medicamentos" class="table table-striped" style="width: 100%">
									<thead>
										<tr>
											<th>Paciente</th>
											<th>Medicamento</th>
											<th>Fecha de reposición</th>
											<th>Nombre de contacto</th>
											<th>Nro de contacto</th>
											<th>Comentarios</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>									
						</div>									
					</div>									
				</div>		





				<!-- REPORTE DIARIO -->
				<div class="col-xs-12">
					<div class="card reportediario">
						<div class="card-header">
							<h4 class="card-title">Histórico de reportes diarios</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="historico_reporte_diario" class="table table-striped" style="width: 100%">
									<thead>
										<tr>
											<th>Fecha</th>
											<th>Preparado por</th>
											<th>Estado</th>
											<th>Acciones</th>			
										</tr>
									</thead>
									<tbody>																			  
									</tbody>
								</table>
							</div>									
						</div>									
					</div>									
				</div>									
				
				<!-- Large modal REPOSICIÓN DE MEDICAMENTOS -->
				<div id="modal-reposicion-medicamentos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Reasignar fecha de reposición</h5>
									<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="card-header border-0 pb-0">
										<h4>Residente: <br>
											<strong id="rep_nombre_paciente" style="color:black"></strong>
										</h4>
									</div>
									<div class="card">
										<div class="card-body">											
											<div class="row">
												<imput type="hiden" id="reposicion_idpaciente"/>
												<div class="col-lg-12">
													<label  for="reposicion_contacto">Contacto</label>
													<input class="form-control" id="reposicion_contacto" type="text" style="width:100%"/> 						
												</div>
												<div class="col-lg-12">
													<label for="reposicion_tlf_contacto">Tel&eacute;fono contacto</label>
													<input class="form-control" id="reposicion_tlf_contacto" type="text"  style="width:100%"/> 						
												</div>
												<br>
												<div class="col-lg-12 ">
													<label class="select2-label" for="filtro_reposicion"><i class="lni lni-search-alt"></i> Buscar por medicamento</label>
													<select class="select2-with-label-single js-states d-block" id="filtro_reposicion" >	
													</select>
												</div>
												<div class="col-lg-12">
												
													<div id="body_reposicion">			
													</div>
												</div>
											</div>										
										</div>
									</div>
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
									<button id="btn-guardar-reposicion" type="button" class="btn btn-primary">Guardar</button>
								</div>	
						</div>
					</div>
				</div>

		<!-- Large modal historial REPOSICIÓN DE MEDICAMENTOS -->
		<div id="modal-historico-reposicion-medicamentos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Histórico de reposición de medicamentos</h5>
						<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
					</div>
					<div class="modal-body">
						<div class="card-header border-0 pb-0">							
								<h4>Residente: <br>
									<strong id="hist_rep_nombre_paciente" style="color:black"></strong>
								<br>
								<br>Medicamento: <br>
									<strong id="hist_rep_medicamento" style="color:black"></strong></h4>
						</div>
						<div class="card">
							<div class="card-body">											
								<div class="row">
									<div class="col-lg-12" id="historico_reposicion">
										
										<!-- <table id="tabla_historico_rep" class="table table-striped" style="width: 100%">
											<thead>
												<tr>
													<th>Fecha</th>														
													<th>Cantidad</th>												
													<th>Comentarios</th>		
													<th>Acciones</th>		
												</tr>
											</thead>
											<tbody>																	  
											</tbody>
										</table> -->
									</div>
								</div>										
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
					</div>	
				</div>
			</div>
		</div>
		
		<!-- Large modal TARJETA DE MEDICAMNETOS DETALLES -->
		<div id="modal-ver-tarjeta" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Tarjeta de medicamentos</h5>
						<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
					</div>
					<div class="modal-body">
						<!--div class="card">
							<div class="card-body"-->											
								<div class="row">

									<div id="tarjeta-medicamento-card"></div>
									
									<!--table id="tabla-tarjeta-medicamentos" class="table table-striped" style="width: 100%; border-collapse: separate; border-spacing: 2px;">
										<thead>
											<tr>
												<th>Estado</th>
												<th>Hora</th>												
												<th>Medicamento</th>		
												<th>Dosis a administrar</th>		
												<th>Vía</th>		
												<th>Frecuencia</th>		
												<th>Fecha inicio</th>		
												<th>Fecha fin</th>		
												<th>Observaciones</th>		
											</tr>
										</thead>
										<tbody id="tabla_ver_tarjeta">																	  
										</tbody>
									</table-->
										
								</div>										
							<!--/div>
						</div-->
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
						<!--button id="btn-guardar-reposicion" type="button" class="btn btn-primary">Guardar</button-->
					</div>	
				</div>
			</div>
		</div>
		
		<!-- Large modal PLAN DE CUIDADOS DETALLE -->
		<div id="modal-ver-plan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Plan de cuidado</h5>
						<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
					</div>
					<div class="modal-body">
						<!--div class="card"-->
							<!--div class="card-body"-->											
								<div class="row">
									
									<div id="plan-cuidado-card"></div>
									<!--table id="tabla-plan-cuidado" class="table table-striped" style="width: 100%; border-collapse: separate; border-spacing: 2px;">
										<thead>
											<tr>
												<th>Horario</th>												
												<th>Tipo de cuidado</th>		
												<th>Estrategia cuidado</th>		
												<th>Frecuencia</th>		
												<th>Fecha inicio</th>		
												<th>Fecha fin</th>	
											</tr>
										</thead>
										<tbody id="tabla_ver_plan">																	  
										</tbody>
									</table-->
										
								</div>										
							<!--/div-->
						<!--/div-->
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
						<!--button id="btn-guardar-reposicion" type="button" class="btn btn-primary">Guardar</button-->
					</div>	
				</div>
			</div>
		</div>



            </div>
					
			
        </div>
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="./vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>
	<script src="./vendor/owl-carousel/owl.carousel.js"></script>
	
	<!-- Daterangepicker -->
    <!-- momment js is must -->
    <script src="./vendor/moment/moment.min.js"></script>

    <!-- Material color picker -->
    <script src="./vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
	<!-- Highcharts-->
	<script src="js/graph/highcharts.js"></script>
	<script src="js/graph/highcharts-more.js"></script>
	<script src="js/graph/highcharts-3d.js"></script>
	<!-- PHM -->
	<script src="./js/phm/phm.js<?php autoVersiones(); ?>" ></script>
	
	<script src="./vendor/select2/js/select2.full.min.js"></script>
    <script src="./js/plugins-init/select2-init.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="https://kit.fontawesome.com/7f9e31f86a.js" crossorigin="anonymous"></script>	
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC037nleP4v84LrVNzb4a0fn33Ji37zC18&callback=initMap" async defer></script>
</body>
</html>