<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
	verificarLogin();
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - Dashboard </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Ajustes -->
    <link href="./css/ajustes.css<?php autoVersiones().''.$nombre; ?>" rel="stylesheet">

</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper" class="show">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="#top" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Chat box start
        ***********************************-->
		<div class="chatbox">
			<div class="chatbox-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#notes">Notas</a>
					</li><!--
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#alerts">Alertas</a>
					</li>-->
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#chat">Chat</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="chat" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box">
							<div class="card-header">
								<div>
									<h6 class="mb-1">Lista de Chats</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body">
								<ul class="contacts" id="chat_usuarios">
								</ul>
							</div>
						</div>
						<div class="card chat dz-chat-history-box d-none" id="sala_chat">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-chat-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Chat con <span id="chatcon"></span></h6>
									<p class="mb-0 text-success">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3">
								<div id="chat_enfermero"></div>
							</div>
							<div class="card-footer type_msg">
								<div class="input-group">
								    <input type="hidden" class="form-control" name="idsala" id="idsala" autocomplete="off">
									<textarea class="form-control" id="body" placeholder="Escribir Mensaje..."></textarea>
									<div class="input-group-append">
										<button type="button" class="btn btn-primary" id="boton-enviar-mensaje"><i class="fa fa-location-arrow"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="notes">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-nota-user-box">
							<div class="card-header">
								<div>
                                      <h6 class="mb-1">Lista de Notas</h6>
								</div>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Notas_Body">
								<ul class="contacts" id="listado_notas">
								</ul>
							</div>
						</div>
						<div class="card chat dz-nota-history-box d-none" id="sala_nota">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-nota-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Nota - <span id="notacon"></h6>
									<p class="mb-0 text-info">Paciente</p>
								</div>							
								<div class="dropdown">
								<!--	<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul> -->
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Notas_Body3">
								<div id="nota_detalle"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Chat box End
        ***********************************-->
        
        <!--**********************************
            Configuración start
        ***********************************-->
		<div class="config">
			<div class="config-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#filtrosconfig">Filtros</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="filtrosconfig" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0">
							<div class="card-header d-none">
								<div>
                                      <h6 class="mb-1">Filtros</h6>
								</div>
							</div>
							<div class="card-body p-0 dz-scroll" id="DZ_W_Filtros_Body">
								<div class="form-config">
                                    <form>
                                        <div class="d-block my-3">
                                             <div class="custom-control custom-radio mb-2">
                                                <input id="evitae" name="empresaConfig" type="radio" class="custom-control-input" checked="" required="">
                                                <label class="custom-control-label" for="evitae">Vitae</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="emapfre" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="emapfre">MAPFRE</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="enestle" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="enestle">Nestlé</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="ebgm" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="ebgm">BGM</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-2">
                                                <input id="eemg" name="empresaConfig" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="eemg">EMG</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Configuración End
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header" name="top">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <a  href="#top">
							<a href="#" class="btn btn-primary ir-arriba"><i class="las la-arrow-up"></i></a>
                            <div class="dashboard_bar">
                                Dashboard
                            </div>
                          </a>  
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
                                    <i class="fas fa-bell text-success"></i>
									<!--<span class="badge light text-white bg-primary" id="totalincidentes">0</span>-->
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
										<ul class="timeline" id="incidentesnotific">
										    
										</ul>
									</div>
                                    <a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
                                </div>
                            </li>
							<li class="nav-item dropdown notification_dropdown" style="display: none;">
                                <a class="nav-link bell bell-link" href="javascript:;">
                                    <i class="fas fa-comments text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown notification_dropdown" style="display:none;">
                                <a class="nav-link bell config-link" href="javascript:;">
                                    <i class="fas fa-cogs text-success"></i>
									<!--<span class="badge light text-white bg-primary">5</span>-->
                                </a>
							</li>
							<li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                                    <!--<img src="images/logo.png" width="20" alt=""/>-->
                                    <div class="round-header"><?php echo $inombre; ?></div>
									<div class="header-info">
										<span><?php echo $nombre; ?></span>
									</div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right"><!--
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>-->
                                    <a href="index.php" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Cerrar Sesion </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php menu(); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid" style="display:padding-top: 0px !important">
				<div class="form-head d-flex mb-3 mb-md-4 align-items-start">
					<div class="mr-auto d-none d-lg-block" style="display: none !important">
						<h3 class="text-black font-w600">Bienvenido a Vitae!</h3>
						<p class="mb-0 fs-18">Tu aliado de salud en casa</p>
					</div>
					<!--
					<div class="input-group search-area ml-auto d-inline-flex">
						<input type="text" class="form-control" placeholder="Buscar...">
						<div class="input-group-append">
							<button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
						</div>
					</div>
					-->
				</div>
				<div class="row">	
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
				  	<a href="#tablaPacientestitulo" class="ancla" name="pacientesFR">
						<div class="card gradient-bx text-white bg-danger">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Pacientes con Alertas</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalpaciente_alertas">0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M23.6667 0.333311C21.1963 0.264777 18.7993 1.17744 17 2.87164C15.2008 1.17744 12.8038 0.264777 10.3334 0.333311C8.9341 0.337244 7.55292 0.649469 6.28803 1.24778C5.02315 1.84608 3.90564 2.71577 3.01502 3.79498C-0.039984 7.45998 -0.261651 13.3333 1.19668 17.5966C1.21002 17.6266 1.22002 17.6566 1.23335 17.6866C3.91168 25.3333 15.2717 33.6666 17 33.6666C18.6983 33.6666 30.025 25.5166 32.7667 17.6866C32.78 17.6566 32.79 17.6266 32.8034 17.5966C34.2417 13.4016 34.0867 7.51498 30.985 3.79498C30.0944 2.71577 28.9769 1.84608 27.712 1.24778C26.4471 0.649469 25.0659 0.337244 23.6667 0.333311ZM17 30.03C14.6817 28.5233 8.23168 24 5.30335 18.6666H12C12.2743 18.6667 12.5444 18.599 12.7863 18.4696C13.0282 18.3403 13.2344 18.1532 13.3867 17.925L14.83 15.7583L17.0867 22.525C17.1854 22.8207 17.3651 23.0829 17.6054 23.2816C17.8456 23.4803 18.1368 23.6076 18.4458 23.6491C18.7548 23.6906 19.0693 23.6446 19.3535 23.5163C19.6376 23.388 19.8801 23.1825 20.0533 22.9233L22.8917 18.6666H28.6984C25.7684 24 19.3183 28.5233 17 30.03ZM29.975 15.3333H22C21.7257 15.3333 21.4556 15.4009 21.2137 15.5303C20.9718 15.6597 20.7656 15.8468 20.6133 16.075L19.17 18.2416L16.9133 11.475C16.8146 11.1792 16.6349 10.9171 16.3947 10.7184C16.1544 10.5196 15.8632 10.3923 15.5542 10.3508C15.2452 10.3093 14.9307 10.3553 14.6466 10.4837C14.3624 10.612 14.1199 10.8174 13.9467 11.0766L11.1084 15.3333H4.02502C3.35835 12.1816 3.50502 8.41164 5.57668 5.92831C6.151 5.22081 6.87614 4.65057 7.69911 4.25927C8.52209 3.86797 9.42209 3.6655 10.3334 3.66664C15.445 3.66664 14.9117 7.16664 16.9817 7.18664H17C19.0733 7.18664 18.5483 3.66664 23.6667 3.66664C24.5785 3.665 25.4792 3.86723 26.3027 4.25855C27.1263 4.64987 27.852 5.22037 28.4267 5.92831C30.4867 8.40331 30.6467 12.1666 29.975 15.3333Z" fill="white"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros" id="div_cc" style="display: none;">
					<a href="#visitasRetraso" class="ancla" name="visitasR">
						<div class="card gradient-bx text-white bg-success">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">  
										<p class="mb-1">Control proximas Visitas</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3"id="totalvretraso">0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M37.3333 15.6666C37.3383 14.7488 37.0906 13.8473 36.6174 13.061C36.1441 12.2746 35.4635 11.6336 34.6501 11.2084C33.8368 10.7831 32.9221 10.5899 32.0062 10.65C31.0904 10.7101 30.2087 11.021 29.4579 11.5489C28.707 12.0767 28.1159 12.8011 27.7494 13.6425C27.3829 14.484 27.255 15.4101 27.3799 16.3194C27.5047 17.2287 27.8774 18.086 28.4572 18.7976C29.0369 19.5091 29.8013 20.0473 30.6667 20.3533V25.6666C30.6667 27.8768 29.7887 29.9964 28.2259 31.5592C26.6631 33.122 24.5435 34 22.3333 34C20.1232 34 18.0036 33.122 16.4408 31.5592C14.878 29.9964 14 27.8768 14 25.6666V23.8666C16.7735 23.4642 19.3097 22.0777 21.1456 19.9603C22.9815 17.8429 23.9946 15.1358 24 12.3333V2.33329C24 1.89127 23.8244 1.46734 23.5118 1.15478C23.1993 0.842221 22.7754 0.666626 22.3333 0.666626H17.3333C16.8913 0.666626 16.4674 0.842221 16.1548 1.15478C15.8423 1.46734 15.6667 1.89127 15.6667 2.33329C15.6667 2.77532 15.8423 3.19924 16.1548 3.5118C16.4674 3.82436 16.8913 3.99996 17.3333 3.99996H20.6667V12.3333C20.6667 14.5434 19.7887 16.663 18.2259 18.2258C16.6631 19.7887 14.5435 20.6666 12.3333 20.6666C10.1232 20.6666 8.00358 19.7887 6.44077 18.2258C4.87797 16.663 4 14.5434 4 12.3333V3.99996H7.33333C7.77536 3.99996 8.19928 3.82436 8.51184 3.5118C8.8244 3.19924 9 2.77532 9 2.33329C9 1.89127 8.8244 1.46734 8.51184 1.15478C8.19928 0.842221 7.77536 0.666626 7.33333 0.666626H2.33333C1.8913 0.666626 1.46738 0.842221 1.15482 1.15478C0.842259 1.46734 0.666664 1.89127 0.666664 2.33329V12.3333C0.672024 15.1358 1.68515 17.8429 3.52106 19.9603C5.35697 22.0777 7.8932 23.4642 10.6667 23.8666V25.6666C10.6667 28.7608 11.8958 31.7283 14.0837 33.9162C16.2717 36.1041 19.2391 37.3333 22.3333 37.3333C25.4275 37.3333 28.395 36.1041 30.5829 33.9162C32.7708 31.7283 34 28.7608 34 25.6666V20.3533C34.9723 20.0131 35.8151 19.3797 36.4122 18.5402C37.0092 17.7008 37.3311 16.6967 37.3333 15.6666Z" fill="white"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
	   				</a>
					</div>
					<div class="col-xl-4 col-xxl-3 col-sm-6 cuadros" id="div_tc" style="display: none;">
<!--					<a href="#turnosCancelados" class="ancla" name="turnosC">-->
					<a href="#visitasRetraso" class="ancla" name="turnosC">
						<div class="card gradient-bx text-white bg-info">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Turnos Cancelados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3"id="totaltcancelados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
											<g clip-path="url(#clip0)">
											<path d="M35 5H33.3333C33.3333 3.67392 32.8065 2.40215 31.8689 1.46447C30.9312 0.526784 29.6594 0 28.3333 0C27.0072 0 25.7355 0.526784 24.7978 1.46447C23.8601 2.40215 23.3333 3.67392 23.3333 5H16.6667C16.6667 3.67392 16.1399 2.40215 15.2022 1.46447C14.2645 0.526784 12.9927 7.45058e-08 11.6667 7.45058e-08C10.3406 7.45058e-08 9.06881 0.526784 8.13113 1.46447C7.19345 2.40215 6.66667 3.67392 6.66667 5H5C3.67392 5 2.40215 5.52678 1.46447 6.46447C0.526784 7.40215 0 8.67392 0 10L0 35C0 36.3261 0.526784 37.5979 1.46447 38.5355C2.40215 39.4732 3.67392 40 5 40H35C36.3261 40 37.5979 39.4732 38.5355 38.5355C39.4732 37.5979 40 36.3261 40 35V10C40 8.67392 39.4732 7.40215 38.5355 6.46447C37.5979 5.52678 36.3261 5 35 5ZM5 8.33333H6.66667C6.66667 9.65942 7.19345 10.9312 8.13113 11.8689C9.06881 12.8065 10.3406 13.3333 11.6667 13.3333C12.1087 13.3333 12.5326 13.1577 12.8452 12.8452C13.1577 12.5326 13.3333 12.1087 13.3333 11.6667C13.3333 11.2246 13.1577 10.8007 12.8452 10.4882C12.5326 10.1756 12.1087 10 11.6667 10C11.2246 10 10.8007 9.8244 10.4882 9.51184C10.1756 9.19928 10 8.77536 10 8.33333V5C10 4.55797 10.1756 4.13405 10.4882 3.82149C10.8007 3.50893 11.2246 3.33333 11.6667 3.33333C12.1087 3.33333 12.5326 3.50893 12.8452 3.82149C13.1577 4.13405 13.3333 4.55797 13.3333 5V6.66667C13.3333 7.10869 13.5089 7.53262 13.8215 7.84518C14.134 8.15774 14.558 8.33333 15 8.33333H23.3333C23.3333 9.65942 23.8601 10.9312 24.7978 11.8689C25.7355 12.8065 27.0072 13.3333 28.3333 13.3333C28.7754 13.3333 29.1993 13.1577 29.5118 12.8452C29.8244 12.5326 30 12.1087 30 11.6667C30 11.2246 29.8244 10.8007 29.5118 10.4882C29.1993 10.1756 28.7754 10 28.3333 10C27.8913 10 27.4674 9.8244 27.1548 9.51184C26.8423 9.19928 26.6667 8.77536 26.6667 8.33333V5C26.6667 4.55797 26.8423 4.13405 27.1548 3.82149C27.4674 3.50893 27.8913 3.33333 28.3333 3.33333C28.7754 3.33333 29.1993 3.50893 29.5118 3.82149C29.8244 4.13405 30 4.55797 30 5V6.66667C30 7.10869 30.1756 7.53262 30.4882 7.84518C30.8007 8.15774 31.2246 8.33333 31.6667 8.33333H35C35.442 8.33333 35.866 8.50893 36.1785 8.82149C36.4911 9.13405 36.6667 9.55797 36.6667 10V16.6667H3.33333V10C3.33333 9.55797 3.50893 9.13405 3.82149 8.82149C4.13405 8.50893 4.55797 8.33333 5 8.33333ZM35 36.6667H5C4.55797 36.6667 4.13405 36.4911 3.82149 36.1785C3.50893 35.866 3.33333 35.442 3.33333 35V20H36.6667V35C36.6667 35.442 36.4911 35.866 36.1785 36.1785C35.866 36.4911 35.442 36.6667 35 36.6667Z" fill="white"/>
											<path d="M20 26.6667C20.9205 26.6667 21.6667 25.9205 21.6667 25C21.6667 24.0795 20.9205 23.3333 20 23.3333C19.0795 23.3333 18.3333 24.0795 18.3333 25C18.3333 25.9205 19.0795 26.6667 20 26.6667Z" fill="white"/>
											<path d="M30 26.6667C30.9205 26.6667 31.6667 25.9205 31.6667 25C31.6667 24.0795 30.9205 23.3333 30 23.3333C29.0796 23.3333 28.3334 24.0795 28.3334 25C28.3334 25.9205 29.0796 26.6667 30 26.6667Z" fill="white"/>
											<path d="M9.99995 26.6667C10.9204 26.6667 11.6666 25.9205 11.6666 25C11.6666 24.0795 10.9204 23.3333 9.99995 23.3333C9.07947 23.3333 8.33328 24.0795 8.33328 25C8.33328 25.9205 9.07947 26.6667 9.99995 26.6667Z" fill="white"/>
											<path d="M20 33.3334C20.9205 33.3334 21.6667 32.5872 21.6667 31.6667C21.6667 30.7462 20.9205 30 20 30C19.0795 30 18.3333 30.7462 18.3333 31.6667C18.3333 32.5872 19.0795 33.3334 20 33.3334Z" fill="white"/>
											<path d="M30 33.3334C30.9205 33.3334 31.6667 32.5872 31.6667 31.6667C31.6667 30.7462 30.9205 30 30 30C29.0796 30 28.3334 30.7462 28.3334 31.6667C28.3334 32.5872 29.0796 33.3334 30 33.3334Z" fill="white"/>
											<path d="M9.99995 33.3334C10.9204 33.3334 11.6666 32.5872 11.6666 31.6667C11.6666 30.7462 10.9204 30 9.99995 30C9.07947 30 8.33328 30.7462 8.33328 31.6667C8.33328 32.5872 9.07947 33.3334 9.99995 33.3334Z" fill="white"/>
											</g>
											<defs>
											<clipPath id="clip0">
											<rect width="40" height="40" fill="white"/>
											</clipPath>
											</defs>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
					    <a href="#tablaPacientes_condicion" class="ancla" name="pacientesCon">
						<div class="card gradient-bx text-white bg-secondary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Condiciones Actualizadas</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="condiciones"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M5.52 19c.64-2.2 1.84-3 3.22-3h6.52c1.38 0 2.58.8 3.22 3"/><circle cx="12" cy="10" r="3"/><circle cx="12" cy="12" r="10"/></svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
					    <a href="#pacientesact" class="ancla" name="pacientesact">
						<div class="card gradient-bx text-white bg-info">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Pacientes Activos</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalpact"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
					    <a href="#tabla_tratamientos_editados" class="ancla" name="tratamientosE">
						<div class="card gradient-bx text-white bg-tertiary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos Actualizados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalteditados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M29.9,17.5C29.7,17.2,29.4,17,29,17c-2.2,0-4.3,1-5.6,2.8L22.5,21c-1.1,1.3-2.8,2-4.5,2h-3c-0.6,0-1-0.4-1-1s0.4-1,1-1h1.9
												c1.6,0,3.1-1.3,3.1-2.9c0,0,0-0.1,0-0.1c0-0.5-0.5-1-1-1l-6.1,0c-3.6,0-6.5,1.6-8.1,4.2l-2.7,4.2c-0.2,0.3-0.2,0.7,0,1l3,5
												c0.1,0.2,0.4,0.4,0.6,0.5c0.1,0,0.1,0,0.2,0c0.2,0,0.4-0.1,0.6-0.2c3.8-2.5,8.2-3.8,12.7-3.8c3.3,0,6.3-1.8,7.9-4.7l2.7-4.8
												C30,18.2,30,17.8,29.9,17.5z"/>
											<path fill="white" d="M12.9,15C12.9,15,12.9,15,12.9,15l6.1,0c1.6,0,3,1.3,3,2.9l0,0.2c0,0,0,0,0,0l6.2-6.4c2.4-2.5,2.4-6.4,0-8.9
												C27,1.7,25.5,1,23.9,1c-1.6,0-3.2,0.7-4.4,1.9L19,3.4l-0.5-0.5C17.3,1.7,15.8,1,14.1,1C12.5,1,11,1.7,9.8,2.9
												c-2.4,2.5-2.4,6.4,0,8.9L12.9,15z M14,9h1.6l1.7-1.7C17.5,7.1,17.8,7,18.2,7c0.3,0.1,0.6,0.3,0.7,0.5l1,2l1.2-2.9
												C21.2,6.3,21.5,6,21.9,6c0.4,0,0.7,0.1,0.9,0.4l2,3c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3l-0.9-1.4l-1.3,3.2
												C20.8,12.7,20.4,13,20,13c0,0,0,0,0,0c-0.4,0-0.7-0.2-0.9-0.6l-1.4-2.8l-1,1C16.5,10.9,16.3,11,16,11h-2c-0.6,0-1-0.4-1-1
												S13.4,9,14,9z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
					    <a href="#tabla_tratamientos_finalizados" class="ancla" name="tratamientosF">
						<div class="card gradient-bx text-white bg-quaternary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos por finalizar</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totaltfinalizados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M22.8,13.6L20,11.5V10h1c0.6,0,1-0.4,1-1V5c0-1.7-1.3-3-3-3h-6c-1.7,0-3,1.3-3,3v4c0,0.6,0.4,1,1,1h1v1.5l-2.8,2.1
												C8.4,14.2,8,15.1,8,16v11c0,1.7,1.3,3,3,3h10c1.7,0,3-1.3,3-3V16C24,15.1,23.6,14.2,22.8,13.6z M12,5c0-0.6,0.4-1,1-1h6
												c0.6,0,1,0.4,1,1v3h-8V5z M19,22h-2v2c0,0.6-0.4,1-1,1s-1-0.4-1-1v-2h-2c-0.6,0-1-0.4-1-1s0.4-1,1-1h2v-2c0-0.6,0.4-1,1-1s1,0.4,1,1
												v2h2c0.6,0,1,0.4,1,1S19.6,22,19,22z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
						<a href="#tabla_planes" class="ancla" name="planesf">
							<div class="card gradient-bx text-white bg-info">	
								<div class="card-body">
									<div class="media align-items-center">
										<div class="media-body">
											<p class="mb-1">Planes de cuidado</p>
											<div class="d-flex flex-wrap">
												<h2 class="fs-40 font-w600 text-white mb-0 mr-3"id="planesdecuidados"><br/>0</h2>
											</div>
										</div>
										<span class="border rounded-circle p-4">
											<svg width="32" height="32" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
												<g clip-path="url(#clip0)">
												<path d="M35 5H33.3333C33.3333 3.67392 32.8065 2.40215 31.8689 1.46447C30.9312 0.526784 29.6594 0 28.3333 0C27.0072 0 25.7355 0.526784 24.7978 1.46447C23.8601 2.40215 23.3333 3.67392 23.3333 5H16.6667C16.6667 3.67392 16.1399 2.40215 15.2022 1.46447C14.2645 0.526784 12.9927 7.45058e-08 11.6667 7.45058e-08C10.3406 7.45058e-08 9.06881 0.526784 8.13113 1.46447C7.19345 2.40215 6.66667 3.67392 6.66667 5H5C3.67392 5 2.40215 5.52678 1.46447 6.46447C0.526784 7.40215 0 8.67392 0 10L0 35C0 36.3261 0.526784 37.5979 1.46447 38.5355C2.40215 39.4732 3.67392 40 5 40H35C36.3261 40 37.5979 39.4732 38.5355 38.5355C39.4732 37.5979 40 36.3261 40 35V10C40 8.67392 39.4732 7.40215 38.5355 6.46447C37.5979 5.52678 36.3261 5 35 5ZM5 8.33333H6.66667C6.66667 9.65942 7.19345 10.9312 8.13113 11.8689C9.06881 12.8065 10.3406 13.3333 11.6667 13.3333C12.1087 13.3333 12.5326 13.1577 12.8452 12.8452C13.1577 12.5326 13.3333 12.1087 13.3333 11.6667C13.3333 11.2246 13.1577 10.8007 12.8452 10.4882C12.5326 10.1756 12.1087 10 11.6667 10C11.2246 10 10.8007 9.8244 10.4882 9.51184C10.1756 9.19928 10 8.77536 10 8.33333V5C10 4.55797 10.1756 4.13405 10.4882 3.82149C10.8007 3.50893 11.2246 3.33333 11.6667 3.33333C12.1087 3.33333 12.5326 3.50893 12.8452 3.82149C13.1577 4.13405 13.3333 4.55797 13.3333 5V6.66667C13.3333 7.10869 13.5089 7.53262 13.8215 7.84518C14.134 8.15774 14.558 8.33333 15 8.33333H23.3333C23.3333 9.65942 23.8601 10.9312 24.7978 11.8689C25.7355 12.8065 27.0072 13.3333 28.3333 13.3333C28.7754 13.3333 29.1993 13.1577 29.5118 12.8452C29.8244 12.5326 30 12.1087 30 11.6667C30 11.2246 29.8244 10.8007 29.5118 10.4882C29.1993 10.1756 28.7754 10 28.3333 10C27.8913 10 27.4674 9.8244 27.1548 9.51184C26.8423 9.19928 26.6667 8.77536 26.6667 8.33333V5C26.6667 4.55797 26.8423 4.13405 27.1548 3.82149C27.4674 3.50893 27.8913 3.33333 28.3333 3.33333C28.7754 3.33333 29.1993 3.50893 29.5118 3.82149C29.8244 4.13405 30 4.55797 30 5V6.66667C30 7.10869 30.1756 7.53262 30.4882 7.84518C30.8007 8.15774 31.2246 8.33333 31.6667 8.33333H35C35.442 8.33333 35.866 8.50893 36.1785 8.82149C36.4911 9.13405 36.6667 9.55797 36.6667 10V16.6667H3.33333V10C3.33333 9.55797 3.50893 9.13405 3.82149 8.82149C4.13405 8.50893 4.55797 8.33333 5 8.33333ZM35 36.6667H5C4.55797 36.6667 4.13405 36.4911 3.82149 36.1785C3.50893 35.866 3.33333 35.442 3.33333 35V20H36.6667V35C36.6667 35.442 36.4911 35.866 36.1785 36.1785C35.866 36.4911 35.442 36.6667 35 36.6667Z" fill="white"/>
												<path d="M20 26.6667C20.9205 26.6667 21.6667 25.9205 21.6667 25C21.6667 24.0795 20.9205 23.3333 20 23.3333C19.0795 23.3333 18.3333 24.0795 18.3333 25C18.3333 25.9205 19.0795 26.6667 20 26.6667Z" fill="white"/>
												<path d="M30 26.6667C30.9205 26.6667 31.6667 25.9205 31.6667 25C31.6667 24.0795 30.9205 23.3333 30 23.3333C29.0796 23.3333 28.3334 24.0795 28.3334 25C28.3334 25.9205 29.0796 26.6667 30 26.6667Z" fill="white"/>
												<path d="M9.99995 26.6667C10.9204 26.6667 11.6666 25.9205 11.6666 25C11.6666 24.0795 10.9204 23.3333 9.99995 23.3333C9.07947 23.3333 8.33328 24.0795 8.33328 25C8.33328 25.9205 9.07947 26.6667 9.99995 26.6667Z" fill="white"/>
												<path d="M20 33.3334C20.9205 33.3334 21.6667 32.5872 21.6667 31.6667C21.6667 30.7462 20.9205 30 20 30C19.0795 30 18.3333 30.7462 18.3333 31.6667C18.3333 32.5872 19.0795 33.3334 20 33.3334Z" fill="white"/>
												<path d="M30 33.3334C30.9205 33.3334 31.6667 32.5872 31.6667 31.6667C31.6667 30.7462 30.9205 30 30 30C29.0796 30 28.3334 30.7462 28.3334 31.6667C28.3334 32.5872 29.0796 33.3334 30 33.3334Z" fill="white"/>
												<path d="M9.99995 33.3334C10.9204 33.3334 11.6666 32.5872 11.6666 31.6667C11.6666 30.7462 10.9204 30 9.99995 30C9.07947 30 8.33328 30.7462 8.33328 31.6667C8.33328 32.5872 9.07947 33.3334 9.99995 33.3334Z" fill="white"/>
												</g>
												<defs>
												<clipPath id="clip0">
												<rect width="40" height="40" fill="white"/>
												</clipPath>
												</defs>
											</svg>
										</span>
									</div>
								</div>
							</div>
						</a>
					</div> 
					<div class="col-xl-4 col-xxl-4 col-sm-6 cuadros">
					    <a href="#tabla_incidentes" class="ancla" name="pacientesinci">
						<div class="card gradient-bx text-white bg-danger">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Incidentes</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalincidentes"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12 col-xxl-12 col-lg-12" id="tabla_m" style="display: none;">
						<div class="card">	
							<div class="card-header d-sm-flex d-block border-0 pb-0">
								<h3 class="fs-20 mb-3 mb-sm-0 text-black">Ubicaciones</h3>
								<div class="card-action card-tabs mt-3 mt-sm-0 mt-3 mt-sm-0">
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active mapadash" data-toggle="tab" href="#monthly" role="tab" id="mapatodos">
												Todos
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link mapadash" data-toggle="tab" href="#monthly" role="tab" id="mapaenf">
												Enfermeros
											</a>
										</li>
									
										<li class="nav-item">
											<a class="nav-link mapadash" data-toggle="tab" href="#weekly" role="tab" id="mapaenfd">
												Enfermeros Disponibles
											</a>
										</li>
										
										<li class="nav-item">
											<a class="nav-link mapadash" data-toggle="tab" href="#today" role="tab" id="mapapac">
												Pacientes
											</a>
										</li>
										
									</ul>
								</div>
							</div>
							<div class="card-body">
								<div id="mapa" style="height: 350px;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card pacientesFR">
						<div class="card-header">
							<h4 class="card-title">Pacientes con valores fuera de rango</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="tablaPacientes_alerta" class="display min-w850" style="width:100%" >
									<thead>
										<tr>											
											<th>Paciente</th>
											<th>FC</th>
											<th>FR</th>
											<th>Oxígeno</th>
											<th>Sistólica</th>
										    <th>Diastólica</th>
											<th>Temp</th>
											<th>Dolor</th>
											<th>Glicemia</th>
											<th>Condicion</th>
											<th>Fecha</th>
											<th>Recurso</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12" id="tabla_c" style="display: none;">
					<div class="card visitasR">
						<div class="card-header">
							<h4 id="title" class="card-title">Control proximas visitas</h4>
                            <button id="boton-tabla" type="button" class="btn btn-primary btn-sm">Turnos cancelados</button>
						</div>
						<div class="card-body">
							<div class="table-responsive">
                                <div id="container-retraso">
                                    <table id="visitasRetraso" class="display min-w850" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Cédula</th>
                                                <th>Paciente</th>
                                                <th>Fecha inicio</th>
                                                <th>Recurso</th>
                                                <th>Tiempo de retraso</th>
                                                <th>Fecha fin</th>
                                                <th>Status</th>
                                                <!--th>Acciones</th-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="container-cancelados" style="display: none;">
                                    <table id="turnosCancelados" class="display min-w850" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Cédula</th>
                                            <th>Paciente</th>
                                            <th>Fecha inicio</th>
                                            <th>Fecha fin</th>
                                            <th>Cancelado por</th>
                                            <th>Recurso Reasignado</th>
                                            <th>Status</th>
                                            <!--th>Acciones</th-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
							</div>
						</div>
					</div>
				</div>
				<!--div class="col-12" id="tabla_t" style="display: none;">
					<div class="card turnosC">
						<div class="card-header">
							<h4 class="card-title">Turnos cancelados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="turnosCancelados" class="display min-w850">
									<thead>
										<tr>
											<th>Cédula</th>
											<th>Paciente</th>
											
											<th>Fecha inicio</th>
											<th>Fecha fin</th>
											<th>Cancelado por</th>
											<th>Recurso Reasignado</th>
											<th>Status</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div-->
				<div class="col-12" >
					<div class="card pacientesCon">
						<div class="card-header">
							<h4 class="card-title">Condicion de pacientes</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="tablaPacientes_condicion" class="display min-w850" style="width:100%">
									<thead>
										<tr>											
											<th>Paciente</th>
											<th>FC</th>
											<th>FR</th>
											<th>Oxígeno</th>
											<th>Sistólica</th>
										    <th>Diastólica</th>
											<th>Temp</th>
											<th>Dolor</th>
											<th>Glicemia</th>
											<th>Condicion</th>
											<th>Fecha</th>
											<th>Recurso</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card pacientesact">
						<div class="card-header">
							<h4 class="card-title">Pacientes Activos</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="pacientesact" class="display min-w850" style="width:100%">
									<thead>
										<tr>
											<th>Cédula</th>
											<th>Paciente</th>
											<th>Edad</th> 
											<!--th>Diagnosticos</th-->
											<th>Medico Tratante</th>
											<th>Condición</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
		
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card tratamientosE">
						<div class="card-header">
							<h4 class="card-title">Tratamientos Editados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_editados" class="display min-w850" style="width:100%">
									<thead>
										<tr>
											<th>Paciente</th>
											<th>Fecha de actualización de tarjeta</th>
											<th>Estado</th>
											<th>Creado por</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
		
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card tratamientosF">
						<div class="card-header">
							<h4 class="card-title">Tratamientos Finalizados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_finalizados" class="display min-w850" style="width:100%">
									<thead>
										<tr>
										    <th>Paciente</th>
											<th>Medicamento</th>
											<th>Vía</th>
											<th>Frecuencia</th>
											<th>Dosis</th>
											<th>Fecha inicio</th>
											<th>Fecha Fin</th>
											<th>Observaciones</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card tratamientosF">
						<div class="card-header">
							<h4 class="card-title">Planes de cuidado</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_planes" class="display min-w850" style="width:100%">
									<thead>
										<tr>
										    <th>Paciente</th>
											<th>Fecha de ultima actualización</th>
											<th>Estado</th>
											<th>Creado por</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 hide">
                    <div class="card pacientesinci">
                        <div class="card-header">
                            <h4 class="card-title">Incidentes</h4>
                     </div>
                     <div class="card-body">
                         <div class="table-responsive">
                             <table id="tabla_incidentes" class="display min-w850" style="width:100%">
                                <thead>
                                   <tr>
										<th>Fecha creación</th>
                                        <th>Prioridad</th>
                                        <th>Tipo</th>
										<th>Paciente</th>
                                        <th>Incidente</th>
                                        
														  
                                        <th>Recurso</th>
                                        <!--th>Evidencias</th-->
                                        <th>Acciones</th>
                                  </tr>
                                </thead>
                               <tbody>
                              </tbody>
                           </table>
                         </div>
                    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12"  style="display:none;"> 
						<div class="card">	
							<div class="card-header border-0 pb-0">
								<h3 class="fs-20 mb-0 text-black">Enfermeros con más visitas</h3><!--
								<a href="page-review.html" class="text-primary font-w500">Ver más >></a>-->
							</div>
							<div class="card-body">
								<div class="assigned-doctor owl-carousel" id="top_colaboradores"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-xxl-4 col-lg-5" style="display: none">
                        <div class="card border-0 pb-0">
                            <div class="card-header flex-wrap border-0 pb-0">
                                <h3 class="fs-20 mb-0 text-black">Pacientes recientes</h3>
								<a href="patient-list.html" class="text-primary font-w500">View more >></a>
                            </div>
                            <div class="card-body"> 
                                <div id="DZ_W_Todo2" class="widget-media dz-scroll ps ps--active-y height320">
                                    <ul class="timeline">
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/1.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/2.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Griezerman</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-info mt-2">On Recovery</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/3.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Oconner</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-danger mt-2">Rejected</a>
											</div>
                                        </li>
										 <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/5.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Uli Trumb</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-primary mt-2">Recovered</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/1.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/2.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->
		<div class="modal fade" id="exampleModalpopover">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detalles de cancelación</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
					<div class="modal-body" id="detalles">
                        
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>                           
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="exampleModalCenter">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Evidencias</h5>
						<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="card">
							<div class="card-body p-4">
								<h4 class="card-intro-title">Slides only</h4>
								<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
										</li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
									</ol>
									<div class="carousel-inner">
										<div class="carousel-item active">
											<img class="d-block w-100" src="./images/big/img1.jpg" alt="First slide">
										</div>
										<div class="carousel-item">
											<img class="d-block w-100" src="./images/big/img2.jpg" alt="Second slide">
										</div>
										<div class="carousel-item">
											<img class="d-block w-100" src="./images/big/img3.jpg" alt="Third slide">
										</div>
									</div><a class="carousel-control-prev" href="#carouselExampleIndicators" data-slide="prev"><span class="carousel-control-prev-icon"></span> <span
											class="sr-only">Previous</span> </a><a class="carousel-control-next" href="#carouselExampleIndicators" data-slide="next"><span
											class="carousel-control-next-icon"></span>
									<span class="sr-only">Next</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
									<div class="modal fade" id="modal_vernota">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Nota de Enfermería - realizada por <p id="nota_recurso"></p></h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" id="nota_enfermeria">
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="./vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>
	<script src="./vendor/owl-carousel/owl.carousel.js"></script>
	
	<!-- Apex Chart -->
	<script src="./vendor/apexchart/apexchart.js"></script>
	
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
	
	<!-- Dashboard 1 -->
	<!-- <script src="./js/dashboard/dashboard.js<?php //autoVersiones(); ?>" ></script> -->
	<!-- <script>
		function assignedDoctor()
		{
		
			/*  testimonial one function by = owl.carousel.js */
			jQuery('.assigned-doctor').owlCarousel({
				loop:false,
				margin:30,
				nav:true,
				autoplaySpeed: 3000,
				navSpeed: 3000,
				paginationSpeed: 3000,
				slideSpeed: 3000,
				smartSpeed: 3000,
				autoplay: false,
				dots: false,
				navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
				responsive:{
					0:{
						items:1
					},
					576:{
						items:2
					},	
					767:{
						items:3
					},			
					991:{
						items:2
					},
					1200:{
						items:3
					},
					1600:{
						items:5
					}
				}
			})
		}
		
		jQuery(window).on('load',function(){
			setTimeout(function(){
				assignedDoctor();
			}, 1000); 
		});
		
	
		function initMap() {	
			var styledMapType = new google.maps.StyledMapType(
				[
				{
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#f5f5f5"
					}
					]
				},
				{
					"elementType": "labels.icon",
					"stylers": [
					{
						"visibility": "off"
					}
					]
				},
				{
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#616161"
					}
					]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [
					{
						"color": "#f5f5f5"
					}
					]
				},
				{
					"featureType": "administrative.land_parcel",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#bdbdbd"
					}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#eeeeee"
					}
					]
				},
				{
					"featureType": "poi",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#757575"
					}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#e5e5e5"
					}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#9e9e9e"
					}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#ffffff"
					}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#757575"
					}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#dadada"
					}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#616161"
					}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#9e9e9e"
					}
					]
				},
				{
					"featureType": "transit.line",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#e5e5e5"
					}
					]
				},
				{
					"featureType": "transit.station",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#eeeeee"
					}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
					{
						"color": "#c9c9c9"
					}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
					{
						"color": "#9e9e9e"
					}
					]
				}
				],
				{name: 'Styled Map'});

			$.ajax({
				url: "controller/dashboardback.php",
				cache: false,
				dataType: "json",
				method: "POST",
				data: {
					"opcion": "MAPA"
				}
			}).done(function(data) {
				var latitud = parseFloat(window.localStorage.getItem("latitud"));
				var longitud = parseFloat(window.localStorage.getItem("longitud"));
				var map = new google.maps.Map(document.getElementById('mapa'), {
				center: {lat: latitud, lng: longitud},
				zoom: 10
				});
				
				$.map(data, function (datos) {
					var marker = new google.maps.Marker({
						position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
						icon: datos.icon,
						title: datos.title,
						label: {
							text: datos.label,
							fontSize: '1px',
							color: '#1f4380'
						},
						map: map
					});
					
					marker.addListener('click', function() {
						map.setZoom(8);
						map.setCenter(marker.getPosition());
						var xUnidad = marker.label.text;
						console.log(marker.title.text);
					});
				});

				//Associate the styled map with the MapTypeId and set it to display.
				map.mapTypes.set('styled_map', styledMapType);
				map.setMapTypeId('styled_map');
			
			});
			
			//setInterval(initMap, 10000);
		}
	</script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC037nleP4v84LrVNzb4a0fn33Ji37zC18&callback=initMap" async defer></script>
<script src="https://kit.fontawesome.com/7f9e31f86a.js" crossorigin="anonymous"></script>	
</body>
</html>