<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
	verificarLogin();
	$nombre = $_SESSION['nombreUsu'];
	$arrnombre = explode(' ', $nombre);
	$inombre = substr($arrnombre[0], 0, 1).''.substr($arrnombre[1], 0, 1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - Historial</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	
	<link href="./vendor/morris/morris.css" rel="stylesheet">
    <!-- Ajustes -->
    <link href="./css/ajustes.css<?php autoVersiones().''.$nombre; ?>" rel="stylesheet">
	 <link href="./vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	 <style>
	 .ir-arriba {
	display:none;
	padding:20px;
	font-size:20px;
	color:#fff;
	cursor:pointer;
	position: fixed;
	bottom:20px;
	right:20px;
	z-index: 10001123;
}</style>
</head>
<body>

		<?php header(); ?>
		
		<!--**********************************
            Content body start
        ***********************************-->
		<div class="content-body">
		
			<div class="col-xl-12">
			
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Historial del paciente</h4>
					<a href="#" class="btn btn-primary ir-arriba"><i class="las la-arrow-up"></i></a>
					</div>
					<div class="card-body">
						<ul class="nav nav-pills mb-4 light">
							<li class=" nav-item">
								<a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">Información principal</a>
							</li>
							<li class="nav-item">
								<a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">Datos clínicos</a>
							</li>
							<li class="nav-item">
								<a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">Historial</a>
							</li>
							<li class="nav-item">
								<a href="#navpills-4" class="nav-link" data-toggle="tab" id="planes" aria-expanded="true">Plan de cuidado</a>
							</li>
							<li class="nav-item">
								<a href="#navpills-5" class="nav-link" data-toggle="tab" id="tarjeta" aria-expanded="true">Tarjeta de medicamentos</a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="navpills-1" class="tab-pane active">
								<div class="row">
									<div class="container-fluid">
										
										<div class="d-md-flex d-block mb-3 align-items-center">
											<div class="widget-timeline-icon py-3 py-md-2 px-1 overflow-auto" id="estatus_hospitalizacion"></div>
											<a href="javascript:void(0);" class="btn btn-primary btn-rounded wspace-no" data-toggle="modal" data-target="#basicModal" style="right: 3%;"><i class="las scale5 la-print mr-2"></i> Imprimir Historial del paciente</a>
										</div>
										<div class="row">
											<div class="col-xl-6 col-xxl-8" id="contenido">
											</div>
											<div class="col-xl-3 col-xxl-4 col-md-6">
												<div class="card" id="enfermedadesactuales">	
												</div>
											</div>
											<div class="col-xl-3 col-xxl-12 col-md-6">
												<div class="card" id="enfermedadespasadas">	
												</div>
											</div>
											<div class="col-xl-6 col-xxl-6">
												<div class="card" id="medico_tratante">
													
												</div>
											</div>
											<div class="col-xl-6 col-xxl-6" >
												<div class="card">
													<div class="card-header border-0 pb-0">
														<h4 class="fs-20 font-w600 mb-0">Contactos del paciente</h4>
													</div>
													<div class="card-body pt-4">
														<div id="DZ_W_Todo2" class="widget-media dz-scroll ps ps--active-y height200">
															<ul class="timeline" id="paciente_contactos">
																
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>	
								</div>
							</div>
							<div id="navpills-2" class="tab-pane">
								<div class="row">
								<div class="col-lg-12 mb-md-0 mb-12"  id="">
									<div class="card">
										<div class="card-header border-0 pb-0">
											<h5 class="card-title">Signos vitales</h5>
										</div>
										<div class="card-body">
											<div id="chart" style="height: 70vh;  max-width: 100%; margin: 0 auto"></div>
										</div>
										<div class="card-footer border-0 pt-0">
										</div>
									</div>
									
								</div>
								<div class="col-xl-6" id="notas_enfermeria">
									
								</div>
								
								<div class="col-xl-6" id="notas_medicos">
								
								</div>
								<div class="col-lg-6  mb-12" id="examen_laboratorio">
								
								</div>
								<div class="col-lg-6  mb-12" id="receta_medica">
								
								</div>
								</div>
							</div>
							<div id="navpills-3" class="tab-pane">
								<div class="row">
										<div class="col-lg-6 mb-md-0 mb-12" id="evaluaciones">
										
										</div>	
										<div class="col-lg-6 mb-md-0 mb-12">
											<div class="card">
								<div class="card-header d-block">
									<h4 class="card-title">Antecedentes</h4>
								</div>
								<div class="card-body">
									<div id="accordion-ante" class="accordion">
										<div class="accordion__item">
											<div class="accordion__header collapsed" data-toggle="collapse" data-target="#nopatologicos">
												<span class="accordion__header--text">Antecedentes personales no patologicos</span>
												<span class="accordion__header--indicator indicator_bordered"></span>
											</div>
											<div id="nopatologicos" class="collapse accordion__body" data-parent="#accordion-ante">
												<div class="accordion__body--text" id="antecedentes_nopatologicos">
												</div>
											</div>
										</div>
										<div class="accordion__item">
											<div class="accordion__header collapsed" data-toggle="collapse" data-target="#patologicos">
												<span class="accordion__header--text">Antecedentes personales</span>
												<span class="accordion__header--indicator indicator_bordered"></span>
											</div>
											<div id="patologicos" class="collapse accordion__body" data-parent="#accordion-ante">
												<div class="accordion__body--text" id="antecedentes_personales">
												</div>
											</div>
										</div>
										<div class="accordion__item">
											<div class="accordion__header collapsed" data-toggle="collapse" data-target="#quirur">
												<span class="accordion__header--text">Antecedentes quirúrgicos</span>
												<span class="accordion__header--indicator indicator_bordered"></span>
											</div>
											<div id="quirur" class="collapse accordion__body" data-parent="#accordion-ante">
												<div class="accordion__body--text" id="antecedentes_quirurgicos">
												</div>
											</div>
										</div>
										<div class="accordion__item">
											<div class="accordion__header collapsed" data-toggle="collapse" data-target="#fam">
												<span class="accordion__header--text">Antecedentes familiares</span>
												<span class="accordion__header--indicator indicator_bordered"></span>
											</div>
											<div id="fam" class="collapse accordion__body" data-parent="#accordion-ante">
												<div class="accordion__body--text" id="antecedentes_familiares">
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							</div>			
								</div>
							</div>
							<div id="navpills-4" class="tab-pane">
								<div class="row">
									<div class="col-12">
										<div class="card tratamientosF">
											<div class="card-header">
												<h4 class="card-title">Planes de cuidado</h4>
											</div>
											<div class="card-body">
												<div class="table-responsive">
													<table  id="tabla_planes" class="display min-w850">
														<thead>
															<tr>
																<th>Acciones</th>
																
																<th>Fecha de ultima actualización</th>
																<th>Estado</th>
																<th>Creado por</th>
																
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>								
								</div>
							</div>
							<div id="navpills-5" class="tab-pane">
								<div class="row">
									<div class="col-12">
										<div class="card tratamientosF">
											<div class="card-header">
												<h4 class="card-title">Tratamientos</h4>
											</div>
											<div class="card-body">
												<div class="table-responsive">
													<table  id="tabla_tratamientos_finalizados" class="display min-w850">
														<thead>
															<tr>
																<th>Acciones</th>
																<th>Fecha</th>
																<th>Creado por</th>
																<th>Estado</th>
																<th>Tipo</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
	
	 <div class="modal fade" id="basicModal">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Seleccione la fecha de inicio y fecha fin del reporte</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
													<div class="col-xl-12 mb-12">
														<div class="example">
															<p class="mb-1">Fecha de inicio y fin</p>
															<input class="form-control input-daterange-datepicker" type="text" name="daterange" id="fecha">
														</div>
													</div>
												</div>
                                                <div class="modal-footer">
                                                    
                                                    <button type="button" id="imprimir_historial" class="btn btn-primary">Generar</button>
													<button type="button" class="btn btn-danger light" data-dismiss="modal">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="./vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>
	<script src="./vendor/owl-carousel/owl.carousel.js"></script>
	
	<!-- Apex Chart -->
	<script src="./vendor/apexchart/apexchart.js"></script>
	
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
	
	
    <!-- Daterangepicker -->
    <!-- momment js is must -->
    <script src="./vendor/moment/moment.min.js"></script>
    <script src="./vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- clockpicker -->
    <script src="./vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
    <!-- asColorPicker -->
    <script src="./vendor/jquery-asColor/jquery-asColor.min.js"></script>
    <script src="./vendor/jquery-asGradient/jquery-asGradient.min.js"></script>
    <script src="./vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js"></script>
    <!-- Material color picker -->
    <script src="./vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- pickdate -->
    <script src="./vendor/pickadate/picker.js"></script>
    <script src="./vendor/pickadate/picker.time.js"></script>
    <script src="./vendor/pickadate/picker.date.js"></script>



    <!-- Daterangepicker -->
    <script src="./js/plugins-init/bs-daterange-picker-init.js"></script>
    <!-- Clockpicker init -->
    <script src="./js/plugins-init/clock-picker-init.js"></script>
    <!-- asColorPicker init -->
    <script src="./js/plugins-init/jquery-asColorPicker.init.js"></script>
    <!-- Material color picker init -->
    <script src="./js/plugins-init/material-date-picker-init.js"></script>
    <!-- Pickdate -->
    <script src="./js/plugins-init/pickadate-init.js"></script>

	
	 <!-- Chart Morris plugin files -->
    <script src="./vendor/morris/raphael-min.js"></script>
    <script src="./vendor/morris/morris.min.js"></script>
    
	<script src="js/graph/highcharts.js"></script>
	<script src="js/graph/highcharts-more.js"></script>
	<script src="js/graph/highcharts-3d.js"></script>
	
	<script src="./js/historial/historial.js<?php autoVersiones(); ?>" ></script>
	<script>
	
		var lineChart = function(){
			//line chart
			let line = new Morris.Line({
				element: 'morris_line',
				resize: true,
				data: [{
						y: '2011 Q1',
						item1: 2666
					},
					{
						y: '2011 Q2',
						item1: 2778
					},
					{
						y: '2011 Q3',
						item1: 4912
					},
					{
						y: '2011 Q4',
						item1: 3767
					},
					{
						y: '2012 Q1',
						item1: 6810
					},
					{
						y: '2012 Q2',
						item1: 5670
					},
					{
						y: '2012 Q3',
						item1: 4820
					},
					{
						y: '2012 Q4',
						item1: 15073
					},
					{
						y: '2013 Q1',
						item1: 10687
					},
					{
						y: '2013 Q2',
						item1: 8432
					}
				],
				xkey: 'y',
				ykeys: ['item1'],
				labels: ['Item 1'],
				gridLineColor: 'transparent',
				lineColors: ['rgb(247, 43, 80)'], //here
				lineWidth: 1,
				hideHover: 'auto',
				pointSize: 0,
				axes: false
			});	
		}
		$("span.donut").peity("donut", {
			width: "180",
			height: "180"
		});
		function initMap() {	
	    var styledMapType = new google.maps.StyledMapType(
            [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#ffffff"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#dadada"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#c9c9c9"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  }
			],
            {name: 'Styled Map'});

		$.ajax({
			url: "controller/historialback.php",
			cache: false,
			dataType: "json",
			method: "POST",
			data: {
				"opcion": "mapa"
			}
		}).done(function(data) {
			var map = new google.maps.Map(document.getElementById('mapa'), {
			center: {lat: 8.9992394, lng: -79.5055379},
			zoom: 14
			});
			
			$.map(data, function (datos) {
				var marker = new google.maps.Marker({
					position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
					icon: datos.icon,
					title: datos.title,
					label: {
						text: datos.label,
						fontSize: '1px',
						color: '#1f4380'
					},
					map: map
				});
				
				marker.addListener('click', function() {
					map.setZoom(8);
					map.setCenter(marker.getPosition());
					var xUnidad = marker.label.text;
					console.log(marker.title.text);
				});
			});

			//Associate the styled map with the MapTypeId and set it to display.
			map.mapTypes.set('styled_map', styledMapType);
			map.setMapTypeId('styled_map');
		
		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC037nleP4v84LrVNzb4a0fn33Ji37zC18&callback=initMap" async defer></script>
	<script src="https://kit.fontawesome.com/7f9e31f86a.js" crossorigin="anonymous"></script>	
</body>
</html>