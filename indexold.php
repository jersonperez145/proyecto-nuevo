<?php
	include_once("controller/funciones.php");
	include_once("controller/conexion.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vitae - Dashboard </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
	<link href="./vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
	
	<link href="./vendor/bootstrap-select/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<!-- Datatable -->
    <link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Ajustes -->
    <link href="./css/ajustes.css" rel="stylesheet">
	
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper" class="show menu-toggle">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="./images/logo.png" alt="">
                <img class="logo-compact" src="./images/logo-text.png" alt="">
                <img class="brand-title" src="./images/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Chat box start
        ***********************************-->
		<div class="chatbox">
			<div class="chatbox-close"></div>
			<div class="custom-tab-1">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#notes">Notas</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#alerts">Alertas</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#chat">Chat</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active show" id="chat" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect fill="#000000" x="4" y="11" width="16" height="2" rx="1"/><rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) " x="4" y="11" width="16" height="2" rx="1"/></g></svg></a>
								<div>
									<h6 class="mb-1">Lista de Chats</h6>
									<p class="mb-0">Mostrar Todos</p>
								</div>
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll  " id="DZ_W_Contacts_Body">
								<ul class="contacts">
									<li class="name-first-letter">A</li>
									<li class="active dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/1.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>Archie Parker</span>
												<p>Kalid is online</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/2.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Alfie Mason</span>
												<p>Taherah left 7 mins ago</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/3.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>AharlieKane</span>
												<p>Sami is online</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/4.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Athan Jacoby</span>
												<p>Nargis left 30 mins ago</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">B</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/5.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Bashid Samim</span>
												<p>Rashid left 50 mins ago</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/1.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>Breddie Ronan</span>
												<p>Kalid is online</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/2.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Ceorge Carson</span>
												<p>Taherah left 7 mins ago</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">D</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/3.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>Darry Parker</span>
												<p>Sami is online</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/4.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Denry Hunter</span>
												<p>Nargis left 30 mins ago</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">J</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/5.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Jack Ronan</span>
												<p>Rashid left 50 mins ago</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/1.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>Jacob Tucker</span>
												<p>Kalid is online</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/2.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>James Logan</span>
												<p>Taherah left 7 mins ago</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/3.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon"></span>
											</div>
											<div class="user_info">
												<span>Joshua Weston</span>
												<p>Sami is online</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">O</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/4.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Oliver Acker</span>
												<p>Nargis left 30 mins ago</p>
											</div>
										</div>
									</li>
									<li class="dz-chat-user">
										<div class="d-flex bd-highlight">
											<div class="img_cont">
												<img src="images/avatar/5.jpg" class="rounded-circle user_img" alt=""/>
												<span class="online_icon offline"></span>
											</div>
											<div class="user_info">
												<span>Oscar Weston</span>
												<p>Rashid left 50 mins ago</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="card chat dz-chat-history-box d-none">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;" class="dz-chat-history-back">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/><path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/></g></svg>
								</a>
								<div>
									<h6 class="mb-1">Chat con Khelesh</h6>
									<p class="mb-0 text-success">Online</p>
								</div>							
								<div class="dropdown">
									<a href="javascript:;" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-item"><i class="fa fa-user-circle text-primary mr-2"></i> View profile</li>
										<li class="dropdown-item"><i class="fa fa-users text-primary mr-2"></i> Add to close friends</li>
										<li class="dropdown-item"><i class="fa fa-plus text-primary mr-2"></i> Add to group</li>
										<li class="dropdown-item"><i class="fa fa-ban text-primary mr-2"></i> Block</li>
									</ul>
								</div>
							</div>
							<div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3">
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										Hi, how are you samim?
										<span class="msg_time">8:40 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										Hi Khalid i am good tnx how about you?
										<span class="msg_time_send">8:55 AM, Today</span>
									</div>
									<div class="img_cont_msg">
								<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										I am good too, thank you for your chat template
										<span class="msg_time">9:00 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										You are welcome
										<span class="msg_time_send">9:05 AM, Today</span>
									</div>
									<div class="img_cont_msg">
								<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										I am looking for your next templates
										<span class="msg_time">9:07 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										Ok, thank you have a good day
										<span class="msg_time_send">9:10 AM, Today</span>
									</div>
									<div class="img_cont_msg">
										<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										Bye, see you
										<span class="msg_time">9:12 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										Hi, how are you samim?
										<span class="msg_time">8:40 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										Hi Khalid i am good tnx how about you?
										<span class="msg_time_send">8:55 AM, Today</span>
									</div>
									<div class="img_cont_msg">
								<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										I am good too, thank you for your chat template
										<span class="msg_time">9:00 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										You are welcome
										<span class="msg_time_send">9:05 AM, Today</span>
									</div>
									<div class="img_cont_msg">
								<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										I am looking for your next templates
										<span class="msg_time">9:07 AM, Today</span>
									</div>
								</div>
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										Ok, thank you have a good day
										<span class="msg_time_send">9:10 AM, Today</span>
									</div>
									<div class="img_cont_msg">
										<img src="images/avatar/2.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
								</div>
								<div class="d-flex justify-content-start mb-4">
									<div class="img_cont_msg">
										<img src="images/avatar/1.jpg" class="rounded-circle user_img_msg" alt=""/>
									</div>
									<div class="msg_cotainer">
										Bye, see you
										<span class="msg_time">9:12 AM, Today</span>
									</div>
								</div>
							</div>
							<div class="card-footer type_msg">
								<div class="input-group">
									<textarea class="form-control" placeholder="Type your message..."></textarea>
									<div class="input-group-append">
										<button type="button" class="btn btn-primary"><i class="fa fa-location-arrow"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="alerts" role="tabpanel">
						<div class="card mb-sm-3 mb-md-0 contacts_card">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><circle fill="#000000" cx="5" cy="12" r="2"/><circle fill="#000000" cx="12" cy="12" r="2"/><circle fill="#000000" cx="19" cy="12" r="2"/></g></svg></a>
								<div>
									<h6 class="mb-1">Alertas</h6>
									<p class="mb-0">Agregar alerta</p>
								</div>
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/><path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/></g></svg></a>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body1">
								<ul class="contacts">
									<li class="name-first-letter">SEVER STATUS</li>
									<li class="active">
										<div class="d-flex bd-highlight">
											<div class="img_cont primary">KK</div>
											<div class="user_info">
												<span>David Nester Birthday</span>
												<p class="text-primary">Today</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">SOCIAL</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="img_cont success">RU<i class="icon fa-birthday-cake"></i></div>
											<div class="user_info">
												<span>Perfection Simplified</span>
												<p>Jame Smith commented on your status</p>
											</div>
										</div>
									</li>
									<li class="name-first-letter">SEVER STATUS</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="img_cont primary">AU<i class="icon fa fa-user-plus"></i></div>
											<div class="user_info">
												<span>AharlieKane</span>
												<p>Sami is online</p>
											</div>
										</div>
									</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="img_cont info">MO<i class="icon fa fa-user-plus"></i></div>
											<div class="user_info">
												<span>Athan Jacoby</span>
												<p>Nargis left 30 mins ago</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="card-footer"></div>
						</div>
					</div>
					<div class="tab-pane fade" id="notes">
						<div class="card mb-sm-3 mb-md-0 note_card">
							<div class="card-header chat-list-header text-center">
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect fill="#000000" x="4" y="11" width="16" height="2" rx="1"/><rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) " x="4" y="11" width="16" height="2" rx="1"/></g></svg></a>
								<div>
									<h6 class="mb-1">Notas</h6>
									<p class="mb-0">Agregar nota</p>
								</div>
								<a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"/><path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/><path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/></g></svg></a>
							</div>
							<div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body2">
								<ul class="contacts">
									<li class="active">
										<div class="d-flex bd-highlight">
											<div class="user_info">
												<span>New order placed..</span>
												<p>10 Aug 2020</p>
											</div>
											<div class="ml-auto">
												<a href="javascript:;" class="btn btn-primary btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
												<a href="javascript:;" class="btn btn-danger btn-xs sharp"><i class="fa fa-trash"></i></a>
											</div>
										</div>
									</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="user_info">
												<span>Youtube, a video-sharing website..</span>
												<p>10 Aug 2020</p>
											</div>
											<div class="ml-auto">
												<a href="javascript:;" class="btn btn-primary btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
												<a href="javascript:;" class="btn btn-danger btn-xs sharp"><i class="fa fa-trash"></i></a>
											</div>
										</div>
									</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="user_info">
												<span>john just buy your product..</span>
												<p>10 Aug 2020</p>
											</div>
											<div class="ml-auto">
												<a href="javascript:;" class="btn btn-primary btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
												<a href="javascript:;" class="btn btn-danger btn-xs sharp"><i class="fa fa-trash"></i></a>
											</div>
										</div>
									</li>
									<li>
										<div class="d-flex bd-highlight">
											<div class="user_info">
												<span>Athan Jacoby</span>
												<p>10 Aug 2020</p>
											</div>
											<div class="ml-auto">
												<a href="javascript:;" class="btn btn-primary btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
												<a href="javascript:;" class="btn btn-danger btn-xs sharp"><i class="fa fa-trash"></i></a>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--**********************************
            Chat box End
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">
                                Dashboard
                            </div>
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link  ai-icon" href="javascript:;" role="button" data-toggle="dropdown">
                                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M21.75 14.8385V12.0463C21.7471 9.88552 20.9385 7.80353 19.4821 6.20735C18.0258 4.61116 16.0264 3.61555 13.875 3.41516V1.625C13.875 1.39294 13.7828 1.17038 13.6187 1.00628C13.4546 0.842187 13.2321 0.75 13 0.75C12.7679 0.75 12.5454 0.842187 12.3813 1.00628C12.2172 1.17038 12.125 1.39294 12.125 1.625V3.41534C9.97361 3.61572 7.97429 4.61131 6.51794 6.20746C5.06159 7.80361 4.25291 9.88555 4.25 12.0463V14.8383C3.26257 15.0412 2.37529 15.5784 1.73774 16.3593C1.10019 17.1401 0.751339 18.1169 0.75 19.125C0.750764 19.821 1.02757 20.4882 1.51969 20.9803C2.01181 21.4724 2.67904 21.7492 3.375 21.75H8.71346C8.91521 22.738 9.45205 23.6259 10.2331 24.2636C11.0142 24.9013 11.9916 25.2497 13 25.2497C14.0084 25.2497 14.9858 24.9013 15.7669 24.2636C16.548 23.6259 17.0848 22.738 17.2865 21.75H22.625C23.321 21.7492 23.9882 21.4724 24.4803 20.9803C24.9724 20.4882 25.2492 19.821 25.25 19.125C25.2486 18.117 24.8998 17.1402 24.2622 16.3594C23.6247 15.5786 22.7374 15.0414 21.75 14.8385ZM6 12.0463C6.00232 10.2113 6.73226 8.45223 8.02974 7.15474C9.32723 5.85726 11.0863 5.12732 12.9212 5.125H13.0788C14.9137 5.12732 16.6728 5.85726 17.9703 7.15474C19.2677 8.45223 19.9977 10.2113 20 12.0463V14.75H6V12.0463ZM13 23.5C12.4589 23.4983 11.9316 23.3292 11.4905 23.0159C11.0493 22.7026 10.716 22.2604 10.5363 21.75H15.4637C15.284 22.2604 14.9507 22.7026 14.5095 23.0159C14.0684 23.3292 13.5411 23.4983 13 23.5ZM22.625 20H3.375C3.14298 19.9999 2.9205 19.9076 2.75644 19.7436C2.59237 19.5795 2.50014 19.357 2.5 19.125C2.50076 18.429 2.77757 17.7618 3.26969 17.2697C3.76181 16.7776 4.42904 16.5008 5.125 16.5H20.875C21.571 16.5008 22.2382 16.7776 22.7303 17.2697C23.2224 17.7618 23.4992 18.429 23.5 19.125C23.4999 19.357 23.4076 19.5795 23.2436 19.7436C23.0795 19.9076 22.857 19.9999 22.625 20Z" fill="#36C95F"/>
									</svg>
									<span class="badge light text-white bg-primary" id="totalincidentes">0</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
										<ul class="timeline" id="incidentesnotific">
										    
										</ul>
									</div>
                                    <a href="#tabla_incidentes" class="all-notification ancla"  name="incidentesC">Ver todos los Incidentes <i class="ti-arrow-down"></i></a>
                                </div>
                            </li>
							<li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link bell bell-link" href="javascript:;">
                                    <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M20.4604 0.848846H3.31682C2.64742 0.849582 2.00565 1.11583 1.53231 1.58916C1.05897 2.0625 0.792727 2.70427 0.791992 3.37367V15.1562C0.792727 15.8256 1.05897 16.4674 1.53231 16.9407C2.00565 17.414 2.64742 17.6803 3.31682 17.681C3.53999 17.6812 3.75398 17.7699 3.91178 17.9277C4.06958 18.0855 4.15829 18.2995 4.15843 18.5226V20.3168C4.15843 20.6214 4.24112 20.9204 4.39768 21.1817C4.55423 21.4431 4.77879 21.6571 5.04741 21.8008C5.31602 21.9446 5.61861 22.0127 5.92292 21.998C6.22723 21.9833 6.52183 21.8863 6.77533 21.7173L12.6173 17.8224C12.7554 17.7299 12.9179 17.6807 13.0841 17.681H17.187C17.7383 17.68 18.2742 17.4993 18.7136 17.1664C19.1531 16.8334 19.472 16.3664 19.6222 15.8359L22.8965 4.05007C22.9998 3.67478 23.0152 3.28071 22.9413 2.89853C22.8674 2.51634 22.7064 2.15636 22.4707 1.8466C22.2349 1.53684 21.9309 1.28565 21.5822 1.1126C21.2336 0.93954 20.8497 0.849282 20.4604 0.848846ZM21.2732 3.60301L18.0005 15.3847C17.9499 15.5614 17.8432 15.7168 17.6964 15.8274C17.5496 15.938 17.3708 15.9979 17.187 15.9978H13.0841C12.5855 15.9972 12.098 16.1448 11.6836 16.4219L5.84165 20.3168V18.5226C5.84091 17.8532 5.57467 17.2115 5.10133 16.7381C4.62799 16.2648 3.98622 15.9985 3.31682 15.9978C3.09365 15.9977 2.87966 15.909 2.72186 15.7512C2.56406 15.5934 2.47534 15.3794 2.47521 15.1562V3.37367C2.47534 3.15051 2.56406 2.93652 2.72186 2.77871C2.87966 2.62091 3.09365 2.5322 3.31682 2.53206H20.4604C20.5905 2.53239 20.7187 2.56274 20.8352 2.62073C20.9516 2.67872 21.0531 2.7628 21.1318 2.86643C21.2104 2.97005 21.2641 3.09042 21.2886 3.21818C21.3132 3.34594 21.3079 3.47763 21.2732 3.60301Z" fill="#36C95F"/>
										<path d="M5.84161 8.42333H10.0497C10.2729 8.42333 10.4869 8.33466 10.6448 8.17683C10.8026 8.019 10.8913 7.80493 10.8913 7.58172C10.8913 7.35851 10.8026 7.14445 10.6448 6.98661C10.4869 6.82878 10.2729 6.74011 10.0497 6.74011H5.84161C5.6184 6.74011 5.40433 6.82878 5.2465 6.98661C5.08867 7.14445 5 7.35851 5 7.58172C5 7.80493 5.08867 8.019 5.2465 8.17683C5.40433 8.33466 5.6184 8.42333 5.84161 8.42333Z" fill="#36C95F"/>
										<path d="M13.4161 10.1065H5.84161C5.6184 10.1065 5.40433 10.1952 5.2465 10.353C5.08867 10.5109 5 10.7249 5 10.9481C5 11.1714 5.08867 11.3854 5.2465 11.5433C5.40433 11.7011 5.6184 11.7898 5.84161 11.7898H13.4161C13.6393 11.7898 13.8534 11.7011 14.0112 11.5433C14.169 11.3854 14.2577 11.1714 14.2577 10.9481C14.2577 10.7249 14.169 10.5109 14.0112 10.353C13.8534 10.1952 13.6393 10.1065 13.4161 10.1065Z" fill="#36C95F"/>
									</svg>
									<span class="badge light text-white bg-primary">5</span>
                                </a>
							</li>
							<li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link" href="javascript:;"  role="button" data-toggle="dropdown">
                                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M22.2,14.4L21,13.7c-1.3-0.8-1.3-2.7,0-3.5l1.2-0.7c1-0.6,1.3-1.8,0.7-2.7l-1-1.7c-0.6-1-1.8-1.3-2.7-0.7   L18,5.1c-1.3,0.8-3-0.2-3-1.7V2c0-1.1-0.9-2-2-2h-2C9.9,0,9,0.9,9,2v1.3c0,1.5-1.7,2.5-3,1.7L4.8,4.4c-1-0.6-2.2-0.2-2.7,0.7   l-1,1.7C0.6,7.8,0.9,9,1.8,9.6L3,10.3C4.3,11,4.3,13,3,13.7l-1.2,0.7c-1,0.6-1.3,1.8-0.7,2.7l1,1.7c0.6,1,1.8,1.3,2.7,0.7L6,18.9   c1.3-0.8,3,0.2,3,1.7V22c0,1.1,0.9,2,2,2h2c1.1,0,2-0.9,2-2v-1.3c0-1.5,1.7-2.5,3-1.7l1.2,0.7c1,0.6,2.2,0.2,2.7-0.7l1-1.7   C23.4,16.2,23.1,15,22.2,14.4z M12,16c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4s4,1.8,4,4C16,14.2,14.2,16,12,16z" fill="#36C95F"/>
									</svg>
									<span class="badge light text-white bg-primary">5</span>
                                </a>
								<div class="dropdown-menu dropdown-menu-right p-3">
                                    <div id="DZ_W_Gifts" class="widget-timeline dz-scroll style-1 height300">
										<ul class="timeline">
											<li>
												<div class="timeline-badge primary"></div>
												<a class="timeline-panel text-muted" href="javascript:;">
													<!-- <span>10 minutes ago</span> -->
													<h6 class="mb-0"><strong class="text-primary">Vitae</strong>.</h6>
												</a>
											</li>
											<li>
												<div class="timeline-badge info">
												</div>
												<a class="timeline-panel text-muted" href="javascript:;">
													<!-- <span>20 minutes ago</span> -->
													<h6 class="mb-0"><strong class="text-info">MAPFRE</strong></h6>
													<!-- <p class="mb-0">Quisque a consequat ante Sit amet magna at volutapt...</p> -->
												</a>
											</li>
											<li>
												<div class="timeline-badge danger">
												</div>
												<a class="timeline-panel text-muted" href="javascript:;">
													<h6 class="mb-0"><strong class="text-danger">Nestlé</strong></h6>
												</a>
											</li>
											<li>
												<div class="timeline-badge warning">
												</div>
												<a class="timeline-panel text-muted" href="javascript:;">
													<h6 class="mb-0"><strong class="text-warning">BGM</strong></h6>
												</a>
											</li>
											<li>
												<div class="timeline-badge success">
												</div>
												<a class="timeline-panel text-muted" href="javascript:;">
													<h6 class="mb-0"><strong class="text-success">EMG</strong></h6>
												</a>
											</li>											
										</ul>
									</div>
                                </div>
							</li>
							<li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                                    <img src="images/profile/12.png" width="20" alt=""/>
									<div class="header-info">
										<span>Daniel,<strong> Coronel</strong></span>
									</div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>
                                    <a href="./page-login.html" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php menu(); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head d-flex mb-3 mb-md-4 align-items-start">
					<div class="mr-auto d-none d-lg-block">
						<h3 class="text-black font-w600">Bienvenido a Vitae!</h3>
						<p class="mb-0 fs-18">Tu aliado de salud en casa</p>
					</div>
					
					<div class="input-group search-area ml-auto d-inline-flex">
						<input type="text" class="form-control" placeholder="Buscar...">
						<div class="input-group-append">
							<button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
						</div>
					</div>
					
				</div>
				<div class="row">	
					<div class="col-xl-2 col-xxl-4 col-sm-6">
				  	<a href="#tablaPacientestitulo" class="ancla" name="pacientesFR">
						<div class="card gradient-bx text-white bg-danger">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Pacientes con Alertas</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalpaciente_alertas">0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M23.6667 0.333311C21.1963 0.264777 18.7993 1.17744 17 2.87164C15.2008 1.17744 12.8038 0.264777 10.3334 0.333311C8.9341 0.337244 7.55292 0.649469 6.28803 1.24778C5.02315 1.84608 3.90564 2.71577 3.01502 3.79498C-0.039984 7.45998 -0.261651 13.3333 1.19668 17.5966C1.21002 17.6266 1.22002 17.6566 1.23335 17.6866C3.91168 25.3333 15.2717 33.6666 17 33.6666C18.6983 33.6666 30.025 25.5166 32.7667 17.6866C32.78 17.6566 32.79 17.6266 32.8034 17.5966C34.2417 13.4016 34.0867 7.51498 30.985 3.79498C30.0944 2.71577 28.9769 1.84608 27.712 1.24778C26.4471 0.649469 25.0659 0.337244 23.6667 0.333311ZM17 30.03C14.6817 28.5233 8.23168 24 5.30335 18.6666H12C12.2743 18.6667 12.5444 18.599 12.7863 18.4696C13.0282 18.3403 13.2344 18.1532 13.3867 17.925L14.83 15.7583L17.0867 22.525C17.1854 22.8207 17.3651 23.0829 17.6054 23.2816C17.8456 23.4803 18.1368 23.6076 18.4458 23.6491C18.7548 23.6906 19.0693 23.6446 19.3535 23.5163C19.6376 23.388 19.8801 23.1825 20.0533 22.9233L22.8917 18.6666H28.6984C25.7684 24 19.3183 28.5233 17 30.03ZM29.975 15.3333H22C21.7257 15.3333 21.4556 15.4009 21.2137 15.5303C20.9718 15.6597 20.7656 15.8468 20.6133 16.075L19.17 18.2416L16.9133 11.475C16.8146 11.1792 16.6349 10.9171 16.3947 10.7184C16.1544 10.5196 15.8632 10.3923 15.5542 10.3508C15.2452 10.3093 14.9307 10.3553 14.6466 10.4837C14.3624 10.612 14.1199 10.8174 13.9467 11.0766L11.1084 15.3333H4.02502C3.35835 12.1816 3.50502 8.41164 5.57668 5.92831C6.151 5.22081 6.87614 4.65057 7.69911 4.25927C8.52209 3.86797 9.42209 3.6655 10.3334 3.66664C15.445 3.66664 14.9117 7.16664 16.9817 7.18664H17C19.0733 7.18664 18.5483 3.66664 23.6667 3.66664C24.5785 3.665 25.4792 3.86723 26.3027 4.25855C27.1263 4.64987 27.852 5.22037 28.4267 5.92831C30.4867 8.40331 30.6467 12.1666 29.975 15.3333Z" fill="white"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-2 col-xxl-4 col-sm-6">
					<a href="#visitasRetraso" class="ancla" name="visitasR">
						<div class="card gradient-bx text-white bg-success">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Visitas con Retraso</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3"id="totalvretraso">0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M37.3333 15.6666C37.3383 14.7488 37.0906 13.8473 36.6174 13.061C36.1441 12.2746 35.4635 11.6336 34.6501 11.2084C33.8368 10.7831 32.9221 10.5899 32.0062 10.65C31.0904 10.7101 30.2087 11.021 29.4579 11.5489C28.707 12.0767 28.1159 12.8011 27.7494 13.6425C27.3829 14.484 27.255 15.4101 27.3799 16.3194C27.5047 17.2287 27.8774 18.086 28.4572 18.7976C29.0369 19.5091 29.8013 20.0473 30.6667 20.3533V25.6666C30.6667 27.8768 29.7887 29.9964 28.2259 31.5592C26.6631 33.122 24.5435 34 22.3333 34C20.1232 34 18.0036 33.122 16.4408 31.5592C14.878 29.9964 14 27.8768 14 25.6666V23.8666C16.7735 23.4642 19.3097 22.0777 21.1456 19.9603C22.9815 17.8429 23.9946 15.1358 24 12.3333V2.33329C24 1.89127 23.8244 1.46734 23.5118 1.15478C23.1993 0.842221 22.7754 0.666626 22.3333 0.666626H17.3333C16.8913 0.666626 16.4674 0.842221 16.1548 1.15478C15.8423 1.46734 15.6667 1.89127 15.6667 2.33329C15.6667 2.77532 15.8423 3.19924 16.1548 3.5118C16.4674 3.82436 16.8913 3.99996 17.3333 3.99996H20.6667V12.3333C20.6667 14.5434 19.7887 16.663 18.2259 18.2258C16.6631 19.7887 14.5435 20.6666 12.3333 20.6666C10.1232 20.6666 8.00358 19.7887 6.44077 18.2258C4.87797 16.663 4 14.5434 4 12.3333V3.99996H7.33333C7.77536 3.99996 8.19928 3.82436 8.51184 3.5118C8.8244 3.19924 9 2.77532 9 2.33329C9 1.89127 8.8244 1.46734 8.51184 1.15478C8.19928 0.842221 7.77536 0.666626 7.33333 0.666626H2.33333C1.8913 0.666626 1.46738 0.842221 1.15482 1.15478C0.842259 1.46734 0.666664 1.89127 0.666664 2.33329V12.3333C0.672024 15.1358 1.68515 17.8429 3.52106 19.9603C5.35697 22.0777 7.8932 23.4642 10.6667 23.8666V25.6666C10.6667 28.7608 11.8958 31.7283 14.0837 33.9162C16.2717 36.1041 19.2391 37.3333 22.3333 37.3333C25.4275 37.3333 28.395 36.1041 30.5829 33.9162C32.7708 31.7283 34 28.7608 34 25.6666V20.3533C34.9723 20.0131 35.8151 19.3797 36.4122 18.5402C37.0092 17.7008 37.3311 16.6967 37.3333 15.6666Z" fill="white"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
	   				</a>
					</div>
					<div class="col-xl-2 col-xxl-4 col-sm-6">
					<a href="#turnosCancelados" class="ancla" name="turnosC">
						<div class="card gradient-bx text-white bg-info">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Turnos Cancelados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3"id="totaltcancelados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
											<g clip-path="url(#clip0)">
											<path d="M35 5H33.3333C33.3333 3.67392 32.8065 2.40215 31.8689 1.46447C30.9312 0.526784 29.6594 0 28.3333 0C27.0072 0 25.7355 0.526784 24.7978 1.46447C23.8601 2.40215 23.3333 3.67392 23.3333 5H16.6667C16.6667 3.67392 16.1399 2.40215 15.2022 1.46447C14.2645 0.526784 12.9927 7.45058e-08 11.6667 7.45058e-08C10.3406 7.45058e-08 9.06881 0.526784 8.13113 1.46447C7.19345 2.40215 6.66667 3.67392 6.66667 5H5C3.67392 5 2.40215 5.52678 1.46447 6.46447C0.526784 7.40215 0 8.67392 0 10L0 35C0 36.3261 0.526784 37.5979 1.46447 38.5355C2.40215 39.4732 3.67392 40 5 40H35C36.3261 40 37.5979 39.4732 38.5355 38.5355C39.4732 37.5979 40 36.3261 40 35V10C40 8.67392 39.4732 7.40215 38.5355 6.46447C37.5979 5.52678 36.3261 5 35 5ZM5 8.33333H6.66667C6.66667 9.65942 7.19345 10.9312 8.13113 11.8689C9.06881 12.8065 10.3406 13.3333 11.6667 13.3333C12.1087 13.3333 12.5326 13.1577 12.8452 12.8452C13.1577 12.5326 13.3333 12.1087 13.3333 11.6667C13.3333 11.2246 13.1577 10.8007 12.8452 10.4882C12.5326 10.1756 12.1087 10 11.6667 10C11.2246 10 10.8007 9.8244 10.4882 9.51184C10.1756 9.19928 10 8.77536 10 8.33333V5C10 4.55797 10.1756 4.13405 10.4882 3.82149C10.8007 3.50893 11.2246 3.33333 11.6667 3.33333C12.1087 3.33333 12.5326 3.50893 12.8452 3.82149C13.1577 4.13405 13.3333 4.55797 13.3333 5V6.66667C13.3333 7.10869 13.5089 7.53262 13.8215 7.84518C14.134 8.15774 14.558 8.33333 15 8.33333H23.3333C23.3333 9.65942 23.8601 10.9312 24.7978 11.8689C25.7355 12.8065 27.0072 13.3333 28.3333 13.3333C28.7754 13.3333 29.1993 13.1577 29.5118 12.8452C29.8244 12.5326 30 12.1087 30 11.6667C30 11.2246 29.8244 10.8007 29.5118 10.4882C29.1993 10.1756 28.7754 10 28.3333 10C27.8913 10 27.4674 9.8244 27.1548 9.51184C26.8423 9.19928 26.6667 8.77536 26.6667 8.33333V5C26.6667 4.55797 26.8423 4.13405 27.1548 3.82149C27.4674 3.50893 27.8913 3.33333 28.3333 3.33333C28.7754 3.33333 29.1993 3.50893 29.5118 3.82149C29.8244 4.13405 30 4.55797 30 5V6.66667C30 7.10869 30.1756 7.53262 30.4882 7.84518C30.8007 8.15774 31.2246 8.33333 31.6667 8.33333H35C35.442 8.33333 35.866 8.50893 36.1785 8.82149C36.4911 9.13405 36.6667 9.55797 36.6667 10V16.6667H3.33333V10C3.33333 9.55797 3.50893 9.13405 3.82149 8.82149C4.13405 8.50893 4.55797 8.33333 5 8.33333ZM35 36.6667H5C4.55797 36.6667 4.13405 36.4911 3.82149 36.1785C3.50893 35.866 3.33333 35.442 3.33333 35V20H36.6667V35C36.6667 35.442 36.4911 35.866 36.1785 36.1785C35.866 36.4911 35.442 36.6667 35 36.6667Z" fill="white"/>
											<path d="M20 26.6667C20.9205 26.6667 21.6667 25.9205 21.6667 25C21.6667 24.0795 20.9205 23.3333 20 23.3333C19.0795 23.3333 18.3333 24.0795 18.3333 25C18.3333 25.9205 19.0795 26.6667 20 26.6667Z" fill="white"/>
											<path d="M30 26.6667C30.9205 26.6667 31.6667 25.9205 31.6667 25C31.6667 24.0795 30.9205 23.3333 30 23.3333C29.0796 23.3333 28.3334 24.0795 28.3334 25C28.3334 25.9205 29.0796 26.6667 30 26.6667Z" fill="white"/>
											<path d="M9.99995 26.6667C10.9204 26.6667 11.6666 25.9205 11.6666 25C11.6666 24.0795 10.9204 23.3333 9.99995 23.3333C9.07947 23.3333 8.33328 24.0795 8.33328 25C8.33328 25.9205 9.07947 26.6667 9.99995 26.6667Z" fill="white"/>
											<path d="M20 33.3334C20.9205 33.3334 21.6667 32.5872 21.6667 31.6667C21.6667 30.7462 20.9205 30 20 30C19.0795 30 18.3333 30.7462 18.3333 31.6667C18.3333 32.5872 19.0795 33.3334 20 33.3334Z" fill="white"/>
											<path d="M30 33.3334C30.9205 33.3334 31.6667 32.5872 31.6667 31.6667C31.6667 30.7462 30.9205 30 30 30C29.0796 30 28.3334 30.7462 28.3334 31.6667C28.3334 32.5872 29.0796 33.3334 30 33.3334Z" fill="white"/>
											<path d="M9.99995 33.3334C10.9204 33.3334 11.6666 32.5872 11.6666 31.6667C11.6666 30.7462 10.9204 30 9.99995 30C9.07947 30 8.33328 30.7462 8.33328 31.6667C8.33328 32.5872 9.07947 33.3334 9.99995 33.3334Z" fill="white"/>
											</g>
											<defs>
											<clipPath id="clip0">
											<rect width="40" height="40" fill="white"/>
											</clipPath>
											</defs>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-2 col-xxl-4 col-sm-6">
					    <a href="#pacientesCovid" class="ancla" name="pacientesC">
						<div class="card gradient-bx text-white bg-secondary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Pacientes COVID</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalpcovid"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M 32.9375 13.457031 C 32.351562 13.457031 31.875 13.933594 31.875 14.519531 L 31.875 15.582031 L 28.953125 15.582031 C 28.691406 13.378906 27.828125 11.292969 26.449219 9.554688 L 28.519531 7.484375 L 29.351562 8.1875 C 29.621094 8.457031 30.011719 8.5625 30.378906 8.464844 C 30.746094 8.367188 31.03125 8.078125 31.132812 7.714844 C 31.230469 7.347656 31.125 6.953125 30.855469 6.6875 L 27.3125 3.144531 C 26.898438 2.730469 26.226562 2.730469 25.8125 3.144531 C 25.394531 3.558594 25.394531 4.230469 25.8125 4.648438 L 26.515625 5.480469 L 24.445312 7.550781 C 22.707031 6.171875 20.621094 5.308594 18.417969 5.050781 L 18.417969 2.125 L 19.480469 2.125 C 20.066406 2.125 20.542969 1.648438 20.542969 1.0625 C 20.542969 0.476562 20.066406 0 19.480469 0 L 14.519531 0 C 13.933594 0 13.457031 0.476562 13.457031 1.0625 C 13.457031 1.648438 13.933594 2.125 14.519531 2.125 L 15.582031 2.125 L 15.582031 5.050781 C 13.378906 5.308594 11.292969 6.171875 9.554688 7.550781 L 7.484375 5.480469 L 8.1875 4.648438 C 8.605469 4.230469 8.605469 3.558594 8.1875 3.144531 C 7.773438 2.730469 7.101562 2.730469 6.6875 3.144531 L 3.226562 6.6875 C 2.8125 7.101562 2.8125 7.773438 3.226562 8.1875 C 3.640625 8.601562 4.3125 8.601562 4.730469 8.1875 L 5.480469 7.484375 L 7.550781 9.554688 C 6.171875 11.292969 5.308594 13.378906 5.046875 15.582031 L 2.125 15.582031 L 2.125 14.519531 C 2.125 13.933594 1.648438 13.457031 1.0625 13.457031 C 0.476562 13.457031 0 13.933594 0 14.519531 L 0 19.480469 C 0 20.066406 0.476562 20.542969 1.0625 20.542969 C 1.648438 20.542969 2.125 20.066406 2.125 19.480469 L 2.125 18.417969 L 5.046875 18.417969 C 5.308594 20.621094 6.171875 22.707031 7.550781 24.445312 L 5.480469 26.515625 L 4.648438 25.8125 C 4.230469 25.398438 3.558594 25.398438 3.144531 25.8125 C 2.730469 26.226562 2.730469 26.898438 3.144531 27.3125 L 6.6875 30.855469 C 7.101562 31.269531 7.773438 31.269531 8.1875 30.855469 C 8.605469 30.441406 8.605469 29.769531 8.1875 29.351562 L 7.484375 28.519531 L 9.550781 26.449219 C 11.292969 27.828125 13.378906 28.691406 15.582031 28.949219 L 15.582031 31.875 L 14.519531 31.875 C 13.933594 31.875 13.457031 32.351562 13.457031 32.9375 C 13.457031 33.523438 13.933594 34 14.519531 34 L 19.480469 34 C 20.066406 34 20.542969 33.523438 20.542969 32.9375 C 20.542969 32.351562 20.066406 31.875 19.480469 31.875 L 18.417969 31.875 L 18.417969 28.949219 C 20.621094 28.691406 22.707031 27.828125 24.449219 26.449219 L 26.515625 28.519531 L 25.8125 29.351562 C 25.394531 29.769531 25.394531 30.441406 25.8125 30.855469 C 26.226562 31.269531 26.898438 31.269531 27.3125 30.855469 L 30.855469 27.3125 C 31.125 27.046875 31.230469 26.652344 31.132812 26.285156 C 31.03125 25.921875 30.746094 25.632812 30.378906 25.535156 C 30.011719 25.4375 29.621094 25.542969 29.351562 25.8125 L 28.519531 26.515625 L 26.449219 24.445312 C 27.828125 22.707031 28.691406 20.621094 28.953125 18.417969 L 31.875 18.417969 L 31.875 19.480469 C 31.875 20.066406 32.351562 20.542969 32.9375 20.542969 C 33.523438 20.542969 34 20.066406 34 19.480469 L 34 14.519531 C 34 13.933594 33.523438 13.457031 32.9375 13.457031 Z M 13.105469 14.167969 C 11.734375 14.167969 10.625 13.058594 10.625 11.6875 C 10.625 10.316406 11.734375 9.207031 13.105469 9.207031 C 14.472656 9.207031 15.582031 10.316406 15.582031 11.6875 C 15.582031 13.058594 14.472656 14.167969 13.105469 14.167969 Z M 21.25 23.375 C 20.46875 23.375 19.832031 22.742188 19.832031 21.957031 C 19.832031 21.175781 20.46875 20.542969 21.25 20.542969 C 22.03125 20.542969 22.667969 21.175781 22.667969 21.957031 C 22.667969 22.742188 22.03125 23.375 21.25 23.375 Z M 21.25 23.375 " fill="white"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-2 col-xxl-4 col-sm-6">
					    <a href="#tabla_tratamientos_editados" class="ancla" name="tratamientosE">
						<div class="card gradient-bx text-white bg-tertiary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos Editados</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totalteditados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M29.9,17.5C29.7,17.2,29.4,17,29,17c-2.2,0-4.3,1-5.6,2.8L22.5,21c-1.1,1.3-2.8,2-4.5,2h-3c-0.6,0-1-0.4-1-1s0.4-1,1-1h1.9
												c1.6,0,3.1-1.3,3.1-2.9c0,0,0-0.1,0-0.1c0-0.5-0.5-1-1-1l-6.1,0c-3.6,0-6.5,1.6-8.1,4.2l-2.7,4.2c-0.2,0.3-0.2,0.7,0,1l3,5
												c0.1,0.2,0.4,0.4,0.6,0.5c0.1,0,0.1,0,0.2,0c0.2,0,0.4-0.1,0.6-0.2c3.8-2.5,8.2-3.8,12.7-3.8c3.3,0,6.3-1.8,7.9-4.7l2.7-4.8
												C30,18.2,30,17.8,29.9,17.5z"/>
											<path fill="white" d="M12.9,15C12.9,15,12.9,15,12.9,15l6.1,0c1.6,0,3,1.3,3,2.9l0,0.2c0,0,0,0,0,0l6.2-6.4c2.4-2.5,2.4-6.4,0-8.9
												C27,1.7,25.5,1,23.9,1c-1.6,0-3.2,0.7-4.4,1.9L19,3.4l-0.5-0.5C17.3,1.7,15.8,1,14.1,1C12.5,1,11,1.7,9.8,2.9
												c-2.4,2.5-2.4,6.4,0,8.9L12.9,15z M14,9h1.6l1.7-1.7C17.5,7.1,17.8,7,18.2,7c0.3,0.1,0.6,0.3,0.7,0.5l1,2l1.2-2.9
												C21.2,6.3,21.5,6,21.9,6c0.4,0,0.7,0.1,0.9,0.4l2,3c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3l-0.9-1.4l-1.3,3.2
												C20.8,12.7,20.4,13,20,13c0,0,0,0,0,0c-0.4,0-0.7-0.2-0.9-0.6l-1.4-2.8l-1,1C16.5,10.9,16.3,11,16,11h-2c-0.6,0-1-0.4-1-1
												S13.4,9,14,9z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
					<div class="col-xl-2 col-xxl-4 col-sm-6">
					    <a href="#tabla_tratamientos_finalizados" class="ancla" name="tratamientosF">
						<div class="card gradient-bx text-white bg-quaternary">	
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
										<p class="mb-1">Tratamientos por Finalizar</p>
										<div class="d-flex flex-wrap">
											<h2 class="fs-40 font-w600 text-white mb-0 mr-3" id="totaltfinalizados"><br/>0</h2>
										</div>
									</div>
									<span class="border rounded-circle p-4">
										<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill="white" d="M22.8,13.6L20,11.5V10h1c0.6,0,1-0.4,1-1V5c0-1.7-1.3-3-3-3h-6c-1.7,0-3,1.3-3,3v4c0,0.6,0.4,1,1,1h1v1.5l-2.8,2.1
												C8.4,14.2,8,15.1,8,16v11c0,1.7,1.3,3,3,3h10c1.7,0,3-1.3,3-3V16C24,15.1,23.6,14.2,22.8,13.6z M12,5c0-0.6,0.4-1,1-1h6
												c0.6,0,1,0.4,1,1v3h-8V5z M19,22h-2v2c0,0.6-0.4,1-1,1s-1-0.4-1-1v-2h-2c-0.6,0-1-0.4-1-1s0.4-1,1-1h2v-2c0-0.6,0.4-1,1-1s1,0.4,1,1
												v2h2c0.6,0,1,0.4,1,1S19.6,22,19,22z"/>
										</svg>
									</span>
								</div>
							</div>
						</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12 col-xxl-12 col-lg-12">
						<div class="card">	
							<div class="card-header d-sm-flex d-block border-0 pb-0">
								<h3 class="fs-20 mb-3 mb-sm-0 text-black">Ubicaciones</h3>
								<div class="card-action card-tabs mt-3 mt-sm-0 mt-3 mt-sm-0">
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#monthly" role="tab">
												Todos
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#monthly" role="tab">
												Enfermeros
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#weekly" role="tab">
												Logísticos
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#today" role="tab">
												Pacientes
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-body">
								<div id="mapa" style="height: 350px;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card pacientesFR">
						<div class="card-header">
							<h4 class="card-title">Pacientes con valores fuera de rango</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="tablaPacientes_alerta" class="display min-w850">
									<thead>
										<tr>											
											<th>Paciente</th>
											<th>FC</th>
											<th>FR</th>
											<th>OXI</th>
											<th>SISTOLICA</th>
										    <th>DIASTOLICA</th>
											<th>TEMP</th>
											<th>DOLOR</th>
											<th>GLICEMIA</th>
											<th>FECHA</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card visitasR">
						<div class="card-header">
							<h4 class="card-title">Visitas con Retraso</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="visitasRetraso" class="display min-w850">
									<thead>
										<tr>
											<th>Cédula</th>
											<th>Paciente</th>
											<th>Fecha Inicio</th>
											<th>Recurso</th>
											<th>Tiempo de Retraso</th>
											<th>Fecha Finalización</th>
											<th>Status</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>								
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card turnosC">
						<div class="card-header">
							<h4 class="card-title">Turnos Cancelados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="turnosCancelados" class="display min-w850">
									<thead>
										<tr>
											<th>Cédula</th>
											<th>Paciente</th>
											<th>Tipo Visita</th>
											<th>Fecha inicio</th>
											<th>Fecha Fin</th>
											<th>Recurso</th>
											<th>Status</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card pacientesC">
						<div class="card-header">
							<h4 class="card-title">Pacientes COVID</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="pacientesCovid" class="display min-w850">
									<thead>
										<tr>
											<th>Cédula</th>
											<th>Paciente</th>
											<th>Fecha de Nacimiento</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
		
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card tratamientosE">
						<div class="card-header">
							<h4 class="card-title">Tratamientos Editados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_editados" class="display min-w850">
									<thead>
										<tr>
											<th>Paciente</th>
											<th>Fecha de actualización de tarjeta</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
		
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card tratamientosF">
						<div class="card-header">
							<h4 class="card-title">Tratamientos Finalizados</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table  id="tabla_tratamientos_finalizados" class="display min-w850">
									<thead>
										<tr>
										    <th>Paciente</th>
											<th>Medicamento</th>
											<th>Via</th>
											<th>Frecuencia</th>
											<th>Dosis</th>
											<th>Fecha Inicio</th>
											<th>Fecha Fin</th>
											<th>Observaciones</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
                    <div class="card incidentesC">
                        <div class="card-header">
                            <h4 class="card-title">Incidentes</h4>
                     </div>
                     <div class="card-body">
                         <div class="table-responsive">
                             <table id="tabla_incidentes" class="display min-w850">
                                <thead>
                                   <tr>
                                        <th>Id</th>
                                        <th>Tipo</th>
                                        <th>Incidente</th>
                                        <th>Fecha Creación</th>
                                        <th>Prioridad</th>
                                        <th>Recurso</th>
                                       <th>Evidencias</th>
                                       <th>Acciones</th>
                                  </tr>
                                </thead>
                               <tbody>
                              </tbody>
                           </table>
                         </div>
                    </div>
                </div>
            </div>
				<div class="row">
					<div class="col-xl-12 col-xxl-8 col-lg-7">
						<div class="card">	
							<div class="card-header border-0 pb-0">
								<h3 class="fs-20 mb-0 text-black">Personal mejor calificado</h3>
								<a href="page-review.html" class="text-primary font-w500">Ver más >></a>
							</div>
							<div class="card-body">
								<div class="assigned-doctor owl-carousel">
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/5.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Alexandro Jr.</a></h5>
											<span class="text-primary mb-2 d-block">Enfermera</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/1.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Samantha</a></h5>
											<span class="text-primary mb-2 d-block">Physical Therapy</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/2.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Aliandro M</a></h5>
											<span class="text-primary mb-2 d-block">Nursing</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img  src="images/doctors/3.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Samuel</a></h5>
											<span class="text-primary mb-2 d-block">Gynecologist</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/4.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Melinda</a></h5>
											<span class="text-primary mb-2 d-block">Dentist</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/1.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Alexandro Jr.</a></h5>
											<span class="text-primary mb-2 d-block">Dentist</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img  src="images/doctors/2.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Aliandro M</a></h5>
											<span class="text-primary mb-2 d-block">Nursing</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
									<div class="items">
										<div class="text-center">
											<img src="images/doctors/3.jpg" alt="" >
											<div class="dr-star"><i class="las la-star"></i> 4.2</div>
											<h5 class="fs-16 mb-1 font-w600"><a class="text-black" href="page-review.html">Lic. Samuel</a></h5>
											<span class="text-primary mb-2 d-block">Gynecologist</span>
											<p class="fs-12">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p><!--
											<div class="social-media">
												<a href="javascript:void(0);"><i class="lab la-instagram"></i></a>
												<a href="javascript:void(0);"><i class="lab la-facebook-f"></i></a>
												<a href="javascript:void(0);"><i class="lab la-twitter"></i></a>
											</div>-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-xxl-4 col-lg-5" style="display: none">
                        <div class="card border-0 pb-0">
                            <div class="card-header flex-wrap border-0 pb-0">
                                <h3 class="fs-20 mb-0 text-black">Pacientes Recientes</h3>
								<a href="patient-list.html" class="text-primary font-w500">View more >></a>
                            </div>
                            <div class="card-body"> 
                                <div id="DZ_W_Todo2" class="widget-media dz-scroll ps ps--active-y height320">
                                    <ul class="timeline">
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/1.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/2.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Griezerman</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-info mt-2">On Recovery</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/3.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Oconner</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-danger mt-2">Rejected</a>
											</div>
                                        </li>
										 <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/5.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Uli Trumb</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-primary mt-2">Recovered</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/1.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                        <li>
                                            <div class="timeline-panel flex-wrap">
												<div class="media mr-3">
													<img class="rounded-circle" alt="image" width="50" src="images/widget/2.jpg">
												</div>
                                                <div class="media-body">
													<h5 class="mb-1"><a class="text-black" href="patient-details.html">Aziz Bakree</a></h5>
													<span class="fs-14">24 Years</span>
												</div>
												<a href="javascript:void(0);" class="text-warning mt-2">Pending</a>
											</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="https://vitae-health.com" target="_blank">Vitae Health</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="./vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="./js/custom.min.js"></script>
	<script src="./js/deznav-init.js"></script>
	<script src="./vendor/owl-carousel/owl.carousel.js"></script>
	
	<!-- Apex Chart -->
	<script src="./vendor/apexchart/apexchart.js"></script>
	
	<!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./js/plugins-init/datatables.init.js"></script>
	
	<!-- Dashboard 1 -->
	<script src="./js/dashboard/dashboard.js<?php autoVersiones(); ?>" ></script>
	<script>
		function assignedDoctor()
		{
		
			/*  testimonial one function by = owl.carousel.js */
			jQuery('.assigned-doctor').owlCarousel({
				loop:false,
				margin:30,
				nav:true,
				autoplaySpeed: 3000,
				navSpeed: 3000,
				paginationSpeed: 3000,
				slideSpeed: 3000,
				smartSpeed: 3000,
				autoplay: false,
				dots: false,
				navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
				responsive:{
					0:{
						items:1
					},
					576:{
						items:2
					},	
					767:{
						items:3
					},			
					991:{
						items:2
					},
					1200:{
						items:3
					},
					1600:{
						items:5
					}
				}
			})
		}
		
		jQuery(window).on('load',function(){
			setTimeout(function(){
				assignedDoctor();
			}, 1000); 
		});
		
	
	function initMap() {	
	var styledMapType = new google.maps.StyledMapType(
            [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#f5f5f5"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#ffffff"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#dadada"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#e5e5e5"
				  }
				]
			  },
			  {
				"featureType": "transit.station",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#eeeeee"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#c9c9c9"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  }
			],
            {name: 'Styled Map'});

	$.ajax({
		url: "controller/dashboardback.php",
		cache: false,
		dataType: "json",
		method: "POST",
		data: {
			"opcion": "MAPA"
		}
	}).done(function(data) {
        var map = new google.maps.Map(document.getElementById('mapa'), {
          center: {lat: 8.9992394, lng: -79.5055379},
          zoom: 14
        });
		
		$.map(data, function (datos) {
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(datos.latitud), lng: parseFloat(datos.longitud)},
				icon: datos.icon,
				title: datos.title,
				label: {
					text: datos.label,
					fontSize: '1px',
					color: '#1f4380'
				},
				map: map
			});
			
			marker.addListener('click', function() {
				map.setZoom(8);
				map.setCenter(marker.getPosition());
				var xUnidad = marker.label.text;
				console.log(marker.title.text);
			});
		});

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
      
	});
	
	//setInterval(initMap, 10000);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC037nleP4v84LrVNzb4a0fn33Ji37zC18&callback=initMap" async defer></script>
	
</body>
</html>